﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Auth
{
    public class Forgotpasswordcode
    {
        public string code { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string confirmpassword { get; set; }
    }
}
