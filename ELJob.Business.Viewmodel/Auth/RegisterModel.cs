﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Business.Viewmodel.Auth
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        public string employeecode { get; set; }


        public string mobile { get; set; }

        public int? ZoneId { get; set; }
        [Required]
        public int Role { get; set; }
        public int? Location { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
     public int? Reporting { get; set; }
        public int UserId { get; set; }

        public DateTime? DOB { get; set; }



    }
}
