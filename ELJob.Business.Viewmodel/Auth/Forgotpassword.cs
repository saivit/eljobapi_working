﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Business.Viewmodel.Auth
{
    public class Forgotpassword
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
