﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Auth
{
   public class EmployerRegisterModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Organization { get; set; }
        public string Organizationtype { get; set; }
        public int OrganizationtypeId { get; set; }
        public string Designation { get; set; }
        public string CurrentPlan { get; set; }
        public int Validity { get; set; }
        public decimal Price { get; set; }
        public int Profileviews { get; set; }
        public string Description { get; set; }
        public int totalCVdownloads { get; set; }
        public int tillnowprofileviews { get; set; }
        public int tillnowCVdownload { get; set; }
        public int BoradcastEmailSMS { get; set; }
        public DateTime PlanStart { get; set; }
        public DateTime PlanEnd { get; set; }
        public int GSTStatus { get; set; }
        public string GSTSNumber { get; set; }
        public string CreatedByName { get; set; }
        public string profilepicname { get; set; }
        public string Profilepictype { get; set; }
        public string Profilepicpath { get; set; }
        public string SMSTemplate { get; set; }
        public int CreatedBy { get; set; }


    }
}
