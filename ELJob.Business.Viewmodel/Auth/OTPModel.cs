﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Business.Viewmodel.Auth
{
    public class OTPModel
    {
        //pass the OTP number. shared to mobile
        [Required(ErrorMessage = "OTP Is Required")]
        public int OTP { get; set; }
        [Required(ErrorMessage = "Mobile Numebr is Required")]
        public string Mobile { get; set; }
    }
}
