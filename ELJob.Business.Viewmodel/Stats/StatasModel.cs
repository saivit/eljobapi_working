﻿using ELJob.Business.Viewmodel.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Stats
{
   public class StatasModel
    {
       public DateTime? fromdate { get; set; }
        public DateTime? Todate { get; set; }
        public DateTime? Activefromdate { get; set; }
        public DateTime? ActiveTodate { get; set; }
        public List<Selectionvalues> Location { get; set; }
        public List<Selectionvalues> state { get; set; }
    }
}
