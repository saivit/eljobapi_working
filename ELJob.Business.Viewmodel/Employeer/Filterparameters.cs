﻿using ELJob.Business.Viewmodel.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Employeer
{
   public class Filterparameters
    {
        public string Jobtitle { get; set; }
        public string Jobloaction { get; set; }
       
        public int SalaryRange { get; set; }
        public int Gender { get; set; }
        public int Experince { get; set; }
        public List<Selectionvalues> Sectors { get; set; }
        public List<Selectionvalues> Qualification { get; set; }
        public List<Selectionvalues> AditionalQualification { get; set; }
        public List<Selectionvalues> City { get; set; }
        public int CTC { get; set; }
        public int Openings { get; set; }
        public List<Selectionvalues> Language { get; set; }
        public int Agemin { get; set; }
        public int Agemax { get; set; }

        public List<Selectionvalues> communicationskills { get; set; }
        public List<Selectionvalues> apptitude { get; set; }
        public List<Selectionvalues> computroperations { get; set; }
        public double ? Latitude { get; set; }
        public double? Longitude { get; set; }



    }
}
