﻿using ELJob.Business.Viewmodel.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Employeer
{
   public class EmployerProfileUpdatemodel
    {
        public int EmployeerId { get; set; }
        public string OrganizationName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public string Designation { get; set; }
        public List<Selectionvalues> Sectors { get; set; }
        public int GSTStatus { get; set; }
        public string GST { get; set; }
    }
}
