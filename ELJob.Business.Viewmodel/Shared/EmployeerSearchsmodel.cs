﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Shared
{
   public class EmployeerSearchsmodel
    {
        public string EmployeerName { get; set; }
        public string EmployeerId { get; set; }
        public string FilterId { get; set; }
        public DateTime Viewdate { get; set; }
    }
}
