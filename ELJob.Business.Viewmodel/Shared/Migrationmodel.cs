﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Shared
{
   public class Migrationmodel
    {
        public int Aspirantid { get; set; }
        public int status { get; set; } 
        public DateTime Createddate { get; set; }
        public DateTime LastUpdateddate { get; set; }
        public int RejectReason { get; set; }
        public string OtherreshduleReason { get; set; }
        public DateTime Reschduledate { get; set; }
        public string ReschduleTime { get; set; }
        public int Reasdulereason { get; set; }
        public string RejectOtherreason { get; set; }
        public int Workingstatus { get; set; }
        public int sourceOfLead { get; set; }
        public int UserId { get; set; }
        public int state { get; set; }
        public string street_name { get; set; }
        public int Profiledatastatus { get; set; }
        public string Profiledatastatusname { get; set; }
        public int score { get; set; }

    }
}
