﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Aspirant
{
    public class DocumentsUpload
    {
        public int AspirantID { get; set; }
        public string FileType { get; set; }
        public string FiePath { get; set; }
        public string DocumentType { get; set; }
    }
}
