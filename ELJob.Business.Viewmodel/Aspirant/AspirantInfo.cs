﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Aspirant
{

   public class AspirantInfo
    {
        public string name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public int Location { get; set;}
        public int Gender { get; set; }
        public bool IsEmailvalidated { get; set; }
        public string campaginid { get; set; }

    }
    public class GeoLatlong
    {
        public string type { get; set; }
        public String[] coordinates { get; set; }
    }

    public class AspirantReschdule
    {
        public string Id { get; set; }
        public int Reschdule { get; set; }
        public DateTime ReschduleDate { get; set; }
        public int SourceofLeadId { get; set; }
        public string Resechduletime { get; set; }
    }

    public class AspirantRejectionReason
    {
        public string Id { get; set; }
        public int SourceofLeadId { get; set; }
        public int RejectionReason{get;set;}
        public string otherRejectionReason { get; set; }
    }

    public class Updateprofiletopfields
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public DateTime DOB { get; set; }
        public string Mobile { get; set; }
    }

    public class  emailverify
    {
        public string JobseekrId { get; set; }
        public string Email { get; set; }
    }

}
