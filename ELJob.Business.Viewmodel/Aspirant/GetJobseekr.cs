﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Aspirant
{
    public class GetJobseekr
    {
        public int id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string mobile { get; set; }
        public string location { get; set; }
        public string gender { get; set; }
       
    }
}
