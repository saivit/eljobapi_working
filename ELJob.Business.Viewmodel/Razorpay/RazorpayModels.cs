﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.Viewmodel.Razorpay
{
  public class RazorpayModels
    {
    }



    public class Contact
    {
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public string type { get; set; }
        public string reference_id { get; set; }
        public Notes notes { get; set; }

    }
    public class ContactResponse : Contact
    {
        public string entity { get; set; }
        public string batch_id { get; set; }
        public bool active { get; set; }
        public string id { get; set; }
    }
    public class Notes
    {
        public string extranote { get; set; }
    }

    public class BankAccount
    {
        public string name { get; set; }
        public string ifsc { get; set; }
        public string account_number { get; set; }

    }
    public class Bankaccountrespponse : BankAccount
    {
        public string bank_name { get; set; }
    }

    public class Fundaccount
    {
        public string contact_id { get; set; }
        public string account_type { get; set; }
        public BankAccount bank_account { get; set; }
    }

    public class Fundaccountresponse
    {
        public string contact_id { get; set; }
        public string account_type { get; set; }
        public string id { get; set; }
        public bool active { get; set; }
        public string batch_id { get; set; }
        public string created_at { get; set; }
        public Bankaccountrespponse bank_account { get; set; }
    }

    public class payoutNotes
    {
        public string note_key { get; set; }
    }

    public class PayoutResponse
    {
        public string id { get; set; }
        public string entity { get; set; }
        public string fund_account_id { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public payoutNotes notes { get; set; }
        public int fees { get; set; }
        public int tax { get; set; }
        public string status { get; set; }
        public object utr { get; set; }
        public string mode { get; set; }
        public string purpose { get; set; }
        public string reference_id { get; set; }
        public string narration { get; set; }
        public object batch_id { get; set; }
        public object failure_reason { get; set; }
        public int created_at { get; set; }
        public string account_number { get; set; }
        public bool queue_if_low_balance { get; set; }
    }


    public class PayoutRequest
    {

        public string fund_account_id { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public payoutNotes notes { get; set; }
        public string mode { get; set; }
        public string purpose { get; set; }
        public string reference_id { get; set; }
        public string narration { get; set; }
        public string account_number { get; set; }
        public bool queue_if_low_balance { get; set; }
    }

    public class Vpa
    {
        public string address { get; set; }
    }

    public class VPApayout
    {
        public string account_type { get; set; }
        public string contact_id { get; set; }
        public Vpa vpa { get; set; }
    }


    public class AuthCustomer
    {
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
    }

    public class AuthNotes
    {
        public string address { get; set; }
    }

    public class AuthBankAccount
    {
        public string bank_name { get; set; }
        public string account_number { get; set; }
        public string ifsc_code { get; set; }
        public string beneficiary_name { get; set; }
    }

    public class SubscriptionRegistration
    {
        public string method { get; set; }
        public int max_amount { get; set; }
        public string auth_type { get; set; }
        public string expire_at { get; set; }
        public AuthBankAccount bank_account { get; set; }
    }

    public class AUthorizaionlink
    {
        public AuthCustomer customer { get; set; }
        public string type { get; set; }
        public int email_notify { get; set; }
        public int sms_notify { get; set; }
        public long expire_by { get; set; }
        public string description { get; set; }
        public string currency { get; set; }
        public int amount { get; set; }
        public AuthNotes notes { get; set; }
        public SubscriptionRegistration subscription_registration { get; set; }
    }

    public class AuthlinkResponse
    {
        public string id { get; set; }
        public string entity { get; set; }
        public object receipt { get; set; }
        public string customer_id { get; set; }
        public string order_id { get; set; }
        public List<object> line_items { get; set; }
        public object payment_id { get; set; }
        public string status { get; set; }
        public long expire_by { get; set; }
        public int issued_at { get; set; }
        public object paid_at { get; set; }
        public object cancelled_at { get; set; }
        public object expired_at { get; set; }
        public string sms_status { get; set; }
        public string email_status { get; set; }
        public int date { get; set; }
        public object terms { get; set; }
        public bool partial_payment { get; set; }
        public int gross_amount { get; set; }
        public int tax_amount { get; set; }
        public int amount { get; set; }
        public int amount_paid { get; set; }
        public int amount_due { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public AuthNotes notes { get; set; }
        public object comment { get; set; }
        public string short_url { get; set; }
        public bool view_less { get; set; }
        public object billing_start { get; set; }
        public object billing_end { get; set; }
        public string type { get; set; }
        public bool group_taxes_discounts { get; set; }
        public object user_id { get; set; }
        public int created_at { get; set; }
        public Authcustomer_details customer_details { get; set; }
    }

    public class Authcustomer_details
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public string gstin { get; set; }
        public string billing_address { get; set; }
        public string shipping_address { get; set; }
        public string customer_name { get; set; }
        public string customer_email { get; set; }
        public string customer_contact { get; set; }
    }

    public class RazorpayCustomer
    {
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
    }

    public class RazorpayCustomerNotes
    {
    }

    public class RazorpayCustomerResponse
    {
        public string id { get; set; }
        public string entity { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        // public RazorpayCustomerNotes notes { get; set; }
        public Int64 created_at { get; set; }
    }


    public class RazorpayOrderBankAccount
    {
        public string bank_name { get; set; }
        public string account_number { get; set; }
        public string ifsc_code { get; set; }
        public string beneficiary_name { get; set; }
    }

    public class RazorpayOrderToken
    {
        public int first_payment_amount { get; set; }
        public int max_amount { get; set; }
        public string auth_type { get; set; }
        public Int64 expire_at { get; set; }
        public RazorpayOrderBankAccount bank_account { get; set; }
    }

    public class RazorpayAuthorder
    {
        public int amount { get; set; }
        public string currency { get; set; }
        public string method { get; set; }
        public string receipt { get; set; }
        public string payment_capture { get; set; }
        public string customer_id { get; set; }
        public RazorpayOrderToken token { get; set; }
    }


    public class RazorpayOrderBankAccountResponse
    {
        public string ifsc { get; set; }
        public string bank_name { get; set; }
        public string account_number { get; set; }
        public object account_type { get; set; }
        public string name { get; set; }
        public string beneficiary_email { get; set; }
        public string beneficiary_mobile { get; set; }
    }

    public class RazorpayOrderTokenResponse
    {
        public string method { get; set; }
        public List<object> notes { get; set; }
        public object recurring_status { get; set; }
        public object failure_reason { get; set; }
        public string currency { get; set; }
        public int first_payment_amount { get; set; }
        public int max_amount { get; set; }
        public string auth_type { get; set; }
        public int expire_at { get; set; }
        public RazorpayOrderBankAccountResponse bank_account { get; set; }
    }

    public class RazorpayAuthOrderResponse
    {
        public string id { get; set; }
        public string entity { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public string receipt { get; set; }
        public string status { get; set; }
        public int attempts { get; set; }
        public int created_at { get; set; }
        public List<object> notes { get; set; }
        public RazorpayOrderTokenResponse token { get; set; }
    }

    public partial class RazorpayTokenIdResponse
    {

        public string id { get; set; }


        public string entity { get; set; }

        public object receipt { get; set; }

        public string customer_id { get; set; }


        public string order_id { get; set; }


        public string payment_id { get; set; }

        public string status { get; set; }

        public long expire_by { get; set; }
        public string invoice_id { get; set; }
        public string refund_status { get; set; }
        public bool captured { get; set; }
        public string card_id { get; set; }
        public string bank { get; set; }

        public string currency { get; set; }


        public string description { get; set; }

        public string token_id { get; set; }


        public decimal? fee { get; set; }
        public decimal? tax { get; set; }

        public string error_description { get; set; }
        public long created_at { get; set; }


    }

    public class RazorpayAuthUIpassparamters
    {
        public string key { get; set; }
        public string order_id { get; set; }
        public string customer_id { get; set; }
        public string recurring { get; set; }
    }

    public class Emandate
    {
        public string issuer { get; set; }
    }

    public class RecurringDetails
    {
        public string status { get; set; }
        public object failure_reason { get; set; }
    }

    public class Flows
    {
        public bool recurring { get; set; }
    }

    public class Card
    {
        public string entity { get; set; }
        public string name { get; set; }
        public string network { get; set; }
        public bool international { get; set; }
        public int expiry_month { get; set; }
        public int expiry_year { get; set; }
        public int last4 { get; set; }
        public bool emi { get; set; }
        public Flows flows { get; set; }
    }

    public class Item
    {
        public string id { get; set; }
        public string entity { get; set; }
        public string token { get; set; }
        public string method { get; set; }
        public Emandate emandate { get; set; }
        public bool recurring { get; set; }
        public RecurringDetails recurring_details { get; set; }
        public string auth_type { get; set; }
        public int? used_at { get; set; }
        public int created_at { get; set; }
        public Card card { get; set; }
    }

    public class TokenresponseByCustomer
    {
        public string entity { get; set; }
        public int count { get; set; }
        public List<Item> items { get; set; }
    }

    public class RazorpayRecurringOrder
    {
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string method { get; set; }
        public string receipt { get; set; }
        public string payment_capture { get; set; }

    }



    public class RazorpayAuthRecurringOrderResponse
    {
        public string id { get; set; }
        public string entity { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public string receipt { get; set; }
        public string status { get; set; }
        public int attempts { get; set; }
        public int created_at { get; set; }

    }

    public class RazorpayRecurringpost
    {
        public string email { get; set; }
        public string contact { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string order_id { get; set; }
        public string customer_id { get; set; }
        public string token { get; set; }
        public string recurring { get; set; }
        public string description { get; set; }
    }

    public class RecurringResponse
    {
        public string razorpay_payment_id { get; set; }
        public string razorpay_order_id { get; set; }
        public string razorpay_signature { get; set; }
    }

}
