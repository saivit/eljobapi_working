﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELJob.Infra.Repository
{
    public interface IUnitOfWork : IDisposable
    {

        IGenericRepository<T> Repository<T>() where T : class;

        int Commit();
        Task<int> CommitAsyc();
        Task<bool> Commitmongo();
        void Rollback();
    }
}
