﻿using ELJob.Core.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;


namespace ELJob.Infra.Repository
{

    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        private readonly IMongoContext mongocontext;

        public Dictionary<Type, object> Repositories
        {
            get { return _repositories; }
            set { Repositories = value; }
        }

        public UnitOfWork(ApplicationDbContext dbContext, IMongoContext mongocontext)
        {
            _dbContext = dbContext;
            this.mongocontext = mongocontext;
        }

        public IGenericRepository<T> Repository<T>() where T : class
        {
            if (Repositories.Keys.Contains(typeof(T)))
            {
                return Repositories[typeof(T)] as IGenericRepository<T>;
            }

            IGenericRepository<T> repo = new GenericRepository<T>(_dbContext, mongocontext);
            Repositories.Add(typeof(T), repo);
            return repo;
        }

        public  int Commit()
        {
            return  _dbContext.SaveChanges();
        }


        public async Task<bool> Commitmongo()
        {
           // return await mongocontext.SaveChanges();
            var changeAmount = await mongocontext.SaveChanges();

            return changeAmount > 0;
        }

        public void Rollback()
        {
            _dbContext.ChangeTracker.Entries().ToList().ForEach(x => x.Reload());
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public async Task<int> CommitAsyc()
        {
         return await   _dbContext.SaveChangesAsync();
        }
    }
}
