﻿

using ELJob.Core.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using ELJob.Domain.Model.Mongo;

namespace ELJob.Infra.Repository
{
    public interface IGenericRepository<T> where T :class
    {
        Task<T> Get(int Id);
        Task<IEnumerable<T>> GetAll();
        Task<T> Add(T entity);
        Task<int> Delete(int id);
        void Update(T entity, T model);

        /// <summary>
        /// Get all entities from db
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        IQueryable<T> Getfilter(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// Get query for entity
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        IQueryable<T> Query(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);

        /// <summary>
        /// Get first or default entity by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        T GetFirstOrDefault(
            Expression<Func<T, bool>> filter = null,
            params Expression<Func<T, object>>[] includes);


        Task<T> mongoGet(string key, string value);
        Task<IEnumerable<T>> mongoGetAll();
        Task<int> mongoAdd(T entity);
        Task<int> mongoDelete(int id);
        void mongoUpdate(T entity, T model);
        void mongoUpdateasync(FilterDefinition<T> filter, UpdateDefinition<T> model);
        Task<int> mongoAddMany(List<T> entity);

        /// <summary>
        /// Get all entities from db
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> mongoGetfilter(FilterDefinition<T> filter);

        /// <summary>
        /// Get query for entity
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        IQueryable<T> mongoQuery(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);

        /// <summary>
        /// Get first or default entity by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        Task<T> mongoGetFirstOrDefault(
            Expression<Func<T, bool>> filter = null,
            params Expression<Func<T, object>>[] includes);


        IQueryable<T> Getmongiquerable();


    }

    public class GenericRepository<T>: IGenericRepository<T> where T:class
    {
        protected readonly ApplicationDbContext _context;
        private DbSet<T> dbSet;
        private readonly IMongoContext mongocontext;
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        protected  IMongoCollection<T> mongoDbSet;
        protected readonly IMongoCollection<BsonDocument> collection;
        public GenericRepository(ApplicationDbContext context, IMongoContext mongocontext)
        {
            _context = context;
            this.mongocontext = mongocontext;
            this.dbSet = context.Set<T>();
            mongoDbSet = mongocontext.GetCollection<T>(typeof(T).Name);
            collection = mongocontext.GetCollection<BsonDocument>(typeof(T).Name);
        }

        public async Task<T> Get(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> Add(T entity)
        {
           var result=await _context.Set<T>().AddAsync(entity);
            //_context.SaveChanges();
            return result.Entity;
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public  void Update(T entity,T model)
        {

            //_context.Set<T>().Update(entity);
            //result.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.Entry(entity).CurrentValues.SetValues(model);
            _context.SaveChanges();
       
        }
        public virtual IQueryable<T> Query(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query;
        }
        public virtual IQueryable<T> Getfilter(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = dbSet;

            foreach (Expression<Func<T, object>> include in includes)
                query = query.Include(include);

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query;
        }

        public virtual T GetFirstOrDefault(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = dbSet;

            foreach (Expression<Func<T, object>> include in includes)
                query = query.Include(include);

            return query.FirstOrDefault(filter);
        }


        public Task<int> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<T> mongoGet(string key, string value)
        {
            var data = await mongoDbSet.FindAsync(Builders<T>.Filter.Eq(key, value));
            return data.FirstOrDefault();
        }
        public void mongoUpdateasync(FilterDefinition<T> filter, UpdateDefinition<T> model)
        {
            mongocontext.AddCommand(() =>
            {
                return mongoDbSet.UpdateOneAsync(filter, model);
            });
        }
        public async Task<IEnumerable<T>> mongoGetAll()
        {
            var all = await mongoDbSet.FindAsync(Builders<T>.Filter.Empty);
            return all.ToList();
        }

   

        public async Task<int>mongoAdd(T entity)
        {
            try
            {
                mongocontext.AddCommand(() => mongoDbSet.InsertOneAsync(entity));

                //var result=await mongocontext.SaveChanges();
                //return result;
                return 1;

            }
            catch(Exception Ex)
            {
                return 1;
            }
          
        }

        public Task<int> mongoDelete(int id)
        {
            throw new NotImplementedException();
        }

        public void mongoUpdate(T entity, T model)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Getmongiquerable()
        {
            var data =  mongoDbSet.AsQueryable();
            return data;
        }
        public async Task<IEnumerable<T>> mongoGetfilter(FilterDefinition<T> filter)
        {

          //  var builder = FilterDefinition<live>;

            //var idFilter = builder.Eq(u => u.TotalEnrolledStudents, 1);
    
                var data = await mongoDbSet.FindAsync(filter);
                //  throw new NotImplementedException();
                return await data.ToListAsync();
        }

        public IQueryable<T> mongoQuery(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query;
        }

        public async Task<T> mongoGetFirstOrDefault(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes)
        {
            var data = await mongoDbSet.FindAsync(filter);
            return data.FirstOrDefault();
        }

        public async Task<int> mongoAddMany(List<T> entity)
        {
            mongocontext.AddCommand(() => mongoDbSet.InsertManyAsync(entity));

            //var result=await mongocontext.SaveChanges();
            //return result;
            return 1;
        }
    }
}
