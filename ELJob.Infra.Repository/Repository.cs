﻿using ELJob.Core.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Collections;


namespace ELJob.Infra.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWork _unitOfWork;
        private DbSet<T> dbSet;
        private readonly ApplicationDbContext _dbContext;
        private readonly IMongoContext mongocontext;
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        protected readonly IMongoCollection<T> mongoDbSet;
        protected readonly IMongoCollection<BsonDocument> collection;

        public Dictionary<Type, object> Repositories
        {
            get { return _repositories; }
            set { Repositories = value; }
        }

        public Repository(ApplicationDbContext context,IMongoContext mongocontext)
        {
            _context = context;
            this.mongocontext = mongocontext;
            _unitOfWork = new UnitOfWork(context,mongocontext);
            this.dbSet = context.Set<T>();
            _dbContext = context;
            mongoDbSet = mongocontext.GetCollection<T>(typeof(T).Name);
            collection = mongocontext.GetCollection<BsonDocument>(typeof(T).Name);
        }


        public void Add(T entity)
        {
            _context.Add(entity);          
        }
     

        public IQueryable<T> GetAll()
        {
            return _context.Set<T>().AsQueryable();
        }

        public T Find(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public virtual IQueryable<T> Query(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query;
        }
        public virtual List<T> Getfilter(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = dbSet;

            foreach (Expression<Func<T, object>> include in includes)
                query = query.Include(include);

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query.ToList();
        }

        public virtual T GetFirstOrDefault(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = dbSet;

            foreach (Expression<Func<T, object>> include in includes)
                query = query.Include(include);

            return query.FirstOrDefault(filter);
        }

        public T Addcontent(T entity)
        {
           var result=_context.Add(entity);
            return result.Entity;

        }

        public IGenericRepository<T> ServiceRepository<T>() where T : class
        {
            if (Repositories.Keys.Contains(typeof(T)))
            {
                return Repositories[typeof(T)] as IGenericRepository<T>;
            }

            IGenericRepository<T> repo = new GenericRepository<T>(_context, mongocontext);
            Repositories.Add(typeof(T), repo);
            return repo;
        }



        public virtual async Task<IEnumerable<T>> GetById(string key, string value)
        {
            var data = await mongoDbSet.FindAsync(Builders<T>.Filter.Eq(key, value));
            return data.ToList();
        }

        public virtual async Task<List<BsonDocument>> GetByProjection(BsonDocument _bosnGroup, BsonDocument _bosnproject)
        {

            var _pipeline = new[] { _bosnGroup, _bosnproject };
            var cursor = await collection.AggregateAsync<BsonDocument>(_pipeline);
            List<BsonDocument> list = cursor.ToList();
            return list;
        }

        public virtual async Task<List<BsonDocument>> GetByProjection(BsonDocument _bosnmatch, BsonDocument _bosnGroup, BsonDocument _bosnproject)
        {

            var _pipeline = new[] { _bosnmatch, _bosnGroup, _bosnproject };
            var cursor = await collection.AggregateAsync<BsonDocument>(_pipeline);
            List<BsonDocument> list = cursor.ToList();
            return list;
        }

        public virtual void Remove(Guid id) => mongocontext.AddCommand(() => mongoDbSet.DeleteOneAsync(Builders<T>.Filter.Eq("_id", id)));

        public virtual async Task<dynamic> Update(FilterDefinition<BsonDocument> filter, UpdateDefinition<BsonDocument> update)
        {
            return await collection.UpdateOneAsync(filter, update);
        }
        public virtual void Update(T obj)
        {
            mongocontext.AddCommand(() =>
            {
                return mongoDbSet.ReplaceOneAsync(Builders<T>.Filter.Eq("_id", obj), obj);
            });
        }

        public virtual async Task<dynamic> Update(FilterDefinition<BsonDocument> filter, BsonDocument doc)
        {
            var result = await collection.ReplaceOneAsync(filter, doc);
            return result;
        }



        public Int32 GetDocumentCount(FilterDefinition<T> filter)
        {
            return Convert.ToInt32(mongocontext.CountDocuments(filter));
        }

        public int Commit()
        {
            return _dbContext.SaveChanges();
        }


        public async Task<bool> Commitmongo()
        {
            var changeAmount = await mongocontext.SaveChanges();
            return changeAmount > 0;
        }

        public async Task<int> CommitAsyc()
        {
            return await _dbContext.SaveChangesAsync();
        }


    }
}
