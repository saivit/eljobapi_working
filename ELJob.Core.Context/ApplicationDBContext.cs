﻿using ELJob.Domain.Model.Auth;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Employee;
using ELJob.Domain.Model.Employer;
using ELJob.Domain.Model.Lookup;
using ELJob.Domain.Model.Message;
using ELJob.Domain.Model.Screens;
using ELJob.Domain.Model.Views;

namespace ELJob.Core.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
             : base(options)
        {

        }
        //Auth models
        #region Auth models
        public DbSet<aspnetusers> aspnetusers { get; set; }
        public DbSet<aspnetuserroles> aspnetuserroles { get; set; }
       public DbSet<aspnetroles> aspnetroles { get; set; }
        public DbSet<aspnetusertokens> aspnetusertokens { get; set; }
        public DbSet<Person> Person { get; set; }
        public DbSet<tbl_userloginaudit> tbl_Userloginaudits { get; set; }

        #endregion


        #region Aspirant
        public DbSet<Aspirant> Aspirant { get; set; }
        public DbSet<AspirantAssessment> AspirantAssessment { get; set; }
        public DbSet<AspirantEducation> AspirantEducation { get; set; }
        public DbSet<AspirantExperince> AspirantExperince { get; set; }
        public DbSet<AspirantIntrestSector> AspirantIntrest { get; set; }
        public DbSet<AspirantMigrationLocation> AspirantMigrationLocation { get; set; }
        public DbSet<AspirantPersonal> AspirantPersonal { get; set; }
        public DbSet<AspirantSkillAssessment> AspirantSkillAssessment { get; set; }
        public DbSet<AspirantCertification> aspirantCertification { get; set; }
        #endregion

        #region Employee
        public DbSet<Employee> Employee { get; set; }

        private DbSet<EmployeeHistory> employeeHistory;

        public DbSet<EmployeeHistory> GetEmployeeHistory()
        {
            return employeeHistory;
        }

        public void SetEmployeeHistory(DbSet<EmployeeHistory> value)
        {
            employeeHistory = value;
        }
        #endregion

        #region Employer
        public DbSet<Employer> Employer { get; set; }
        public DbSet<EmployerAddress> EmployerAddress { get; set; }
        public DbSet<EmployerIntrestSectors> EmployerIntrestSectors { get; set; }
        public DbSet<EmployerPlansHistory> EmployerPlansHistory { get; set; }
        #endregion

        #region Lookup
        public DbSet<LookupEducation> LookupEducation { get; set; }
        public DbSet<LookupOtherEducation> LookupOtherEducation { get; set; }
        public DbSet<LookupPlanhistory> lookupPlanhistory { get; set; }
        public DbSet<LookupPlans> LookupPlans { get; set; }
        public DbSet<Lookupstate> Lookupstate { get; set; }
        public DbSet<LookupStatus> LookupStatus { get; set; }
        public DbSet<vw_StatusMapping> LookupStatusMapping { get; set; }
        public DbSet<Lookupstatustype> Lookupstatustype { get; set; }
        public DbSet<LookupLocation> LookupLocation { get; set; }
        public DbSet<LookupZone> LookupZone { get; set; }
        public DbSet<LookupCity> LookupCity { get; set; }
        public DbSet<LookupSector> LookupSector { get; set; }
        public DbSet<LookupObjective> LookupObjective { get; set; }
        public DbSet<aspnetroles> aspnetrole { get; set; }
        public DbSet<messagelist> messagelist { get; set; }
        public DbSet<EmployeeHistory>EmployeeHistory { get; set; }
        public DbSet<LookupPlanhistory> LookupPlanhistory { get; set; }
        public DbSet<LookupLanguage> LookupLanguage { get; set; }

        



        #endregion

        #region Message
        public DbSet<Customermessaging> Customermessaging { get; set; }
        #endregion

        #region Screens
        public DbSet<Screen> Screen { get; set; }
        public DbSet<ScreenAssign> ScreenAssign { get; set; }

        public DbSet<SubScreen> SubScreen { get; set; }
        #endregion


        #region View
        public DbSet<vw_Lookupstatus> vw_Lookupstatus { get; set; }
        public DbSet<vw_aspiranteducation> vw_aspiranteducation { get; set; }
        public DbSet<vw_AssignScreenToRole> vw_AssignScreenToRole { get; set; }
        public DbSet<vw_citylist> vw_citylist { get; set; }
        public DbSet<vw_employeereporting> vw_employeereporting { get; set; }
        public DbSet<vw_locationlist> vw_locationlist { get; set; }
        public DbSet<vw_statusmapping> vw_statusmapping { get; set; }
        public DbSet<vw_SubScreen> vw_SubScreen { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.Types().Configure(entity => entity.ToTable(entity.ClrType.Name.ToLower()));

            modelBuilder.Entity<ApplicationUser>(b =>
            {
                // Each User can have many UserClaims

                b.HasMany(e => e.Claims)
                        .WithOne()
                        .HasForeignKey(uc => uc.UserId)
                        .IsRequired();


            });


            //modelBuilder.Entity<aspnetroles>().HasData(new aspnetroles
            //{
            //    Id = 1,
            //    Name = "Super Admin",
            //    NormalizedName = "Super Admin",
            //    ConcurrencyStamp = "Super Admin"

            //});

        }

    }
    }
