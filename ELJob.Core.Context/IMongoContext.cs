﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace ELJob.Core.Context
{
    public interface IMongoContext : IDisposable
    {
        void AddCommand(Func<Task> func);
        Task<int> SaveChanges();
       IMongoCollection<T> GetCollection<T>(string name);
        long CountDocuments<T>(FilterDefinition<T> filter) where T : class;
       
    }
}
