﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization;

namespace ELJob.Core.Context.Persistance
{
    public class Live
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<Domain.Model.Mongo.live>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
                map.MapMember(x => x._id).SetIsRequired(true);
            });
        }
    }
}
