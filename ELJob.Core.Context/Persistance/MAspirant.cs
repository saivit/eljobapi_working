﻿using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Core.Context.Persistance
{
   public class MAspirant
    {

        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<Domain.Model.Mongo.MAspirant>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
                map.MapMember(x => x._id).SetIsRequired(true);
            });
        }
    }
}
