﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace ELJob.Core.Context
{
    public class MongoContext : IMongoContext
    {
        private IMongoDatabase Database { get; set; }
        private readonly List<Func<Task>> _commands;
        private readonly ILogger<MongoContext> _logger;

        [Obsolete]
        public MongoContext(IConfiguration configuration, ILogger<MongoContext> logger)
        {
            _logger = logger;



            // Set Guid to CSharp style (with dash -)
#pragma warning disable CS0618 // Type or member is obsolete
            BsonDefaults.GuidRepresentation = GuidRepresentation.CSharpLegacy;
#pragma warning restore CS0618 // Type or member is obsolete

            // Every command will be stored and it'll be processed at SaveChanges
            _commands = new List<Func<Task>>();

            RegisterConventions();

            // Configure mongo (You can inject the config, just to simplify)
            try
            {
                _logger.LogDebug("Mongo DB connection!!!");
                var mongoClient = new MongoClient(new MongoClientSettings
                {
                    Server = MongoServerAddress.Parse(configuration.GetSection("MongoSettings").GetSection("Connection").Value),
                    Credential = MongoCredential.CreateCredential(configuration.GetSection("MongoSettings").GetSection("DatabaseName").Value,
                    configuration.GetSection("MongoSettings").GetSection("UserName").Value,
                    configuration.GetSection("MongoSettings").GetSection("Password").Value),
                    UseTls = false,
                    AllowInsecureTls = false,

                    SslSettings = new SslSettings
                    {
                        CheckCertificateRevocation = false
                        // EnabledSslProtocols= SslProtocols.Tls12
                    }

                }); ;

                //var credential = MongoCredential.CreateMongoCRCredential("Sample", "useradmin", "user@123");

                //var settings = new MongoClientSettings
                //{
                //    Credentials = new[] { credential }
                //};

                //var mongoClient = new MongoClient(settings);

                Database = mongoClient.GetDatabase(configuration.GetSection("MongoSettings").GetSection("DatabaseName").Value);


            }
            catch (Exception exc)
            {
                _logger.LogError(exc, "Unable to connect MongoDb");
            }
        }


        private void RegisterConventions()
        {
            var pack = new ConventionPack
            {
                new IgnoreExtraElementsConvention(true),
                new IgnoreIfDefaultConvention(true)
            };
            ConventionRegistry.Register("My Solution Conventions", pack, t => true);
        }

        public void AddCommand(Func<Task> func)
        {
            _commands.Add(func);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return Database.GetCollection<T>(name);
        }

        public async Task<int> SaveChanges()
        {
            var commandTasks = _commands.Select(c => c());

            await Task.WhenAll(commandTasks);

            return _commands.Count;
        }

        public long CountDocuments<T>(FilterDefinition<T> filter) where T : class
        {
            throw new NotImplementedException();
        }
    }
}
