﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration.Json;


namespace ELJob.Helper.Authorization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class JWTAuthorizeAttribute : Attribute, IAuthorizationFilter
    {

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string token = string.Empty;
            if (context.HttpContext.Request.Headers.TryGetValue("Authorization", out var traceValue))
            {
                token = traceValue.ToString().Split("Bearer ")[1];
            }
            var mySecret = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("JWT")["Secret"];
            var mySecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(mySecret));
            var myIssuer = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("JWT")["ValidIssuer"];
            var myAudience = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("JWT")["ValidAudience"];

            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {

                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = myIssuer,
                    ValidAudience = myAudience,
                    IssuerSigningKey = mySecurityKey
                }, out SecurityToken validatedToken);
            }
            catch (Exception Ex)
            {
                context.Result = new JsonResult(new { message = "User is not Authorized. Please try again" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }

            // var account = context.HttpContext.Request.Headers.TryGetValue("Hea", "");

            //if (account == null) {
            //    // not logged in
            //    context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
            //}
        }


    }
}
