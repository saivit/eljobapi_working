﻿using ELJob.Infra.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using ELJob.DAO.DAOLayer.Service;
using ELJob.DAO.DAOLayer.ServiceImpl;
using ELJob.Business.BL.Service;
using ELJob.Business.BL.ServiceImpl;
using ELJob.Core.Context;

namespace ELJob.Helper.DependencyInjection
{
    public static class DependencyInjection
    {

        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            // services.AddTransient<ISeasonlityBusinessLayer, SeasonlityBusinessImpl>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IMongoContext, MongoContext>();
            services.AddTransient<IAuthBL, AuthBLimpl>();
            services.AddTransient<IAuthDAO, AuthDAOImpl>();
            services.AddTransient<IEmployeeDAO,EmployeeDAOImpl>();
            services.AddTransient<IEmployeeBL,EmployeeBLImpl>();
            services.AddTransient<ILookUpDAO, LookupDAOImpl>();
            services.AddTransient<ILookupBL, LookupBLImpl>();
            services.AddTransient<IAspirantDAO, AspirantDAOImpl>();
            services.AddTransient<IAspirantBL, AspirantBLImpl>();
            services.AddTransient<IEmployerDAO, EmployerDAOImpl>();
            services.AddTransient<IEmployerBL, EmployerBLImpl>();
            services.AddTransient<ILocationsDAO, LocationsDAOImpl>();
            services.AddTransient<ILocationsBL, LocationsBLImpl>();
            services.AddTransient<IFoundationDAO, FoundationDAOImpl>();
            services.AddTransient<IFoundationBL, FOundationBLImpl>();
            services.AddTransient<IStatsBL, StatsBLImpls>();
            services.AddTransient<IFinanceBL, FinanceBLImpl>();
            services.AddTransient<IApplicationServices, ApplicationServices>();

            return services;
        }
    }
}
