﻿

using AppEmailVerifier.Viewmodel;
using ELJobPaymentGateway.Data;
using ELJobPaymentGateway.Middleware;
using ELJobPaymentGateway.Remote;
using ELJobPaymentGateway.Repository;
using ELJobpayments.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELJobPaymentGateway.Data
{
    /// <summary>
    /// Verify Email Address
    /// </summary>
    public class PaymentImpl : IPaymentservice
    {
        /// <summary>
        /// MongoContext
        /// </summary>
        private IMongoContext mongocontext;
        private ApplicationDbContext sqlcontext;
        private IConfiguration configuration;

        public PaymentImpl(IMongoContext context,IConfiguration _configuration)
        {
            mongocontext = context;
            sqlcontext = new ApplicationDbContext();
            configuration = _configuration;
        }

        public async Task<Response> Paymentfailure(string order_id)
        {
            var response = new Response() { };
            var Transactionfilter = Builders<MTransactionsmodel>.Filter.Eq(x => x._id, ObjectId.Parse(order_id));
            var updateTransactionBuilder = Builders<MTransactionsmodel>.Update.Set(x => x.Status, Appdata.PaymentFailure);
            mongocontext.GetCollection<MTransactionsmodel>("MTransactionsmodel").UpdateOne(Transactionfilter, updateTransactionBuilder);
            response.Iserror = false;
            response.Message = "Payment Failed";
            response.StatusCode = StatusCodes.Status201Created;
            return await Task.FromResult(response);
        }

        /// <summary>
        /// Verify the Email
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<Response> PaymentProcess(string Employeer, int Plan)
        {
            var response = new Response() { };
            try
            {
                decimal IGST=0, CGST=0, SGST = 0;
                var Plandata = sqlcontext.LookupPlans.Where(x => x.Id == Plan).FirstOrDefault();
                
                string verificationmessage = "";              
                var Jobseekerfilter = Builders<MEmployer>.Filter.Eq(x => x._id, ObjectId.Parse(Employeer));
                var Addressfilter = Builders<MEmployerAddress>.Filter.Eq(x => x.MEmployerId, Employeer);
                var MEmployeer = mongocontext.GetCollection<MEmployer>("MEmployer").FindAsync(Jobseekerfilter).Result.FirstOrDefault();
                var MEmployerAddress = mongocontext.GetCollection<MEmployerAddress>("MEmployerAddress").FindAsync(Addressfilter).Result.FirstOrDefault();
                var statedata = sqlcontext.Lookupstate.Where(x => x.Id == MEmployeer.state).FirstOrDefault();
                var GST=GenerateGST(statedata, Plandata);
                var Transactionnew = new MTransactionsmodel()
                {
                    LastName = MEmployeer.LastName,
                    FirstName = MEmployeer.FirstName,
                    PriceIncludeGST = Plandata.PriceIncludeGST,
                    EmployerId = MEmployeer.EmployerId,
                    ValidityinDays = Plandata.ValidityinDays,
                    GST = Plandata.GST,
                    Validity = Plandata.Validity,
                    TotalCVdownload = Plandata.TotalCVdownload,
                    SMSandEmail = Plandata.SMSandEmail,
                    ProfileViewsnumber = Plandata.ProfileViewsnumber,
                    ProfileViews = Plandata.ProfileViewsnumber,
                    IsUnlimited = Plandata.IsUnlimited,
                    Planmodel = Plandata.Planmodel,
                    Price = Plandata.Price,
                    Status = Appdata.PaymentInprocess,
                    MEmployerId = MEmployeer._id.ToString(),
                    SGST = GST.Item2,
                    CGST = GST.Item1,
                    IGST = GST.Item3,
                    StateId = statedata.Id,
                    StateGSTcode= statedata.code,
                    finalamount = Plandata.PriceIncludeGST,
                    Statename= statedata.Name,
                    companyAccounname= configuration.GetValue<string>("ApplicationSettings:companyname"),
                    companyaccountsno = configuration.GetValue<string>("ApplicationSettings:companyaccountno"),
                    companybank = configuration.GetValue<string>("ApplicationSettings:compnaybank"),
                    companybranch = configuration.GetValue<string>("ApplicationSettings:compnanybrach"),
                    companyifsc = configuration.GetValue<string>("ApplicationSettings:compnayifsc"),
                    pannumber = configuration.GetValue<string>("ApplicationSettings:PAN"),
                    placeofsupply = configuration.GetValue<string>("ApplicationSettings:placeofsupplay"),
                    SACCategorey = configuration.GetValue<string>("ApplicationSettings:ServiceCategorey"),
                    SACcode = configuration.GetValue<string>("ApplicationSettings:SACcode"),
                    UnderpaybleRCM = "N",
                  plandescription= Plandata.Planmodel,
                  //planstartdate=DateTime.Now,
                  //purchasedate=DateTime.Now,
                  //planenddate=DateTime.Now.AddDays(Plandata.ValidityinDays),
                  disscount=0,
                  InvoiceValue=Plandata.PriceIncludeGST,
                  PlanId=Plan,
                  Roundingoff=0,
                  //Invoicenumber= GenerateInvoicenumber(),
                  Invoicein_words= NumberToWords.ConvertToWords(Plandata.PriceIncludeGST.ToString()),
                  address= MEmployerAddress.Address,
                  GSTOfparty=MEmployeer.GSTSNumber,
                  GSTsatatus=MEmployeer.GSTStatus==9?"Registered":"Un-Registered",
                  Gstno= configuration.GetValue<string>("ApplicationSettings:GSTno")

                };
                mongocontext.GetCollection<MTransactionsmodel>("MTransactionsmodel").InsertOne(Transactionnew);
                RazorpayClient client = new RazorpayClient(configuration.GetValue<string>("PaymentGateway:Payment_gatway_Key"), configuration.GetValue<string>("PaymentGateway:Payment_gateway_Secrect"));
                Dictionary<string, object> options = new Dictionary<string, object>();

                if (configuration.GetValue<bool>("PaymentGateway:Isliveenabled"))
                {
                    options.Add("amount", Plandata.PriceIncludeGST * 100); // amount in the smallest currency unit
                }
                else
                {
                    options.Add("amount", 100); // amount in the smallest currency unit
                }                               //}

                options.Add("receipt", Transactionnew._id.ToString());
                options.Add("currency", configuration.GetValue<string>("PaymentGateway:currency"));
                options.Add("payment_capture", "0");
                Order order = client.Order.Create(options);

                IHttpContextAccessor httpContextAccessor = new HttpContextAccessor();

                RemotePost rp = new RemotePost(httpContextAccessor);

                string Sucessurl = string.Format(configuration.GetValue<string>("PaymentGateway:callback_url"), Transactionnew._id.ToString());
                string Failureurl = string.Format(configuration.GetValue<string>("PaymentGateway:cancel_url"), Transactionnew._id.ToString());

                rp.Url = configuration.GetValue<string>("PaymentGateway:Payment_gatway_Url");
                rp.Method = "POST";
                rp.Add("key_id", configuration.GetValue<string>("PaymentGateway:Payment_gatway_Key"));
                rp.Add("order_id", order.Attributes.id.Value);
                rp.Add("name", configuration.GetValue<string>("PaymentGateway:Payment_Gateway_Header"));
                rp.Add("prefill[contact]", MEmployeer.Mobile == null ? "7330991163" : MEmployeer.Mobile);
                rp.Add("prefill[name]", MEmployeer.FirstName+" " +MEmployeer.LastName);
                rp.Add("prefill[email]", MEmployeer.Email == null ? "defalutuserel@eljobs.com" : MEmployeer.Email);
                rp.Add("callback_url", Sucessurl);
                rp.Add("cancel_url", Failureurl);
                rp.Post();

                
                response.Iserror = false;
                response.Message = verificationmessage;
                response.StatusCode = StatusCodes.Status201Created;
                return await Task.FromResult(response);
            }
            catch(Exception Ex)
            {
                response.Iserror = true;
                response.Message = string.Format(AppMessages.Exception);
                response.StatusCode = StatusCodes.Status201Created;
                return await Task.FromResult(response);
            }
           
        }

        public async Task<Response> Paymentsuccess(string order_id)
        {
            var response = new Response() { };
            var Transactionfilter = Builders<MTransactionsmodel>.Filter.Eq(x => x._id, ObjectId.Parse(order_id));

            var MTransactionmodel = mongocontext.GetCollection<MTransactionsmodel>("MTransactionsmodel").FindAsync(Transactionfilter).Result.FirstOrDefault();
            var Jobseekerfilter = Builders<MEmployer>.Filter.Eq(x => x._id, ObjectId.Parse(MTransactionmodel.MEmployerId));
            var MEmployeer = mongocontext.GetCollection<MEmployer>("MEmployer").FindAsync(Jobseekerfilter).Result.FirstOrDefault();

            var updateTransactionBuilder = Builders<MTransactionsmodel>.Update.Set(x => x.Status, Appdata.PaymentSuccess).Set(x=>x.Invoicenumber,GenerateInvoicenumber()).Set(x=>x.planenddate,DateTime.Now.AddDays(MTransactionmodel.Validity))
                .Set(x => x.planstartdate, DateTime.Now).Set(x => x.purchasedate, DateTime.Now)
                ;
            mongocontext.GetCollection<MTransactionsmodel>("MTransactionsmodel").UpdateOne(Transactionfilter, updateTransactionBuilder);

            var updateEmployeerBuilder = Builders<MEmployer>.Update.Set(x => x.LastActivedate, DateTime.Now).Set(x => x.totalCVdownloads, (MEmployeer.totalCVdownloads) + MTransactionmodel.TotalCVdownload)
                .Set(x => x.Profileviews, (MEmployeer.Profileviews) + MTransactionmodel.ProfileViews).Set(x => x.planend, DateTime.Now.AddDays(MTransactionmodel.Validity)).Set(x => x.CurrentPlan, MTransactionmodel.Planmodel);
            mongocontext.GetCollection<MEmployer>("MEmployer").UpdateOne(Jobseekerfilter, updateEmployeerBuilder);

            response.Iserror = false;
            response.Message = "Payment Paid Sccuessfully";
            response.StatusCode = StatusCodes.Status201Created;
            return await Task.FromResult(response);
        }
        private string GenerateInvoicenumber()
        {
            var Mtransacationfilter = Builders<MTransactionsmodel>.Filter.Gt(x => x.Price,0);
            var Mtrasnsactionmodel = mongocontext.GetCollection<MTransactionsmodel>("MTransactionsmodel").FindAsync(Mtransacationfilter).Result;
            string invoicenumber = string.Format("VISELJ{0}{1}", DateTime.Now.Year.ToString().Substring(2, 2) + "" + DateTime.Now.AddYears(1).Year.ToString().Substring(2,2), (Mtrasnsactionmodel.ToList().Count + 1).ToString("00000"));
            return invoicenumber;
        }
        private Tuple<decimal,decimal,decimal> GenerateGST(Lookupstate state,LookupPlans plan)
        {
            decimal CGST = 0, SGST = 0, IGST = 0;
            if(state.Id==Convert.ToInt32(Defalutstate.Defalutstate))
            {

                CGST = Convert.ToDecimal(value: (plan.Price
                    * (state.CGST / 100)).ToString("F"));
                SGST = Convert.ToDecimal((plan.Price * (state.SGST / 100)).ToString("F"));
            }
            else
            {
                IGST = Convert.ToDecimal((plan.Price * (state.IGST / 100)).ToString("F"));
            }
            return Tuple.Create(CGST, SGST, IGST);
        }


        private static class NumberToWords
        {
            public static String ConvertToWords(String numb)
            {
                String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
                String endStr = "Only";
                try
                {
                    int decimalPlace = numb.IndexOf(".");
                    if (decimalPlace > 0)
                    {
                        wholeNo = numb.Substring(0, decimalPlace);
                        points = numb.Substring(decimalPlace + 1);
                        if (Convert.ToInt32(points) > 0)
                        {
                            andStr = "and";// just to separate whole numbers from points/cents    
                            endStr = "Paisa " + endStr;//Cents    
                            pointStr = ConvertDecimals(points);
                        }
                    }
                    val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
                }
                catch { }
                return val;
            }

            private static String ConvertWholeNumber(String Number)
            {
                string word = "";
                try
                {
                    bool beginsZero = false;//tests for 0XX    
                    bool isDone = false;//test if already translated    
                    double dblAmt = (Convert.ToDouble(Number));
                    //if ((dblAmt > 0) && number.StartsWith("0"))    
                    if (dblAmt > 0)
                    {//test for zero or digit zero in a nuemric    
                        beginsZero = Number.StartsWith("0");

                        int numDigits = Number.Length;
                        int pos = 0;//store digit grouping    
                        String place = "";//digit grouping name:hundres,thousand,etc...    
                        switch (numDigits)
                        {
                            case 1://ones' range    

                                word = ones(Number);
                                isDone = true;
                                break;
                            case 2://tens' range    
                                word = tens(Number);
                                isDone = true;
                                break;
                            case 3://hundreds' range    
                                pos = (numDigits % 3) + 1;
                                place = " Hundred ";
                                break;
                            case 4://thousands' range    
                            case 5:
                            case 6:
                                pos = (numDigits % 4) + 1;
                                place = " Thousand ";
                                break;
                            case 7://millions' range    
                            case 8:
                            case 9:
                                pos = (numDigits % 7) + 1;
                                place = " Million ";
                                break;
                            case 10://Billions's range    
                            case 11:
                            case 12:

                                pos = (numDigits % 10) + 1;
                                place = " Billion ";
                                break;
                            //add extra case options for anything above Billion...    
                            default:
                                isDone = true;
                                break;
                        }
                        if (!isDone)
                        {//if transalation is not done, continue...(Recursion comes in now!!)    
                            if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                            {
                                try
                                {
                                    word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                                }
                                catch { }
                            }
                            else
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                            }

                            //check for trailing zeros    
                            //if (beginsZero) word = " and " + word.Trim();    
                        }
                        //ignore digit grouping names    
                        if (word.Trim().Equals(place.Trim())) word = "";
                    }
                }
                catch { }
                return word.Trim();
            }

            private static String tens(String Number)
            {
                int _Number = Convert.ToInt32(Number);
                String name = null;
                switch (_Number)
                {
                    case 10:
                        name = "Ten";
                        break;
                    case 11:
                        name = "Eleven";
                        break;
                    case 12:
                        name = "Twelve";
                        break;
                    case 13:
                        name = "Thirteen";
                        break;
                    case 14:
                        name = "Fourteen";
                        break;
                    case 15:
                        name = "Fifteen";
                        break;
                    case 16:
                        name = "Sixteen";
                        break;
                    case 17:
                        name = "Seventeen";
                        break;
                    case 18:
                        name = "Eighteen";
                        break;
                    case 19:
                        name = "Nineteen";
                        break;
                    case 20:
                        name = "Twenty";
                        break;
                    case 30:
                        name = "Thirty";
                        break;
                    case 40:
                        name = "Fourty";
                        break;
                    case 50:
                        name = "Fifty";
                        break;
                    case 60:
                        name = "Sixty";
                        break;
                    case 70:
                        name = "Seventy";
                        break;
                    case 80:
                        name = "Eighty";
                        break;
                    case 90:
                        name = "Ninety";
                        break;
                    default:
                        if (_Number > 0)
                        {
                            name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                        }
                        break;
                }
                return name;
            }

            private static String ones(String Number)
            {
                int _Number = Convert.ToInt32(Number);
                String name = "";
                switch (_Number)
                {

                    case 1:
                        name = "One";
                        break;
                    case 2:
                        name = "Two";
                        break;
                    case 3:
                        name = "Three";
                        break;
                    case 4:
                        name = "Four";
                        break;
                    case 5:
                        name = "Five";
                        break;
                    case 6:
                        name = "Six";
                        break;
                    case 7:
                        name = "Seven";
                        break;
                    case 8:
                        name = "Eight";
                        break;
                    case 9:
                        name = "Nine";
                        break;
                }
                return name;
            }

            private static String ConvertDecimals(String number)
            {
                String cd = "", digit = "", engOne = "";
                for (int i = 0; i < number.Length; i++)
                {
                    digit = number[i].ToString();
                    if (digit.Equals("0"))
                    {
                        engOne = "Zero";
                    }
                    else
                    {
                        engOne = ones(digit);
                    }
                    cd += " " + engOne;
                }
                return cd;
            }
        }
    }
}
