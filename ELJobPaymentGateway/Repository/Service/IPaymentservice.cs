﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEmailVerifier.Viewmodel;

namespace ELJobPaymentGateway.Repository
{
    public interface IPaymentservice
    {
        Task<Response> PaymentProcess(string Employeer, int Plan);
        Task<Response> Paymentsuccess(string order_id);
        Task<Response> Paymentfailure(string order_id);
    }
}
