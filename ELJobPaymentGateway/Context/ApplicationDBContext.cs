﻿using ELJobPaymentGateway.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace ELJobpayments.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
             : base(options)
        {

        }
        //Auth models
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("connectionString");
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        #region Lookup

        public DbSet<LookupPlans> LookupPlans { get; set; }
        public DbSet<Lookupstate> Lookupstate { get; set; }
        #endregion

     
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.Types().Configure(entity => entity.ToTable(entity.ClrType.Name.ToLower()));

            modelBuilder.Entity<ApplicationUser>(b =>
            {
                // Each User can have many UserClaims

                b.HasMany(e => e.Claims)
                        .WithOne()
                        .HasForeignKey(uc => uc.UserId)
                        .IsRequired();


            });


            //modelBuilder.Entity<aspnetroles>().HasData(new aspnetroles
            //{
            //    Id = 1,
            //    Name = "Super Admin",
            //    NormalizedName = "Super Admin",
            //    ConcurrencyStamp = "Super Admin"

            //});

        }

    }
    }
