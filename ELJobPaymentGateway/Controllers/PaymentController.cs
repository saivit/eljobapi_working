﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ELJobPaymentGateway.Utility;
using ELJobPaymentGateway.Repository;
using ELJobPaymentGateway.Data;
using ELJobPaymentGateway.Middleware;
using ELJobpayments.Context;

namespace ELJobPaymentGateway.Controllers
{
    public class PaymentController : Controller
    {
        /// <summary>
        /// Get the Configuration builder
        /// </summary>
        private IConfiguration configuration;

        /// <summary>
        /// BL Payment service
        /// </summary>
        private IPaymentservice paymentservice;

        private ApplicationDbContext dbcontext;

        public PaymentController(IConfiguration _configuration,IMongoContext mongocontext)
        {
            configuration = _configuration;
            paymentservice = new PaymentImpl(mongocontext, configuration);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Payment/Paymentsucess")]
        public IActionResult paymentsucess(string order_id)
        {           
            var razorpay_order_id = Request.Form["razorpay_order_id"];
            var razorpay_payment_id = Request.Form["razorpay_payment_id"];
            var razorpay_signature = Request.Form["razorpay_signature"];

            var generated_signature = Utility.Utility.HmacSha256Digest(razorpay_order_id + "|" + razorpay_payment_id, configuration.GetValue<string>("PaymentGateway:Payment_gateway_Secrect"));

            if (generated_signature == razorpay_signature)
            {
                paymentservice.Paymentsuccess(order_id);
            }
            return View("Controllers/PaymentConfirmation.cshtml");
        }


        [AllowAnonymous]
        [HttpGet]
        [Route("Payment/Paymentfailure")]
        public IActionResult paymentfailure(string order_id)
        {          
            paymentservice.Paymentfailure(order_id);    
            return View("Controllers/Paymentfailure.cshtml");
        }

    }
}
