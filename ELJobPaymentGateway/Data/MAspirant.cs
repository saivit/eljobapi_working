﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEmailVerifier.Data
{
    public class MAspirant
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string JobseekerId { get; set; }
        public int AspirantId { get; set; }
        public bool EmailVerified { get; set; }
    }
}
