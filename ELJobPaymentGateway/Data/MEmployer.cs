﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ELJobPaymentGateway.Data
{

    public class MEmployer
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public int EmployerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Organizationtype { get; set; }
        public int OrganizationtypeId { get; set; }
        public string Organization { get; set; }
        public string CurrentPlan { get; set; }
        public int Validity { get; set; }
        public decimal Price { get; set; }
        public int Profileviews { get; set; }
        public int totalCVdownloads { get; set; }
        public int BoradcastEmailSMS { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime planstart { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime planend { get; set; }
        public int tillnowprofileviews { get; set; }
        public int tillnowCVdownload { get; set; }
        public string Description { get; set; }
        public string Designation { get; set; }
        public int GSTStatus { get; set; }
        public string GSTSNumber { get; set; }
        public string Profilepicpath { get; set; }
        public string profilepicname { get; set; }
        public string SMSTemplate { get; set; }
        public string Profilepictype { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public int UserId { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Createddate { get; set; }
        public string Enc_id { get; set; }
        public DateTime LastActivedate { get; set; }
        public bool isSelfregistration { get; set; }
        public int state { get; set; }
        public int[] state_array { get; set; }
        public string statename { get; set; }
        public int city { get; set; }
        public int[] city_array { get; set; }
        public string Cityname { get; set; }
        public string EmployeeId { get; set; }
        public int status { get; set; }
        public string statusname { get; set; }

    }

 

}
