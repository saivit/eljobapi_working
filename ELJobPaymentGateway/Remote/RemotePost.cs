﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELJobPaymentGateway.Remote
{

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class RemotePost
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    {
        public IHttpContextAccessor _httpContextAccessor;

        public RemotePost(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private System.Collections.Specialized.NameValueCollection Inputs = new System.Collections.Specialized.NameValueCollection();
        public string Url = "";
        public string Method = "post";
        public string FormName = "form1";
        public void Add(string name, string value)
        {
            Inputs.Add(name, value);
        }

        public void Post()
        {
            _httpContextAccessor.HttpContext.Response.Clear();
            _httpContextAccessor.HttpContext.Response.WriteAsync("<html><head>");
            _httpContextAccessor.HttpContext.Response.WriteAsync(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
            _httpContextAccessor.HttpContext.Response.WriteAsync(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, Url));
            for (int i = 0; i < Inputs.Keys.Count; i++)
            {
                _httpContextAccessor.HttpContext.Response.WriteAsync(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", Inputs.Keys[i], Inputs[Inputs.Keys[i]]));
            }
            _httpContextAccessor.HttpContext.Response.WriteAsync("</form>");
            _httpContextAccessor.HttpContext.Response.WriteAsync("</body></html>");

        }
    }
}
