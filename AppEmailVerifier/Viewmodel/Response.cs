﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AppEmailVerifier.Viewmodel
{
    #region response Businessmodel


    /// <summary>
    /// returns the response of Http request. check the logic and return back to subscriber.
    /// </summary>
    /// 

    public class Response
    {
        /// <summary>
        /// set the return status text, all [Ex :Created,Ok,Internal server Error]
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// set the return status code. [Ex: 200,210,400,401,404,500...]
        /// </summary>
        public int StatusCode { get; set; }
        /// <summary>
        /// indiacate the subscribers there is erro inside the resonse. 
        /// </summary>
        public bool Iserror { get; set; }
        /// <summary>
        /// List of all error, with standard description
        /// </summary>
        public List<string> errors { get; set; }
        /// <summary>
        /// Success message will return back to subscriber.
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// This is the customer refence Id used for Uniquerefence identification.
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// This is the customer refence Id used for Uniquerefence identification.
        /// </summary>
        public string LoanId { get; set; }

        /// <summary>
        /// Returns the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// It is object , pass the any type of return data.
        /// </summary>
        public object response { get; set; }

        /// <summary>
        /// stream object
        /// </summary>
        public MemoryStream stream { get; set; }

        /// <summary>
        /// return the file name
        /// </summary>
        public string Filename { get; set; }
        /// <summary>
        /// Return the file type
        /// </summary>
        public string filetype { get; set; }

        /// <summary>
        /// Validations
        /// </summary>
        public List<Exceptions> validations { get; set; }

    }

    public class ResponseAsyn:Response
    {
   
    }
    
    #endregion
}
