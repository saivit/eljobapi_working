﻿using AppEmailVerifier.Middleware;
using AppEmailVerifier.Repository;
using AppEmailVerifier.Viewmodel;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEmailVerifier.Data
{
    /// <summary>
    /// Verify Email Address
    /// </summary>
    public class EmailVerificationservice : IEmailVerificationservice
    {
        /// <summary>
        /// MongoContext
        /// </summary>
        private IMongoContext mongocontext;

        public EmailVerificationservice(IMongoContext context)
        {
            mongocontext = context;
        }
        /// <summary>
        /// Verify the Email
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<Response> VerifyEmail(string key)
        {
            var response = new Response() { };
            try
            {
                string verificationmessage = "";              
                var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(key));
                var Maspirant = mongocontext.GetCollection<MAspirant>("MAspirant").FindAsync(Jobseekerfilter).Result.FirstOrDefault();
                if (Maspirant == null)
                {
                    response.Iserror = true;
                    response.Message = string.Format(AppMessages.NodataAvilable, "User");
                    response.StatusCode = StatusCodes.Status404NotFound;
                    return await Task.FromResult(response);
                }
                if (Maspirant.IsEmailvalidated)
                    verificationmessage = string.Format(AppMessages.AlreadyEmailValidated);
                else
                verificationmessage = string.Format(AppMessages.EmailValidated);

                var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.IsEmailvalidated, true);
                mongocontext.GetCollection<MAspirant>("MAspirant").UpdateOne(Jobseekerfilter, updateJobseekrBuilder);
                response.Iserror = false;
                response.Message = verificationmessage;
                response.StatusCode = StatusCodes.Status201Created;
                return await Task.FromResult(response);
            }
            catch(Exception Ex)
            {
                response.Iserror = true;
                response.Message = string.Format(AppMessages.Exception);
                response.StatusCode = StatusCodes.Status201Created;
                return await Task.FromResult(response);
            }
           
        }
    }
}
