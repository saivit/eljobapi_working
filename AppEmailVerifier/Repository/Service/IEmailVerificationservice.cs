﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEmailVerifier.Viewmodel;

namespace AppEmailVerifier.Repository
{
    public interface IEmailVerificationservice
    {
        Task<Response> VerifyEmail(string key);
    }
}
