﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEmailVerifier.Data
{
    public static class AppMessages
    {
        public static string NodataAvilable = "Sorry!,Seems No Data Avilable with this {0}";

        public static string EmailValidated = "Thank you!Email is Validate Sucessfully";

        public static string AlreadyEmailValidated = "Thank you!Already your Email is Validated Sucessfully";
        public static string Exception = "Sorry ! Something Went to wrong";

    }
}
