﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ELJobAPI
{
    [Table("tbl_trail")]
    public class tbl_trail
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName ="nvachar(50)")]
        public string Name { get; set; }
    }
}
