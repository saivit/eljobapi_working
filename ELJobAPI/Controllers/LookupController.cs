﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELJob.Business.BL.Service;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using ELJob.Helper.Authorization;
using ELJob.Helper.Formater;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using static ELJob.Helper.Common.Constants;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
     //[JWTAuthorize]
    public class LookupController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly ILookupBL lookupervice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        #region Construtor
        public LookupController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration, ILookupBL lookupservice)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            lookupervice = lookupservice;
        }

        #endregion


        #region API Methods

        /// <summary>
        /// Get Source of Lead
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getsourceoflead")]
        public IActionResult GetSourceofLead()
        {
            var response = lookupervice.GetStatusbyType(Lookupstatustype.SourceOfLead.ToInt32());
            return Ok(response);
        }

        /// <summary>
        /// Get Gender
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getgender")]
        public IActionResult GetGender()
        {
            var genderresponse = lookupervice.GetStatusbyType(Lookupstatustype.Gender.ToInt32());
            return Ok(genderresponse);
        }

        /// <summary>
        /// Get Conditional Yes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getconditionalyes")]
        public IActionResult GetConditionalYes()
        {
            var conditionyesresponse = lookupervice.GetStatusbyType(Lookupstatustype.ConditionalYes.ToInt32());
            return Ok(conditionyesresponse);
        }


        /// <summary>
        /// Get Followup
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getfollowup")]
        public IActionResult GetFollowup()
        {
            var followupresponse = lookupervice.GetStatusbyType(Lookupstatustype.Followup.ToInt32());
            return Ok(followupresponse);
        }


        /// <summary>
        /// Get Rejection Reason
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getrejectionreason")]
        public IActionResult GetRejectionReason()
        {
            var rejectionresponse = lookupervice.GetStatusbyType(Lookupstatustype.RejectionReason.ToInt32());
            return Ok(rejectionresponse);
        }



        /// <summary>
        /// Get Rejection Reason
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("genderinfilter")]
        public IActionResult GenderInFilter()
        {
            var genderinfilterresponse = lookupervice.GetStatusbyType(Lookupstatustype.GenderInFilter.ToInt32());
            return Ok(genderinfilterresponse);
        }


        /// <summary>
        /// Get Migrate Reason
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("migratereason")]
        public IActionResult MigrateReason()
        {
            var gmigratereasonresponse = lookupervice.GetStatusbyType(Lookupstatustype.MigrateReason.ToInt32());
            return Ok(gmigratereasonresponse);
        }

        /// <summary>
        /// Get Experience
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("experience")]
        public IActionResult Experience()
        {
            var experienceresponse = lookupervice.GetStatusbyType(Lookupstatustype.Experience.ToInt32());
            return Ok(experienceresponse);
        }

        /// <summary>
        /// Get Mode Of Education
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("modeofeducation")]
        public IActionResult ModeOfEducation()
        {
            var modeofeducationresponse = lookupervice.GetStatusbyType(Lookupstatustype.ModeOfEducation.ToInt32());
            return Ok(modeofeducationresponse);
        }

        /// <summary>
        /// Get Language Proficiency
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("languageproficiency")]
        public IActionResult LanguageProficiency()
        {
            var languageproficiencyresponse = lookupervice.GetStatusbyType(Lookupstatustype.LanguageProficiency.ToInt32());
            return Ok(languageproficiencyresponse);
        }

        /// <summary>
        /// Get Aptitude and Computer Skills
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("aptitudeandcomputerskills")]
        public IActionResult AptitudeandComputerSkills()
        {
            var aptitudeandcomputerskillsresponse = lookupervice.GetStatusbyType(Lookupstatustype.AptitudeandComputerSkills.ToInt32());
            return Ok(aptitudeandcomputerskillsresponse);
        }

        /// <summary>
        /// Get Skill Scale
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("skillscale")]
        public IActionResult SkillScale()
        {
            var skillscaleresponse = lookupervice.GetStatusbyType(Lookupstatustype.SkillScale.ToInt32());
            return Ok(skillscaleresponse);
        }

        /// <summary>
        /// Get Organization Type
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("organizationtype")]
        public IActionResult OrganizationType()
        {
            var organizationtyperesponse = lookupervice.GetStatusbyType(Lookupstatustype.OrganizationType.ToInt32());
            return Ok(organizationtyperesponse);
        }

        /// <summary>
        /// Get GST Status
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("gststatus")]
        public IActionResult GSTstatus()
        {
            var gststatusresponse = lookupervice.GetStatusbyType(Lookupstatustype.GSTstatus.ToInt32());
            return Ok(gststatusresponse);
        }

        /// <summary>
        /// Get Pricing  Plans
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("pricingplans")]
        public IActionResult PricingPlans()
        {
            var pricingplansresponse = lookupervice.GetStatusbyType(Lookupstatustype.PricingPlans.ToInt32());
            return Ok(pricingplansresponse);
        }

        /// <summary>
        /// Get Zone
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getzones")]
        public IActionResult GetZones()
        {
            var response = lookupervice.GetZones();
            return Ok(response);
        }

        /// <summary>
        /// Get Zone
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getroles")]
        public IActionResult GetRoles()
        {
            var response = lookupervice.GetRoles();
            return Ok(response);
        }

        /// <summary>
        /// Get Territories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getterritoriesbyid")]
         public IActionResult GetTerritories(int Zone)
        {
            var response = lookupervice.GetTerritoriesbyId(Zone);
            return Ok(response);
        }


        /// <summary>
        /// Get OtherEducationById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getothereducationbyid")]
        public IActionResult GetOtherEducationById(int Id)
        {
            return Ok(lookupervice.GetOtherEducationById(Id));
        }

        /// <summary>
        /// Get Counceling status
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getcouncelingstatus")]
        public IActionResult GetCouncelingstatus()
        {
            var response = lookupervice.GetCouncelingstatus();
            return Ok(response);
        }

        /// <summary>
        /// Get JobseekerDDLValue
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getjobseekerddlvalues")]
        public IActionResult GetJobseekerDDLValues()
        {
            return Ok(lookupervice.GetJobseekerDDLValues());
        }


        /// <summary>
        /// Get Employeer Update Profile Parameters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getemployeerupdateprofileparameters")]
        public IActionResult GetEmployeerUpdateProfileParameters()
        {
            var response = lookupervice.GetEmployeerUpdateProfileParameters();
            return Ok(response);
        }

        /// <summary>
        /// Get Josb search Parameters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getjosbsearchparameters")]
        public IActionResult GetJosbsearchParameters()
        {
            var response = lookupervice.GetJosbsearchParameters();
            return Ok(response);
        }

        /// <summary>
        /// Get Languaqges
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getlanguages")]
        public IActionResult GetLanguages()
        {
            var response = lookupervice.GetLanguages();
            return Ok(response);
        }


        #endregion

    }
}
