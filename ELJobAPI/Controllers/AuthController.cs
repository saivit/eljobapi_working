﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ELJob.Business.BL.Service;
using ELJob.Helper.Authorization;
using ELJob.Domain.Model.Auth;
using ELJob.Core.Context;
using ELJob.Business.Viewmodel.Auth;
using ELJob.Business.Viewmodel;
using ELJob.Helper.Common;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
 
    public class AuthController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IAuthBL Authservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        private readonly RoleManager<ApplicationRole> roleManager;

        private readonly SignInManager<ApplicationUser> signInManager;

        #endregion

        #region Construtor
        public AuthController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration,
            IAuthBL AuthBlservice, SignInManager<ApplicationUser> signInManager, RoleManager<ApplicationRole> roleManager)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Authservice = AuthBlservice;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
        }

        #endregion

        #region API methods
        [HttpPost]
        [Route("login")]

        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var user = await userManager.FindByNameAsync(model.Username);
            //Checking here Either the Login User is Active or Not. if he is inactive throw response as invalid
           
            if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
            {
               Authservice.SaveLogin(user.Id);
                var userRoles = await userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };


                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);

                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Role, "Customer"));
                //foreach (var userRole in userRoles)
                //{
                //    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                //}
                await signInManager.SignInAsync(user, isPersistent: false);

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: configuration["JWT:ValidIssuer"],
                    audience: configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddDays(27),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }
            return Unauthorized();
        }

        /// <summary>
        /// Employer login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("employerlogin")]

        public async Task<IActionResult> EmployerLogin([FromBody] LoginModel model)
        {
            try
            {
                var user = await userManager.FindByNameAsync(model.Username);
                //Checking here Either the Login User is Active or Not. if he is inactive throw response as invalid

                if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
                {
                    Authservice.SaveLogin(user.Id);
                    var userRoles = await userManager.GetRolesAsync(user);

                    var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };


                    var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);

                    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserName));
                    identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
                    identity.AddClaim(new Claim(ClaimTypes.Role, "Customer"));
                    //foreach (var userRole in userRoles)
                    //{
                    //    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                    //}
                    await signInManager.SignInAsync(user, isPersistent: false);

                    var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));

                    var token = new JwtSecurityToken(
                        issuer: configuration["JWT:ValidIssuer"],
                        audience: configuration["JWT:ValidAudience"],
                        expires: DateTime.Now.AddDays(27),
                        claims: authClaims,
                        signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                        );

                    return Ok(new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo,
                        employerkey = Authservice.Employertoken(user.Id),
                        profilcheck=Authservice.IsEmployeerprofileupdated(user.Id)
                    });
                }
            }
            catch(Exception Ex)
            {
                Console.WriteLine(Ex.Message);
            }
         
            return Unauthorized();
        }

        [HttpPost]
        [Route("otplogin")]
        public async Task<IActionResult> OTPLogin([FromBody] OTPModel model)
        {
            var user = Authservice.Applicationuser(model.Mobile);
            if (user != null && Authservice.IsLoginOTPvalid(user.Id, model.OTP.ToString()))
            {
                ApplicationUser signedUser = await signInManager.UserManager.FindByNameAsync(user.UserName);
                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);
                var userRoles = await userManager.GetRolesAsync(user);
               // _authunticateservice.SaveLogin(user.Id);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                //  var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);

                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Role, "Customer"));
                //foreach (var userRole in userRoles)
                //{
                //    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                //}
                await signInManager.SignInAsync(user, isPersistent: false);

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: configuration["JWT:ValidIssuer"],
                    audience: configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });

            }
            return Unauthorized();

        }


        [HttpPost]
        [Route("sendotp")]

        public IActionResult SendOTP([FromBody] OTPModel model)
        {
            var user = Authservice.Applicationuser(model.Mobile);
            if (user != null)
            {
                return Ok(Authservice.SendOTP(user.Id, model));
            }
            return BadRequest();

        }


        [HttpGet]
        [Route("getscreensbyrole")]
        public async Task<IActionResult> GetScreensbyRole()
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Authservice.GetScreensbyRole(users.Id));

        }


        [HttpPost]
        [Route("fogotpassword")]

        public async Task<IActionResult> ForgotPassword([FromBody] Forgotpassword model)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToPage("./ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please 
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await userManager.GeneratePasswordResetTokenAsync(user);
                //code = code.Replace("+", configuration.GetSection("Controls:ReplaceSymbollplus").Value).Replace("/", "bslh").Replace("=", "zklu");
                string callbackUrl = String.Format(configuration.GetSection("ApplicationUrls:ClientWebURL").Value, code, model.Email);
                string body = string.Empty;
                //  model.code = model.code.Replace(configuration.GetSection("Controls:ReplaceSymbollplus").Value, "+").Replace("bslh", "/").Replace("zklu", "=");
                string password = CommonHelper.Randompassword().ToString();
                var result = await userManager.ResetPasswordAsync(user, code, password);
                if (result.Succeeded)
                {
                    //using streamreader for reading my htmltemplate   
                    using (StreamReader reader = new StreamReader(Path.Combine("Content", "Html", "Forgotpassword/forgot pssword.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{Username}", Authservice.UserProfile(user.Id).name).Replace("{password}", password).Replace("{url}", callbackUrl).Replace("{Datetime}", DateTime.Now.ToShortDateString());

                    return Ok(Authservice.SendforgotEmail(user.Id, model, body));
                }

                
            }
            return BadRequest();
        }


        [HttpGet]
        [Route("fogotpasswordchange")]

        public async Task<IActionResult> ForgotPasswordChange([FromQuery] Forgotpasswordcode model)
        {
            return Redirect(string.Format(configuration.GetSection("ApplicationUrls:ClientWebURL").Value, model.code, model.Email));
        }

        [HttpPost]
        [Route("changepassword")]

        public async Task<IActionResult> ChangePassword(Forgotpasswordcode model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToPage("./ResetPasswordConfirmation");
            }
            model.code = await userManager.GeneratePasswordResetTokenAsync(user);
            var result = await userManager.ResetPasswordAsync(user, model.code, model.Password);
            if (result.Succeeded)
            {
                return Ok(new Response() { StatusCode = StatusCodes.Status201Created, Message = "Password Changed Sucessfully", Title = Constants.messageTitles.success });
            }
            List<string> errors = new List<string>();
            foreach (var error in result.Errors)
            {

                errors.Add(error.Description);
            }
            return BadRequest(new Response() { StatusCode = StatusCodes.Status400BadRequest, Iserror = true, errors = errors });

         
        }

        [HttpPost]
        [Route("register-admin")]
        [JWTAuthorize]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model)
        {
            var userExists = await userManager.FindByNameAsync(model.Username);
            if (userExists != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User already exists!" });

            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username
            };
            var result = await userManager.CreateAsync(user, CommonHelper.Randompassword().ToString());
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });

            //if (!await roleManager.RoleExistsAsync(UserRoles.Admin))
            //    await roleManager.CreateAsync(new ApplicationRole(UserRoles.Admin));
            //if (!await roleManager.RoleExistsAsync(UserRoles.User))
            //    await roleManager.CreateAsync(new IdentityRole(UserRoles.User));

            if (await roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await userManager.AddToRoleAsync(user, UserRoles.Admin);
            }

            return Ok(new Response { Status = "Success", Message = "User created successfully!" });
        }


        /// <summary>
        /// Get User Profile
        /// </summary>
        /// <param name="JobseekerId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Getuserprofile")]
        public async Task<ActionResult> UserProfile(string JobseekerId)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Authservice.UserProfile(users.Id));
        }

        #endregion


    }
}
