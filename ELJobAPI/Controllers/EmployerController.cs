﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Auth;
using ELJob.Business.Viewmodel.Employeer;
using ELJob.Business.Viewmodel.Shared;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Employer;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Authorization;
using ELJob.Helper.Common;
using ELJob.Helper.Communication;
using ELJob.Helper.Grid;
using ELJobAPI.Remote;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static ELJob.Helper.Common.Constants;

namespace ELJobAPI.Controllers
{
    /// <summary>
    /// Enables to the Employeer HTTP API communication.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    //[JWTAuthorize]
    public class EmployerController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IEmployerBL Employerservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IAuthBL Authservice;


        private readonly SignInManager<ApplicationUser> signInManager;

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        #region Construtor
        public EmployerController(ApplicationDbContext context, UserManager<ApplicationUser> userManager,
            IConfiguration _configuration, IEmployerBL EmployerBlservice
            , SignInManager<ApplicationUser> signInManager, IAuthBL _AuthBlservice
            )
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Employerservice = EmployerBlservice;
            this.signInManager = signInManager;
            Authservice = _AuthBlservice;
        }

        #endregion

        #region API Method
        /// <summary>
        /// Add Employer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addemployer")]
        public async Task<IActionResult> AddEmployer(EmployerRegisterModel model)
        {
            try
            {
                var userExists = await userManager.FindByNameAsync(model.Email);
                ApplicationUser user = new ApplicationUser()
                {
                    Email = model.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.Email,
                    IsActive = true,
                    PhoneNumber = model.Mobile
                };
                string Password = CommonHelper.Randompassword().ToString();
                var result = await userManager.CreateAsync(user, Password);
                if (!result.Succeeded)
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   
                using (StreamReader reader = new StreamReader(Path.Combine("Content", "Html", "Register/Employer Registration.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Username}", model.FirstName+" "+model.LastName).Replace("{email}", model.Email)
                    .Replace("{mail}", configuration.GetValue<string>("ApplicationSettings:supportmail")).Replace("{password}", Password).Replace("{employeemail}", model.Email).Replace("{url}", configuration.GetValue<string>("ApplicationSettings:EmployeerLogin"))
                     .Replace("{contact}", configuration.GetValue<string>("ApplicationSettings:contactnumber"))
                    ;
                model.Id = user.Id;
                if (HttpContext.User.Identity.Name != null)
                {
                    var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
                    
                    model.CreatedBy = users.Id;
                }
             
                {
                   
                    Employerservice.AddEmployer(model, model.CreatedBy);
                    EmailHelper.To = model.Email;
                    EmailHelper.Subject = "Login Credintails-" + model.FirstName;
                    EmailHelper.Body = body;
                    EmailHelper.IsHtml = true;
                    EmailHelper.SendEmail(configuration.GetValue<string>("EmailConfiguration:SmtpServer"), configuration.GetValue<string>("EmailConfiguration:SmtpUsername"),
                        configuration.GetValue<string>("EmailConfiguration:SmtpPassword"), configuration.GetValue<bool>("EmailConfiguration:SSLenabled"),
                        configuration.GetValue<int>("EmailConfiguration:SmtpPort")
                        );

                   
                }
                return Ok(new Response { Status = "Success", Message = "User created successfully!", StatusCode = (StatusCodes.Status201Created) });


            }
            catch (Exception Ex)
            {
                return Ok(new Response() { Iserror = true, errors = new List<string>() { Ex.Message + " " + Ex.Message }, StatusCode = StatusCodes.Status500InternalServerError });

            }
        }

        /// <summary>
        /// Update Employeer Address
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateemployeraddress")]
        public async Task<ActionResult> UpdateEmployerAddress(EmployerAddress model)
        {
            if(HttpContext.User.Identity.Name != null)
            {
                var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
                return Ok(Employerservice.UpdateEmployerAddress(model, users.Id));
            }
            else
            {                
                return Ok(Employerservice.UpdateEmployerAddress(model, 0));
            }
               
        }

        /// <summary>
        /// Employer Profile Update model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateemployeerprofile")]
        public async Task<ActionResult> UpdateEmployeerprofile(EmployerProfileUpdatemodel model)
        {
            if (HttpContext.User.Identity.Name != null)
            {

                var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
                return Ok(Employerservice.UpdateEmployeerprofile(model, users.Id));
            }
            else
            {   
                return Ok(Employerservice.UpdateEmployeerprofile(model, 0));
            }
        }

        /// <summary>
        /// Update Organization
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateorganization")]
        public async Task<ActionResult> UpdateOrganization(EmployerOrganizationalModel model)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.UpdateOrganization(model, users.Id));
        }
        /// <summary>
        /// Get Employeer Organizational details
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Route("getemployeerorganizationaldetails")]
        public async Task<ActionResult> GetEmployeerOrganizationaldetails()
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.GetEmployeerOrganizationaldetails(users.Id));
        }

        /// <summary>
        /// Get Fileterprofiles
        /// </summary>
        /// <param name="filterId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfileterprofiles")]
        public async Task<ActionResult> GetFileterprofiles(string filterId, [FromQuery] PageData page)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(await Employerservice.GetFileterprofiles(filterId, page));
        }


        /// <summary>
        /// Get Previous Searchs
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="Fromdate"></param>
        /// <param name="Todate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getprevioussearchs")]
        public async Task<ActionResult> GetFileterprofiles([FromQuery] PageData page, string Sort, bool descending, string name, DateTime Fromdate, DateTime Todate)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(await Employerservice.GetPreviousSearchs(page, Sort, descending, name, Fromdate, Todate, users.Id));
        }

        /// <summary>
        /// Search profiles
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("searchprofiles")]
        public async Task<ActionResult> Registerfilter(Filterparameters model)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.Registerfilter(model, users.Id));
        }

        /// <summary>
        /// Register Employeer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("registeremployeer")]
        public async Task<IActionResult> RegisterEmployeer(EmployerRegisterModel model)
        {
            try
            {
                var userExists = await userManager.FindByNameAsync(model.Email);
                ApplicationUser user = new ApplicationUser()
                {
                    Email = model.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.Email,
                    IsActive = true,
                    PhoneNumber = model.Mobile
                };
                string Password = CommonHelper.Randompassword().ToString();
                var result = await userManager.CreateAsync(user, Password);
                if (!result.Succeeded)
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });

                return Ok(Employerservice.RegisterEmployeer(model, user.Id, Password));

            }
            catch (Exception Ex)
            {
                return Ok(new Response() { Iserror = true, errors = new List<string>() { Ex.Message + " " + Ex.Message }, StatusCode = StatusCodes.Status500InternalServerError });

            }
        }



        /// <summary>
        /// Get Employeer Profile
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getemployeerprofile")]
        public async Task<ActionResult> GetEmployeerProfile()
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.GetEmployeerProfile(users.Id));
        }


        /// <summary>
        /// Get Empyolyeer
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="location"></param>
        /// <param name="EmployeerId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getempyolyeer")]
        public async Task<ActionResult> GetEmpyolyeer([FromQuery] PageData page, string Sort, bool descending, string name, string email, string mobile, string location, int EmployeerId, int status)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(await Employerservice.GetEmpyolyeer(page, Sort, descending, name, email, mobile, location, EmployeerId, status));
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("GetUserToekn")]
        public async Task<ActionResult> GetUserToekn(string userId)
        {
            var aspnetuser = Employerservice.Getuser(userId);
            var user = await userManager.FindByNameAsync(aspnetuser.UserName);


            var userRoles = await userManager.GetRolesAsync(user);

            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };


            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);

            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Customer"));
            //foreach (var userRole in userRoles)
            //{
            //    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            //}
            await signInManager.SignInAsync(user, isPersistent: false);

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: configuration["JWT:ValidIssuer"],
                audience: configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(27),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo,
                profilcheck = Authservice.IsEmployeerprofileupdated(user.Id)

            });
        }

        
        [HttpPost]
        [Route("getUserToeknwithoutuser")]
        public async Task<ActionResult> GetUserToeknwithoutuser()
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);           
            var user = await userManager.FindByNameAsync(users.Email);
            var userRoles = await userManager.GetRolesAsync(user);

            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };


            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);

            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Customer"));
            //foreach (var userRole in userRoles)
            //{
            //    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            //}
            await signInManager.SignInAsync(user, isPersistent: false);

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: configuration["JWT:ValidIssuer"],
                audience: configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(27),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo,
                profilcheck = Authservice.IsEmployeerprofileupdated(user.Id)

            });
        }


        /// <summary>
        /// Get Employer List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="organizationtype"></param>
        /// <param name="organization"></param>
        /// <param name="fname"></param>
        /// <param name="lname"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="currentplan"></param>
        /// <param name="validity"></param>
        /// <param name="createdbyname"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getemployerlist")]
        public ActionResult GetEmployerlist([FromQuery] PageData page, string Sort, bool descending, string organizationtype, string organization, string fname, string lname, string email, string mobile, string currentplan, int validity, string createdbyname)
        {
            return Ok(Employerservice.GetEmployerlist(page, Sort, descending, organizationtype, organization, fname, lname, email, mobile, currentplan, validity, createdbyname));
        }

        /// <summary>
        /// Get Employeer Id by User Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getemployeeridbyuserid")]
        public async Task<ActionResult> GetEmployeerIdbyUserId()
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.GetEmployeerIdbyUserId(users.Id));
        }


        /// <summary>
        /// Get Employeer Transactions
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="Fromdate"></param>
        /// <param name="Todate"></param>
        /// <param name="EmployeerId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getemployeertransactions")]
        public ActionResult GetEmployeerTransactions([FromQuery] PageData page, string Sort, bool descending, string name, DateTime Fromdate, DateTime Todate, string EmployeerId)
        {
            return Ok(Employerservice.GetEmployeerTransactions(page, Sort, descending, name, Fromdate, Todate, EmployeerId));
        }

        /// <summary>
        /// Get Payment Invoice
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getpaymentinvoice")]
        public ActionResult GetPaymentInvoive(string transactionId)
        {
            var response = Employerservice.GetPaymentInvoive(transactionId);
            return File(response.stream, "application/pdf", "Invoice" + transactionId);
        }

        /// <summary>
        /// Get EmployerId by Mobile
        /// </summary>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getemployeridbymobile")]
        public ActionResult GetemployerIDbyMobile(string Mobile)
        {
            return Ok(Employerservice.GetemployerIDbyMobile(Mobile));
        }

        /// <summary>
        /// get ACtivity Log
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <param name="employeerId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getactivitylog")]
        public async Task<ActionResult> GetActivitylog([FromQuery] PageData page, string Sort, bool descending, string name, DateTime fromdate, DateTime todate, int employeerId)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.GetActivitylog(page, Sort, descending, name, fromdate, todate, employeerId,users.Id));
        }




        /// <summary>
        /// Export Users Excel
        /// </summary>
        /// <param name="Jobseekrers"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("exportusersexcel")]
        public async Task<ActionResult> ExportUsersExcel(List<Selectionvalues> Jobseekers)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var data = await Employerservice.ExportUsersExcel(Jobseekers.Select(x=>x.Name).ToList(),users.Id);
            return File(data, ImageFormat.excel, "Jobseekers.xlsx");
        }


        /// <summary>
        /// Check Limit Before Export
        /// </summary>
        /// <param name="Jobseekrers"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("checklimitbeforeexport")]
        public async Task<ActionResult> CheckLimitBeforeExport(List<Selectionvalues> Jobseekers)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
          return Ok(Employerservice.CheckLimitBeforeExport(Jobseekers.Select(x=>x.Name).ToList(), users.Id));

        }



        /// <summary>
        /// Export Users Resume
        /// </summary>
        /// <param name="Jobseekers"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("exportusersresume")]
        public async Task<IActionResult> ExportUsersResume(List<Selectionvalues> Jobseekers)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            const string contentType = "application/zip";
            HttpContext.Response.ContentType = contentType;
            var file = await Employerservice.ExportUsersResume(Jobseekers, users.Id);
            var result = new FileContentResult(System.IO.File.ReadAllBytes(file.path), contentType)
            {
                FileDownloadName = $"" + file.filename + "+"
            };

            
            Directory.Delete(file.filepath1, true);
            if (System.IO.File.Exists(file.path))
            {
                System.IO.File.Delete(file.path);
            }
            return result;
        }


        /// <summary>
        /// Upload
        /// </summary>
        /// <param name="file"></param>
        /// <param name="MJobseekr"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateemployeerprofilepic")]
        public async Task<ActionResult> UpdateEmployeerProfilepic(IFormFile file)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(await Employerservice.UpdateEmployeerProfilepic(users.Id,file));
        }

        /// <summary>
        /// Get Jobseeker contact Details
        /// </summary>
        /// <param name="JobseekerId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getjobseekercontactdetails")]
        public async Task<ActionResult> GetJobseekerContactDetails(string JobseekerId)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(await Employerservice.GetJobseekerContactDetails(JobseekerId, users.Id));
        }

        [HttpPost]
        [Route("addemployertransactions")]
        public ActionResult AddEmployerTransactions(MTransactionsmodel model)
        {
            return Ok(Employerservice.AddEmployerTransactions(model));
        }

        /// <summary>
        /// Get Employeer viewed Profiles
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getemployeerviewdprofiles")]
        public async Task<ActionResult> GetEmployeerviewdProfiles([FromQuery] PageData page, string Sort, bool descending, string name, string email, string mobile, DateTime fromdate, DateTime todate)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.GetEmployeerviewdProfiles( page,  Sort,  descending,  name,  email,  mobile,  fromdate,  todate, users.Id));
        }

        /// <summary>
        /// Get Employeer viewd Profiles
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getemployeerviewdprofilesstats")]
        public async Task<ActionResult> GetEmployeerviewdProfiles()
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.GetEmployeerviewdProfiles(users.Id));
        }

        /// <summary>
        /// Get Fifth Plan Details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfifthplandetails")]
        public ActionResult GetFifthplandetails(EmployerFifthPlan model)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Path.Combine("Content", "Html", "Register/Fifth Plan Details.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Content}", model.content).Replace("{content}", model.content)
                .Replace("{mail}", configuration.GetValue<string>("ApplicationSettings:")).Replace("{content}", model.content)
                ;

            return Ok(new Response { Status = "Success", Message = SucessMessages.ScuessLoad, StatusCode = (StatusCodes.Status201Created) });
            //return Ok(Employerservice.GetFifthplandetails(model));
        }


        /// <summary>
        /// Filter Employeer
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="organization"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("filteremployeer")]
        public async Task<ActionResult> FilterEmployeer([FromQuery] string name, string email, string mobile, string organization)
        {
            return Ok(Employerservice.FilterEmployeer( name, email, mobile, organization));
        }

        /// <summary>
        /// Add Cmapign for Employeer
        /// </summary>
        /// <param name="model"></param>
        /// <param name="EmployeerId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addcmapignforemployeer")]
        public async Task<ActionResult> AddCmapignforEmployeer(MCampaignmaster model, string EmployeerId)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.AddCmapignforEmployeer( model, users.Id,EmployeerId));
        }

        /// <summary>
        /// Get Campign transactions
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getcampigtransactions")]
        public async Task<ActionResult> GetCampigtransactions([FromQuery] PageData page, string Sort, bool descending, string name, string email, string mobile, DateTime fromdate, DateTime todate)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.GetCampigtransactions(page, Sort, descending, name, email, mobile, fromdate, todate, users.Id));
        }

        /// <summary>
        /// Get Campign Registered Aspirants
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <param name="campaignid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getcampignregisteredaspirants")]
        public async Task<ActionResult> GetCampignRegisteredAspirants([FromQuery] PageData page, string Sort, bool descending, string name, string email, string mobile, DateTime fromdate, DateTime todate,string campaignid)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employerservice.GetCampignRegisteredAspirants(page, Sort, descending, name, email, mobile, fromdate, todate, users.Id, campaignid));
        }
        #endregion


    }
}
