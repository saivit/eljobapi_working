﻿using ELJob.Business.BL.Service;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Lookup;
using ELJob.Domain.Model.Screens;
using ELJob.Helper.Grid;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoundationController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IFoundationBL Foundationservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        #region Construtor
        public FoundationController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration, IFoundationBL FoundationBlservice)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Foundationservice = FoundationBlservice;
        }

        #endregion

        #region API Method
        /// <summary>
        /// Add Screen
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addscreen")]
        public ActionResult AddScreen(Screen model)
        {
            return Ok(Foundationservice.AddScreen(model,0));
        }

        /// <summary>
        /// Update Screen
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatescreen")]
        public ActionResult UpdateScreen(Screen model)
        {
            return Ok(Foundationservice.UpdateScreen(model,0));
        }

        /// <summary>
        /// Remove Screen
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removescreen")]
       public ActionResult RemoveScreen(int Id)
        {
            return Ok(Foundationservice.RemoveScreen(Id,0));
        }

        /// <summary>
        /// Add SubScreen
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addsubscreen")]
        public ActionResult AddSubScreen(SubScreen model)
        {
            return Ok(Foundationservice.AddSubScreen(model,0));
        }

        /// <summary>
        /// Update SubScreen
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatesubscreen")]
        public ActionResult UpdateSubScreen(SubScreen model)
        {
            return Ok(Foundationservice.UpdateSubScreen(model,0));
        }


        /// <summary>
        /// Remove SubScren
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removesubscreen")]
        public ActionResult RemoveSubScreen(int Id)
        {
            return Ok(Foundationservice.RemoveSubScreen(Id,0));
        }


        /// <summary>
        /// Add State
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addstate")]
        public ActionResult AddState(Lookupstate model)
        {
            return Ok(Foundationservice.AddState(model,0));
        }


        /// <summary>
        /// Update State
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatestate")]
        public ActionResult UpdateState(Lookupstate model)
        {
            return Ok(Foundationservice.UpdateState(model,0));
        }

        /// <summary>
        /// Remove State
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removestate")]
        public ActionResult RemoveState(int Id)
        {
            return Ok(Foundationservice.RemoveState(Id, 0));
        }

        /// <summary>
        /// Add City
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addcity")]
        public ActionResult AddCity(LookupCity model)
        {
            return Ok(Foundationservice.AddCity(model,0));
        }

        /// <summary>
        /// Update City
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatecity")]
        public ActionResult UpdateCity(LookupCity model)
        {
            return Ok(Foundationservice.UpdateCity(model,0));
        }

        /// <summary>
        /// Remove City
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removecity")]
        public ActionResult RemoveCity(int Id)
        {
            return Ok(Foundationservice.RemoveCity(Id, 0));
        }


        /// <summary>
        /// Add Sector
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addsector")]
        public ActionResult AddSector(LookupSector model)
        {
            return Ok(Foundationservice.AddSector(model, 0));
        }


        /// <summary>
        /// Update Sector
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatesector")]
        public ActionResult UpdateSector(LookupSector model)
        {
            return Ok(Foundationservice.UpdateSector(model, 0));
        }

        /// <summary>
        /// Remove Sector
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removesector")]
        public ActionResult RemoveSector(int Id)
        {
            return Ok(Foundationservice.RemoveSector(Id, 0));
        }


        /// <summary>
        /// Add objective
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addobjective")]
        public ActionResult AddObjective(LookupObjective model)
        {
            return Ok(Foundationservice.AddObjective(model, 0));
        }

        /// <summary>
        /// Update Objective
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateobjective")]
        public ActionResult UpdateObjective(LookupObjective model)
        {
            return Ok(Foundationservice.UpdateObjective(model, 0));
        }

        /// <summary>
        /// Remove Objective
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeobjective")]
        public ActionResult RemoveObjective(int Id)
        {
            return Ok(Foundationservice.RemoveObjective(Id, 0));
        }

        /// <summary>
        /// Add Plan
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addplan")]
        public ActionResult Addplan(LookupPlans model)
        {
            return Ok(Foundationservice.AddPlan(model, 0));
        }

        /// <summary>
        /// Update Plan
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateplan")]
        public ActionResult UpdatePlan(LookupPlans model)
        {
            return Ok(Foundationservice.UpdatePlan(model, 0));    
        }

        /// <summary>
        /// Remove Plan
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeplan")]
        public ActionResult RemovePlan(LookupPlans model)
        {
            return Ok(Foundationservice.RemovePlan(model, 0));

        }

        /// <summary>
        /// Add ScreenAssign
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addscreenassign")]
        public ActionResult AddScreenAssign(ScreenAssign model)
        {
            return Ok(Foundationservice.AddScreenAssign(model, 0));
        }

        /// <summary>
        /// Update AssignScreen
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatescreenassign")]
        public ActionResult UpdateScreenAssign(ScreenAssign model)
        {
            return Ok(Foundationservice.UpdateScreenAssign(model, 0));

        }

        /// <summary>
        /// Remove ScreenAssign
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removescreenassign")]
        public ActionResult RemoveScreenAssign(int Id)
        {
            return Ok(Foundationservice.RemoveScreenAssign(Id, 0));
        }

        /// <summary>
        /// Add Education
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addeducation")]
        public ActionResult AddEducation(LookupEducation model)
        {
            return Ok(Foundationservice.AddEducation(model, 0));
        }

        /// <summary>
        /// Update Education
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateeducation")]
        public ActionResult UpdateEducation(LookupEducation model)
        {
            return Ok(Foundationservice.UpdateEducation(model, 0));
        }

        /// <summary>
        /// Remove Education
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeeducation")]
        public ActionResult RemoveEducation(int Id)
        {
            return Ok(Foundationservice.RemoveEducation(Id, 0));
        }


        /// <summary>
        /// Add OtherEducation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addothereducation")]
        public ActionResult AddOtherEducation(LookupOtherEducation model)
        {
            return Ok(Foundationservice.AddOtherEducation(model, 0));
        }


        /// <summary>
        /// Update OtherEducation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateothereducation")]
        public ActionResult UpdateOtherEducation(LookupOtherEducation model)
        {
            return Ok(Foundationservice.UpdateOtherEducation(model, 0));
        }


        /// <summary>
        /// Remove OtherEducation
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeothereducation")]
        public ActionResult RemoveOtherEducation(int Id)
        {
            return Ok(Foundationservice.RemoveOtherEducation(Id, 0));
        }

        /// <summary>
        /// Add Status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addstatus")]
        public ActionResult AddStatus(LookupStatus model)
        {
            return Ok(Foundationservice.AddStatus(model,0));

        }

        /// <summary>
        /// Update Status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatestatus")]
        public ActionResult UpdateStatus(LookupStatus model)
        {
            return Ok(Foundationservice.UpdateStatus(model, 0));
        }

        /// <summary>
        /// Remove Status
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removestatus")]
        public ActionResult RemoveStatus(int Id)
        {
            return Ok(Foundationservice.RemoveStatus(Id, 0));
        }

        /// <summary>
        /// Add StatusMapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addstatusmapping")]
        public ActionResult AddStautusMapping(vw_StatusMapping model)
        {
            return Ok(Foundationservice.AddSatatusMapping(model, 0));
        }
       

        /// <summary>
        /// Update StatusMapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatestatusmapping")]
        public ActionResult UpdateStatusMapping(vw_StatusMapping model)
        {
            return Ok(Foundationservice.UpdateStatusMapping(model, 0));
        }

        /// <summary>
        /// Remove StatusMapping
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removestatusmapping")]
        public ActionResult RemoveStatusMapping(int Id)
        {
            return Ok(Foundationservice.RemoveStatusMapping(Id, 0));
        }


        /// <summary>
        /// Get State List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="cgst"></param>
        /// <param name="igst"></param>
        /// <param name="sgst"></param>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getstatelist")]
        public async Task<IActionResult> GetStateList([FromQuery] PageData page, string Sort, bool descending, string name, string code, decimal cgst, decimal igst, decimal sgst)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetStateList(page, Sort, descending, name,code,cgst,igst,sgst,1));
        }

        /// <summary>
        /// Get City List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="Stateid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getcitylist")]
        public async Task<IActionResult> GetCityList([FromQuery] PageData page, string Sort, bool descending, string name, int Stateid)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetCityList(page, Sort, descending, name, Stateid,1));
        }

        /// <summary>
        /// Get Sector List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsectorlist")]
        public async Task<IActionResult> GetSectorList([FromQuery] PageData page, string Sort, bool descending, string name)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetSctorList(page, Sort, descending,name,1));
        }

        /// <summary>
        /// Get Status List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getstatuslist")]
        public async Task<ActionResult>GetStatusList([FromQuery] PageData page, string Sort, bool descending, string name)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetStatusList(page, Sort, descending, name, 1));
        }

        /// <summary>
        /// Get StatusMapping List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="StatusType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getstatusmappinglist")]
        public async Task<ActionResult> GetStatusMappingList([FromQuery] PageData page, string Sort, bool descending, string name, string StatusType)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetStatusMappingList(page, Sort, descending, name,StatusType, 1));
        }

        /// <summary>
        /// Get Plan List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="ValidityModel"></param>
        /// <param name="ProfileViewsnumber"></param>
        /// <param name="TotalCVdownload"></param>
        /// <param name="Price"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getplanlist")]
        public async Task<ActionResult>GetPlanList([FromQuery] PageData page, string Sort, bool descending, string name, string ValidityModel, int ProfileViewsnumber, int TotalCVdownload, string Price)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetPlanList(page, Sort, descending,name,ValidityModel,Price,ProfileViewsnumber,TotalCVdownload, 1));
        }


        /// <summary>
        /// Get Objective List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getObjectiveList")]
        public async Task<ActionResult> GetObjetiveList([FromQuery] PageData page, string Sort, bool descending, string name)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetObjectiveList(page, Sort, descending, name,1));
        }


        /// <summary>
        /// Get Screen List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getscreenList")]
        public async Task<ActionResult> GetScreenList([FromQuery] PageData page, string Sort, bool descending, string name)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetScreenList(page, Sort, descending, name, 1));
        }

        /// <summary>
        /// Get SubScrenn List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="SubScreen"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsubscreenList")]
        public async Task<ActionResult> GetSubScreenList([FromQuery] PageData page, string Sort, bool descending, string name, string SubScreen)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetSubScreenList(page, Sort, descending, name, SubScreen, 1));
        }


        /// <summary>
        /// Get Education List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("geteducationlist")]
        public async Task<ActionResult>GetEducationList([FromQuery]PageData page, string Sort, bool descending, string name)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetEducationList(page, Sort, descending, name, 1));
        }

        /// <summary>
        /// Get OtherEduication List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getothereducation")]
        public async Task<ActionResult>GetOtherEducationList([FromQuery]PageData page, string Sort, bool descending,string name)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetOtherEducationList(page, Sort, descending, name, 1));

        }


        /// <summary>
        /// Get GetassignScreenToRole List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="role"></param>
        /// <param name="screen"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getassignscreentorole")]
        public async Task<ActionResult> GetAssignScreenToRoleList([FromQuery]PageData page, string Sort, bool descending, string role, string screen)
        {
            //var users = userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetAssignScreenToRole(page, Sort, descending, role, screen, 1));
        }


        /// <summary>
        /// Get PlanHistoryList
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="ValidityinDays"></param>
        /// <param name="Price"></param>
        /// <param name="PriceIncludeGST"></param>
        /// <param name="ProfileViewsnumber"></param>
        /// <param name="TotalCVdownload"></param>
        /// <param name="IsUnlimited"></param>
        /// <param name="SMSandEmail"></param>
        /// <param name="Createdat"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getplanhistorylist")]
        public async Task<ActionResult>GetPlanHistoryList([FromQuery] PageData page, string Sort, bool descending, string name, string ValidityinDays, string Price, decimal PriceIncludeGST, int ProfileViewsnumber, int TotalCVdownload, bool IsUnlimited, string SMSandEmail, DateTime Createdat)
        {
            var users = userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetPlanHistoryList(page, Sort, descending, name, ValidityinDays, Price, PriceIncludeGST, ProfileViewsnumber, TotalCVdownload, IsUnlimited, SMSandEmail, Createdat, 1));
        }
        
        /// <summary>
        /// Assign ScreentoRole
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("assignscreentorole")]
        public async Task<ActionResult>AssignScreentoRole(int Id)
        {
            return Ok(Foundationservice.AssignScreentoRole(Id));
        }


        /// <summary>
        /// Screen List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("screenlist")]
        public ActionResult ScreenList()
        {
            return Ok(Foundationservice.ScreenList());
        }


        [HttpGet]
        [Route("getscreenbyid")]
        public ActionResult GetScreenbyId(int Id)
        {
            return Ok(Foundationservice.GetScreenbyId(Id));
        }


        /// <summary>
        /// Get Zone List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getzonelist")]
        public async Task<ActionResult> GetZonelist([FromQuery] PageData page, string Sort, bool descending, string name)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetZonelist(page,Sort,descending,name,1));

        }


        /// <summary>
        /// Get PlanbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getplanbyid")]
        public ActionResult GetPlanbyId(int Id)
        {
            return Ok(Foundationservice.GetPlanbyId(Id));
        }


        /// <summary>
        /// Get Location List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="Zone"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getlocationlist")]
        public async Task<ActionResult> GetLocationlist([FromQuery] PageData page, string Sort, bool descending, string name,string Zone)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Foundationservice.GetLocationlist(page, Sort, descending, name, Zone, 1));

        }

        /// <summary>
        /// Get EducationbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("geteducationbyid")]

        public ActionResult GeteducationbyId(int Id)
        {
            return Ok(Foundationservice.GeteducationbyId(Id));
        }


        [HttpGet]
        [Route("getstatebyid")]

        public IActionResult GetStatebyId(int Id)
        {
            return Ok(Foundationservice.GetStatebyId(Id));

        }

        /// <summary>
        /// Get SectorById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsectorbyid")]
        public IActionResult GetSectorById(int Id)
        {
            return Ok(Foundationservice.GetSectorById(Id));
        }


        /// <summary>
        /// Get ObjectivebyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getobjectivebyid")]
        public IActionResult GetObjectiveById(int Id)
        {
            return Ok(Foundationservice.GetObjectiveById(Id));
        }


        /// <summary>
        /// Get SubScreenById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsubscreenbyid")]
        public IActionResult GetSubScreenById(int Id)
        {
            return Ok(Foundationservice.GetSubScreenById(Id));
        }

        /// <summary>
        /// Get Education
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("geteducation")]
        public IActionResult GetEducation()

        {
            return Ok(Foundationservice.GetEducation());
        }


        /// <summary>
        /// Add Language
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addlanguage")]
        public IActionResult AddLanguage(LookupLanguage model)

        {
            return Ok(Foundationservice.AddLanguage(model));
        }

        /// <summary>
        /// Update Language
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatelanguage")]
        public IActionResult UpdateLanguage(LookupLanguage model)

        {
            return Ok(Foundationservice.UpdateLanguage(model));
        }

        /// <summary>
        /// Remove Language
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removelanguage")]
        public IActionResult RemoveLanguage(int Id)

        {
            return Ok(Foundationservice.RemoveLanguage(Id));
        }

        /// <summary>
        /// Get LanguagebyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getlanguagebyid")]
        public IActionResult GetLanguagebyId(int Id)

        {
            return Ok(Foundationservice.GetLanguagebyId(Id));
        }


        /// <summary>
        /// Get LanguageList
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("languagelist")]
        public IActionResult LanguageList(PageData page, string Sort, bool descending, string name)

        {
            return Ok(Foundationservice.LanguageList(page,Sort,descending,name));
        }

        /// <summary>
        /// Get Languages
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getlanguage")]
        public IActionResult GetLanguages()

        {
            return Ok(Foundationservice.GetLanguages());
        }

        /// <summary>
        /// Get Objective
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getobjective")]
        public IActionResult GetObjective()

        {
            return Ok(Foundationservice.GetObjective());
        }

        /// <summary>
        /// Get Cities
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getcities")]
        public IActionResult GetCities()

        {
            return Ok(Foundationservice.GetCities());
        }

        /// <summary>
        /// Get States
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getgtates")]
        public IActionResult GetStates()

        {
            return Ok(Foundationservice.GetStates());
        }

        /// <summary>
        /// Get Sectors
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getsectors")]
        public IActionResult GetSectors()

        {
            return Ok(Foundationservice.GetSectors());
        }
        #endregion
    }
}
