﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel.Stats;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using ELJob.Helper.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[JWTAuthorize]
    public class StatsController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IStatsBL Statsservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        /// <summary>
        /// Object Intilizing
        /// </summary>
        /// <param name="context"></param>
        /// <param name="userManager"></param>
        /// <param name="_configuration"></param>
        /// <param name="StatsBlservice"></param>
        #region Construtor
        public StatsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration, IStatsBL _StatsBlservice)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Statsservice = _StatsBlservice;
        }

        #endregion

        #region API
        /// <summary>
        /// Get Job seeker Stats
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("jobseekerstats")]
        public ActionResult Jobseekerstats(StatasModel model)
        {
            return Ok(Statsservice.Jobseekerstats(model));
        }

        [HttpPost]
        [Route("employeerstats")]
        public ActionResult EmployeerStats(StatasModel model)
        {
            return Ok(Statsservice.EmployeerStats(model));
        }
        #endregion

    }
}
