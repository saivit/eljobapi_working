﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Aspirant;
using ELJob.Core.Context;
using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Authorization;
using ELJob.Helper.Common;
using ELJob.Helper.Communication;
using ELJob.Helper.Grid;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static ELJob.Helper.Common.Constants;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController] 
    //[JWTAuthorize]
    public class JobseekerController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IAspirantBL Aspirantservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        #region Construtor
        public JobseekerController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration, IAspirantBL AspirantBLservice)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Aspirantservice = AspirantBLservice;
        }

        #endregion

        #region API Method


        /// <summary>
        /// Aspirant Quick Registration
        /// </summary>
        /// <param name="model">
        /// Gender 4	Male 5	Female
        ///  state 1
        /// </param>
        /// <returns></returns>
        [HttpPost]
        [Route("aspirantinfo")]
        [AllowAnonymous]
        public async Task< ActionResult> AspirantQuickRegistraion(AspirantInfo model)
        {
            try
            {              
              return Ok(await Aspirantservice.AspirantQuickRegistraion(model));
            }
            catch (Exception Ex)
            {
                return Ok(new Response() { Iserror = true, errors = new List<string>() { Ex.Message + " " + Ex.Message }, StatusCode = StatusCodes.Status500InternalServerError });
            }
        }

        /// <summary>
        /// Get Jobseeker
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="location"></param>
        /// <param name="jobseekerId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getjoseeker")]
        public async Task<ActionResult> GetJobseeker([FromQuery] PageData page, string Sort, bool descending, string name, string email, string mobile,int location, int zone, int jobseekerId, int status, int profiledatastatus, int Gender, int sourceofLead,DateTime fromdate, DateTime todate)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(await Aspirantservice.GetJobseeker(page, Sort, descending,name,email,mobile,location, zone,jobseekerId,status,profiledatastatus,Gender,sourceofLead,fromdate,todate));
        }


        /// <summary>
        /// Add Aspirant Education
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("addaspiranteducation")]
        public ActionResult AddAspirantEducation(MAspirantEducation model)
        {
            return Ok(Aspirantservice.AddAspirantEducation(model));
            
        }


        /// <summary>
        /// Update Aspirant Education
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspiranteducation")]
        public ActionResult UpdayteAspirantEducation(MAspirantEducation model,string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantEducation(model,Id));
        }


        /// <summary>
        /// Remove Aspirant Education
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspiranteducation")]
        public ActionResult RemoveAspirantEducation(string Id)
        {
            return Ok(Aspirantservice.RemoveAspirantEducation(Id));
        }


        /// <summary>
        /// Get ApirantEduList
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantedulist")]
        public ActionResult GetAspirantEduList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantEduList(Id));
        }


        /// <summary>
        /// Add AspirantExperience
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantexperience")]
        public ActionResult AddAspirantExperience(MAspirantExperience model)
        {
            return Ok(Aspirantservice.AddAspirantExperience(model));
        }


        /// <summary>
        /// Update Aspirant Experience
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspirantexperience")]
        public ActionResult UpdateAspirantExperience(MAspirantExperience model,string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantExperience(model,Id));
        }


        /// <summary>
        /// Remove Aspirant Experience
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantexperience")]
        public ActionResult RemoveAspirantEXperience(string Id)
        {
            return Ok(Aspirantservice.RemoveAspirantExperience(Id));
        }

        /// <summary>
        /// Add Aspirant Personal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantpersonal")]
        public ActionResult AddAspirentPersonal(MAspirantPersonal model)
        {
            return Ok(Aspirantservice.AddAspirantPersonal(model));
        }

        /// <summary>
        /// Update Aspirant Personal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspirantpersonal")]
        public ActionResult UpdateAspirantPersonal(MAspirantPersonal model)
        {
            return Ok(Aspirantservice.UpdateAspirantPersonal(model));
        }

        /// <summary>
        /// Remove Aspirant Personal
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantpersonal")]
        public ActionResult RemoveAspirantPersonal(int Id)
        {
            return Ok(Aspirantservice.RemoveAspirantPersonal(Id));
        }

        /// <summary>
        /// Add Aspirant SkillAssessment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantskillassessment")]
        public ActionResult AddSkillAssessment(MAspirantSkillAssessment model)
        {
            return Ok(Aspirantservice.AddSkillAssessment(model));
        }

        /// <summary>
        /// Update Aspirant Skill Assessment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspirantskillassessment")]
        public ActionResult UpdateSkillAssessment(MAspirantSkillAssessment model)
        {
            return Ok(Aspirantservice.UpdateSkillAssessment(model));
        }


        /// <summary>
        /// Remove Aspirant Skill Assessment
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantskillassessment")]
        public ActionResult RemoveAspirantSkillAssessment(int Id)
        {
            return Ok(Aspirantservice.RemoveAspirantSkillAssement(Id));
        }

        /// <summary>
        /// Get the aspirant Details by Id
        /// </summary>
        /// <param name="AspirantId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantbyid")]
        public ActionResult GetAspirantbyId(string AspirantId)
        {
            return Ok(Aspirantservice.GetAspirantbyId(AspirantId));
        }


        /// <summary>
        /// Reschdule the Aspirant slot
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("reschduleledslot")]
        public async Task<ActionResult> Reschduleledslot(AspirantReschdule model)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            return Ok(Aspirantservice.Reschduleledslot(model, users.Id));
        }

        /// <summary>
        /// Reject Aspirant
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("rejectaspirant")]
        public async Task<ActionResult> RejectAspirant(AspirantRejectionReason model)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            return Ok(Aspirantservice.RejectAspirant(model, users.Id));
        }

        /// <summary>
        /// Proceed to counciling
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="sourceoflead"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("proceedtocounclling")]
        public async Task<ActionResult> ProceedTocounclling(string Id, int sourceoflead)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            return Ok(Aspirantservice.ProceedTocounclling(Id, users.Id, sourceoflead));
        }


        /// <summary>
        /// Get AspirantExpbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantexpbyid")]
        public ActionResult getaspirantexpbyid(int Id)
        {
            return Ok(Aspirantservice.GetAspirantExpbyId(Id));
        }


        /// <summary>
        /// Add AspirantCertification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantcertification")]
        public ActionResult AddAspirantCertification(MAspirantCertification model)
        {
            return Ok(Aspirantservice.AddAspirantCertification(model));
        }

        /// <summary>
        /// Update AspirantCertification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateaspirantcertification")]
        public ActionResult UpdateAspirantCertification(MAspirantCertification model,string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantCertification(model,Id));
        }

        /// <summary>
        /// Remove AspirantCertification
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
         [HttpDelete]
         [Route("removeaspirantcretification")]
         public ActionResult RemoveAspirantCertification(string Id)
        {
            return Ok(Aspirantservice.RemoveAspirantCertification(Id));
        }

        /// <summary>
        /// Get AspirantExperienceList
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantexperience")]
        public ActionResult GetAspirantExpList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantExpList(Id));
        }


        /// <summary>
        /// Get AspirantCertificationList
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantcertificationList")]
        public ActionResult GetAspirantCertificationList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantCertificationList(Id));
        }



        /// <summary>
        /// Get Aspirant SkillAssessment List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantskillassessmentList")]
        public ActionResult GetAspirantSkillassessmentList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantSkillList(Id));
        }

        /// <summary>
        /// Get JobseekerbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getjobseekerbyid")]
        public ActionResult GetJobseekerById(string Id)
        {
            return Ok(Aspirantservice.GetJobseekerbyId(Id));
        }

        /// <summary>
        /// Add Aspirant Languages
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("addaspirantlanguages")]
        public ActionResult AddAspirantLanguages(MJobseekerLanguge model)
        {
            return Ok(Aspirantservice.AddAspirantLanguages(model));
        }


        /// <summary>
        /// Update Aspirant Languages
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspirantlanguages")]
        public ActionResult UpdateAspirantLanguages(MJobseekerLanguge model, string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantLanguages(model, Id));
        }


        /// <summary>
        /// Remove Aspirant Languages
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantlanguages")]
        public ActionResult RemoveAspirantLanguages(string Id)
        {
            return Ok(Aspirantservice.RemoveAspirantLanguages(Id));
        }


        /// <summary>
        /// Get Aspirant Languages
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantlanguages")]
        public ActionResult GetAspirantLanguages(string Id)
        {
            return Ok(Aspirantservice.GetAspirantLanguages(Id));
        }

        /// <summary>
        /// Get AspirantEducationbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspiranteducationbyid")]
        public ActionResult GetAspirantEducationById(int Id)
        {
            return Ok(Aspirantservice.GetAspirantEducationById(Id));
        }


        /// <summary>
        /// Get AspirantCertificationbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantcerticationbyid")]
        public ActionResult GetAspirantcerificationById(int Id)
        {
            return Ok(Aspirantservice.GetAspirantcerificationById(Id));
        }


        /// <summary>
        /// Get AspirantSkillbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantskillbyid")]
        public ActionResult GetAspirantSkillbyId(string Id)
        {
            return Ok(Aspirantservice.GetAspirantSkillbyId(Id));
        }


        /// <summary>
        /// Get AspirantPersonalbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantpersonalbyid")]
        public ActionResult GetAspirantPersonalbyId(int Id)
        {
            return Ok(Aspirantservice.GetAspirantPersonalbyId(Id));
        }


        /// <summary>
        /// Add Aspirant Objective
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantobjective")]
        public ActionResult AddAspirantObjective(MAspirantObjective model)
        {
            return Ok(Aspirantservice.AddAspirantObjective(model));
        }


        /// <summary>
        /// Update Aspirant Objective
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateaspirantobjective")]
        public ActionResult UpdateAspirantObjective(MAspirantObjective model, string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantObjective(model,Id));
        }

        /// <summary>
        /// Get Aspirant objective ById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantobjectivebyid")]
        public ActionResult GetAspirantObjectivebyId(string Id)
        {
            return Ok(Aspirantservice.GetAspirantObjectivebyId(Id));
        }


        /// <summary>
        /// Get Jobseekeer Documents
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="documentType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getjobseekeerdocuments")]
        public ActionResult GetJobseekeerDocuments(string Id,string documentType)
        {
            return Ok(Aspirantservice.GetJobseekeerDocuments(Id,documentType));
        }


        /// <summary>
        /// Upload
        /// </summary>
        /// <param name="file"></param>
        /// <param name="MJobseekr"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("upload")]
        public async Task<ActionResult> Upload(IFormFile file, string MJobseekr,string DocumentType)
        {
            return Ok(await Aspirantservice.Upload(file, MJobseekr, DocumentType));
        }

        /// <summary>
        /// Get Aspirant Personal List
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantpersonallist")]
        public ActionResult GetAspirantPersonalList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantPersonalList(Id));
        }

        /// <summary>
        /// Download Resouces
        /// </summary>
        /// <param name="ResourceId"></param>
        /// <returns></returns>

        [HttpGet]
        [Route("downloadjobseekerdocuments")]
        public async Task<IActionResult> DownloadJobseekerDocuments(string ResourceId)
        {
            var response = await Aspirantservice.DownloadJobseekerDocuments(ResourceId);
            return File(response.stream, response.filetype, response.Filename);
        }


        /// <summary>
        /// Update Profile top Fields
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateprofiletopfields")]
        public ActionResult UpdateProfileTopFields(Updateprofiletopfields model, string Id)
        {
            return Ok(Aspirantservice.UpdateProfileTopFields(model, Id));
        }


        /// <summary>
        /// Complete Profile
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("completeprofile")]
        public ActionResult CompleteProfile(string Id)
        {
            return Ok(Aspirantservice.CompleteProfile(Id));
        }

        /// <summary>
        /// Yet To Submit documents
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("yettosubmitdocuments")]
        public ActionResult YetToSubmitdocuments(string Id)
        {
            return Ok(Aspirantservice.YetToSubmitdocuments(Id));
        }


        /// <summary>
        /// Aspirant Email Verfication
        /// </summary>
        /// <param name="Id-JobseekerId">Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("emailverfication")]
        public async Task<ActionResult> Emailverfication(string Id)
        {
            return Ok(await Aspirantservice.Emailverfication(Id)); 
        }

        /// <summary>
        /// Get AspirantID by Mobile
        /// </summary>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getaspirantmobilebyid")]
        public ActionResult GetaspinrantIDbyMobile(string Mobile)
        {
            return Ok(Aspirantservice.GetaspinrantIDbyMobile(Mobile));
        }

        /// <summary>
        /// Export Resume
        /// </summary>
        /// <param name="JobseekerId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("exportresume")]
        public async Task<ActionResult> ExportResume(string JobseekerId,bool mailsent)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var response=await Aspirantservice.ExportResume(JobseekerId,mailsent,false, users.Id);
            return File(response.stream, "application/pdf", "Resume" + JobseekerId);
        }



        /// <summary>
        /// Export Resume
        /// </summary>
        /// <param name="JobseekerId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ExportResumebyemployeer")]
        public async Task<ActionResult> ExportResumebyEmployeer(string JobseekerId, bool mailsent)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var response = await Aspirantservice.ExportResume(JobseekerId, mailsent, true, users.Id);
            return File(response.stream, "application/pdf", "Resume" + JobseekerId);
        }

        /// <summary>
        /// Upload Documents
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploaddocuments")]
        public ActionResult UploadDocuments(List<MJobseekerDocuments> model)
        {
            return Ok(Aspirantservice.UploadDocuments(model));
        }


        /// <summary>
        /// Update Working status
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <param name="workingstatus"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateworkingstatus")]
        public ActionResult UpdateWorkingstatus(string JobseekrId,int workingstatus)
        {
            return Ok(Aspirantservice.UpdateWorkingstatus(JobseekrId,workingstatus));
        }

        /// <summary>
        /// Get Working status
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getworkingstatus")]
        public ActionResult GetWorkingstatus(string JobseekrId)
        {
            return Ok(Aspirantservice.GetWorkingstatus(JobseekrId));
        }

        /// <summary>
        /// Get AspnetUser ID by Mobile
        /// </summary>
        /// <param name="Mobile"></param>getaspiranteducationbyid
        /// <returns></returns>
        [HttpPost]
        [Route("getaspnetuseridbymobile")]
        public ActionResult GetaspnetuserIDbyMobile(string Mobile)
        {
            return Ok(Aspirantservice.GetaspnetuserIDbyMobile(Mobile));
        }


        /// <summary>
        /// Get Preimum Job seekers
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="location"></param>
        /// <param name="zone"></param>
        /// <param name="jobseekerId"></param>
        /// <param name="status"></param>
        /// <param name="profiledatastatus"></param>
        /// <param name="Gender"></param>
        /// <param name="sourceofLead"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getpreimumjobseekers")]
        public async Task<ActionResult> GetPreimumJobseekers([FromQuery] PageData page, string Sort, bool descending, string name, string email, string mobile, int location, int zone, int jobseekerId, int status, int profiledatastatus, int Gender, int sourceofLead, DateTime fromdate, DateTime todate)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(await Aspirantservice.GetPreimumJobseekers(page, Sort, descending, name, email, mobile, location, zone, jobseekerId, status, profiledatastatus, Gender, sourceofLead, fromdate, todate));
        }


        /// <summary>
        /// Update Preimum User
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatepreimumUser")]
        public ActionResult UpdatePreimumUser(string JobseekrId)
        {
            return Ok(Aspirantservice.UpdatePreimumUser(JobseekrId));
        }

        /// <summary>
        /// Get Login User Jobseeker Id
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getloginUserjobseekerId")]
        public async Task<ActionResult> GetLoginUserJobseekerId()
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Aspirantservice.GetLoginUserJobseekerId(users.Id));
        }

        /// <summary>
        /// Last Updated date
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("lastupdateddate")]
        public ActionResult LastUpdatedDate(string Id)
        {
            return Ok(Aspirantservice.LastUpdatedDate(Id));
        }



        #endregion
    }
}
