﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELJob.Business.BL.Service;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using ELJob.Helper.Authorization;
using ELJob.Helper.Grid;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [JWTAuthorize]
    public class FinanceController : ControllerBase
    {

        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IFinanceBL Financeservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        #region Construtor
        public FinanceController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration, IFinanceBL _FinananceBLservice)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Financeservice = _FinananceBLservice;
        }

        #endregion

        #region API Methods

        /// <summary>
        /// Get the Payment Transaction
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="location"></param>
        /// <param name="zone"></param>
        /// <param name="jobseekerId"></param>
        /// <param name="status"></param>
        /// <param name="profiledatastatus"></param>
        /// <param name="Gender"></param>
        /// <param name="sourceofLead"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettransactions")]
        public async Task<ActionResult> GetTransactions([FromQuery] PageData page, string Sort, bool descending, string name, string email, string mobile, int location, int zone, int jobseekerId, int status, int profiledatastatus, int Gender, int sourceofLead, DateTime fromdate, DateTime todate)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(await Financeservice.GetTransactions(page, Sort, descending, name, email, mobile, location, zone, jobseekerId, status, profiledatastatus, Gender, sourceofLead, fromdate, todate, users.Id));
        }

        #endregion

    }
}
