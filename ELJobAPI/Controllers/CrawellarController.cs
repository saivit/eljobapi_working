﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel.Shared;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrawellarController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IAspirantBL Aspirantservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        /// 

        private readonly IApplicationServices applicationservice;
        private bool disposed;

        #endregion

        #region Construtor
        public CrawellarController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration, 
            IAspirantBL AspirantBLservice, IApplicationServices _applicationservice)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Aspirantservice = AspirantBLservice;
            applicationservice = _applicationservice;
        }

        #endregion

        #region API Methods

        [HttpPost]
        [Route("StatusConevrtion")]
        [AllowAnonymous]
        public async Task<ActionResult> StatusConevrtion(Migrationmodel model)
        {         
                return Ok(await applicationservice.StatusConevrtion(model));
        }

        /// <summary>
        /// update Update Workexper incestatus
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("updateWorkexperincestatus")]
        [AllowAnonymous]
        public async Task<ActionResult> UpdateWorkexperincestatus()
        {
            return Ok(await applicationservice.UpdateWorkexperincestatus());
        }
        #endregion

    }
}
