﻿using ELJob.Business.BL.Service;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Lookup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly ILocationsBL Locationsservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        #region Construtor
        public LocationsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration, ILocationsBL LocationsBlservice)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Locationsservice = LocationsBlservice;
        }

        #endregion

        #region API method
        /// <summary>
        /// Add Zone
        /// </summary>
        /// <param name="Zone"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        
        [HttpPost]
        [Route("addzone")]
        public ActionResult AddZone(string Zone, int UserId)
        {
            return Ok(Locationsservice.AddZone(Zone,0));
        }

        /// <summary>
        /// Update Zone
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Zone"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatezone")]
        public ActionResult UpdateZone(int Id, string Zone, int UserId)
        {
            return Ok(Locationsservice.UpdateZone(Id,Zone,0));
        }

        /// <summary>
        /// Add Location
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addlocation")]
        public ActionResult AddLocation(LookupLocation model)
        {
            return Ok(Locationsservice.Addlocation(model));
        }

        /// <summary>
        /// Update Location
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatelocation")]
        public ActionResult UpdateLocation(LookupLocation model)
        {

            return Ok(Locationsservice.UpdateLocation(model));
        }

        /// <summary>
        /// Remove Location
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removelocation")]
        public ActionResult RemoveLocation(int Id)
        {
            return Ok(Locationsservice.RemoveLocation(Id));
        }


        /// <summary>
        /// Get LocationbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getlocationid")]
        public ActionResult GetLocationId(int Id)
        {
            return Ok(Locationsservice.GetLocationId(Id));
        }


        /// <summary>
        /// Get Location by Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getlocationbyname")]
        public ActionResult GetLocationbyName(string name)
        {
            return Ok(Locationsservice.GetLocationbyName(name));
        }

        [HttpGet]
        [Route("getlocationlist")]
        public ActionResult GetLocationList()
        {
            return Ok(Locationsservice.GetLocationList());
        }
        #endregion
    }   
}
