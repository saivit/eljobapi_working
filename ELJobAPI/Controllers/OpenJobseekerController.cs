﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Aspirant;
using ELJob.Core.Context;
using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Authorization;
using ELJob.Helper.Common;
using ELJob.Helper.Communication;
using ELJob.Helper.Grid;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static ELJob.Helper.Common.Constants;
namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpenJobseekerController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IAspirantBL Aspirantservice;

        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        #region Construtor
        public OpenJobseekerController(ApplicationDbContext context,  IConfiguration _configuration, IAspirantBL AspirantBLservice)
        {
            this.configuration = _configuration;
            Aspirantservice = AspirantBLservice;
        }

        #endregion

        #region API Method


        /// <summary>
        /// Aspirant Quick Registration
        /// </summary>
        /// <param name="model">
        /// Gender 4	Male 5	Female
        ///  state 1
        /// </param>
        /// <returns></returns>
        [HttpPost]
        [Route("aspirantinfo")]
        [AllowAnonymous]
        public async Task<ActionResult> AspirantQuickRegistraion(AspirantInfo model)
        {
            try
            {
                return Ok(await Aspirantservice.AspirantQuickRegistraion(model));
            }
            catch (Exception Ex)
            {
                return Ok(new Response() { Iserror = true, errors = new List<string>() { Ex.Message + " " + Ex.Message }, StatusCode = StatusCodes.Status500InternalServerError });
            }
        }

       

        /// <summary>
        /// Add Aspirant Education
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("addaspiranteducation")]
        public ActionResult AddAspirantEducation(MAspirantEducation model)
        {
            return Ok(Aspirantservice.AddAspirantEducation(model));

        }


        /// <summary>
        /// Update Aspirant Education
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspiranteducation")]
        public ActionResult UpdayteAspirantEducation(MAspirantEducation model, string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantEducation(model, Id));
        }


        /// <summary>
        /// Remove Aspirant Education
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspiranteducation")]
        public ActionResult RemoveAspirantEducation(string Id)
        {
            return Ok(Aspirantservice.RemoveAspirantEducation(Id));
        }


        /// <summary>
        /// Get ApirantEduList
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantedulist")]
        public ActionResult GetAspirantEduList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantEduList(Id));
        }


        /// <summary>
        /// Add AspirantExperience
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantexperience")]
        public ActionResult AddAspirantExperience(MAspirantExperience model)
        {
            return Ok(Aspirantservice.AddAspirantExperience(model));
        }


        /// <summary>
        /// Update Aspirant Experience
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspirantexperience")]
        public ActionResult UpdateAspirantExperience(MAspirantExperience model, string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantExperience(model, Id));
        }


        /// <summary>
        /// Remove Aspirant Experience
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantexperience")]
        public ActionResult RemoveAspirantEXperience(string Id)
        {
            return Ok(Aspirantservice.RemoveAspirantExperience(Id));
        }

        /// <summary>
        /// Add Aspirant Personal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantpersonal")]
        public ActionResult AddAspirentPersonal(MAspirantPersonal model)
        {
            return Ok(Aspirantservice.AddAspirantPersonal(model));
        }

        /// <summary>
        /// Update Aspirant Personal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspirantpersonal")]
        public ActionResult UpdateAspirantPersonal(MAspirantPersonal model)
        {
            return Ok(Aspirantservice.UpdateAspirantPersonal(model));
        }

        /// <summary>
        /// Remove Aspirant Personal
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantpersonal")]
        public ActionResult RemoveAspirantPersonal(int Id)
        {
            return Ok(Aspirantservice.RemoveAspirantPersonal(Id));
        }

        /// <summary>
        /// Add Aspirant SkillAssessment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantskillassessment")]
        public ActionResult AddSkillAssessment(MAspirantSkillAssessment model)
        {
            return Ok(Aspirantservice.AddSkillAssessment(model));
        }

        /// <summary>
        /// Update Aspirant Skill Assessment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspirantskillassessment")]
        public ActionResult UpdateSkillAssessment(MAspirantSkillAssessment model)
        {
            return Ok(Aspirantservice.UpdateSkillAssessment(model));
        }


        /// <summary>
        /// Remove Aspirant Skill Assessment
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantskillassessment")]
        public ActionResult RemoveAspirantSkillAssessment(int Id)
        {
            return Ok(Aspirantservice.RemoveAspirantSkillAssement(Id));
        }

        /// <summary>
        /// Get the aspirant Details by Id
        /// </summary>
        /// <param name="AspirantId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantbyid")]
        public ActionResult GetAspirantbyId(string AspirantId)
        {
            return Ok(Aspirantservice.GetAspirantbyId(AspirantId));
        }


     

        /// <summary>
        /// Get AspirantExpbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantexpbyid")]
        public ActionResult getaspirantexpbyid(int Id)
        {
            return Ok(Aspirantservice.GetAspirantExpbyId(Id));
        }


        /// <summary>
        /// Add AspirantCertification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantcertification")]
        public ActionResult AddAspirantCertification(MAspirantCertification model)
        {
            return Ok(Aspirantservice.AddAspirantCertification(model));
        }

        /// <summary>
        /// Update AspirantCertification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateaspirantcertification")]
        public ActionResult UpdateAspirantCertification(MAspirantCertification model, string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantCertification(model, Id));
        }

        /// <summary>
        /// Remove AspirantCertification
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantcretification")]
        public ActionResult RemoveAspirantCertification(string Id)
        {
            return Ok(Aspirantservice.RemoveAspirantCertification(Id));
        }

        /// <summary>
        /// Get AspirantExperienceList
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantexperience")]
        public ActionResult GetAspirantExpList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantExpList(Id));
        }


        /// <summary>
        /// Get AspirantCertificationList
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantcertificationList")]
        public ActionResult GetAspirantCertificationList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantCertificationList(Id));
        }



        /// <summary>
        /// Get Aspirant SkillAssessment List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantskillassessmentList")]
        public ActionResult GetAspirantSkillassessmentList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantSkillList(Id));
        }

        /// <summary>
        /// Get JobseekerbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getjobseekerbyid")]
        public ActionResult GetJobseekerById(string Id)
        {
            return Ok(Aspirantservice.GetJobseekerbyId(Id));
        }

        /// <summary>
        /// Add Aspirant Languages
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("addaspirantlanguages")]
        public ActionResult AddAspirantLanguages(MJobseekerLanguge model)
        {
            return Ok(Aspirantservice.AddAspirantLanguages(model));
        }


        /// <summary>
        /// Update Aspirant Languages
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateaspirantlanguages")]
        public ActionResult UpdateAspirantLanguages(MJobseekerLanguge model, string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantLanguages(model, Id));
        }


        /// <summary>
        /// Remove Aspirant Languages
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("removeaspirantlanguages")]
        public ActionResult RemoveAspirantLanguages(string Id)
        {
            return Ok(Aspirantservice.RemoveAspirantLanguages(Id));
        }


        /// <summary>
        /// Get Aspirant Languages
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantlanguages")]
        public ActionResult GetAspirantLanguages(string Id)
        {
            return Ok(Aspirantservice.GetAspirantLanguages(Id));
        }

        /// <summary>
        /// Get AspirantEducationbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspiranteducationbyid")]
        public ActionResult GetAspirantEducationById(int Id)
        {
            return Ok(Aspirantservice.GetAspirantEducationById(Id));
        }


        /// <summary>
        /// Get AspirantCertificationbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantcerticationbyid")]
        public ActionResult GetAspirantcerificationById(int Id)
        {
            return Ok(Aspirantservice.GetAspirantcerificationById(Id));
        }


        /// <summary>
        /// Get AspirantSkillbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantskillbyid")]
        public ActionResult GetAspirantSkillbyId(string Id)
        {
            return Ok(Aspirantservice.GetAspirantSkillbyId(Id));
        }


        /// <summary>
        /// Get AspirantPersonalbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantpersonalbyid")]
        public ActionResult GetAspirantPersonalbyId(int Id)
        {
            return Ok(Aspirantservice.GetAspirantPersonalbyId(Id));
        }


        /// <summary>
        /// Add Aspirant Objective
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addaspirantobjective")]
        public ActionResult AddAspirantObjective(MAspirantObjective model)
        {
            return Ok(Aspirantservice.AddAspirantObjective(model));
        }


        /// <summary>
        /// Update Aspirant Objective
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateaspirantobjective")]
        public ActionResult UpdateAspirantObjective(MAspirantObjective model, string Id)
        {
            return Ok(Aspirantservice.UpdateAspirantObjective(model, Id));
        }

        /// <summary>
        /// Get Aspirant objective ById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantobjectivebyid")]
        public ActionResult GetAspirantObjectivebyId(string Id)
        {
            return Ok(Aspirantservice.GetAspirantObjectivebyId(Id));
        }


 

        /// <summary>
        /// Get Aspirant Personal List
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getaspirantpersonallist")]
        public ActionResult GetAspirantPersonalList(string Id)
        {
            return Ok(Aspirantservice.GetAspirantPersonalList(Id));
        }

    
        /// <summary>
        /// Update Profile top Fields
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateprofiletopfields")]
        public ActionResult UpdateProfileTopFields(Updateprofiletopfields model, string Id)
        {
            return Ok(Aspirantservice.UpdateProfileTopFields(model, Id));
        }



        /// <summary>
        /// Aspirant Email Verfication
        /// </summary>
        /// <param name="Id-JobseekerId">Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("emailverfication")]
        public async Task<ActionResult> Emailverfication(string Id)
        {
            return Ok(await Aspirantservice.Emailverfication(Id));
        }

        /// <summary>
        /// Get AspirantID by Mobile
        /// </summary>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getaspirantmobilebyid")]
        public ActionResult GetaspinrantIDbyMobile(string Mobile)
        {
            return Ok(Aspirantservice.GetaspinrantIDbyMobile(Mobile));
        }



        /// <summary>
        /// Upload Documents
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploaddocuments")]
        public ActionResult UploadDocuments(List<MJobseekerDocuments> model)
        {
            return Ok(Aspirantservice.UploadDocuments(model));
        }


        /// <summary>
        /// Update Working status
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <param name="workingstatus"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateworkingstatus")]
        public ActionResult UpdateWorkingstatus(string JobseekrId, int workingstatus)
        {
            return Ok(Aspirantservice.UpdateWorkingstatus(JobseekrId, workingstatus));
        }

        /// <summary>
        /// Get Working status
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getworkingstatus")]
        public ActionResult GetWorkingstatus(string JobseekrId)
        {
            return Ok(Aspirantservice.GetWorkingstatus(JobseekrId));
        }

        /// <summary>
        /// Get AspnetUser ID by Mobile
        /// </summary>
        /// <param name="Mobile"></param>getaspiranteducationbyid
        /// <returns></returns>
        [HttpPost]
        [Route("getaspnetuseridbymobile")]
        public ActionResult GetaspnetuserIDbyMobile(string Mobile)
        {
            return Ok(Aspirantservice.GetaspnetuserIDbyMobile(Mobile));
        }


      

        /// <summary>
        /// Update Preimum User
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatepreimumUser")]
        public ActionResult UpdatePreimumUser(string JobseekrId)
        {
            return Ok(Aspirantservice.UpdatePreimumUser(JobseekrId));
        }

     
        /// <summary>
        /// Last Updated date
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("lastupdateddate")]
        public ActionResult LastUpdatedDate(string Id)
        {
            return Ok(Aspirantservice.LastUpdatedDate(Id));
        }



        #endregion
    }
}
