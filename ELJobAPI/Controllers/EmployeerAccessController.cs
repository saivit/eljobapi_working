﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Auth;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using ELJob.Helper.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ELJobAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeerAccessController : ControllerBase
    {

        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IEmployerBL Employerservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 



        private readonly SignInManager<ApplicationUser> signInManager;

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        #region Construtor
        public EmployeerAccessController(ApplicationDbContext context, UserManager<ApplicationUser> userManager,
            IConfiguration _configuration, IEmployerBL EmployerBlservice
            , SignInManager<ApplicationUser> signInManager
            )
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Employerservice = EmployerBlservice;
            this.signInManager = signInManager;
        }

        #endregion
        /// <summary>
        /// Register Employeer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>       
        [HttpPost, AllowAnonymous]
        [Route("registeremployeer")]

        public async Task<IActionResult> RegisterEmployeer(EmployerRegisterModel model)
        {
            try
            {
                var userExists = await userManager.FindByNameAsync(model.Email);
                ApplicationUser user = new ApplicationUser()
                {
                    Email = model.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.Email,
                    IsActive = true,
                    PhoneNumber = model.Mobile
                };
                string Password = CommonHelper.Randompassword().ToString();
                var result = await userManager.CreateAsync(user, Password);
                if (!result.Succeeded)
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });
                return Ok(Employerservice.RegisterEmployeer(model, user.Id, Password));
            }
            catch (Exception Ex)
            {
                return Ok(new Response() { Iserror = true, errors = new List<string>() { Ex.Message + " " + Ex.Message }, StatusCode = StatusCodes.Status500InternalServerError });

            }
        }


    }
}
