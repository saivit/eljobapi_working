﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ELJob.Core.Context;
using Microsoft.AspNetCore.Identity;
using ELJob.Domain.Model.Auth;
using Microsoft.Extensions.Configuration;
using ELJob.Business.BL.Service;
using ELJob.Helper.Authorization;
using ELJob.Helper.Grid;
using ELJob.Business.Viewmodel;
using ELJob.Helper.Communication;
using ELJob.Business.Viewmodel.Auth;
using System.IO;
using ELJob.Helper.Common;
using Microsoft.AspNetCore.Authorization;

namespace ELJobAPI.Controllers
{
    /// <summary>
    /// Employee controller. Manage the Employee 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    //[JWTAuthorize]
    public class EmployeeController : ControllerBase
    {
        #region Private Variables
        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IEmployeeBL Employeeservice;
        /// <summary>
        ///  User Persistance store
        /// </summary>
        /// 
        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        private readonly IConfiguration configuration;
        /// <summary>
        /// Disposed Obect 
        /// </summary>
        private bool disposed;

        #endregion

        /// <summary>
        /// Object Intilizing
        /// </summary>
        /// <param name="context"></param>
        /// <param name="userManager"></param>
        /// <param name="_configuration"></param>
        /// <param name="EmployeeBlservice"></param>
        #region Construtor
        public EmployeeController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IConfiguration _configuration,IEmployeeBL EmployeeBlservice)
        {

            this.userManager = userManager;
            this.configuration = _configuration;
            Employeeservice = EmployeeBlservice;
        }

        #endregion


        #region API method

        /// <summary>
        /// Create Employee
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<IActionResult> CreateEmployee(RegisterModel model)
        {
            try
            {
                var userExists = await userManager.FindByNameAsync(model.Email);
              

                ApplicationUser user = new ApplicationUser()
                {
                    Email = model.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.Email,
                    IsActive = true,
                    PhoneNumber=model.mobile,
                };
                string Password = CommonHelper.Randompassword().ToString();
                var result = await userManager.CreateAsync(user, Password);
                if (!result.Succeeded)
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   
                using (StreamReader reader = new StreamReader(Path.Combine("Content", "Html", "Register/Employee Registration.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Username}", model.Name).Replace("{email}", model.Email)
                    .Replace("{mail}", configuration.GetValue<string>("ApplicationSettings:supportmail")).Replace("{password}", Password).Replace("{employeemail}", model.Email).Replace("{url}", configuration.GetValue<string>("ApplicationSettings:url"))
                     .Replace("{contact}", configuration.GetValue<string>("ApplicationSettings:contactnumber"))
                    ;
                model.UserId = user.Id;
                var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
                Employeeservice.Createemployee(model, users.Id);
                EmailHelper.To = model.Email;
                EmailHelper.Subject = "Login Credintails-" + model.Name;
                EmailHelper.Body = body;
                EmailHelper.IsHtml = true;
                EmailHelper.SendEmail(configuration.GetValue<string>("EmailConfiguration:SmtpServer"), configuration.GetValue<string>("EmailConfiguration:SmtpUsername"),
                    configuration.GetValue<string>("EmailConfiguration:SmtpPassword"), configuration.GetValue<bool>("EmailConfiguration:SSLenabled"),
                    configuration.GetValue<int>("EmailConfiguration:SmtpPort")
                    );

                return Ok(new Response { Status = "Success", Message = "User created successfully!", StatusCode = (StatusCodes.Status201Created) });

            }
            catch (Exception Ex)
            {
                return Ok(new Response() { Iserror = true, errors = new List<string>() { Ex.Message + " " + Ex.Message }, StatusCode = StatusCodes.Status500InternalServerError });

            }

        }

        [HttpPost]
        [Route("UpdateEmployee")]
        [JWTAuthorize]
        public async Task<IActionResult> UpdateEmployee(RegisterModel model)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var response = Employeeservice.Updateemployee(model,users.Id);
            return Ok(response);
        }

        [HttpPost]
        [Route("getemployeebyid")]
        [JWTAuthorize]
        public IActionResult GetEmployeebyId(int UserId)
        {
            var response = Employeeservice.GetEmployeebyId(UserId);
            return Ok(response);
        }

        [HttpGet]
        [Route("getemployees")]
        public async Task<IActionResult> GetEmployees([FromQuery] PageData page, string Sort, bool descending, int zone, int Location, string name, DateTime fromdate, DateTime todate, int UserId, int RoleId)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employeeservice.GetEmployees( page,  Sort,  descending,  zone,  Location,  name,  fromdate,  todate, users.Id,  RoleId));
        }

        [HttpGet]
        [Route("getrolesgrid")]
        public async Task<IActionResult> GetRoles([FromQuery] PageData page, string Sort, bool descending, string name, DateTime fromdate, DateTime todate)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employeeservice.GetRoles(page, Sort, descending, users.Id, 0, name));
        }

        [HttpGet]
        [Route("getloginaudit")]
        public async Task<IActionResult> GetLoginAudit([FromQuery] PageData page, string Sort, bool descending, int Zone, int Location, DateTime fromdate, DateTime todate, int UserId, int RoleId, string name)
        {
            var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employeeservice.GetLoginAudit(page,Sort,descending, Zone, Location,fromdate,todate, users.Id, 0,  name));
        }




        /// <summary>
        /// Add new Role
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addrole")]
        public async Task<IActionResult> AddRole(aspnetroles model)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var response = await Employeeservice.AddRole(model, 0);
            return Ok(response);
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updaterole")]
        public async Task<IActionResult>UpdateRole(aspnetroles model)
        {
            //var users = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employeeservice.UpdateRole(model, 1));
        }

        [HttpGet]
        [Route("getemployeehistorylist")]
        public async Task<ActionResult> GetEmployeeHistoryList([FromQuery] PageData page, string Sort, bool descending, string role, string reportring, string zone)
        {
            //var users = userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employeeservice.GetEmployeeHistoryList(page, Sort, descending, role, reportring,zone));
        }


     [HttpGet]
     [Route("getmessagelist")]
     public async Task<ActionResult>GetMessageList([FromQuery] PageData page, string Sort, bool descending, string name, int AspirantId, int JobseekerId, string Mobile, string Email, string Event, string Message, string Location, string Mode, int Type, DateTime Createddate)
        {
            var users = userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return Ok(Employeeservice.GetMessageList(page, Sort, descending, name, AspirantId, JobseekerId, Mobile, Email, Event, Message, Location, Mode, Type,Createddate));
        }

        [HttpGet]
        [Route("employeereoprting")]
        public ActionResult EmployeeReportingList()
        {
            return Ok(Employeeservice.EmployeeReportingList());
        }

       
        [HttpGet]
        [Route("getrolebyid")]

        public ActionResult GetRolebyId(int Id)
        {
            return Ok(Employeeservice.GetRolebyId(Id));
        }

        #endregion


    }
}
