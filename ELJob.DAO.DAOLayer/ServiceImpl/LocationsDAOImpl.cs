﻿using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Employer;
using ELJob.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.DAO.DAOLayer.ServiceImpl
{
    public class  LocationsDAOImpl : Repository<Employer>, ILocationsDAO
    {
        IRepository<Employer> Repo;
        public LocationsDAOImpl(Core.Context.ApplicationDbContext context, IMongoContext mongocontext) : base(context, mongocontext) { Repo = new Repository<Employer>(context, mongocontext); }
        public void Dispose() { }
    }
}
