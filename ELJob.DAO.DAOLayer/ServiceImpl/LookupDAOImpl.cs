﻿using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Auth;
using ELJob.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.DAO.DAOLayer.ServiceImpl
{
   public class LookupDAOImpl: Repository<aspnetroles>, ILookUpDAO
    {
        IRepository<aspnetroles> Repo;
        public LookupDAOImpl(Core.Context.ApplicationDbContext context, IMongoContext mongocontext) : base(context, mongocontext) { Repo = new Repository<aspnetroles>(context, mongocontext); }
        public void Dispose() { }

    }
  
}
