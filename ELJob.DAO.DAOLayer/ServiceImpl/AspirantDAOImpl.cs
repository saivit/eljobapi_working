﻿using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Auth;
using ELJob.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.DAO.DAOLayer.ServiceImpl
{
        public class AspirantDAOImpl : Repository<Aspirant>, IAspirantDAO
        {
            IRepository<Aspirant> Repo;
            public AspirantDAOImpl(Core.Context.ApplicationDbContext context, IMongoContext mongocontext) : base(context, mongocontext) { Repo = new Repository<Aspirant>(context, mongocontext); }
            public void Dispose() { }
        }
    
}
