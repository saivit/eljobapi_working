﻿using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Screens;
using ELJob.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.DAO.DAOLayer.ServiceImpl
{

    public class FoundationDAOImpl : Repository<Screen>, IFoundationDAO
    {
        IRepository<Screen> Repo;
        public FoundationDAOImpl(Core.Context.ApplicationDbContext context, IMongoContext mongocontext) : base(context, mongocontext) { Repo = new Repository<Screen>(context, mongocontext); }
        public void Dispose() { }
    }
}
