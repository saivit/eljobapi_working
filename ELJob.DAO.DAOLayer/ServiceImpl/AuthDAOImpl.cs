﻿using System;
using System.Collections.Generic;
using System.Text;
using ELJob.Domain.Model.Auth;
using ELJob.Infra.Repository;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Core.Context;

namespace ELJob.DAO.DAOLayer.ServiceImpl
{
   public class AuthDAOImpl : Repository<aspnetroles>,IAuthDAO
    {
        IRepository<aspnetroles> Repo;
        public AuthDAOImpl(Core.Context.ApplicationDbContext context, IMongoContext mongocontext) : base(context, mongocontext) { Repo = new Repository<aspnetroles>(context, mongocontext); }
        public void Dispose() { }

    }
}
