﻿using System;
using System.Collections.Generic;
using System.Text;
using  ELJob.Infra.Repository;
using ELJob.Domain.Model.Auth;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Core.Context;

namespace ELJob.DAO.DAOLayer.ServiceImpl
{
   public class EmployeeDAOImpl:Repository<aspnetroles>, IEmployeeDAO
    {
        IRepository<aspnetroles> Repo;
        public EmployeeDAOImpl(Core.Context.ApplicationDbContext context,IMongoContext mongocontext) : base(context,mongocontext){Repo = new Repository<aspnetroles>(context,mongocontext);}
        public void Dispose(){}
    }
}
