﻿using System;
using System.Collections.Generic;
using System.Text;
using ELJob.Domain.Model.Auth;
using ELJob.Infra.Repository;

namespace ELJob.DAO.DAOLayer.Service
{
   public interface IAuthDAO : IRepository<aspnetroles>, IDisposable
    {

    }
}
