﻿using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Auth;
using ELJob.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.DAO.DAOLayer.Service
{
    public interface IAspirantDAO : IRepository<Aspirant>, IDisposable
    {
    }
}
