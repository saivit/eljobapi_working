﻿using ELJob.Domain.Model.Employer;
using ELJob.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.DAO.DAOLayer.Service
{
    public interface IEmployerDAO : IRepository<Employer>, IDisposable
    {
    }
}
