﻿using ELJob.Domain.Model.Screens;
using ELJob.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.DAO.DAOLayer.Service
{
    public interface IFoundationDAO : IRepository<Screen>, IDisposable
    {
    }
}
