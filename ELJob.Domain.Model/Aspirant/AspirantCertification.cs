﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("AspirantCertification")]
    public class AspirantCertification
    {
        [Key]
        public int Id { get; set; }
        public int AspirantId { get; set; }
        public string Certification { get; set; }
        public int Completedyear { get; set; }
        public string Duration { get; set; }
        public string Institute { get; set; }
        public bool IsActive { get; set; }

    }
}
