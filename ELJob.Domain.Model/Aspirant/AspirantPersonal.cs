﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("AspirantPersonal")]
    public class AspirantPersonal
    {
        [Key]
        public int Id { get; set; }
        public int AspirantId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LasttName { get; set; }
        public int Gender { get; set; }
        public string Mobile { get; set; }
        public string Alternative { get; set; }
        public string Email { get; set; }
        public string ValidateEmailKey { get; set; }
        public bool IsEmailvalidated{get;set;}
        public string Housenumber { get; set; }
        public string streetName { get; set; }
        public string City { get; set; }
        public int State { get; set; }
        public int Pincode { get; set; }
        public string JobSeekingLocation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int ReadyToMigrate { get; set; }
        public DateTime DOB { get; set; }
        public int SourceofLead { get; set; }
        public int WorkExperince { get; set; }

    }
}
