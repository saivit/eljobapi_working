﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("AspirantSkillAssessment")]
   public class AspirantSkillAssessment
    {
        [Key]

        public int Id { get; set; }
        public int AspirantId { get; set; }
        public int Salary { get; set; }
        public string NotIntrestRoles { get; set; }
        public string objective { get; set; }
        public int objectiveId { get; set; }
        public string MJobseekerId { get; set; }
    }
}
