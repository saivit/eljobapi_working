﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("AspirantMigrationLocation")]
  public  class AspirantMigrationLocation
    {
        [Key]
        public int Id { get; set; }
        public int AspirantId { get; set; }
        public int City { get; set; }
        public int Reason { get; set; }
        public string OtherReason { get; set; }
        
        public bool IsActive { get; set; }

    }
}
