﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("Aspirant")]
  public  class Aspirant
    {
        [Key]

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string JobSeekerId { get; set; }
        public int statusId { get; set; }      
        public string Gender { get; set; }       
        public int GenderId { get; set; }
        public string Location { get; set; }     
        public int LocationId { get; set; }
        public string SourceofLead { get; set; }
        public int SourceofLeadId { get; set; }
        public int profilefillscore { get; set; }
        public DateTime Createdat { get; set; }
        public int Createdby{ get; set; }
        public DateTime Modifiedat { get; set; }
        public int modifiedby { get; set; }
        public int UserId { get; set; }

        public int ? reschdule { get; set; }

        public DateTime? reschduleDate { get; set; }

        public string resechduletime { get; set; }

        public int ? rejectionReason { get; set; }

        public string rejectionReason_name { get; set; }

        public string otherRejectionReason { get; set; }

    }
}
