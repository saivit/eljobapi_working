﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("AspirantExperince")]
   public class AspirantExperince
    {
        [Key]
        public int Id { get; set; }
        public int AspirantId { get; set; }
        public string Organization { get; set; }
        public DateTime Workingfrom { get; set; }
        public DateTime WorkingTo { get; set; }
        public int Duration { get; set; }
        public bool IscurrentEmployer { get; set; }
        public bool IsActive { get; set; }


    }
}
