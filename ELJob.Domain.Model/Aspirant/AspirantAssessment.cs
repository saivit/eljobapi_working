﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("AspirantAssessment")]
   public class AspirantAssessment
    {
        [Key]
        public int Id { get; set; }
        public int AspirantId { get; set; }
        public string competency { get; set; }
        public int ComtencyResult { get; set; }
    }
}
