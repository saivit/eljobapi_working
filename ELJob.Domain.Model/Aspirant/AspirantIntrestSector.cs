﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("AspirantIntrestSector")]
   public class AspirantIntrestSector
    {
        [Key]
      
        public int Id { get; set; }
        public int AspirantId { get; set; }
        public int Sector { get; set; }  
        public bool IsActive { get; set; }
    }
}
