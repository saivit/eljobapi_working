﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Aspirant
{
    [Table("AspirantEducation")]
   public class AspirantEducation
    {
        [Key]
        public int Id { get; set; }
        public int AspirantId { get; set; }
        public int Qulification { get; set; }
        //public int OtherQulification { get; set; }
        public int ModeofEducation { get; set; }
        public int Completedyear { get; set; }
        public string Institue { get; set; }
        public bool IsActive { get; set; }

    }
}
