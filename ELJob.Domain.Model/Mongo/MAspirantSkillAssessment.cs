﻿using ELJob.Business.Viewmodel.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
    public class MAspirantSkillAssessment
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }
        public int AspirantId { get; set; }
        public int Apptitude { get; set; }
        public int CommunicativeEnglish { get; set; }
        public int Computeroperations { get; set; }

        public string Apptitude_name { get; set; }
        public string CommunicativeEnglish_name { get; set; }
        public string Computeroperations_name { get; set; }
        public int Salary { get; set; }
        public string NotIntrestRoles { get; set; }
        public string MJobseekerId { get; set; }
        public List<Selectionvalues> lst_sectors { get; set; }
    }
}
