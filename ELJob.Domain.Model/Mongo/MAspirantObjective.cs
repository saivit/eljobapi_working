﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
    public class MAspirantObjective
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string MJobseekerId { get; set; }
        public int AspirantId { get; set; }
        public int Objective { get; set; }
        public string ObjectiveName { get; set; }
    }
}
