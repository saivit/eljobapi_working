﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public  class MAspirantCertification
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }
        public int AspirantId { get; set; }
        public string Certification { get; set; }
        public int Completedyear { get; set; }
        public string Duration { get; set; }
        public string Institute { get; set; }
        public bool IsActive { get; set; }
        public string MJobseekerId { get; set; }

    }
}
