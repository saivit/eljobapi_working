﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public class MAspirantEducation
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }
        public int AspirantId { get; set; }
        public int Qulification { get; set; }
        public string QulificationName { get; set; }
        public int OtherQulification { get; set; }
        public string OtherQulificationName { get; set; }
        public int ModeofEducation { get; set; }
        public string ModeofEducationName { get; set; }
        public int Completedyear { get; set; }
        public string Institue { get; set; }
        public bool IsActive { get; set; }
        public string MJobseekerId { get; set; }
    }
}
