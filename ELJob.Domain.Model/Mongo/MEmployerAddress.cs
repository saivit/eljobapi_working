﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public class MEmployerAddress
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public int EmployerId { get; set; }
        public int UserId { get; set; }
        public string MEmployerId { get; set; }
        public string Address { get; set; }
        public string Addressstwo { get; set; }
        public int city { get; set; }
        public string cityname { get; set; }
        public int state { get; set; }
        public string statename { get; set; }
        public int pincode { get; set; }
        
    }
}
