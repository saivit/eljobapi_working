﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
  public class MAspirantResshduleHistory
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string JobseekerId { get; set; }
        public int AspirantId { get; set; }
        public string Name { get; set; }     
        public int RescduleId { get; set; }
        public string ReschduleReason { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Reschduledate { get; set; }
        public string Reschduletime { get; set; }
        public string Createdby { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Createdat { get; set; }
    }
}
