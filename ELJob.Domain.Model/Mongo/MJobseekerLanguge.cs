﻿using ELJob.Business.Viewmodel.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public class MJobseekerLanguge
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string MJobseekerId { get; set; }
        public int AspirantId { get; set; }
        public int Language { get; set; }
        public string LanguageName { get; set; }
        public string MotherTongue { get; set; }
        public List<Selectionvalues> Readwrite { get; set; }
        public bool IsActive { get; set; }
    }
}
