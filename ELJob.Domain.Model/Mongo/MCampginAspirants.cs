﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public class MCampginAspirants
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string JobseekerId { get; set; }
        public int AspirantId { get; set; }
        public int AspirantUserId { get; set; }
        public string Name { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string Mobile { get; set; }
        public string Location { get; set; }
        public string GenderText { get; set; }
        public int GenderId { get; set; }
        public string Email { get; set; }
        public string campgainId { get; set; }
        public string EmployeerId { get; set; }
        public string Employeername { get; set; }
        public string Organazation { get; set; }
        public int EmployeeruserId { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime DOB { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int LocationId { get; set; }
    }
}
