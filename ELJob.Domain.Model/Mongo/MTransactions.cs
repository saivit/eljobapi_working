﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
    public class MTransactionsmodel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public int EmployerId { get; set; }
        public string MEmployerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PlanId { get; set; }
        public string Planmodel { get; set; }
        public int Validity { get; set; }
        public int ValidityinDays { get; set; }
        public decimal Price { get; set; }
        public decimal PriceIncludeGST { get; set; }
        public decimal GST { get; set; }
        public decimal CGST { get; set; }
        public decimal SGST { get; set; }
        public decimal IGST { get; set; }
        public int ProfileViews { get; set; }
        public bool IsUnlimited { get; set; }
        public int ProfileViewsnumber { get; set; }
        public int TotalCVdownload { get; set; }
        public string SMSandEmail { get; set; }
        public string Status { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime purchasedate { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime planstartdate { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime planenddate { get; set; }
        public string Invoicenumber { get; set; }
        public string RazorpayInvoiceNumber { get; set; }
        public string StateGSTcode { get; set; }
        public string GSTsatatus { get; set; }
        public string GSTOfparty { get; set; }
        public string placeofsupply { get; set; }
        public string UnderpaybleRCM { get; set; }
        public string pannumber { get; set; }
        public string SACcode { get; set; }
        public string SACCategorey { get; set; }
        public decimal disscount { get; set; }
        public decimal finalamount { get; set; }
        public decimal InvoiceValue { get; set; }
        public string Invoicein_words { get; set; }
        public string companyAccounname { get; set; }
        public string companyaccountsno { get; set; }
        public string companybank { get; set; }
        public string companybranch { get; set; }
        public string companyifsc { get; set; }
        public string plandescription { get; set; }
        public decimal Roundingoff { get; set; }
        public int StateId { get; set; }
        public string Statename { get; set; }
        public string address { get; set; }

        public string Gstno { get; set; }


    }
}
