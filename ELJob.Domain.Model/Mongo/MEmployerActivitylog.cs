﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
  public class MEmployerActivitylog
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        [BsonDefaultValue(null)]
        public string EmployerId { get; set; }
        public string EmployerName { get; set; }
        public string Log { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Logdate { get; set; }
        public string Logtype { get; set; }
    }
}
