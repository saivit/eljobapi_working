﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using ELJob.Business.Viewmodel.Aspirant;
using MongoDB.Driver.GeoJsonObjectModel;

namespace ELJob.Domain.Model.Mongo
{
    public class live
    {
        /// <summary>
        /// This is Auto generated unique id , please dont pass while inserting or updating
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        /// <summary>
        /// Total Enrolled students of all batches whose batches are Started + batch not started)
        /// </summary>
        /// 
        public string Name { get; set; }

        public string Jobcity { get; set; }

        public int TotalEnrolledStudents { get; set; }
        /// <summary>
        /// Students who Enrolled and batch Started but not ended
        /// </summary>
        public int ActiveBatchStudents { get; set; }
        /// <summary>
        /// Enrolled but not actively participating(batch not started )
        /// </summary>
        public int InActiveBatchStudents { get; set; }
        /// <summary>
        /// Students completing/Attempted ? at least 1 quiz once in last 7 days whose batch is in progress
        /// </summary>
        public int ActiveStudents { get; set; }
        /// <summary>
        /// Students Enrolled but not actively participating ( batch in progress but batch Not Ended and Number of students not completing at least 1 quiz once in last 7 days)
        /// </summary>
        public int InActiveStudents { get; set; }
        /// <summary>
        /// Total Count of students whose batches are complted
        /// </summary>
        public int TotalOldStudents { get; set; }
        /// <summary>
        /// Enrolled but not actively participating ( batch started and ended but Number of students not completing at least 1 quiz once other than Pre and post assesment)
        /// </summary>
        public int InActiveOldStudents { get; set; }
        /// <summary>
        ///Enrolled but actively participating ( batch started and ended but Number of students completing at least 1 quiz other than Pre and post assesment)
        /// </summary>
        public int ActiveOldStudents { get; set; }
        /// <summary>
        ///Monthly Percentage of Active Students Active Batches
        /// </summary>
         /// <summary>
        /// Below 2 Propertices are used to store Active students Monthly percentage (last 12 months)
        /// </summary>
        public List<string> month { get; set; }
        public List<int> StdentsPercentage { get; set; }

        /// <summary>
        /// Below 2 Propertices are used to store Active students counts by batches
        /// </summary>
        public List<int> ActiveStudentsCountbyBatch { get; set; }
        public List<string> ActiveStudentsBatchNames { get; set; }

        /// <summary>
        ///  Below 2 Propertices are used to store  In Active students counts by batches
        /// </summary>
        public List<int> InActiveStudentsCountbyBatch { get; set; }
        public List<string> InActiveStudentsBatchNames { get; set; }

        /// <summary>
        /// Created by email address(logged in user email) , mandatory to  pass while inserting
        /// </summary>
        public string crt_by { get; set; }
        /// <summary>
        /// Created date - Current Date Time stamp in UTC mandatory to pass while inserting
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime crt_dt { get; set; }
        /// <summary>
        /// Updated by email address(logged in user email), mandatory to  pass while updating
        /// </summary>
        public object upd_by { get; set; }
        /// <summary>
        /// Updated date , Current date time stamp in UTC mandatory to  pass while updating
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime upd_dt { get; set; }

        public GeoJsonPoint<GeoJson2DGeographicCoordinates> Location { get; set; }

        public GeoLatlong latlong { get; set; }
    }
}
