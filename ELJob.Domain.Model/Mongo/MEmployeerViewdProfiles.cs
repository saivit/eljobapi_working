﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public class MEmployeerViewdProfiles
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string JobseekerId { get; set; }
        public int AspirantId { get; set; }
        public string Name { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Viewedat { get; set; }
        public string MEmployeerId { get; set; }
        public string Employeername { get; set; }
        public string Jobseekrprofilepath { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

    }
}
