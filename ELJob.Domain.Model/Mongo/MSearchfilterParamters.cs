﻿using ELJob.Business.Viewmodel.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
    public class MSearchfilterParamters
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string EmployerId { get; set; }
        public string Jobtitle { get; set; }
        public string Jobloaction { get; set; }
        public int SalaryRange { get; set; }
        public int Gender { get; set; }
        public int Experince { get; set; }
        public List<Selectionvalues> Sectors { get; set; }
        public List<Selectionvalues> Qualification { get; set; }
        public List<Selectionvalues> AditionalQualification { get; set; }
        public List<Selectionvalues> City { get; set; }
        public int CTC { get; set; }
        public int Openings { get; set; }
        public List<Selectionvalues> Language { get; set; }
        public int Agemin { get; set; }
        public int Agemax { get; set; }

        public List<Selectionvalues> communicationskills { get; set; }
        public List<Selectionvalues> apptitude { get; set; }
        public List<Selectionvalues> computroperations { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Seachedat { get; set; }
       
        public int UserId { get; set; }
        public string salaryranagetext { get; set; }
        public string Employeername { get; set; }
        public string Gendertext { get; set; }
        public string ExperinceText { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> LatLocation { get; set; }
    }


}
