﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public class MCampaignmaster
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string EmployeerId { get; set; }
        public int EmployeerUserId { get; set; }
        public string Name { get; set; }
        public string Organizationname { get; set; }
        public string Mobile { get; set; }
        public string Location { get; set; }     
        public string Email { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Startedat { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Endedat { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }
        public int createdbyUserId { get; set; }
        public string Plan_name { get; set; }
        public decimal amount { get; set; }
        public decimal totalgrossamount { get; set; }
        public decimal disscount { get; set; }
        public decimal nettaxableamount { get; set; }
        public decimal CGST { get; set; }
        public decimal SGST { get; set; }
        public decimal IGST { get; set; }
        public decimal GST { get; set; }
        public decimal Roundingoff { get; set; }
        public decimal Totalinvoicevalue { get; set; }
        public int stateId { get; set; }
        


    }
}
