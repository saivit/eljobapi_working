﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public class MJobseekerDocuments
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string JobseekerName { get; set; }
        public string Documentype { get; set; }
        public string Documentname { get; set; }
        public string Filpath { get; set; }
        public long filesize { get; set; }
        public string fileextension { get; set; }
        public string JobseekerId { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Uploadedtime { get; set; }


    }
}
