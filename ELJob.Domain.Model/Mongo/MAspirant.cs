﻿using ELJob.Business.Viewmodel.Aspirant;
using ELJob.Business.Viewmodel.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
    public class MAspirant
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        public string JobseekerId { get; set; }
        public int AspirantId { get; set; }
        public int AspirantUserId { get; set; }
        public string Name { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string Mobile { get; set; }
        public string Location { get; set; }
        public string GenderText { get; set; }
        public int GenderId { get; set; }
        public string Email { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime DOB { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CreatedDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime ModifiedDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime UpdatedDate { get; set; }
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> LatLocation { get; set; }
        public int Salary { get; set; }
        public int[] Sector { get; set; }
        public int[] MigrateLocation { get; set;}
        public int ReadytoMigrate { get; set; }
        public int Experience { get; set; }
        public int[] Language { get; set; }
        public int Age { get; set; }
        public int LocationId { get; set; }
        public int score { get; set; }
        public string SourceofLead { get; set; }
        public int SourceofLeadId { get; set; }
        public int Status { get; set; }
        public string Statusname { get; set; }
        public int rejectReasonId { get; set; }
        public string rejectReason { get; set; }
        public int ReschduleReasonId { get; set; }
        public string ReschduleReason { get; set; }
        public int [] Joblookingcity { get; set; }
        public int SalaryRange { get; set; }
        public int Gender { get; set; }
        public int [] Qualification { get; set; }
        public int[] AditionalQualification { get; set; }
        public int CTC { get; set; }
        public int communicationskills { get; set; }
        public int apptitude { get; set; }
        public int computroperations { get; set; }
        public List<Selectionvalues> lst_Jobblookingcity { get; set; }
        public List<Selectionvalues> lst_qualification { get; set; }
        public List<Selectionvalues> lst_additionalqulification { get; set; }
        public List<Selectionvalues> lst_Languages { get; set; }
        public List<Selectionvalues> lst_sectors { get; set; }
        public List<Selectionvalues> lst_certificates { get; set; }
        public string Workingstatus { get; set; }
        public int WorkingstatusId { get; set; }
        public string Certification { get; set; }
        public string Profilepicture { get; set; }
        public string Profilepicture_altname { get; set; }
        public bool IsEmailvalidated { get; set; }
        public int Zone { get; set; }
        public string Zonename { get; set; }
        public string ProfileDataStatus { get; set; }
        public int ProfileDataStatusId { get; set; }
        public int ProfileOrder { get; set; }
        public bool Ispremium { get; set; }
        public string otherRejectionReason { get; set; }
        public int [] LocationIdarray { get; set; }
        public List<EmployeerSearchsmodel> SearchedEmployeer { get; set; }
        public string CampaginId { get; set; }
        public string CampaginEmployeerid { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime EmployeerViewddate { get; set; }
        public string Reschduletime { get; set; }
        public DateTime ReschduleDate { get; set; }

        public int Processedby { get; set; }
        public string processedbyName { get; set; }
    }
}
