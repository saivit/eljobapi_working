﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
    public class MAspirantPersonal
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }
        public int AspirantId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public int Age { get; set; }
        public string LasttName { get; set; }
        public int Gender { get; set; }
        public string Mobile { get; set; }
        public string Alternative { get; set; }
        public string Email { get; set; }
        public string ValidateEmailKey { get; set; }
        public int IsEmailvalidated { get; set; }
        public string Housenumber { get; set; }
        public string streetName { get; set; }
        public int City { get; set; }
        public string City_Name { get; set; }
        public int State { get; set; }
        public string State_Name { get; set; }
        public int Pincode { get; set; }
        public int JobSeekingLocation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int ReadyToMigrate { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime DOB { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int SourceofLead { get; set; }
        public int WorkExperince { get; set; }
        public string MJobseekerId { get; set; }
        public int Location1 { get; set; }
        public int Location2 { get; set; }
        public int Location3 { get; set; }

        public string Location1_Name{ get; set; }
        public string Location2_Name { get; set; }
        public string Location3_Name { get; set; }
        public int Reason1 { get; set; }
        public int Reason2 { get; set; }
        public int Reason3 { get; set; }

        public string Reason_Name1 { get; set; }
        public string Reason_Name2 { get; set; }
        public string Reason_Name3 { get; set; }

        public bool readToMigrateLocation { get; set; }

    }
}
