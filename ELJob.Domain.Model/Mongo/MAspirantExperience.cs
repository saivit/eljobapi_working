﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
   public class MAspirantExperience
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }
        public int AspirantId { get; set; }
        public string Organization { get; set; }
        public DateTime Workingfrom { get; set; }
        public DateTime WorkingTo { get; set; }
        public int Duration { get; set; }
        public bool IscurrentEmployer { get; set; }
        public bool IsActive { get; set; }
        public string MJobseekerId { get; set; }
    }
}
