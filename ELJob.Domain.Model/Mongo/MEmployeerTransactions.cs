﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Mongo
{
    public class MEmployeerTransactions
    {
        public int EmployeerId { get; set; }
        public string PlanName { get; set; }
        public string PlanAmount { get; set; }
        public string pricing_model { get; set; }
        public int validity { get; set; }
        public double price { get; set; }
        public int profile_views { get; set; }
        public int total_cvs_downloaded { get; set; }
        public DateTime Date_activeated { get; set; }
        public string Description { get; set; }
        public DateTime valid_until { get; set; }
        public decimal amount_paid { get; set; }


    }
}
