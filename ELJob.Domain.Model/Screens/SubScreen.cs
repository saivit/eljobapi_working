﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Screens
{
    [Table("SubScreen")]
   public class SubScreen
    {
        [Key]
        public int Id { get; set; }
        public int Screen { get; set; }
        public string Name { get; set; }
        public string Route { get; set; }
        public string Icon { get; set; }
        public string Lable { get; set; }
        public bool IsActive { get; set; }

    }
}
