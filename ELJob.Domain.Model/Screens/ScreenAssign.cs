﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Screens
{
    [Table("ScreenAssign")]
  public class ScreenAssign
    {
        [Key]
        public int Id { get; set; }
        public int Role { get; set; }
        public int Screen { get; set; }
        public bool IsActive { get; set; }

    }
}
