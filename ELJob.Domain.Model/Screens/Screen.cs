﻿using System;
using System.Collections.Generic;
using System.Text;
using  System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Screens
{
    [Table("Screen")]
   public class Screen
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Route { get; set; }
        public string Icon { get; set; }
        public string Lable { get; set; }
        public bool IsActive { get; set; }
        public bool Treeview { get; set; }

    }
}
