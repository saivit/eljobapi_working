﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Views
{
    [Table("vw_AssignScreenToRole")]
    public class vw_AssignScreenToRole
    {
        [Key]
        public int Id { get; set; }
        public string Role { get; set; }
        public string Screen { get; set; }
        public bool IsActive { get; set; }
        public string Route { get; set; }
        public string Icon { get; set; }
        public string Lable { get; set; }
        public bool Treeview { get; set; }
        public int screenId { get; set; }

        public int roleId { get; set; }
    }
}
