﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Views
{
    [Table("vw_employeereporting")]
    public class vw_employeereporting
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
