﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Views
{
    [Table("vw_statusmapping")]
   public class vw_statusmapping
    {
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }
        public string StatusType { get; set; } 
        public bool IsActive { get; set; }
    }
}
