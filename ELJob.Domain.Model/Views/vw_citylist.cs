﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Views
{
    [Table("vw_citylist")]
    public class vw_citylist
    {
        [Key]
        public int Id { get; set; }
        public string City { get; set; }
        public int Stateid { get; set; }
        public string StateName { get; set; }
        public bool IsActive { get; set; }
    }
}
