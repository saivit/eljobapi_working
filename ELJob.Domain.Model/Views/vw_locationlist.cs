﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Views
{
    [Table("vw_locationlist")]
   public class vw_locationlist
    {
        [Key]
        public int Id { get; set; }
        public string Location { get; set; }
        public string Zone { get; set; }
        public bool Isactive { get; set; }

    }
}
