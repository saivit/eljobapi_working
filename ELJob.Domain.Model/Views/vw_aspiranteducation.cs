﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Views
{
    [Table("vw_aspiranteducation")]
    public class vw_aspiranteducation
    {
        public int Id { get; set; }
        public string Education { get; set; }
        public string ModeofEducation { get; set; }
        public int Completedyear { get; set; }
        public string Institue { get; set; }
    }
}
