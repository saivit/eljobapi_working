﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Message
{
    [Table("customermessaging")]
    public class Customermessaging
    {
        [Key]
        public int Id { get; set; }
        public int AspirantId { get; set; }
        public string JobSeekrID { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Event { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
         public string Location { get; set; }
        public string Mode { get; set; }
        public int Type { get; set; }
        public int createdby { get; set; }
        public DateTime Createddate { get; set; }
    }
}
