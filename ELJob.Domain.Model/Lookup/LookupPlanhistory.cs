﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Lookup
{
    [Table("LookupPlanhistory")]
   public class LookupPlanhistory
    {
        [Key]
        public int Id { get; set; }
        public int PlanId { get; set; }
        public string Planmodel { get; set; }
        public string Validity { get; set; }
        public string ValidityinDays { get; set; }
        public string Price { get; set; }
        public decimal PriceIncludeGST { get; set; }
        public decimal GST { get; set; }
        public string ProfileViews { get; set; }
        public bool IsUnlimited { get; set; }
        public int ProfileViewsnumber { get; set; }
        public int TotalCVdownload { get; set; }
        public string SMSandEmail { get; set; }
        public bool IsActive { get; set; }
        public string Createdby { get; set; }
        public DateTime Createdat { get; set; }
    }
}
