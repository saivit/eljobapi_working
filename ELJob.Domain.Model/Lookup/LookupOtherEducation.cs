﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Lookup
{
    [Table("LookupOtherEducation")]
   public class LookupOtherEducation
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int EducationId { get; set; }
        public bool IsActive { get; set; }
    }
}
