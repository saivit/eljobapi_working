﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Lookup
{
    [Table("vw_StatusMapping")]
  public  class vw_StatusMapping
    {
        [Key]
        public int Id { get; set; }
        public string status { get; set; }
        public int StatusType { get; set; }
        public bool IsActive { get; set; }
    }
}
