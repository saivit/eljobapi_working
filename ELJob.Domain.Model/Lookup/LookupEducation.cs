﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ELJob.Domain.Model.Lookup
{
    [Table("LookupEducation")]
   public class LookupEducation
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int EductionId { get; set; }
        public bool IsActive { get; set; }
    }
}
