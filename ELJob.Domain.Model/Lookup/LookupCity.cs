﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Lookup
{
    [Table("LookupCity")]
    public class LookupCity
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Stateid { get; set; }

        public bool IsActive { get; set; }
    }
}
