﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Lookup
{
    [Table("Lookupstatustype")]
   public class Lookupstatustype
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
