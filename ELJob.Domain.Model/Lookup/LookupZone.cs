﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ELJob.Domain.Model.Lookup
{
    [Table("LookupZone")]
   public class LookupZone
    {
        [Key]
        public int Id { get; set; }
        public String Name { get; set; }

    }
}
