﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Employer
{
    [Table("EmployerIntrestSectors")]
  public  class EmployerIntrestSectors
    {
        [Key]
        public int Id { get; set; }
        public int EmployerId { get; set; }
        public int sector { get; set; }
        public bool Isactive { get; set; }
    }
}
