﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Employer
{
    [Table("EmployerAddress")]
   public class EmployerAddress
    {
        [Key]
        public int Id { get; set; }
        public int EmployerId { get; set; }
        public int UserId { get; set; }
        public string Address { get; set; }
        public string Addressstwo { get; set; }
        public int city { get; set; }
        public int state { get; set; }
        public string cityname { get; set; }
        public string statename { get; set; }
        public int pincode { get; set; }


    }
}
