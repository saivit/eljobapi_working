﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Employer
{
    [Table("EmployerPlansHistory")]
   public class EmployerPlansHistory
    {

        [Key]
        public int Id { get; set; }
        public string PlanName { get; set; }
        public string Validity { get; set; }
        public string Price { get; set; }
        public string ProfileViews { get; set; }
        public string TotalCVs { get; set; }
        public DateTime planstart { get; set; }
        public DateTime planend { get; set; }
        public string SMSandemail { get; set; }
        public bool Istrail { get; set; }
        public string EmployerId { get; set; }
        public bool Financecheck { get; set; }
        public DateTime FinancecheckDate { get; set; }
        public string Financecheckby { get; set; }


    }
}
