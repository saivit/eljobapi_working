﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Employee
{
    [Table("EmployeeHistory")]
   public class EmployeeHistory
    {
        [Key]
        public int Id { get; set; }
        public string Role { get; set; }
        public string Reporting { get; set; }
        public string Zone { get; set; }
        public DateTime Createdat { get; set; }

    }
}
