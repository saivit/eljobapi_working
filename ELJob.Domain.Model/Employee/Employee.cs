﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Employee
{
    [Table("Employee")]
  public  class Employee
    {
        [Key]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int? Role { get; set; }
        public string Rolename { get; set; }
        public int? Location { get; set; }
        public string Locationname { get; set; }
        public int ? Zone { get; set; }
        public string Zonename { get; set; }
        public string Mobile { get; set; }
        public string Reporting { get; set; }
        public int ? ReportingId { get; set; }
        public int? UserId { get; set; }
        public string EmployeeCode { get; set; }
        public bool? IsActive { get; set; }
        public DateTime Createdat { get; set; }
        public string Createdby { get; set; }
        public DateTime Modifiedat { get; set; }
        public int? Modifiedby { get; set; }

    }
}
