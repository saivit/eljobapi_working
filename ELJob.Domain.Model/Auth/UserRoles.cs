﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Domain.Model.Auth
{
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
