﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ELJob.Domain.Model.Auth
{
    [Table("aspnetuserroles")]
    public class aspnetuserroles
    {
        [Key]
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string Discriminator { get; set; }
        public int? UserId1 { get; set; }
        public int? RoleId1 { get; set; }


    }
}
