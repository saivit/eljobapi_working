﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Auth
{
    [Table("Person")]
  public class Person
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int InitalIntraction { get; set; }
        public int Profilestatus { get; set; }
        public int followpschdule { get; set; }
        public DateTime? Latestfollowpdate { get; set; }
        public string Latestfollowptime { get; set; }
        public int RejectionReason { get; set; }
        public string Role { get; set; }
        public int RoleId { get; set; }
        public string Mobile { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; } 
        public int UserId { get; set; }
        public int? AspirantId { get; set; }
        public int? StatusId { get; set; }
        public string Statusname { get; set; }


    }
}
