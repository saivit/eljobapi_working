﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ELJob.Domain.Model.Auth
{
    [Table("tbl_userloginaudit")]
    public class tbl_userloginaudit
    {
        [Key]

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public int Roleid { get; set; }
        public string Location { get; set; }
        public int LocationId { get; set; }
        public DateTime Logindate { get; set; }
    }
}
