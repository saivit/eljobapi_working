﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ELJob.Helper.Formater
{
   public class CustomStringLength : StringLengthAttribute
    {
        private string name;
        private bool required;
        private bool minlength_required;
        private bool max_length_required;
        public CustomStringLength(int maximumLength) : base(maximumLength)
        {
        }

        public CustomStringLength(int maximumLength,string Name)
                : base(maximumLength)
        {
            name = Name;
        }

        public CustomStringLength(int maximumLength, string Name,bool Required)
             : base(maximumLength)
        {
            name = Name;
            required = Required;
        }

        public CustomStringLength(int maximumLength, string Name, bool Required,bool Minlength_required,bool Max_length_required)
     : base(maximumLength)
        {
            name = Name;
            required = Required;
            minlength_required = Minlength_required;
            max_length_required = Max_length_required;
        }

        public override bool IsValid(object value)
        {
           
            string val = Convert.ToString(value);
            // Is Required filed or Not
            if(required&& val.Length<=0)
            {
                base.ErrorMessage = name +" Is Rquired";
                return base.IsValid(value);
            }
            if (minlength_required  && val.Length < base.MinimumLength)
                base.ErrorMessage = name+" Minimum length should be " +base.MinimumLength;
            if (max_length_required && val.Length > base.MaximumLength)
                base.ErrorMessage = name + " Maximum length should be " +base.MaximumLength;
            return base.IsValid(value);
        }

    }
}
