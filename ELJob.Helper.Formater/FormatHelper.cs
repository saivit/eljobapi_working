﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ELJob.Helper.Formater
{
    public static class FormatHelper
    {
        public static Int32 ToInt32(this object obj)
        {
            return Convert.ToInt32(obj);
        }
        public static string Tostring(this object obj)
        {
            return Convert.ToString(obj);
        }
        public static bool NotEmpty(this string obj)
        {
            return !String.IsNullOrWhiteSpace(obj);
        }
        public static bool Isdatenull(this object obj)
        {
            return Convert.ToDateTime(obj).Year==1?false:true;
        }
        public static bool Contains(this IEnumerable<object> obj)
        {
            return obj.Count()>0? true:false;
        }
        public static decimal Todecimal(this object obj)
        {
            return Convert.ToDecimal(obj);
        }
        public static string Toshortmonth(this object obj)
        {
            StringBuilder returndate = new StringBuilder();
            DateTime date = Convert.ToDateTime(obj);
            returndate = returndate.Append(date.Year);
            if (date.Month < 10)
            {
                returndate = returndate.Append(string.Format("{0}{1}", 0, date.Month));

            }
            else
            {
                returndate = returndate.Append(date.Month.ToString());
            }
            if (date.Day<10)
            {
                returndate = returndate.Append(string.Format("{0}{1}", 0, date.Day));

            }
            else
            {
                returndate = returndate.Append(date.Day.ToString());
            }
           
           
            return returndate.ToString();
        }
    }
}
