﻿using ELJob.Business.Viewmodel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.BL.Service
{
  public  interface ILookupBL
    {
        /// <summary>
        /// Get Status by Type
        /// </summary>
        /// <param name="StatusType"></param>
        /// <returns></returns>
        Response GetStatusbyType(int StatusType);

        /// <summary>
        /// Get Lookup zones
        /// </summary>
        /// <returns></returns>
        Response GetZones();

        /// <summary>
        /// Get Lookup zones
        /// </summary>
        /// <returns></returns>
        Response GetRoles();

        /// <summary>
        /// Get TerritoriesbyId
        /// </summary>
        /// <param name="Zone"></param>
        /// <returns></returns>
        Response GetTerritoriesbyId(int Zone);



        /// <summary>
        /// Get OtherEducationById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetOtherEducationById(int Id);

        /// <summary>
        /// get Courceling status
        /// </summary>
        /// <returns></returns>
        Response GetCouncelingstatus();

        /// <summary>
        /// Get All the GST Type,State,City values
        /// </summary>
        /// <returns>
        ///  GST- Registered/Non Registred
        /// States- MP/AP
        /// City -Hyd,VZA
        ///  
        /// </returns>
        Response GetEmployeerUpdateProfileParameters();

        /// <summary>
        /// Get JobseekerDDLValues
        /// </summary>
        /// <returns></returns>
        Response GetJobseekerDDLValues();

        /// <summary>
        /// Get the Job search profile parameteres
        /// </summary>
        /// <returns></returns>
        Response GetJosbsearchParameters();

        /// <summary>
        /// Get Languages
        /// </summary>
        /// <returns></returns>
        Response GetLanguages();
    }
}
