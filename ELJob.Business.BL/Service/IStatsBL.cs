﻿using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Stats;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELJob.Business.BL.Service
{
    public interface IStatsBL
    {

        Task<Response> Jobseekerstats(StatasModel model);
        Task<Response> EmployeerStats(StatasModel model);
        
    }
}
