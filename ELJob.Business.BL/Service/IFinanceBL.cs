﻿using ELJob.Business.Viewmodel;
using ELJob.Helper.Grid;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELJob.Business.BL.Service
{
   public interface IFinanceBL
    {
        /// <summary>
        /// Get Jobseeker
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="location"></param>
        /// <param name="jobseekerId"></param>
        /// <returns></returns>
        Task<GridResponse> GetTransactions(PageData page, string Sort, bool descending, string name, string email, string mobile, int location, int zone, int jobseekerId,
            int status, int profiledatastatus, int Gender, int sourceofLead, DateTime fromdate, DateTime todate,int UserId);

        /// <summary>
        /// Get transacrion by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetTransactionById(string Id);
    }
}
