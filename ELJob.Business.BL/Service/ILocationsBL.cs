﻿using ELJob.Business.Viewmodel;
using ELJob.Domain.Model.Lookup;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.BL.Service
{
    public interface ILocationsBL
    {
        /// <summary>
        /// Add Zone
        /// </summary>
        /// <param name="Zone"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddZone(string Zone, int UserId);

        /// <summary>
        /// Update Zone
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Zone"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateZone(int Id, string Zone, int UserId);

        /// <summary>
        /// Add Location
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response Addlocation(LookupLocation model);

       
        /// <summary>
        /// Update Location
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateLocation(LookupLocation model);

        /// <summary>
        /// Remove Location
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response RemoveLocation(int Id);

        /// <summary>
        /// Get PlanById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetLocationId(int Id);

        /// <summary>
        /// Get LocationList
        /// </summary>
        /// <returns></returns>
        Response GetLocationList();

        /// <summary>
        /// Get Location by Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Response GetLocationbyName(string name);
    }
}
