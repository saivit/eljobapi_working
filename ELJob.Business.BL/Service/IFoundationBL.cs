﻿using ELJob.Business.Viewmodel;
using ELJob.Domain.Model.Lookup;
using ELJob.Domain.Model.Screens;
using ELJob.Helper.Grid;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Business.BL.Service
{
    public interface IFoundationBL
    {
        /// <summary>
        /// Add Screen
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddScreen(Screen model, int UserId);

        /// <summary>
        /// Update Screen
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateScreen(Screen model,int UserId);

        /// <summary>
        /// Remove Screen
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveScreen(int Id, int UserId);

        /// <summary>
        /// Add SubScreen
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddSubScreen(SubScreen model, int UserId);

        /// <summary>
        /// Update SubScreen
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateSubScreen(SubScreen model, int UserId);

        /// <summary>
        /// Remove SubScreen
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveSubScreen(int Id, int UserId);

        /// <summary>
        /// Add State
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddState(Lookupstate model, int UserId);

        /// <summary>
        /// Update State
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateState(Lookupstate model,int UserId);

        /// <summary>
        /// Remove State
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveState(int Id, int UserId);

        /// <summary>
        /// Add City
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddCity(LookupCity model, int UserId);

        /// <summary>
        /// Update City
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateCity(LookupCity model, int UserId);

        /// <summary>
        /// Remove City
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveCity(int Id, int UserId);

        /// <summary>
        /// Add Sector
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddSector(LookupSector model, int UserId);

        /// <summary>
        /// Update Sector
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateSector(LookupSector model, int UserId);

        /// <summary>
        /// Remove Sector
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveSector(int Id, int UserId);

        /// <summary>
        /// Add Objective
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddObjective(LookupObjective model, int UserId);

        /// <summary>
        /// Update Objective
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateObjective(LookupObjective model, int UserId);


        /// <summary>
        /// Remove Objective
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveObjective(int Id, int UserId);


        /// <summary>
        /// Add Plan
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddPlan(LookupPlans model, int UserId);

        /// <summary>
        /// Update Plan
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdatePlan(LookupPlans model, int UserId);

        /// <summary>
        /// Remove Plan
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemovePlan(LookupPlans model, int UserId);

        /// <summary>
        /// Add ScreenAssign
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddScreenAssign(ScreenAssign model, int UserId);

        /// <summary>
        /// Update ScreenAssign
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateScreenAssign(ScreenAssign model, int UserId);

        /// <summary>
        /// Remove ScreenAssign
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UsedrId"></param>
        /// <returns></returns>
        Response RemoveScreenAssign(int Id, int UsedrId);

        /// <summary>
        /// Add Education
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddEducation(LookupEducation model, int UserId);

        /// <summary>
        /// Update Education
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateEducation(LookupEducation model, int UserId);

        /// <summary>
        /// Remove Education
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveEducation(int Id, int UserId);

        /// <summary>
        /// Add OtherEducation
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddOtherEducation(LookupOtherEducation model, int UserId);

        /// <summary>
        /// Update OtherEducation
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateOtherEducation(LookupOtherEducation model, int UserId);

        /// <summary>
        /// Remove OtherEducation
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveOtherEducation(int Id, int UserId);


        /// <summary>
        /// Add Status
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddStatus(LookupStatus model, int UserId);


        /// <summary>
        /// Update Status
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateStatus(LookupStatus model, int UserId);


        /// <summary>
        /// Remove Status
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveStatus(int Id, int UserId);


        /// <summary>
        /// Add StatusMapping
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddSatatusMapping(vw_StatusMapping model, int UserId);


        /// <summary>
        /// Update StatusMapping
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateStatusMapping(vw_StatusMapping model, int UserId);


        /// <summary>
        /// Remove StatusMapping
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RemoveStatusMapping(int Id, int UserId);

        /// <summary>
        /// Get State List     
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetStateList(PageData page, string Sort, bool descending, string name, string code, decimal cgst, decimal igst, decimal sgst, int UserId);


        /// <summary>
        /// Get City List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="Stateid"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetCityList(PageData page, string Sort, bool descending, string name, int Stateid, int UserId);

        /// <summary>
        /// Get Sctor List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetSctorList(PageData page, string Sort, bool descending, string name, int UserId);


        /// <summary>
        /// Get Status List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetStatusList(PageData page, string Sort, bool descending, string name, int UserId);




        /// <summary>
        /// Get Plan List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>        /// <param name="ValidityinDays"></param>
        /// <param name="Price"></param>
        /// <param name="ProfileViewsnumber"></param>
        /// <param name="TotalCVdownload"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetPlanList(PageData page, string Sort, bool descending, string name, string ValidityinDays, string Price, int ProfileViewsnumber, int TotalCVdownload, int UserId);


        /// <summary>
        /// Get Objective List
        /// </summary>
        /// <param name="page"></param> 
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetObjectiveList(PageData page, string Sort, bool descending, string name, int UserId);

        /// <summary>
        /// Get Screen List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetScreenList(PageData page, string Sort, bool descending, string name, int UserId);


       /// <summary>
       /// Get SubScreen List
       /// </summary>
       /// <param name="page"></param>
       /// <param name="Sort"></param>
       /// <param name="descending"></param>
       /// <param name="name"></param>
       /// <param name="SubScreen"></param>
       /// <param name="UserId"></param>
       /// <returns></returns>
        GridResponse GetSubScreenList(PageData page, string Sort, bool descending, string name, string SubScreen, int UserId);

        /// <summary>
        /// Get Education List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetEducationList(PageData page, string Sort, bool descending, string name, int UserId);


        /// <summary>
        /// Get StatusMapping List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="StatusType"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetStatusMappingList(PageData page, string Sort, bool descending, string name, string StatusType, int UserId);


        /// <summary>
        /// Get OtherEducation List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetOtherEducationList(PageData page, string Sort, bool descending, string name, int UserId);


        /// <summary>
        /// Get AssignScreenToRole
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="role"></param>
        /// <param name="screen"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetAssignScreenToRole(PageData page, string Sort, bool descending, string role, string screen, int UserId);


        /// <summary>
        /// Get PlanHistory List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="ValidityinDays"></param>
        /// <param name="Price"></param>
        /// <param name="PriceIncludeGST"></param>
        /// <param name="ProfileViewsnumber"></param>
        /// <param name="TotalCVdownload"></param>
        /// <param name="UserId"></param>
        /// <param name="IsUnlimited"></param>
        /// <param name="SMSandEmail"></param>
        /// <param name="Createdat"></param>
        /// <returns></returns>
        GridResponse GetPlanHistoryList(PageData page, string Sort, bool descending, string name, string ValidityinDays, string Price, decimal PriceIncludeGST, int ProfileViewsnumber, int TotalCVdownload, bool IsUnlimited, string SMSandEmail, DateTime Createdat, int UserId);

        /// <summary>
        /// Assign ScreentoRole 
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        Response AssignScreentoRole(int RoleId);

        /// <summary>
        /// Screen List
        /// </summary>
        /// <returns></returns>
        Response ScreenList();


        /// <summary>
        /// Get ScreenbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetScreenbyId(int Id);



        /// <summary>
        /// Get Zone List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetZonelist(PageData page, string Sort, bool descending, string name, int UserId);


        /// <summary>
        /// Get PlanById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetPlanbyId(int Id);

      /// <summary>
      /// Get Location List
      /// </summary>
      /// <param name="page"></param>
      /// <param name="Sort"></param>
      /// <param name="descending"></param>
      /// <param name="name"></param>
      /// <param name="Zone"></param>
      /// <param name="UserId"></param>
      /// <returns></returns>
        GridResponse GetLocationlist(PageData page, string Sort, bool descending, string name, string Zone, int UserId);



        /// <summary>
        /// Get Education by ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GeteducationbyId(int Id);


        /// <summary>
        /// Get StateById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetStatebyId(int Id);


        /// <summary>
        /// Get SectorById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetSectorById(int Id);

        /// <summary>
        /// Get ObjectById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetObjectiveById(int Id);

        /// <summary>
        /// Get SubScreenById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetSubScreenById(int Id);

        /// <summary>
        /// Get Education
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetEducation();

        /// <summary>
        /// Add Language
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response AddLanguage(LookupLanguage model);


        /// <summary>
        /// Update Language
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateLanguage(LookupLanguage model);

        /// <summary>
        /// Remove Language
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response RemoveLanguage(int Id);


        /// <summary>
        /// Get LanguagebyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetLanguagebyId(int Id);


        /// <summary>
        /// GetLanguages
        /// </summary>
        /// <returns></returns>
        Response GetLanguages();


        /// <summary>
        /// LanguageList
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse LanguageList(PageData page, string Sort, bool descending, string name);


        /// <summary>
        /// GetObjective
        /// </summary>
        /// <returns></returns>
        Response GetObjective();

        /// <summary>
        /// Get Cities
        /// </summary>
        /// <returns></returns>
        Response GetCities();

        /// <summary>
        /// Get States
        /// </summary>
        /// <returns></returns>
        Response GetStates();

        /// <summary>
        /// Get Sectors
        /// </summary>
        /// <returns></returns>
        Response GetSectors();
    }
}
