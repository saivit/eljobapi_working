﻿using System;
using System.Collections.Generic;
using System.Text;
using ELJob.Domain.Model.Auth;
using ELJob.Business.Viewmodel;
using System.Threading.Tasks;
using ELJob.Helper.Grid;
using ELJob.Business.Viewmodel.Auth;

namespace ELJob.Business.BL.Service
{
   public interface IEmployeeBL
    {
        /// <summary>
        ///  Add Roles
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
       Task<Response> AddRole(aspnetroles model, int UserId);

        /// <summary>
        /// Create employee
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response Createemployee(RegisterModel model,int UserId);
        /// <summary>
        /// Update employee
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response Updateemployee(RegisterModel model,int UserId);

        /// <summary>
        /// Get Employee by Id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response GetEmployeebyId(int UserId);

        /// <summary>
        /// Get the All List of Seasonlities
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        GridResponse GetEmployees(PageData page, string Sort, bool descending, int zone, int Location,string name, DateTime fromdate, DateTime todate, int UserId, int RoleId);

        /// <summary>
        /// Get Roles
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        GridResponse GetRoles(PageData page, string Sort, bool descending, int UserId, int RoleId, string name);


        /// <summary>
        /// Get Roles
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        GridResponse GetLoginAudit(PageData page, string Sort, bool descending, int Zone,int Location, DateTime fromdate, DateTime todate, int UserId, int RoleId, string name);


        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateRole(aspnetroles model, int UserId);

        /// <summary>
        /// Get Last login
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        object GetLastlogin(int UserId);


        /// <summary>
        /// Get EmployeeHistory List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="role"></param>
        /// <param name="reportring"></param>
        /// <param name="zone"></param>
        /// <returns></returns>
        GridResponse GetEmployeeHistoryList(PageData page, string Sort, bool descending, string role, string reportring, string zone);

        /// <summary>
        /// Get MessageList
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="AspirantId"></param>
        /// <param name="JobseekerId"></param>
        /// <param name="Mobile"></param>
        /// <param name="Email"></param>
        /// <param name="Event"></param>
        /// <param name="Message"></param>
        /// <param name="Location"></param>
        /// <param name="Mode"></param>
        /// <param name="Type"></param>
        /// <param name="Createddate"></param>
        /// <returns></returns>
        GridResponse GetMessageList(PageData page, string Sort, bool descending, string name, int AspirantId, int JobseekerId, string Mobile, string Email, string Event, string Message, string Location, string Mode, int Type, DateTime Createddate);


/// <summary>
/// EMployee Reporting
/// </summary>
/// <param name="Id"></param>
/// <returns></returns>
        Response EmployeeReportingList();


        /// <summary>
        /// Get RolebyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetRolebyId(int Id);
    }
}
