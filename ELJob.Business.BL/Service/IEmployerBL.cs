﻿using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Auth;
using ELJob.Business.Viewmodel.Employeer;
using ELJob.Business.Viewmodel.Shared;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Employer;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Grid;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELJob.Business.BL.Service
{
    public interface IEmployerBL
    {
        /// <summary>
        /// Add Employer
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddEmployer(EmployerRegisterModel model, int UserId);

        /// <summary>
        /// Update EmployerAddress
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateEmployerAddress(EmployerAddress model, int UserId);


        /// <summary>
        /// Get Employeer Profile
        /// </summary>
        /// <returns></returns>
        Response GetEmployeerProfile(int UserId);

        //
        /// <summary>
        /// Register Employeer by Internal User
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response RegisterEmployeer(EmployerRegisterModel model, int UserId, string Password);

        /// <summary>
        /// Update Employer Profile
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UpdateEmployeerprofile(EmployerProfileUpdatemodel model, int UserId);


        /// <summary>
        /// Search profiles
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response Registerfilter(Filterparameters model, int UserId);


        /// <summary>
        /// Get Filter Profiles
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        Task<GridResponse> GetFileterprofiles(string filterId, PageData page);


        /// <summary>
        /// Get Empyolyeer
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="location"></param>
        /// <param name="employeerId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task<GridResponse> GetEmpyolyeer(PageData page, string Sort, bool descending, string name, string email, string mobile, string location, int employeerId, int status);

        /// <summary>
        /// Get User by Id
        /// </summary>
        /// <param name="UserI"></param>
        /// <returns></returns>
        aspnetusers Getuser(string UserId);

        /// <summary>
        /// Update the Organization details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateOrganization(EmployerOrganizationalModel model,int UserId);


        /// <summary>
        /// Get the Employeer Organizationl details
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Response GetEmployeerOrganizationaldetails(int userId);


        /// <summary>
        /// Get Employer List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="organizationtype"></param>
        /// <param name="organization"></param>
        /// <param name="fname"></param>
        /// <param name="lname"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="currentplan"></param>
        /// <param name="validity"></param>
        /// <param name="createdbyname"></param>
        /// <returns></returns>
        GridResponse GetEmployerlist(PageData page, string Sort, bool descending, string organizationtype, string organization, string fname, string lname, string email, string mobile, string currentplan, int validity, string createdbyname);


        /// <summary>
        /// Get Previous search
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="Fromdate"></param>
        /// <param name="Todate"></param>
        /// <returns></returns>
        Task<GridResponse> GetPreviousSearchs(PageData page, string Sort, bool descending, string name, DateTime Fromdate,DateTime Todate,int UserId);

        /// <summary>
        /// Get Employeer Id by UserId .
        /// Login User Id is Input. Fetch the mongo DB Employeer Id.
        /// If Login User as Employeer then only it will picks the Employeer Id. other wise return status as Empty.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>Employeer Id from Mongo Tables</returns>
        Task<string> GetEmployeerIdbyUserId(int UserId);

        /// <summary>
        /// Get the Employeer transactions
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="Fromdate"></param>
        /// <param name="Todate"></param>
        /// <param name="EmployeerId"></param>
        /// <returns></returns>
        Task<GridResponse> GetEmployeerTransactions(PageData page, string Sort, bool descending, string name, DateTime Fromdate, DateTime Todate,string EmployeerId);

        /// <summary>
        /// Get Payment Invoice
        /// </summary>
        /// <param name="TransactionId"></param>
        /// <returns></returns>
        Response GetPaymentInvoive(string TransactionId);

        /// <summary>
        /// Get EmployerID by Mobile
        /// </summary>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        Response GetemployerIDbyMobile(string Mobile);

        /// <summary>
        /// Get Activitylog
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <param name="employeerId"></param>
        /// <returns></returns>
        GridResponse GetActivitylog(PageData page, string Sort, bool descending, string name, DateTime fromdate, DateTime todate, int employeerId, int UserId);


        /// <summary>
        /// Export Joobseekrs profiles
        /// </summary>
        /// <param name="Jobseekrs"></param>
        /// <returns></returns>
        Task<byte []> ExportUsersExcel(List<string> Jobseekrs,int UserId);

        /// <summary>
        /// check the employeer values count.
        /// </summary>
        /// <param name="Jobseekes"></param>
        /// <returns></returns>
        Task<Response> CheckLimitBeforeExport(List<string> Jobseekes,int UserId);


        /// <summary>
        /// Export UserResume
        /// </summary>
        /// <param name="Jobseekrs"></param>
        /// <returns></returns>
        Task<Filedonwload> ExportUsersResume(List<Selectionvalues> Jobseekrs,int UserId);


        /// <summary>
        /// Upload Employee Profile pic
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        Task<Response> UpdateEmployeerProfilepic(int UserId, IFormFile file);

        /// <summary>
        /// Get the Jobseeker contact details
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        Task<Response> GetJobseekerContactDetails(string JobseekrId,int UserId);


        /// <summary>
        /// Update the job seeker shuffling 
        /// </summary>
        /// <returns></returns>
        Task<Response> UpdateJobseekershuffling();

        /// <summary>
        /// Add Employer Transactions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Response AddEmployerTransactions(MTransactionsmodel model);

        /// <summary>
        /// Get Employeer viewd Profiles
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns></returns>
        GridResponse GetEmployeerviewdProfiles(PageData page, string Sort, bool descending, string name,  string email, string mobile, DateTime fromdate,DateTime todate,int UserId);


        /// <summary>
        /// Get the Get Employeer viewd Profiles
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response GetEmployeerviewdProfiles(int UserId);



        /// <summary>
        /// Get Fifith Plan Details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response GetFifthplandetails(EmployerFifthPlan model);

        /// <summary>
        /// Update profile view count
        /// </summary>
        /// <param name="Jobseekers"></param>
        /// <param name="userId"></param>
        void Updateprofileviewcount(List<string> Jobseekers, int userId);

        /// <summary>
        /// Filter the employeer by parameters
        /// </summary>
        /// <param name="name"></param>
        /// <param name="mobile"></param>
        /// <param name="email"></param>
        /// <param name="organization"></param>
        /// <returns></returns>
        Response FilterEmployeer(string name,string mobile,string email,string organization);

        /// <summary>
        /// Add Cmapign for Employeer
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response AddCmapignforEmployeer(MCampaignmaster model,int UserId, string EmployeerId);


        /// <summary>
        /// Get Campig transactions
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        GridResponse GetCampigtransactions(PageData page, string Sort, bool descending, string name, string email, string mobile, DateTime fromdate, DateTime todate, int UserId);


        /// <summary>
        /// Get Campign Registered Aspirants
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <param name="UserId"></param>
        /// <param name="campaginid"></param>
        /// <returns></returns>
        GridResponse GetCampignRegisteredAspirants(PageData page, string Sort, bool descending, string name, string email, string mobile, DateTime fromdate, DateTime todate, int UserId,string campaginid);

    }
}
