﻿using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Aspirant;
using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Grid;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELJob.Business.BL.Service
{
    public interface IAspirantBL
    {
        /// <summary>
        /// Aspirant Signup
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
       Task< Response> AspirantQuickRegistraion(AspirantInfo model);


        /// <summary>
        /// Get Jobseeker
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="location"></param>
        /// <param name="jobseekerId"></param>
        /// <returns></returns>
        Task<GridResponse> GetJobseeker(PageData page, string Sort, bool descending, string name, string email, string mobile, int location, int zone, int jobseekerId,
            int status, int profiledatastatus, int Gender, int sourceofLead,DateTime fromdate,DateTime todate);

        /// <summary>
        /// Add AspirantEducation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response AddAspirantEducation(MAspirantEducation model);

        /// <summary>
        /// Updat AspirantEducation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateAspirantEducation(MAspirantEducation model,string Id);



        /// <summary>
        /// Remove AspirantEducation
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response RemoveAspirantEducation(string Id);

        /// <summary>
        /// Add AddAspirantExperience
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response AddAspirantExperience(MAspirantExperience model);

        /// <summary>
        /// Update AspirantExperience
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateAspirantExperience(MAspirantExperience model,string Id);

        /// <summary>
        /// Remove AspirantExperience
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response RemoveAspirantExperience(string Id);

        /// <summary>
        /// Add AspirantPesrsonal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response AddAspirantPersonal(MAspirantPersonal model);

        /// <summary>
        /// Update AspirantPersonal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateAspirantPersonal(MAspirantPersonal model);

        /// <summary>
        /// Remove AspirantPersonal
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response RemoveAspirantPersonal(int Id);


        /// <summary>
        /// Add AspirantSkillAssessment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response AddSkillAssessment(MAspirantSkillAssessment model);


        /// <summary>
        /// Update AspirantSkillAssessment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateSkillAssessment(MAspirantSkillAssessment model);


        /// <summary>
        /// Remove Aspirant SkillAssessment
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response RemoveAspirantSkillAssement(int Id);

        /// <summary>
        /// Get Aspirant by Id
        /// </summary>
        /// <param name="AspirantId"></param>
        /// <returns></returns>
        Response GetAspirantbyId(string AspirantId);


        /// <summary>
        /// Reschdule slot
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response Reschduleledslot(AspirantReschdule model,int UserId);


        /// <summary>
        /// Reject the Aspirant
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response RejectAspirant(AspirantRejectionReason model, int UserId);

        /// <summary>
        /// Proced for Councling
        /// </summary>
        /// <param name="AspirantId"></param>
        /// <returns></returns>
        Response ProceedTocounclling(string Id, int UserId,int sourceofLead);


        /// <summary>
        /// Get AspirantExpbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetAspirantExpbyId(int Id);

        /// <summary>
        /// Add Aspirant Certification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response AddAspirantCertification(MAspirantCertification model);


        /// <summary>
        /// Update Aspirant Certification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateAspirantCertification(MAspirantCertification model,string Id);


        /// <summary>
        /// Remove Aspirant Certification
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response RemoveAspirantCertification(string Id);


        /// <summary>
        /// Get AspirantEduList
        /// </summary>
        /// <returns></returns>
        Response GetAspirantEduList(string Id);



        /// <summary>
        /// Add Aspirant Languages
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response AddAspirantLanguages(MJobseekerLanguge model);


        /// <summary>
        /// Update Aspirant Languages
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateAspirantLanguages(MJobseekerLanguge model, string Id);


        /// <summary>
        /// Remove Aspirant Certification
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response RemoveAspirantLanguages(string Id);


        /// <summary>
        /// Get AspirantEduList
        /// </summary>
        /// <returns></returns>
        Response GetAspirantLanguages(string Id);

        


        /// <summary>
        /// Get AspirantExpList
        /// </summary>
        /// <returns></returns>
        Response GetAspirantExpList(string Id);


        /// <summary>
        /// Get Aspirant CertificationList
        /// </summary>
        /// <returns></returns>
        Response GetAspirantCertificationList(string Id);

        
        /// <summary>
        /// Get Aspirant Skill List
        /// </summary>
        /// <returns></returns>
        Response GetAspirantSkillList(string Id);

        /// <summary>
        /// Get JobseekerbyId
        /// </summary>
        /// <returns></returns>
        Response GetJobseekerbyId(string Id);


        /// <summary>
        /// Get AspirantCertificationbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetAspirantcerificationById(int Id);


        /// <summary>
        /// Get AspirantEducationbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetAspirantEducationById(int Id);

        /// <summary>
        /// Get AspirantSkillbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetAspirantSkillbyId(string Id);

        /// <summary>
        /// Get AspirantPersonalbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetAspirantPersonalbyId(int Id);

        /// <summary>
        /// Add Aspirant Objective
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response AddAspirantObjective(MAspirantObjective model);


        /// <summary>
        /// Update Aspirant Objective
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UpdateAspirantObjective(MAspirantObjective model, string Id);

        /// <summary>
        /// Get AspirantPersonalbyId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetAspirantObjectivebyId(string Id);


        /// <summary>
        /// Get Aspirant Personal List
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response GetAspirantPersonalList(string Id);

        /// <summary>
        /// Update Profile Top Fileds
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response UpdateProfileTopFields(Updateprofiletopfields model, string Id);
        /// <summary>
        /// UploadResume
        /// </summary>
        /// <param name="file"></param>
        /// <param name="AspirantId"></param>
        /// <returns></returns>
        Task<Response> Upload(IFormFile file, string AspirantId,string documentType);

        /// <summary>
        /// Get Jobseekeer Documents
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="documentType"></param>
        /// <returns></returns>
        Response GetJobseekeerDocuments(string Id,string documentType);

        /// <summary>
        /// Download Jobseeker Documents
        /// </summary>
        /// <param name="ResourceId"></param>
        /// <returns></returns>
        Task<Response> DownloadJobseekerDocuments(string ResourceId);

        /// <summary>
        /// Completed the profile
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response CompleteProfile(string Id);


        /// <summary>
        /// Yet To Submit documents
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response YetToSubmitdocuments(string Id);


        /// <summary>
        /// Aspirant Email verfication
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Response> Emailverfication(string Id);

        /// <summary>
        /// Get AspireantId by Mobile
        /// </summary>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        Response GetaspinrantIDbyMobile(string Mobile);

        /// <summary>
        /// Export Resume
        /// </summary>
        /// <returns></returns>
        Task<Response> ExportResume(string JobseekerId,bool Mailrequest,bool IsEmployeer,int UserId);

        /// <summary>
        /// Upload Documents
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Response UploadDocuments(List<MJobseekerDocuments> model);

        /// <summary>
        /// Update Working status
        /// </summary>
        /// <param name="JobseekerId"></param>
        /// <param name="workingstatus"></param>
        /// <returns></returns>
        Response UpdateWorkingstatus(string JobseekerId,int workingstatus);

        /// <summary>
        /// Get the working status
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        int GetWorkingstatus(string JobseekrId);

        /// <summary>
        /// Get ASPnetUsers ID by Mobile
        /// </summary>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        Response GetaspnetuserIDbyMobile(string Mobile);


        /// <summary>
        /// Update the Jobseekr into Premium user
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        Response UpdatePreimumUser(string JobseekrId);


        /// <summary>
        /// get Premium jobseekers
        /// </summary>
        /// <param name="page"></param>
        /// <param name="Sort"></param>
        /// <param name="descending"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="location"></param>
        /// <param name="zone"></param>
        /// <param name="jobseekerId"></param>
        /// <param name="status"></param>
        /// <param name="profiledatastatus"></param>
        /// <param name="Gender"></param>
        /// <param name="sourceofLead"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns></returns>
        Task<GridResponse> GetPreimumJobseekers(PageData page, string Sort, bool descending, string name, string email, string mobile, int location, int zone, int jobseekerId,
        int status, int profiledatastatus, int Gender, int sourceofLead, DateTime fromdate, DateTime todate);

        /// <summary>
        /// Get the Login UserId
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        string GetLoginUserJobseekerId(int UserId);

        /// <summary>
        /// LastUpdatedDate
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Response LastUpdatedDate(string Id);
        
    }
}
