﻿using System;
using System.Collections.Generic;
using System.Text;
using ELJob.Business.Viewmodel;
using ELJob.Domain.Model.Auth;
using ELJob.Business.Viewmodel.Auth;

namespace ELJob.Business.BL.Service
{
   public interface IAuthBL
    {
       ApplicationUser Applicationuser(string mobile);

        Response SendOTP(int UserId, OTPModel OTP);

        Response SendforgotEmail(int UserId, Forgotpassword forgotpassword, string message);

        /// <summary>
        /// Is Login OTP valid
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="OTP"></param>
        /// <returns></returns>
        bool IsLoginOTPvalid(int UserId, string OTP);

        /// <summary>
        /// Save Login
        /// </summary>
        /// <param name="UserId"></param>
        void SaveLogin(int UserId);

        /// <summary>
        /// Get Employer token
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        string Employertoken(int UserId);

        /// <summary>
        /// check is employeer profile updated
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Tuple<bool,bool,string> IsEmployeerprofileupdated(int UserId);

        /// <summary>
        /// Get Screens by Role
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response GetScreensbyRole(int UserId);

        /// <summary>
        /// Get Login User Profile
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        Response UserProfile(int UserId);
    }
}
