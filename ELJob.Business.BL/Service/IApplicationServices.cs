﻿using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELJob.Business.BL.Service
{
    public interface IApplicationServices
    {
        /// <summary>
        /// JobProfileViewCraweler- Runs the Every Forght night and check the Employeer View Date profiles and Change the Profile view order.
        /// </summary>
        /// <returns></returns>
        Task<Response> JobProfileViewCraweler();

        Task<Response> StatusConevrtion(Migrationmodel model);

        /// <summary>
        /// Update work experince status as fresher.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Response> UpdateWorkexperincestatus();

    }
}
