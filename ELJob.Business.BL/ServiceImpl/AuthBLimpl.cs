﻿using System;
using System.Collections.Generic;
using System.Text;
using ELJob.Business.BL.Service;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Infra.Repository;
using Microsoft.Extensions.Configuration;
using ELJob.Core.Context;
using ELJob.Domain.Model.Auth;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Auth;
using ELJob.Helper.Common;
using ELJob.Helper.Communication;
using ELJob.Domain.Model.Message;
using ELJob.Helper.Formater;
using Microsoft.AspNetCore.Http;
using System.Linq;
using ELJob.Domain.Model.Employer;
using ELJob.Domain.Model.Views;
using ELJob.Domain.Model.Employee;
using static ELJob.Helper.Common.Constants;
using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Mongo;

namespace ELJob.Business.BL.ServiceImpl
{
   public class AuthBLimpl: IAuthBL
    {
        #region Private declarations
        /// <summary>
        /// Authentication service
        /// </summary>
        readonly IAuthDAO AuthDAOservice;

        /// <summary>
        /// Unit of Work dependecny resolvence
        /// </summary>
        readonly IUnitOfWork _Unitofwork;

        /// <summary>
        /// configuration settings 
        /// </summary>
        readonly IConfiguration configuration;
        #endregion

        #region Constructor
        public AuthBLimpl(ApplicationDbContext context, IUnitOfWork uintofwork, IConfiguration _configuration, IAuthDAO AuthDAOservice)
        {
            this.AuthDAOservice = AuthDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;

        }

        #endregion

        #region Bussiness methods

        /// <summary>
        /// Get the Application user by User Id
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns>Application user profile</returns>
        public ApplicationUser Applicationuser(string mobile) => AuthDAOservice.ServiceRepository<ApplicationUser>().GetFirstOrDefault(x => x.PhoneNumber == mobile);

        /// <summary>
        /// Send the OTP to Requested User
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="OTP"></param>
        /// <returns>Send OTP to Requested User</returns>
        public Response SendOTP(int UserId, OTPModel OTP)
        {
            try
            {
                string RandomOTP = Utility.OTPGenerator();
                
                SMSHelper.MobileNos = OTP.Mobile;
                SMSHelper.Message = string.Format(ResponseMessages.OTPset_to_mobile, RandomOTP);
                SMSHelper.SendSMS(configuration["SMSGateway:SMSUrl"], configuration["SMSGateway:UserName"],
                    configuration["SMSGateway:Password"], configuration["SMSGateway:SMSfrom"],
                    Convert.ToBoolean( configuration["SMSGateway:isSMSSendingEnabled"]));
                
                AuthDAOservice.ServiceRepository<aspnetusertokens>().Add(new aspnetusertokens(){ UserId = UserId, Value = RandomOTP });
                AuthDAOservice.ServiceRepository<Customermessaging>().Add(new Customermessaging { Name="", createdby = UserId, Createddate = DateTime.Now, Type = (int)Constants.CummunicationType.Mobile, Message = string.Format(ResponseMessages.OTPset_to_mobile, RandomOTP) });
                _Unitofwork.Commit();

                return new Response() { StatusCode = StatusCode.Created, Iserror = false, Message = string.Format(ResponseMessages.OTPsendMessage) };
            }
            catch (Exception Ex)
            {
                return new Response() { StatusCode = StatusCode.InternalServerError, Iserror = true, Message = string.Format(ResponseMessages.Error, Ex.Message) };
            }
        }

        /// <summary>
        /// Send forgot password mail
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="forgotpassword"></param>
        /// <param name="message"></param>
        /// <returns>Send email to requested user mail</returns>
        public Response SendforgotEmail(int UserId, Forgotpassword forgotpassword, string message)
        {
            try
            {
                
                EmailHelper.To = forgotpassword.Email;
                EmailHelper.Subject = "Frogot Password";
                EmailHelper.Body = message;
                EmailHelper.IsHtml = true;
                EmailHelper.SendEmailAsync(configuration["EmailConfiguration:SmtpServer"], configuration["EmailConfiguration:SmtpUsername"],
                 configuration["EmailConfiguration:SmtpPassword"], Convert.ToBoolean(configuration["EmailConfiguration:SSLenabled"]),
                 configuration["EmailConfiguration:SmtpPort"].ToInt32()
                 );
                
                AuthDAOservice.ServiceRepository<Customermessaging>().Add(new Customermessaging {createdby = UserId, Createddate = DateTime.Now, Type = (int)Constants.CummunicationType.Email, Message = "mail sent to user" });
                _Unitofwork.Commit();
                
                return new Response() { StatusCode = StatusCodes.Status201Created, Iserror = false, Message = ResponseMessages.Email_sent, Title = Constants.messageTitles.success };
            }
            catch (Exception ex)
            {
                return new Response() { StatusCode = StatusCode.InternalServerError, Iserror = true, Message = string.Format(ResponseMessages.Error, " ") };

            }
        }

        /// <summary>
        /// OTP Validation
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="OTP"></param>
        /// <returns>Boolean either Valid or Invalid</returns>
        public bool IsLoginOTPvalid(int UserId, string OTP) => AuthDAOservice.ServiceRepository<aspnetusertokens>().Query(x => x.UserId == UserId && x.Value == OTP).Count() > 0;

        /// <summary>
        /// Save User Login
        /// </summary>
        /// <param name="UserId"></param>
        public void SaveLogin(int UserId)
        {
            AuthDAOservice.ServiceRepository<tbl_userloginaudit>().Add(new tbl_userloginaudit() { Logindate = DateTime.Now, UserId = UserId });
            _Unitofwork.Commit();
        }

        /// <summary>
        /// Get the employer token
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public string Employertoken(int UserId) => AuthDAOservice.ServiceRepository<Employer>().GetFirstOrDefault(x => x.UserId == UserId).Enc_id;

        /// <summary>
        /// Fetch the Login user screens based on role
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>List os screen assigned to user role</returns>
        public Response GetScreensbyRole(int UserId)
        {
            var vw_screen = AuthDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == UserId);
            var asiginedscreens = AuthDAOservice.ServiceRepository<vw_AssignScreenToRole>().Getfilter(x => x.roleId == vw_screen.RoleId && x.IsActive == true);
            var vw_SubScreen = AuthDAOservice.ServiceRepository<vw_SubScreen>().Getfilter();

            var Result = (from asr in asiginedscreens
                          select new
                          {
                              scree = asr.Screen,
                              route = asr.Route,
                              label = asr.Lable,
                              treeview = asr.Treeview,
                              icon = asr.Icon,
                              subscreen = vw_SubScreen.Where(x => x.screenId == asr.screenId && x.IsActive == true).Select(x => new
                              {
                                  screen = x.Screen,
                                  route = x.Route,
                                  label = x.Lable,
                                  icon = x.Icon,
                                  name = x.SubScreen
                              }).ToList()
                          }).ToList();


            return new Response()
            {
                response = new
                {
                    screens = Result
                },
                StatusCode = StatusCodes.Status200OK,
                Iserror = false,
                Message = "",
                Title = Constants.messageTitles.success
            };

        }

        /// <summary>
        /// Feych the Login user profile
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>Logined used profile</returns>
        public Response UserProfile(int UserId)
        {       
            var Aspnetroles = AuthDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == UserId);
            string Name = "";
            switch (Aspnetroles.RoleId)
            {
                case (int)Userroles.Job_Seeker:
                    Name = AuthDAOservice.ServiceRepository<Aspirant>().GetFirstOrDefault(x => x.UserId == UserId).Name;
                    break;

                case (int)Userroles.Employer:
                    var Employeer = AuthDAOservice.ServiceRepository<Employer>().GetFirstOrDefault(x => x.UserId == UserId);
                    Name = Employeer.FirstName + " " + Employeer.LastName;
                    break;

                default:
                    Name = AuthDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.UserId == UserId).Name;
                    break;

            }

            return new Response()
            {
                response = new
                {
                    name = Name
                },
                StatusCode = StatusCodes.Status200OK,
                Iserror = false,
                Message = "",
                Title = Constants.messageTitles.success,
                name=Name
            };
        }

        public Tuple<bool,bool,string> IsEmployeerprofileupdated(int UserId)
        {
            Tuple<bool,bool, string> profilechech;
            bool IsAddressUpdated = false;
            bool IsSectorsUpdated = true;
            string profilechecknotification = "";
            var Employeer = AuthDAOservice.ServiceRepository<Employer>().GetFirstOrDefault(x => x.UserId == UserId);
            var MEmployeer = AuthDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.EmployerId == Employeer.Id).Result;
            var MEmployeerAddress = AuthDAOservice.ServiceRepository<MEmployerAddress>().mongoGetFirstOrDefault(x => x.MEmployerId == MEmployeer._id.ToString()).Result;

            if (MEmployeerAddress != null)
                IsAddressUpdated = true;

            if (MEmployeer.Sectors!=null&&MEmployeer.Sectors.Count == 0)
            {
                profilechecknotification = "Sectors and GST Details are not Updated.";
                IsSectorsUpdated = false;
            }
               

            profilechech = Tuple.Create(IsAddressUpdated, IsSectorsUpdated,profilechecknotification);
            
            return profilechech;
        }


        #endregion

    }
}
