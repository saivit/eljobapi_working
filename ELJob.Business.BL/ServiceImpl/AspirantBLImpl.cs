﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Aspirant;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Lookup;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Common;
using ELJob.Helper.Communication;
using ELJob.Helper.Formater;
using ELJob.Helper.Grid;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ELJob.Helper.Common.Constants;
using ELJob.Framework.SourceFramework;
using ELJob.Domain.Model.Employee;
using MongoDB.Bson;
using ELJob.Business.Viewmodel.Shared;
using RestSharp;
using Newtonsoft.Json;

namespace ELJob.Business.BL.ServiceImpl
{
    public class AspirantBLImpl : IAspirantBL
    {
        #region Private declarations
        IAspirantDAO aspirantDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;
        IEmployerBL EmployeerBl;

        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IAuthBL Authservice;

        #endregion

        #region Constructor
        public AspirantBLImpl(ApplicationDbContext context, IUnitOfWork uintofwork,
            IConfiguration _configuration, IAspirantDAO aspirantDAOservice, UserManager<ApplicationUser> userManager, IEmployerBL _EmployeerBl, IAuthBL _AuthBlservice)
        {
            this.aspirantDAOservice = aspirantDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;
            this.userManager = userManager;
            EmployeerBl = _EmployeerBl;
            Authservice = _AuthBlservice;

        }


        #endregion

        #region Aspirant BL Operations

        public async Task<Response> AspirantQuickRegistraion(AspirantInfo model)
        {
            try
            {
                var aspnetuser = this.aspirantDAOservice.ServiceRepository<aspnetusers>().Getfilter(x => x.Email == model.Email || x.PhoneNumber == model.Mobile);
                if (aspnetuser.Count() <= 0)
                {
                    ApplicationUser user = new ApplicationUser()
                    {
                        Email = model.Email,
                        SecurityStamp = Guid.NewGuid().ToString(),
                        UserName = model.Email,
                        IsActive = true,
                        PhoneNumber = model.Mobile,
                    };
                    string Password = CommonHelper.Randompassword().ToString();
                    var result = await userManager.CreateAsync(user, Password);
                    if (!result.Succeeded)
                        return new Response
                        {
                            Status = "Error",
                            Message = "User creation failed! Please check user details and try again.",
                            Title = messageTitles.Warning,
                            StatusCode = StatusCode.NoContent
                        };
                    string body = string.Empty;
                    //using streamreader for reading my htmltemplate   
                    using (StreamReader reader = new StreamReader(Path.Combine("Content", "Html", "Register/Job Seeker Registration.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{Username}", model.name+" "+model.LastName).Replace("{email}", model.Email)
                        .Replace("{mail}", configuration.GetSection("ApplicationSettings:supportmail").Value).Replace("{password}", Password).Replace("{employeemail}", model.Email).Replace("{url}", configuration.GetSection("ApplicationSettings:JobseekerLogin").Value)
                         .Replace("{contact}", configuration.GetSection("ApplicationSettings:contactnumber").Value);
                    // model.UserId = user.Id;
                    //  var users = await userManager.FindByNameAsync(userManager.ht.User.Identity.Name);
                    //  Employeeservice.Createemployee(model, users.Id);


                    EmailHelper.To = model.Email;
                    EmailHelper.Subject = "Login Credintails-" + model.name;
                    EmailHelper.Body = body;
                    EmailHelper.IsHtml = true;
                    EmailHelper.SendEmail(configuration.GetSection("EmailConfiguration:SmtpServer").Value, configuration.GetSection("EmailConfiguration:SmtpUsername").Value,
                        configuration.GetSection("EmailConfiguration:SmtpPassword").Value, Convert.ToBoolean(configuration.GetSection("EmailConfiguration:SSLenabled").Value),
                        configuration.GetSection("EmailConfiguration:SmtpPort").Value.ToInt32());

                    var Lookupstatus = this.aspirantDAOservice.ServiceRepository<LookupStatus>().Getfilter();
                    var LookupLocation = this.aspirantDAOservice.ServiceRepository<LookupLocation>().Getfilter();
                    var LookupZone= this.aspirantDAOservice.ServiceRepository<LookupZone>().Getfilter();
                    string Gendername = Lookupstatus.FirstOrDefault(x => x.Id == model.Gender).Name;
                    string Locationname = LookupLocation.FirstOrDefault(x => x.Id == model.Location).Name;
                    string Statusname = Lookupstatus.FirstOrDefault(x => x.Id == Constants.Lookupstatus.New.ToInt32()).Name;
                    string SourceofLeadname = Lookupstatus.FirstOrDefault(x => x.Id == Constants.Lookupstatus.Website.ToInt32()).Name;
                    var userrole = this.aspirantDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == user.Id);
                    int Zone = LookupLocation.FirstOrDefault(x => x.Id == model.Location).Zone;
                    string Zonename = LookupZone.Where(x => x.Id == Zone).FirstOrDefault().Name;
                    

                    if (userrole==null)
                    {
                       await this.aspirantDAOservice.ServiceRepository<aspnetuserroles>().Add(new aspnetuserroles() {
                       UserId= user.Id,
                       RoleId=Userroles.Job_Seeker.ToInt32(),
                           UserId1 = user.Id,
                           RoleId1 = Userroles.Job_Seeker.ToInt32(),
                           Discriminator= Userroles.Job_Seeker.ToString()

                       });
                    }
                    int AspirantCount = this.aspirantDAOservice.ServiceRepository<Aspirant>().Getfilter().Count();
                    var aspirant = await this.aspirantDAOservice.ServiceRepository<Aspirant>().Add(new Aspirant()
                    {
                        Name = model.name,
                        Email = model.Email,
                        Phone = model.Mobile,
                        LocationId = model.Location,
                        GenderId = model.Gender,
                        Createdat = DateTime.Now,
                        Createdby = 0,
                        Gender = Gendername,
                        Location = Locationname,
                        Modifiedat = DateTime.Now,
                        modifiedby = 0,
                        profilefillscore = Profilescores.Quickregistration.ToInt32(),
                        SourceofLead = SourceofLeadname,
                        SourceofLeadId = Constants.Lookupstatus.Website.ToInt32(),
                        statusId = Constants.Lookupstatus.New.ToInt32(),
                        Status = Statusname,
                        Address = "",
                        JobSeekerId = AspirantCount.ToString("JDI000000000"),
                        UserId = user.Id,
                    });
                   await  _Unitofwork.CommitAsyc();

                    await this.aspirantDAOservice.ServiceRepository<Person>().Add(new Person()
                    {
                        Email = model.Email,
                        AspirantId = aspirant.Id,
                        InitalIntraction = 0,
                        LastName = model.LastName,
                        Latestfollowpdate = null,
                        Latestfollowptime = "",
                        MiddleName = "",
                        Mobile = model.Mobile,
                        FirstName = model.name,
                        DOB = null,
                        followpschdule = 0,
                        Profilestatus = 0,
                        RejectionReason = 0,
                        Role = "Job Seeker",
                        RoleId = Userroles.Job_Seeker.ToInt32(),
                        UserId = user.Id,
                        StatusId = Constants.Lookupstatus.New.ToInt32(),
                        Statusname = Statusname
                    });

                    var Maspirantdata = new Domain.Model.Mongo.MAspirant()
                    {
                        Mobile = model.Mobile,
                        Name = model.name,
                        DOB = DateTime.Now,
                        fname = model.name,
                        lname = model.LastName,
                        Age = 0,
                        AspirantId = aspirant.Id,
                        AspirantUserId = user.Id,
                        CreatedDate = DateTime.Now,
                        Email = model.Email,
                        GenderId = model.Gender,
                        GenderText = Gendername,
                        Location = Locationname,
                        LocationId = model.Location,
                        JobseekerId = aspirant.JobSeekerId,
                        score = Profilescores.Quickregistration.ToInt32(),
                        Status = Constants.Lookupstatus.New.ToInt32(),
                        Statusname = Statusname,
                        SourceofLead = SourceofLeadname,
                        SourceofLeadId = Constants.Lookupstatus.Website.ToInt32(),
                        IsEmailvalidated = model.IsEmailvalidated,
                        Zone = Zone,
                        Zonename = Zonename,
                        ProfileDataStatusId = ProfiledataStatus.Regisitered.ToInt32(),
                        ProfileDataStatus = ProfiledataStatustext.Regisitered,
                        ProfileOrder = ProfileOrder.show_in_search.ToInt32(),
                        Ispremium = false,
                        LocationIdarray = new int[] { model.Location },
                        ModifiedDate = DateTime.Now,
                        WorkingstatusId = WorkingExperience.Fresher.ToInt32(),
                        CampaginId = model.campaginid,
                        CampaginEmployeerid = "",
                        UpdatedDate=DateTime.Now,
                        Workingstatus="Fresher",

                    };

                    if(model.campaginid!=string.Empty &&model.campaginid!=null)
                    {
                            string employeerid = AddCampaginAspirant(Maspirantdata, model.campaginid);
                            Maspirantdata.CampaginEmployeerid = employeerid;
                    }
                
                    await this.aspirantDAOservice.ServiceRepository<Domain.Model.Mongo.MAspirant>().mongoAdd(Maspirantdata);

                    await _Unitofwork.CommitAsyc();
                    await aspirantDAOservice.Commitmongo();

                    return new Response()
                    {
                        response = new
                        {

                        },
                        StatusCode = (StatusCodes.Status201Created),
                        CustomerId = Maspirantdata._id.ToString(),
                        Status = SucessMessages.OK,
                        Iserror = false,
                        Message = SucessMessages.Inserted

                    };

                }
                else
                {

                    return new Response()
                    {
                        response = new
                        {

                        },
                        StatusCode = (StatusCodes.Status206PartialContent),
                        CustomerId = "",
                        Status = SucessMessages.Exception,
                        Iserror = false,
                        Title = Constants.messageTitles.Warning,
                        Message = "Already Mobile/Email is Exists.Please click below to Login"

                    };
                }
            }
            catch (Exception Ex)
            {
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = (StatusCodes.Status206PartialContent),
                    CustomerId = "",
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Title = Constants.messageTitles.Warning,
                    Message = Ex.Message

                };
            }
        }

        public async Task<GridResponse> GetJobseeker(PageData page, string Sort, bool descending, string name, string email, string mobile, 
            int location,int zone, int jobseekerId, int status,int profiledatastatus,int Gender,int sourceofLead, DateTime fromdate, DateTime todate)
        {

            //    List<MAspirant> modelist = new List<MAspirant>();
            //    for (int i = 0; i < 170000; i++)
            //    {
            //        modelist.Add(
            //                        new Domain.Model.Mongo.MAspirant()
            //                        {
            //                            Mobile = "66876543211",
            //                            Name = "Hyderabad Raghu",
            //                            Age = 25,
            //                            AspirantId = 21,
            //                            AspirantUserId = 101,
            //                            CreatedDate = DateTime.Now.AddDays(-90),
            //                            Email = "hyderabad@gmail.com",
            //                            GenderId = 5,
            //                            GenderText = "male",
            //                            Location = "Thane",
            //                            LocationId = 1,
            //                            JobseekerId = "JD1234568",
            //                            score = Profilescores.Quickregistration.ToInt32(),
            //                            Status = Constants.Lookupstatus.New.ToInt32(),
            //                            Statusname = "New",
            //                            LatLocation = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
            //                             new GeoJson2DGeographicCoordinates(17.436318440345676, 78.44834121886626))

            //                        });

            //    }

            //    await aspirantDAOservice.ServiceRepository<MAspirant>().mongoAddMany(modelist);

            //    await aspirantDAOservice.Commitmongo();

            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var filter = Builders<MAspirant>.Filter.GeoWithinCenter("LatLocation", 17.436318440345676, 78.44834121886626, 200);

            var filterbuilder = Builders<MAspirant>.Filter.And(
             //  Builders<MAspirant>.Filter.Eq("Location", "Hyderabad")
             Builders<MAspirant>.Filter.Empty

                );

            var eiffelTower = new GeoJson2DGeographicCoordinates(17.430200, 78.449990);

            //find all places within 1km of eiffel tower.

            var result = aspirantDAOservice.ServiceRepository<MAspirant>().Getmongiquerable();

            result = result.Where(x => x.Mobile != "");
            if (status > 0)
                result = result.Where(x => x.Status == status);
            if (zone > 0)
                result = result.Where(x => x.Zone == zone);
            if (location > 0)
                result = result.Where(x => x.LocationId == location);
            if (profiledatastatus > 0)
                result = result.Where(x => x.ProfileDataStatusId == profiledatastatus);
            if (!string.IsNullOrEmpty(name))
                result = result.Where(x => x.Name.ToUpperInvariant().Contains(name.ToUpperInvariant()));
            if (!string.IsNullOrEmpty(email))
                result = result.Where(x => x.Email.ToUpperInvariant().Contains(email.ToUpperInvariant()));
            if (!string.IsNullOrEmpty(mobile))
                result = result.Where(x => x.Mobile.Contains(mobile));
            if (Gender > 0)
                result = result.Where(x => x.GenderId == Gender);
            if (sourceofLead > 0)
                result = result.Where(x => x.SourceofLeadId == sourceofLead);
            if (fromdate.Isdatenull())
                result = result.Where(x => x.CreatedDate >= fromdate);
            if (todate.Isdatenull())
                result = result.Where(x => x.CreatedDate <= todate);


            //var result = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetfilter(filterbuilder);

            double longi = 17.430200;
            gridResponse.pageData = await page.Processasync(result.Count(), skip, limit);
            skip = gridResponse.pageData.selectedPageSize * (gridResponse.pageData.currentPage - 1);
            if (skip < 0) skip = 0;
            var finalresult = result.OrderBy(Sort, descending).Skip(skip).Take(gridResponse.pageData.selectedPageSize);

            gridResponse.rows.AddRange(finalresult
             .Select(x => new
             {
                 name = x.Name,
                 mobile = x.Mobile,
                 email = x.Email,
                 createdat = x.CreatedDate,
                 JobseekerId = x.JobseekerId,
                 statusname = x.Statusname,
                 id = x._id.ToString(),
                 location=x.Location,
                 jobseeker_id = x.AspirantId,
                
                 
                
                 
                 

             }
            )
             );
            //.OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }

        public Response AddAspirantEducation(MAspirantEducation model)
        {
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            var LookupEducation = aspirantDAOservice.ServiceRepository<LookupEducation>().GetFirstOrDefault(x => x.Id == model.Qulification).Name;
            var LookupmodeofEducation = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.ModeofEducation).Name;
            model.QulificationName = LookupEducation;
            model.ModeofEducationName = LookupmodeofEducation;
            model.IsActive = true;
            model.AspirantId = aspirantpersonal.Result.AspirantId;
            LookupOtherEducation Othereducation;
            if(model.Qulification== Qulification.PG_and_Other.ToInt32())
            {

                Othereducation = aspirantDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Id == model.OtherQulification);
                model.OtherQulificationName = Othereducation.Name;
            }

            aspirantDAOservice.ServiceRepository<MAspirantEducation>().mongoAdd(model);
            aspirantDAOservice.Commitmongo();

            var MJobseekerEducation = aspirantDAOservice.ServiceRepository<MAspirantEducation>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId &&x.IsActive==true);
            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Educationlist = MJobseekerEducation.Select(x => new Selectionvalues()
            {
                Id = x.Qulification,
                Name = x.QulificationName,
            }).ToList();
            //check is PG & Other Education.

            
                var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_qualification, Educationlist).Set(x => x.Qualification, Educationlist.Select(x => x.Id).ToArray());
            if (model.Qulification == Qulification.PG_and_Other.ToInt32())
            {

                updateJobseekrBuilder = updateJobseekrBuilder.Set(x=>x.AditionalQualification, MJobseekerEducation.Where(x=>x.OtherQulification>0 && x.IsActive == true).Select(x=>x.OtherQulification).ToArray()).
                    Set(x => x.lst_additionalqulification, MJobseekerEducation.Where(x => x.OtherQulification > 0 &&x.IsActive==true).Select(x =>new Selectionvalues() { Id = x.OtherQulification, Name = x.QulificationName, IsActive = true }).ToList())
                    ;
            }
          
            if ( MJobseekerEducation == null)
            {
                updateJobseekrBuilder.Set(x => x.score, (aspirantpersonal.Result.score + Profilescores.euducational.ToInt32()));
            }
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);

            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }
        public Response UpdateAspirantEducation(MAspirantEducation model, string Id)
        {
            var LookupEducation = aspirantDAOservice.ServiceRepository<LookupEducation>().GetFirstOrDefault(x => x.Id == model.Qulification).Name;
            var LookupmodeofEducation = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.ModeofEducation).Name;
            if (model.Qulification == Qulification.PG_and_Other.ToInt32())
                model.OtherQulificationName = aspirantDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Id == model.OtherQulification).Name;

            var filter = Builders<MAspirantEducation>.Filter.Eq(x => x.Id, ObjectId.Parse(Id));
            var update = Builders<MAspirantEducation>.Update.Set(x => x.Institue, model.Institue).Set(x => x.Qulification, model.Qulification)
                .Set(x => x.QulificationName, LookupEducation).Set(x => x.Completedyear, model.Completedyear).Set(x => x.OtherQulification, model.OtherQulification)
                .Set(x => x.OtherQulificationName, model.OtherQulificationName).Set(x=>x.ModeofEducation,model.ModeofEducation).Set(x=>x.ModeofEducationName,LookupmodeofEducation);

            aspirantDAOservice.ServiceRepository<MAspirantEducation>().mongoUpdateasync(filter, update);
            aspirantDAOservice.Commitmongo();

            var MJobseekerEducation = aspirantDAOservice.ServiceRepository<MAspirantEducation>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId && x.IsActive == true);
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));

            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Educationlist = MJobseekerEducation.Select(x => new Selectionvalues()
            {
                Id = x.Qulification,
                Name = x.QulificationName
            }).ToList();
          
            if(model.OtherQulification>0)
            { 
               model.OtherQulificationName = aspirantDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Id == model.OtherQulification).Name;
            }
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_qualification, Educationlist)
                .Set(x => x.Joblookingcity, Educationlist.Select(x => x.Id).ToArray());
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };

        }


        public Response RemoveAspirantEducation(string Id)
        {   
            var filter = Builders<MAspirantEducation>.Filter.Eq(x => x.Id, ObjectId.Parse(Id));
            var update = Builders<MAspirantEducation>.Update.Set(x => x.IsActive, false);
            aspirantDAOservice.ServiceRepository<MAspirantEducation>().mongoUpdateasync(filter, update);
            aspirantDAOservice.Commitmongo();
            var MJobseekerEducationbyId = aspirantDAOservice.ServiceRepository<MAspirantEducation>().mongoGetFirstOrDefault(x => x.Id == ObjectId.Parse(Id));

            var MJobseekerEducation = aspirantDAOservice.ServiceRepository<MAspirantEducation>().Getmongiquerable().Where(x => x.MJobseekerId == MJobseekerEducationbyId.Result.MJobseekerId);
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(MJobseekerEducationbyId.Result.MJobseekerId));

            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(MJobseekerEducationbyId.Result.MJobseekerId));

            var Educationlist = MJobseekerEducation.Select(x => new Selectionvalues()
            {
                Id = x.Qulification,
                Name = x.QulificationName
            }).ToList();
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_qualification, Educationlist).Set(x => x.Joblookingcity, Educationlist.Select(x => x.Id).ToArray());
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Removed
            };
        }

        public Response GetAspirantEduList(string Id)
        {
            try
            {
                var qualification = aspirantDAOservice.ServiceRepository<MAspirantEducation>().Getmongiquerable().Where(x => x.MJobseekerId == Id && x.IsActive == true).ToList().Select(x => new
                {


                    Id = x.Id.ToString(),
                    qulification = x.Qulification,
                    modeofEducation = x.ModeofEducation,
                    modeofEducationName=x.ModeofEducationName,
                    completedyear = x.Completedyear,
                    institue = x.Institue,
                    quilificationname = x.QulificationName,
                    otherQulification = x.OtherQulification,
                    otherQulificationname=x.OtherQulificationName

                });

                return new Response()
                {
                    response = new
                    {
                        model = qualification

                    },
                    StatusCode = StatusCodes.Status200OK,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = SucessMessages.ScuessLoad
                };
            }
            catch (Exception Ex)
            {
                return new Response() { StatusCode = StatusCodes.Status500InternalServerError, Status = "Error", Message = "An Error Has Occured " + Ex.Message };

            }
        }




        public Response AddAspirantLanguages(MJobseekerLanguge model)
        {
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            var LookupLanguage = aspirantDAOservice.ServiceRepository<LookupLanguage>().GetFirstOrDefault(x => x.Id == model.Language).Name;
            model.LanguageName = LookupLanguage;
            model.IsActive = true;
            model.AspirantId = aspirantpersonal.Result.AspirantId;
            aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().mongoAdd(model);
            aspirantDAOservice.Commitmongo();

            var MJobseekerLanguages = aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId && x.IsActive == true);


            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Languageslist = MJobseekerLanguages.Select(x => new Selectionvalues()
            {
                Id = x.Language,
                Name = x.LanguageName
            }).ToList();
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_Languages, Languageslist).Set(x => x.Language, Languageslist.Select(x => x.Id).ToArray());
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }
        public Response UpdateAspirantLanguages(MJobseekerLanguge model, string Id)
        {
            var LookupLanguage = aspirantDAOservice.ServiceRepository<LookupLanguage>().GetFirstOrDefault(x => x.Id == model.Language).Name;
            var filter = Builders<MJobseekerLanguge>.Filter.Eq(x => x._id, ObjectId.Parse(Id));
            var update = Builders<MJobseekerLanguge>.Update.Set(x => x.Language, model.Language).Set(x => x.LanguageName, LookupLanguage)
                .Set(x => x.Readwrite, model.Readwrite);

            aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().mongoUpdateasync(filter, update);
            aspirantDAOservice.Commitmongo();

            var MJobseekerEducation = aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId && x.IsActive == true);
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));

            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Languagelist = MJobseekerEducation.Select(x => new Selectionvalues()
            {
                Id = x.Language,
                Name = x.LanguageName
            }).ToList();
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_Languages, Languagelist).Set(x => x.Language, Languagelist.Select(x => x.Id).ToArray());
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };

        }


        public Response RemoveAspirantLanguages(string Id)
        {
            var filter = Builders<MJobseekerLanguge>.Filter.Eq(x => x._id, ObjectId.Parse(Id));
            var update = Builders<MJobseekerLanguge>.Update.Set(x => x.IsActive, false);
            aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().mongoUpdateasync(filter, update);
            aspirantDAOservice.Commitmongo();
            var MJobseekerEducationbyId = aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(Id));

            var MJobseekerEducation = aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().Getmongiquerable().Where(x => x.MJobseekerId == MJobseekerEducationbyId.Result.MJobseekerId);
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(MJobseekerEducationbyId.Result.MJobseekerId));

            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(MJobseekerEducationbyId.Result.MJobseekerId));

            var languagelist = MJobseekerEducation.Select(x => new Selectionvalues()
            {
                Id = x.Language,
                Name = x.LanguageName
            }).ToList();
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_qualification, languagelist).Set(x => x.Joblookingcity, languagelist.Select(x => x.Id).ToArray());
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Removed
            };
        }

        public Response GetAspirantLanguages(string Id)
        {
            var qualification = aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().Getmongiquerable().Where(x => x.MJobseekerId == Id && x.IsActive==true).Select(x => new
            {
                Id = x._id.ToString(),
                Language = x.Language,
                Readwrite = x.Readwrite,
                LanguageName = x.LanguageName,
            });

            return new Response()
            {
                response = new
                {
                    model = qualification

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }


        public Response AddAspirantExperience(MAspirantExperience model)
        {

            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            model.AspirantId = aspirantpersonal.Result.AspirantId;
            model.IsActive = true;
            aspirantDAOservice.ServiceRepository<MAspirantExperience>().mongoAdd(model);

            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Workexperince = aspirantDAOservice.ServiceRepository<MAspirantExperience>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId);
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.Experience, WorkingExperience.Experience.ToInt32()).Set(x => x.Workingstatus, "Experience");

            if (Workexperince == null)
            {
                updateJobseekrBuilder.Set(x => x.score, aspirantpersonal.Result.score + Profilescores.workexperince.ToInt32());
            }
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();


            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }

        public Response UpdateAspirantExperience(MAspirantExperience model, string Id)
        {

            var Jobseekerfilter = Builders<MAspirantExperience>.Filter.Eq(x => x.Id, ObjectId.Parse(Id));
            var updateJobseekrBuilder = Builders<MAspirantExperience>.Update.Set(x => x.Organization, model.Organization).Set(x => x.IscurrentEmployer, model.IscurrentEmployer)
                .Set(x => x.Workingfrom, model.Workingfrom).Set(x => x.WorkingTo, model.WorkingTo).Set(x=>x.Duration,model.Duration);
            aspirantDAOservice.ServiceRepository<MAspirantExperience>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Updated
            };

        }

        public Response RemoveAspirantExperience(string Id)
        {
            var Jobseekerfilter = Builders<MAspirantExperience>.Filter.Eq(x => x.Id, ObjectId.Parse(Id));
            var updateJobseekrBuilder = Builders<MAspirantExperience>.Update.Set(x => x.IsActive, false);
            aspirantDAOservice.ServiceRepository<MAspirantExperience>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Experience Removed Successfully"

            };
        }

        public Response AddAspirantPersonal(MAspirantPersonal model)
        {
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));
            var asppersonal = aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoGetFirstOrDefault(x => x.MJobseekerId == model.MJobseekerId).Result;
            int Age = (int)((DateTime.Now - model.DOB).TotalDays) / 365;
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.Age, Age).Set(x => x.DOB, model.DOB);
            var Lookuplocarion = aspirantDAOservice.ServiceRepository<LookupLocation>().Getfilter();
            var LookupReason=aspirantDAOservice.ServiceRepository<LookupStatus>().Getfilter();
            var Lookupcity = aspirantDAOservice.ServiceRepository<LookupCity>().Getfilter();
            var Lookupstate = aspirantDAOservice.ServiceRepository<Lookupstate>().Getfilter();

            model.Reason_Name1 = ""; model.Reason_Name2 = ""; model.Reason_Name3 = "";
            model.Location1_Name = ""; model.Location2_Name = ""; model.Location3_Name = "";

           
            List<Selectionvalues> lstlocation = new List<Selectionvalues>();
            List<int> Locations = new List<int>();
            if(model.readToMigrateLocation)
            {
                if (model.Location1 > 0)
                {
                    string Location1_name = Lookuplocarion.Where(x => x.Id == model.Location1).FirstOrDefault().Name;
                    lstlocation.Add(new Selectionvalues()
                    {
                        Id = model.Location1,
                        Name = Location1_name
                    });
                    Locations.Add(model.Location1);
                    model.Location1_Name = Location1_name;
                }

                // Location 2

                if (model.Location2 > 0)
                {
                    string Location2_name = Lookuplocarion.Where(x => x.Id == model.Location2).FirstOrDefault().Name;
                    lstlocation.Add(new Selectionvalues()
                    {
                        Id = model.Location2,
                        Name = Location2_name
                    });
                    Locations.Add(model.Location2);
                    model.Location2_Name = Location2_name;
                }

                //Location 3

                if (model.Location3 > 0)
                {
                    string Location3_name = Lookuplocarion.Where(x => x.Id == model.Location3).FirstOrDefault().Name;
                    lstlocation.Add(new Selectionvalues()
                    {
                        Id = model.Location3,
                        Name = Location3_name
                    });
                    Locations.Add(model.Location3);
                    model.Location3_Name = Location3_name;
                }


                //Reason 1

                if (model.Reason1 > 0)
                {
                    string Reason_Name1 = LookupReason.Where(x => x.Id == model.Reason1).FirstOrDefault().Name;
                    //lstlocation.Add(new Selectionvalues()
                    //{
                    //    Id = model.Reason1,
                    //    Name = Reason_Name1
                    //});
                    //Locations.Add(model.Reason1);
                    model.Reason_Name1 = Reason_Name1;
                }

                //Reason 2

                if (model.Reason2 > 0)
                {
                    string Reason_Name2 = LookupReason.Where(x => x.Id == model.Reason2).FirstOrDefault().Name;
                    //lstlocation.Add(new Selectionvalues()
                    //{
                    //    Id = model.Reason2,
                    //    Name = Reason_Name2
                    //});
                    //Locations.Add(model.Reason2);
                    model.Reason_Name2 = Reason_Name2;
                }
                //Reason 3
                if (model.Reason3 > 0)
                {
                    string Reason_Name3 = LookupReason.Where(x => x.Id == model.Reason3).FirstOrDefault().Name;
                    //lstlocation.Add(new Selectionvalues()
                    //{
                    //    Id = model.Reason3,
                    //    Name = Reason_Name3
                    //});
                    //Locations.Add(model.Reason3);
                    model.Reason_Name3 = Reason_Name3;
                }

            }
            else
            {
                model.Location1 = 0; model.Location2 = 0; model.Location3 = 0;
                model.Reason1 = 0; model.Reason2 = 0; model.Reason3 = 0;
                
            }


            List<Selectionvalues> lstcity = new List<Selectionvalues>();
            List<int> cities = new List<int>();
            if (model.City > 0)
            {
                string City_Name = Lookupcity.Where(x => x.Id == model.City).FirstOrDefault().Name;
                lstcity.Add(new Selectionvalues()
                {
                    Id = model.City,
                    Name = City_Name
                });
                cities.Add(model.City);

                model.City_Name = City_Name;

                if (model.Reason3 > 0)
                {
                    string Reason_Name3 = LookupReason.Where(x => x.Id == model.Reason3).FirstOrDefault().Name;
                }

                   

                lstlocation.Add(new Selectionvalues()
                {
                    Id = model.City,
                    Name = City_Name
                });
                Locations.Add(model.City);

            }

            List<Selectionvalues> lststate = new List<Selectionvalues>();
            List<int> states = new List<int>();
            if (model.State > 0)
            {
                string State_Name = Lookupstate.Where(x => x.Id == model.State).FirstOrDefault().Name;
                lststate.Add(new Selectionvalues()
                {
                    Id = model.State,
                    Name = State_Name
                });
                states.Add(model.State);
                model.State_Name = State_Name;
            }

            if (asppersonal == null)
            {
                
                aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoAdd(model);
               
                updateJobseekrBuilder.Set(x => x.score, aspirantpersonal.Result.score + Profilescores.personaldetails.ToInt32());
                aspirantDAOservice.Commitmongo();
            }
            else
            { 

            var filter = Builders<MAspirantPersonal>.Filter.Eq(x => x.MJobseekerId, model.MJobseekerId);
            var update = Builders<MAspirantPersonal>.Update.Set(x => x.Housenumber, model.Housenumber)
                .Set(x => x.streetName, model.streetName).Set(x => x.Latitude, model.Latitude)
                .Set(x => x.Longitude, model.Longitude).Set(x => x.City, model.City).Set(x => x.State, model.State)
                .Set(x => x.Pincode, model.Pincode).Set(x => x.readToMigrateLocation, model.readToMigrateLocation).Set(x => x.State_Name, model.State_Name)
                .Set(x => x.City_Name, model.City_Name).Set(x => x.DOB, model.DOB).Set(x=>x.UpdatedDate,model.UpdatedDate).Set(x => x.Age, Age)
                .Set(x => x.Location1, model.Location1).Set(x => x.Location1_Name, model.Location1_Name)
                .Set(x => x.Location2, model.Location2).Set(x => x.Location2_Name, model.Location2_Name)
                .Set(x => x.Location3, model.Location3).Set(x => x.Location3_Name, model.Location3_Name)
                .Set(x => x.Reason1, model.Reason1).Set(x => x.Reason_Name1, model.Reason_Name1)
                .Set(x => x.Reason2, model.Reason1).Set(x => x.Reason_Name2, model.Reason_Name2)
                .Set(x => x.Reason_Name3, model.Reason_Name3);
                 
                aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoUpdateasync(filter, update);

           
            }
            
            if (Locations.Count() > 0)
            {
                updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.lst_Jobblookingcity, lstlocation).Set(x => x.Joblookingcity, Locations.ToArray());
                lstlocation.Add(new Selectionvalues() { Id = aspirantpersonal.Result.LocationId, Name = aspirantpersonal.Result.Location });
                Locations.Add(aspirantpersonal.Result.LocationId);
            }
            updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.LatLocation, new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
                                      new GeoJson2DGeographicCoordinates(Convert.ToDouble(model.Longitude),Convert.ToDouble(model.Latitude))));
            
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };



        }

        public Response UpdateAspirantPersonal(MAspirantPersonal model)
        {

            var asppersonal = aspirantDAOservice.ServiceRepository<MAspirantPersonal>().GetFirstOrDefault();

            asppersonal.FirstName = model.FirstName;
            aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoUpdate(asppersonal, model);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }

        public Response RemoveAspirantPersonal(int Id)
        {
            var aspexpremove = aspirantDAOservice.ServiceRepository<AspirantPersonal>().GetFirstOrDefault(x => x.Id == Id);
            //aspexpremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Personal Removed Successfully"

            };
        }

        public Response AddSkillAssessment(MAspirantSkillAssessment model)
        {
            var aspirantskill = aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoGetFirstOrDefault(x => x.MJobseekerId == model.MJobseekerId).Result;

            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));

            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));
            string Objective = "";
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_sectors, model.lst_sectors)
                .Set(x => x.Sector, model.lst_sectors.Select(x => x.Id).ToArray()).Set(x=>x.SalaryRange,model.Salary).Set(x => x.Salary, model.Salary);

            var Lookupstatus = aspirantDAOservice.ServiceRepository<LookupStatus>().Getfilter();

            model.Apptitude_name = Lookupstatus.Where(x => x.Id == model.Apptitude).FirstOrDefault().Name;
            model.CommunicativeEnglish_name = Lookupstatus.Where(x => x.Id == model.CommunicativeEnglish).FirstOrDefault().Name;
            model.Computeroperations_name = Lookupstatus.Where(x => x.Id == model.Computeroperations).FirstOrDefault().Name;

            List<Selectionvalues> lstsectors = new List<Selectionvalues>();
            List<int> sectors = new List<int>();

            if (aspirantskill == null)
            {
                updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.score, aspirantpersonal.Result.score + Profilescores.skill_assessment.ToInt32());
                aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoAdd(model);
            }
            else
            {
                var JobseekerSkillfilter = Builders<MAspirantSkillAssessment>.Filter.Eq(x => x.MJobseekerId, model.MJobseekerId);
                var updateJobseekrskillBuilder = Builders<MAspirantSkillAssessment>.Update.Set(x => x.lst_sectors, model.lst_sectors)
                    .Set(x => x.NotIntrestRoles, model.NotIntrestRoles)
                    .Set(x => x.Salary, model.Salary).Set(x => x.Computeroperations_name, model.Computeroperations_name)
                    .Set(x => x.Apptitude_name, model.Apptitude_name).Set(x => x.CommunicativeEnglish_name, model.CommunicativeEnglish_name)
                    .Set(x => x.CommunicativeEnglish, model.CommunicativeEnglish).Set(x => x.Apptitude, model.Apptitude)
                    .Set(x => x.Computeroperations, model.Computeroperations);

                aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoUpdateasync(JobseekerSkillfilter, updateJobseekrskillBuilder);
            }

            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);

            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }

        public Response UpdateSkillAssessment(MAspirantSkillAssessment model)
        {
            var asppersonal = aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().GetFirstOrDefault();

            aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoUpdate(asppersonal, model);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };

        }

        public Response RemoveAspirantSkillAssement(int Id)
        {
            var aspexpremove = aspirantDAOservice.ServiceRepository<AspirantSkillAssessment>().GetFirstOrDefault(x => x.Id == Id);
            //aspexpremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Skill Removed Successfully"

            };
        }

        public Response GetAspirantbyId(string AspirantId)
        {
            var MAspirant = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(AspirantId)).Result;
            var Person = aspirantDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.AspirantId == MAspirant.AspirantId);
            var Aspirant = aspirantDAOservice.ServiceRepository<Aspirant>().GetFirstOrDefault(x => x.Id == MAspirant.AspirantId);
            var Aspirants = aspirantDAOservice.ServiceRepository<Aspirant>().Query();
            var Persons = aspirantDAOservice.ServiceRepository<Person>().Query();
            var Reschdulehistory = aspirantDAOservice.ServiceRepository<MAspirantResshduleHistory>().Getmongiquerable().Where(x => x.AspirantId == MAspirant.AspirantId);

            var nextusers = (from asp in Aspirants
                             join p in Persons on asp.Id equals p.AspirantId
                             where asp.LocationId == Aspirant.LocationId
                             && asp.statusId == Aspirant.statusId
                             select new
                             {
                                 Id = asp.Id,
                                 Name = p.FirstName + " " + p.MiddleName + " " + p.LastName,
                                 createdat = asp.Createdat,
                                 JobseekerId = asp.JobSeekerId,
                                 mobile = Person.Mobile,
                                 location = asp.Location
                             }).Skip(0).Take(10).ToList();

            return new Response()
            {
                response = new
                {
                    Person = Person,
                    Aspirant = Aspirant,
                    nextusers = nextusers,
                    Reschdulehistory = Reschdulehistory.ToList()
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }

        public Response Reschduleledslot(AspirantReschdule model, int UserId)
        {

            try
            {

                DateTime Reschduledate = new DateTime();
                Reschduledate = Reschduledate.AddDays(model.ReschduleDate.Day);
                Reschduledate = Reschduledate.AddMonths(model.ReschduleDate.Month);
                Reschduledate = Reschduledate.AddYears(model.ReschduleDate.Year);
                var Maspirant = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.Id)).Result;

                var Person = aspirantDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.AspirantId == Maspirant.AspirantId);
                var Aspirant = aspirantDAOservice.ServiceRepository<Aspirant>().GetFirstOrDefault(x => x.Id == Maspirant.AspirantId);

                var Lookupstatus = this.aspirantDAOservice.ServiceRepository<LookupStatus>().Getfilter();
                string Statusname = Lookupstatus.FirstOrDefault(x => x.Id == Constants.Lookupstatus.Rescheduled.ToInt32()).Name;
                string reschulereason = Lookupstatus.FirstOrDefault(x => x.Id == model.Reschdule).Name;
                string Sourceofleadname = Lookupstatus.Where(x => x.Id == model.SourceofLeadId).FirstOrDefault().Name;

                Person.StatusId = Constants.Lookupstatus.Rescheduled.ToInt32();
                Person.Statusname = Statusname;
                Aspirant.statusId = Constants.Lookupstatus.Rescheduled.ToInt32();
                Aspirant.Status = Statusname;
                Aspirant.SourceofLeadId = model.SourceofLeadId;
                Aspirant.SourceofLead = Sourceofleadname;
                aspirantDAOservice.Commit();

                var Employee = aspirantDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.UserId == UserId);

                var filter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.Id));
                var update = Builders<MAspirant>.Update.Set(x => x.Status, Constants.Lookupstatus.Rescheduled.ToInt32()).Set(x=>x.Processedby,UserId).Set(x=>x.processedbyName, Employee.Name)
                    .Set(x => x.Statusname, Statusname).Set(x => x.ReschduleReasonId, Constants.Lookupstatus.Rescheduled.ToInt32())
                    .Set(x => x.ReschduleReason, reschulereason).Set(x => x.SourceofLeadId, model.SourceofLeadId).Set(x => x.SourceofLead, Sourceofleadname).Set(x=>x.Reschduletime,model.Resechduletime).Set(x=>x.ReschduleDate, Reschduledate);
                aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(filter, update);


                aspirantDAOservice.ServiceRepository<MAspirantResshduleHistory>().Add(new MAspirantResshduleHistory()
                {
                    AspirantId = Aspirant.Id,
                    Createdat = DateTime.Now,
                    Createdby = Employee.Name,
                    Name = Aspirant.Name,
                    JobseekerId =Maspirant._id.ToString(),
                    Reschduledate = Reschduledate,
                    Reschduletime = model.Resechduletime,
                    RescduleId = model.Reschdule,
                    ReschduleReason = Lookupstatus.Where(x => x.Id == model.Reschdule).FirstOrDefault().Name
                });

                aspirantDAOservice.Commitmongo();



                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Reschdule Successfully!"

                };
            }
            catch (Exception Ex)
            {

                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Something Went to Wrong-" + Ex.Message

                };
            }

        }

        public Response RejectAspirant(AspirantRejectionReason model, int UserId)
        {
            try
            {
                var Maspirant = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.Id)).Result;
                var Person = aspirantDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.AspirantId == Maspirant.AspirantId);
                var Aspirant = aspirantDAOservice.ServiceRepository<Aspirant>().GetFirstOrDefault(x => x.Id == Maspirant.AspirantId);
                var Lookupstatus = this.aspirantDAOservice.ServiceRepository<LookupStatus>().Getfilter();
                string Statusname = Lookupstatus.FirstOrDefault(x => x.Id == Constants.Lookupstatus.Rejected.ToInt32()).Name;
                string Sourceofleadname = Lookupstatus.Where(x => x.Id == model.SourceofLeadId).FirstOrDefault().Name;

                string Rejectedreason = Lookupstatus.FirstOrDefault(x => x.Id == model.RejectionReason).Name;
                Person.StatusId = Constants.Lookupstatus.Rejected.ToInt32();
                Person.Statusname = Statusname;
                Aspirant.statusId = Constants.Lookupstatus.Rejected.ToInt32();
                Aspirant.Status = Statusname;
                Aspirant.modifiedby = UserId;
                Aspirant.Modifiedat = DateTime.Now;
                Aspirant.SourceofLeadId = model.SourceofLeadId;
                Aspirant.SourceofLead = Sourceofleadname;
                Aspirant.rejectionReason = model.RejectionReason;
                Aspirant.rejectionReason_name = Rejectedreason;
                Aspirant.otherRejectionReason = model.otherRejectionReason;

                aspirantDAOservice.Commit();
                var Employee = aspirantDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.UserId == UserId);

                var filter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.Id));
                var update = Builders<MAspirant>.Update.Set(x => x.Status, Constants.Lookupstatus.Rejected.ToInt32()).Set(x => x.Processedby, UserId).Set(x => x.processedbyName, Employee.Name)
                    .Set(x => x.Statusname, Statusname).Set(x => x.rejectReasonId, Constants.Lookupstatus.Rejected.ToInt32())
                    .Set(x => x.rejectReason, Rejectedreason).Set(x => x.SourceofLeadId, model.SourceofLeadId)
                    .Set(x => x.SourceofLead, Sourceofleadname).Set(x=>x.otherRejectionReason,model.otherRejectionReason);
                try
                {
                    aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(filter, update);
                    aspirantDAOservice.Commitmongo();
                }
                catch (Exception Ex)
                {
                    string Exce = Ex.Message;
                }



                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Rejected Successfully!"

                };
            }
            catch (Exception Ex)
            {

                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Something Went to Wrong-" + Ex.Message

                };
            }
        }

        public Response ProceedTocounclling(string Id, int UserId, int SourceofLead)
        {
            try
            {

                var Maspirant = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(Id)).Result;

                var Person = aspirantDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.AspirantId == Maspirant.AspirantId);
                var Aspirant = aspirantDAOservice.ServiceRepository<Aspirant>().GetFirstOrDefault(x => x.Id == Maspirant.AspirantId);
                var Lookupstatus = this.aspirantDAOservice.ServiceRepository<LookupStatus>().Getfilter();
                string Statusname = Lookupstatus.FirstOrDefault(x => x.Id == Constants.Lookupstatus.Inprocess.ToInt32()).Name;
                string Sourceofleadname = Lookupstatus.Where(x => x.Id == SourceofLead).FirstOrDefault().Name;
                Person.StatusId = Constants.Lookupstatus.Inprocess.ToInt32();
                Person.Statusname = Statusname;
                Aspirant.statusId = Constants.Lookupstatus.Inprocess.ToInt32();
                Aspirant.Status = Statusname;
                Aspirant.modifiedby = UserId;
                Aspirant.SourceofLeadId = SourceofLead;
                Aspirant.SourceofLead = Sourceofleadname;
                Aspirant.Modifiedat = DateTime.Now;

                aspirantDAOservice.Commit();

                var Employee = aspirantDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.UserId == UserId);

                var filter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(Id));
                var update = Builders<MAspirant>.Update.Set(x => x.Status, Constants.Lookupstatus.Inprocess.ToInt32()).Set(x => x.Processedby, UserId).Set(x => x.processedbyName, Employee.Name)
                    .Set(x => x.Statusname, Statusname).Set(x => x.SourceofLead, Sourceofleadname).Set(x => x.SourceofLeadId, SourceofLead);
                aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(filter, update);
                aspirantDAOservice.Commitmongo();

                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Moved to Inprocess!"

                };
            }
            catch (Exception Ex)
            {

                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Something Went to Wrong-" + Ex.Message

                };
            }
        }

        public Response GetAspirantExpbyId(int Id)
        {
            var getExpbyId = aspirantDAOservice.ServiceRepository<AspirantExperince>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = getExpbyId
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response AddAspirantCertification(MAspirantCertification model)
        {

            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            model.AspirantId = aspirantpersonal.Result.AspirantId;
            model.IsActive = true;
            aspirantDAOservice.ServiceRepository<MAspirantCertification>().mongoAdd(model);
            aspirantDAOservice.Commitmongo();
            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Certifications = aspirantDAOservice.ServiceRepository<MAspirantCertification>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId).ToList();
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.Certification, "Certified");

            var certificates = Certifications.Select(x => new Selectionvalues() { Id = 0, Name = x.Institute }).ToList();
            if (Certifications.Count() > 0)
            {
                updateJobseekrBuilder.Set(x => x.lst_certificates, certificates);
            }
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };


        }

        public Response UpdateAspirantCertification(MAspirantCertification model, string Id)
        {
            var Jobseekerfilter = Builders<MAspirantCertification>.Filter.Eq(x => x.Id, ObjectId.Parse(Id));

            var updateJobseekrBuilder = Builders<MAspirantCertification>.Update.Set(x => x.Institute, model.Institute).Set(x => x.Certification, model.Certification)
                .Set(x => x.Completedyear, model.Completedyear).Set(x => x.Duration, model.Duration);
            aspirantDAOservice.ServiceRepository<MAspirantCertification>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();

            var JobseekerAspirantfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Certifications = aspirantDAOservice.ServiceRepository<MAspirantCertification>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId);
            var updateJobseekrAspirantBuilder = Builders<MAspirant>.Update.Set(x => x.Certification, "Certified");

            if (Certifications.Count() == 0 || Certifications == null)
            {
                updateJobseekrAspirantBuilder.Set(x => x.lst_certificates, Certifications.Select(x => new Selectionvalues() { Id = 0, Name = x.Institute }).ToList());
            }
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(JobseekerAspirantfilter, updateJobseekrAspirantBuilder);
            aspirantDAOservice.Commitmongo();


            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Updated
            };
        }

        public Response RemoveAspirantCertification(string Id)
        {
            var Jobseekerfilter = Builders<MAspirantCertification>.Filter.Eq(x => x.Id, ObjectId.Parse(Id));
            var updateJobseekrBuilder = Builders<MAspirantCertification>.Update.Set(x => x.IsActive, false);
            aspirantDAOservice.ServiceRepository<MAspirantCertification>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Certification Removed Successfully"

            };
        }



        public Response GetAspirantExpList(string Id)
        {
            var experience = aspirantDAOservice.ServiceRepository<MAspirantExperience>().Getmongiquerable().Where(x => x.MJobseekerId == Id && x.IsActive==true).Select(x => new
            {
                Id = x.Id.ToString(),
                Organization = x.Organization,
                Workingfrom = x.Workingfrom,
                WorkingTo = x.WorkingTo,
                duration = x.Duration,

            });

            return new Response()
            {
                response = new
                {
                    model = experience
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetAspirantCertificationList(string Id)
        {
            var certification = aspirantDAOservice.ServiceRepository<MAspirantCertification>().Getmongiquerable().Where(x => x.MJobseekerId == Id && x.IsActive==true).Select(x => new
            {
                Id = x.Id.ToString(),
                Certification = x.Certification,
                Completedyear = x.Completedyear,
                Duration = x.Duration,
                Institute = x.Institute



            });

            return new Response()
            {
                response = new
                {
                    model = certification
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }


        public Response GetAspirantSkillList(string Id)
        {
            var certification = aspirantDAOservice.ServiceRepository<AspirantSkillAssessment>().mongoGetFirstOrDefault(x => x.MJobseekerId == Id);

            return new Response()
            {
                response = new
                {
                    model = certification
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
        public Response GetJobseekerbyId(string Id)
        {
            var Maspirant = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(Id)).Result;
            var jobseekerbyid = aspirantDAOservice.ServiceRepository<Aspirant>().GetFirstOrDefault(x => x.Id == Maspirant.AspirantId);

            return new Response()
            {
                response = new
                {
                    model = jobseekerbyid,
                    Maspirant = Maspirant,
                    actions = CheckProfiledata(Id),
                    storagepath=configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value
                   
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
        public async Task<Response> Upload(IFormFile file, string MJobseekr, string documentType)
        {
            if (file == null || file.Length == 0)
            {
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = StatusCodes.Status206PartialContent,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = "Please select the file"
                };
            }
            var Jobseekr = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(MJobseekr)).Result;
            string Sesionresourcepath = "JobseekerDocuments";
            var DBstoragelocation = Path.Combine(Sesionresourcepath, Jobseekr.AspirantUserId.ToString(), documentType, file.FileName);
            var storagepath = Path.Combine(configuration.GetSection("ApplicationSettings:fileStoragePath").Value,Sesionresourcepath, Jobseekr.AspirantUserId.ToString(), documentType);
            var filepath = Path.Combine(storagepath,file.FileName);

            if (!Directory.Exists(storagepath))
            {
                Directory.CreateDirectory(storagepath);
            }

            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }
            
            if (!File.Exists(filepath))
            {
                using (var stream = new FileStream(filepath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                var documents = aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().mongoGetFirstOrDefault(x => x.JobseekerId == MJobseekr &&x.Documentype==documentType).Result;
                if (documents == null)
                {
                    await aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().mongoAdd(new MJobseekerDocuments()
                    {
                        Uploadedtime = DateTime.Now,
                        Documentname = file.FileName,
                        Documentype = documentType,
                        fileextension = file.ContentType,
                        filesize = file.Length,
                        JobseekerName = Jobseekr.Name,
                        Filpath = DBstoragelocation,
                        JobseekerId = MJobseekr
                    });             
                }
                else
                {
                    var documentId = aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().mongoGetFirstOrDefault(x => x.JobseekerId == MJobseekr && x.Documentype == documentType).Result;
                    var Jobseekerdocuments = Builders<MJobseekerDocuments>.Filter.Eq(x => x._id, documentId._id);
                    var updateJobseekrdouumentbuilder = Builders<MJobseekerDocuments>.Update.Set(x => x.Uploadedtime, DateTime.Now)
                        .Set(x => x.Documentname, file.FileName)
                        .Set(x => x.fileextension, file.ContentType)
                        .Set(x => x.Filpath, DBstoragelocation);
                    aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().mongoUpdateasync(Jobseekerdocuments, updateJobseekrdouumentbuilder);
                }

                if (documentType == Docuemnts.profilepicture)
                {
                    this.UpdateJobseekrProfilePicture(DBstoragelocation, file.FileName, MJobseekr);
                }

                await aspirantDAOservice.Commitmongo();
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = StatusCodes.Status200OK,
                    Message = SucessMessages.Updated,
                    Iserror = false,
                    Status = SucessMessages.Inserted
                };
            }
            else
            {
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = StatusCodes.Status206PartialContent,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = "Same file already avilable  for this session"
                };
            }
        }
        public Response GetAspirantcerificationById(int Id)
        {
            var getCertifibyId = aspirantDAOservice.ServiceRepository<AspirantCertification>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = getCertifibyId
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
        public Response GetAspirantEducationById(int Id)
        {
            var getEducationbyId = aspirantDAOservice.ServiceRepository<AspirantEducation>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = getEducationbyId
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
        public Response GetAspirantSkillbyId(string Id)
        {
            var getskiklbyId = aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoGetFirstOrDefault(x => x.MJobseekerId == Id);
            return new Response()
            {
                response = new
                {
                    model = getskiklbyId.Result
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
        public Response GetAspirantPersonalbyId(int Id)
        {
            var getpersonalbyId = aspirantDAOservice.ServiceRepository<AspirantPersonal>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = getpersonalbyId
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
        public Response AddAspirantObjective(MAspirantObjective model)

        {

            var aspirantobj = aspirantDAOservice.ServiceRepository<MAspirantObjective>().mongoGetFirstOrDefault(x => x.MJobseekerId == model.MJobseekerId).Result;
            //var aspirantobjective = aspirantDAOservice.ServiceRepository<MAspirantObjective>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            //model.AspirantId = aspirantobjective.Result.AspirantId;

            var objective = aspirantDAOservice.ServiceRepository<LookupObjective>().GetFirstOrDefault(x => x.Id == model.Objective).Name;
            model.ObjectiveName = objective;

            if (aspirantobj == null)
            {

                aspirantDAOservice.ServiceRepository<MAspirantObjective>().mongoAdd(model);

                aspirantDAOservice.Commitmongo();
            }
            else
            {
                var JobseekerOBJECTIVEfilter = Builders<MAspirantObjective>.Filter.Eq(x => x.MJobseekerId, model.MJobseekerId);
                var updateJobseekrskillBuilder = Builders<MAspirantObjective>.Update.Set(x => x.Objective, model.Objective)
                    .Set(x => x.ObjectiveName, model.ObjectiveName);


                aspirantDAOservice.ServiceRepository<MAspirantObjective>().mongoUpdateasync(JobseekerOBJECTIVEfilter, updateJobseekrskillBuilder);
            }
            

            aspirantDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }
        public Response UpdateAspirantObjective(MAspirantObjective model, string Id)
        {
            var AspirantObjectivefilter = Builders<MAspirantObjective>.Filter.Eq(x => x._id, ObjectId.Parse(Id));
            var updateAspirantObjfilterBuilder = Builders<MAspirantObjective>.Update.Set(x => x.Objective, model.Objective).Set(x => x.ObjectiveName, model.ObjectiveName);

            aspirantDAOservice.ServiceRepository<MAspirantObjective>().mongoUpdateasync(AspirantObjectivefilter, updateAspirantObjfilterBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Updated
            };
        }
        public Response GetAspirantObjectivebyId(string Id)
        {
            var aspirantbobjectivebyid = aspirantDAOservice.ServiceRepository<MAspirantObjective>().Getmongiquerable().Where(x => x.MJobseekerId == Id).Select(x => new
            {
                Id = x._id.ToString(),
                objectivename=x.ObjectiveName,
                Objective=x.Objective

            });

            return new Response()
            {
                response = new
                {
                    model = aspirantbobjectivebyid
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
        public Response GetAspirantPersonalList(string Id)
        {
            try
            {
                var PersonalList = aspirantDAOservice.ServiceRepository<MAspirantPersonal>().Getmongiquerable().Where(x => x.MJobseekerId == Id);
                return new Response()
                {
                    response = new
                    {
                        model = PersonalList
                    },
                    StatusCode = StatusCodes.Status200OK,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = SucessMessages.ScuessLoad
                };
            }
            catch(Exception Ex)
            {
                Console.WriteLine($"Error in Personal" + (nameof(AuthBLimpl)));
            }
            

            return new Response()
            {
                response = new
                {
                   
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
        public Response UpdateProfileTopFields(Updateprofiletopfields model, string Id)
        {
            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(Id));
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.fname, model.fname).Set(x => x.lname, model.lname)
                .Set(x => x.DOB, model.DOB).Set(x => x.Mobile, model.Mobile);
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Updated
            };

        }
        public Response GetJobseekeerDocuments(string Id, string documentType)
        {
            var Result = aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().Getmongiquerable().Where(x => x.JobseekerId == Id && x.Documentype == documentType)
                .Select(x => new { 
                id=x._id.ToString(),
                    Filpath = x.Filpath,
                    Documentname = x.Documentname
                })
                ;
            return new Response()
            {
                response = new
                {
                    model = Result
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }
        public async Task<Response> DownloadJobseekerDocuments(string ResourceId)
        {
            var sessionresources = aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(ResourceId)).Result;
            if (sessionresources == null)
            {
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = StatusCodes.Status206PartialContent,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = "File Respurces is not exists"


                };
            }
            else
            {


                var memory = new MemoryStream();
                var downloadpath = Path.Combine(configuration.GetSection("ApplicationSettings:fileStoragePath").Value, sessionresources.Filpath);
                using (var stream = new FileStream(downloadpath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;

                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = StatusCodes.Status200OK,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = SucessMessages.Inserted,
                    stream = memory,
                    Filename = sessionresources.Documentname,
                    filetype = sessionresources.fileextension
                };
            }
        }
        public Response CompleteProfile(string Id)
        {
            var Exceptionsdata = CheckProfiledata(Id);
            var Exceptionsdocuments = CheckDocuments(Id);
            List<Exceptions> exceptions = new List<Exceptions>();
            exceptions.AddRange(Exceptionsdata);
            
            exceptions.AddRange(Exceptionsdocuments);

            if(exceptions.Count()==0)
            {
                var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(Id));
                string statusname = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == Lookupstatus.Completed.ToInt32()).Name;
                var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.Status, Lookupstatus.Completed.ToInt32()).Set(x => x.Statusname, statusname);

                aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
                aspirantDAOservice.Commitmongo();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.Updated
                };

            }
            else
            {

                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = true,
                    Message = SucessMessages.Updated,
                    validations=exceptions
                };
            }


           
        }
        public Response YetToSubmitdocuments(string Id)
        {
            var Exceptionsdata = CheckProfiledata(Id);
           
            List<Exceptions> exceptions = new List<Exceptions>();
           
            exceptions = Exceptionsdata;

            if (exceptions.Count() == 0)
            {
                var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(Id));
                string statusname = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == Lookupstatus.Yet_to_Submit_document.ToInt32()).Name;
                var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.Status, Lookupstatus.Yet_to_Submit_document.ToInt32()).Set(x => x.Statusname, statusname);

                aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
                aspirantDAOservice.Commitmongo();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.Updated
                };

            }
            else
            {

                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = true,
                    Message = SucessMessages.Updated,
                    validations = exceptions
                };
            }

        }
        public async Task<Response> Emailverfication(string Id)
        {
            try
            {
                var aspnetuser = this.aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(Id)).Result;
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   
                using (StreamReader reader = new StreamReader(Path.Combine("Content", "Html", "Register/Job Seeker Email verfication.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Username}", aspnetuser.Name)
                 .Replace("{url}", string.Format(configuration.GetSection("ApplicationUrls:Emailverififcation").Value, Id))
                     .Replace("{contact}", configuration.GetSection("ApplicationSettings:contactnumber").Value);
                EmailHelper.To = aspnetuser.Email;
                EmailHelper.Subject = "Emai Verfication for-" + aspnetuser.Email;
                EmailHelper.Body = body;
                EmailHelper.IsHtml = true;
                EmailHelper.SendEmail(configuration.GetSection("EmailConfiguration:SmtpServer").Value, configuration.GetSection("EmailConfiguration:SmtpUsername").Value,
                    configuration.GetSection("EmailConfiguration:SmtpPassword").Value, Convert.ToBoolean(configuration.GetSection("EmailConfiguration:SSLenabled").Value),
                    configuration.GetSection("EmailConfiguration:SmtpPort").Value.ToInt32()
                    );
                return await Task.FromResult(new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = (StatusCodes.Status201Created),
                    CustomerId = "",
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Your Email Verfication link sent to your register mail. Please check your mail"
                });

            }
            catch (Exception Ex)
            {
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = (StatusCodes.Status206PartialContent),
                    CustomerId = "",
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Title = Constants.messageTitles.Warning,
                    Message = Ex.Message

                };
            }
        }
        public Response GetaspinrantIDbyMobile(string Mobile)
        {
            var getaspirantmobile = aspirantDAOservice.ServiceRepository<Aspirant>().GetFirstOrDefault(x => x.Phone == Mobile);
            if (getaspirantmobile != null)
            {

                var Aspiranjobseekerid = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x.AspirantId == getaspirantmobile.Id).Result;

                return new Response()
                {
                    response = new
                    {
                        model1 = getaspirantmobile.Id,
                        model2 = Aspiranjobseekerid._id.ToString()
                    },
                    StatusCode = StatusCodes.Status200OK,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = SucessMessages.ScuessLoad
                };
            }
            else
            {

                return new Response()
                {
                    StatusCode = StatusCodes.Status206PartialContent,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Mobile Number Not exits",
                };
            }


        }
        public async Task<Response> ExportResume(string JobseekerId, bool Mailrequest, bool IsEmployeer,int UserId)
        {
            if (IsEmployeer)
            {
                var Profileviewcount = await EmployeerBl.CheckLimitBeforeExport(new List<string>() { JobseekerId }, UserId);
                if (!Profileviewcount.ISAuthoruizetoviewProfiles)
                {
                    return Profileviewcount;
                }
            }
            var CAMhost = new RestClient(configuration.GetSection("DLCPDF:Host").Value);
            var CAMreuest = new RestRequest(configuration.GetSection("DLCPDF:AspirantResume").Value, Method.POST);

            CAMreuest.RequestFormat = DataFormat.Json;
            CAMreuest.AddHeader("Content-Type", "application/json");

            var maspirantresult = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(JobseekerId)).Result;
            var personalresult = aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoGetFirstOrDefault(x => x.MJobseekerId == JobseekerId).Result;
            var objectiveresult = aspirantDAOservice.ServiceRepository<MAspirantObjective>().mongoGetFirstOrDefault(x => x.MJobseekerId == JobseekerId).Result;
            var educationresult = aspirantDAOservice.ServiceRepository<MAspirantEducation>().Getmongiquerable().Where(x => x.MJobseekerId == JobseekerId).ToList()
                .Select(x => new
                {
                    Particulars = x.QulificationName,
                    Year = x.Completedyear,
                    Institute = x.Institue,
                    Mode = x.ModeofEducationName
                });

            var othereducation = aspirantDAOservice.ServiceRepository<MAspirantCertification>().Getmongiquerable().Where(x => x.MJobseekerId == JobseekerId).ToList()
                 .Select(x => new
                 {
                     Particulars = x.Certification,
                     Year = x.Completedyear,
                     Institute = x.Institute,
                     Duration = x.Duration

                 });
            var languagesresult = aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().Getmongiquerable().Where(x => x.MJobseekerId == JobseekerId).ToList()
                .Select(x => new
                {
                    Language = x.LanguageName,
                    Speak = x.Readwrite.Where(y => y.Id == LanguageGrip.Speak.ToInt32()).Select(y => y.Name).FirstOrDefault(),
                    Read = x.Readwrite.Where(y => y.Id == LanguageGrip.Read.ToInt32()).Select(y => y.Name).FirstOrDefault(),
                    Write = x.Readwrite.Where(y => y.Id == LanguageGrip.Write.ToInt32()).Select(y => y.Name).FirstOrDefault(),

                });
            var documnetsresult = aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().Getmongiquerable().Where(x => x.JobseekerId == JobseekerId);


            var documents = new
            {
                addressProof = documnetsresult.Where(x => x.Documentype == Docuemnts.addressProof).FirstOrDefault()!=null? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value+documnetsresult.Where(x => x.Documentype == Docuemnts.addressProof).FirstOrDefault().Filpath:string.Empty,
                Euducational = documnetsresult.Where(x => x.Documentype == Docuemnts.Euducational).FirstOrDefault()!=null? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.Euducational).FirstOrDefault().Filpath:string.Empty,
                additinalEducation = documnetsresult.Where(x => x.Documentype == Docuemnts.additinalEducation).FirstOrDefault()!=null?configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.additinalEducation).FirstOrDefault().Filpath:string.Empty,
                workexperinceCertificate = documnetsresult.Where(x => x.Documentype == Docuemnts.workexperinceCertificate).FirstOrDefault()!=null?configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.workexperinceCertificate).FirstOrDefault().Filpath:string.Empty,
                profilepicture = documnetsresult.Where(x => x.Documentype == Docuemnts.profilepicture).FirstOrDefault()!=null? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.profilepicture).FirstOrDefault().Filpath:string.Empty,
                identityDocument = documnetsresult.Where(x => x.Documentype == Docuemnts.identityDocument).FirstOrDefault()!=null?configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.identityDocument).FirstOrDefault().Filpath:string.Empty,
                dirivingLicence = documnetsresult.Where(x => x.Documentype == Docuemnts.dirivingLicence).FirstOrDefault()!=null? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.dirivingLicence).FirstOrDefault().Filpath:string.Empty,
            };


            var pesrsonal = new
            {

                Name = maspirantresult.Name,
                lbl_Contact = "Contact",
                contact = maspirantresult.Mobile,
                lbl_Email = "Email Id",
                Email = maspirantresult.Email,
                Address = personalresult.Housenumber,
                Address2 = personalresult.streetName,
                City = personalresult.City_Name,
                lbl_State = "State",
                State = personalresult.State_Name,
                lbl_Pincode = "Pincode",
                Pincode = personalresult.Pincode,
                DOB = personalresult.DOB.ToString("dd/MM/yyyy"),
                Objective = objectiveresult.ObjectiveName

            };

            var Request = new
            {
                pesrsonal = pesrsonal,
                education = educationresult.ToList(),
                otherqualification = othereducation.ToList(),
                languages = languagesresult.ToList(),
                documents = documents,

            };


            CAMreuest.AddJsonBody(Request);
            System.Net.ServicePointManager.Expect100Continue = false;
            var camresponseexecution = CAMhost.Execute(CAMreuest);
            var ResponseFund = JsonConvert.DeserializeObject<ReturnUitlityPDFResponse>(camresponseexecution.Content);
            if(IsEmployeer)
            {
                EmployeerBl.Updateprofileviewcount(new List<string> { JobseekerId }, UserId);
            }
            if(Mailrequest)
            {
                string PDFFileName = string.Format("{0}-Resume.pdf",personalresult.FirstName+" "+personalresult.LasttName);


                string Sesionresourcepath = "JobseekerDocuments";
               
                var storagepath = Path.Combine(configuration.GetSection("ApplicationSettings:fileStoragePath").Value, Sesionresourcepath, maspirantresult.AspirantUserId.ToString(), Docuemnts.resume);
                var filepath = Path.Combine(storagepath, PDFFileName);

                if (!Directory.Exists(storagepath))
                {
                    Directory.CreateDirectory(storagepath);
                }

                if (File.Exists(filepath))
                {
                    File.Delete(filepath);
                }

                if (!File.Exists(filepath))
                {
                    using (FileStream fs = new FileStream(filepath, FileMode.Create))
                    {
                        fs.Write(ResponseFund.Response, 0, ResponseFund.Response.Length);
                    }
                }
                //EmailHelper.To = "harilakkakula28@gmail.com";
                EmailHelper.To = pesrsonal.Email;
                EmailHelper.FileName = filepath;
                EmailHelper.Subject = "Your ELJobs Resume";
                EmailHelper.SendEmail(configuration.GetSection("EmailConfiguration:SmtpServer").Value, configuration.GetSection("EmailConfiguration:SmtpUsername").Value,
                        configuration.GetSection("EmailConfiguration:SmtpPassword").Value, Convert.ToBoolean(configuration.GetSection("EmailConfiguration:SSLenabled").Value),
                        configuration.GetSection("EmailConfiguration:SmtpPort").Value.ToInt32());
                //test

                if (File.Exists(filepath))
                {
                    File.Delete(filepath);
                }
            
            }
         

            MemoryStream stream = new MemoryStream(ResponseFund.Response);

            return new Response()
            {
                response = ResponseFund.Response,
                StatusCode = (StatusCodes.Status200OK),
                CustomerId = "",
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.OK,
                stream = stream

            };
        }

        public Response UploadDocuments(List<MJobseekerDocuments> model)
        {
            try
            {
                var profilepicture = model.Where(x => x.Documentype == Docuemnts.profilepicture).FirstOrDefault();
                if(profilepicture!=null)
                {
                    this.UpdateJobseekrProfilePicture(profilepicture.Filpath, profilepicture.Documentname, profilepicture.JobseekerId);
                }
                 aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().mongoAddMany(model);
                 aspirantDAOservice.Commitmongo();
                return new Response()
                {
                   
                    StatusCode = (StatusCodes.Status201Created),
                    CustomerId = "",
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.OK,
                   

                };

            }
            catch (Exception Ex)
            {
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = (StatusCodes.Status206PartialContent),
                    CustomerId = "",
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Title = Constants.messageTitles.Warning,
                    Message = Ex.Message

                };
            }

        }

        public Response UpdateWorkingstatus(string JobseekerId, int workingstatus)
        {
            string workingstatusname = workingstatus == WorkingExperience.Experience.ToInt32() ? "Experince" : "Fresher";
            var Jobseekerdocuments = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(JobseekerId));
            var updateJobseekrdouumentbuilder = Builders<MAspirant>.Update.Set(x => x.WorkingstatusId, workingstatus).Set(x => x.Workingstatus, workingstatusname);
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerdocuments, updateJobseekrdouumentbuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                CustomerId = "",
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Updated,
            };
        }

        public int GetWorkingstatus(string JobseekrId)
        {
            return aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(JobseekrId)).Result.WorkingstatusId;
        }

        public Response GetaspnetuserIDbyMobile(string Mobile)
        {
            var getaspnetusers = aspirantDAOservice.ServiceRepository<aspnetusers>().GetFirstOrDefault(x => x.PhoneNumber == Mobile);

            return new Response()
            {
                response = new
                {
                    model = getaspnetusers.Id

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };

        }

        public Response UpdatePreimumUser(string JobseekrId)
        {
            var Jobseekerdocuments = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(JobseekrId));
            var updateJobseekrdouumentbuilder = Builders<MAspirant>.Update.Set(x => x.Ispremium,true).Set(x => x.ProfileOrder, Constants.ProfileOrder.preimumUser.ToInt32());
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerdocuments, updateJobseekrdouumentbuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.Updated
            };

        }

        public async Task<GridResponse> GetPreimumJobseekers(PageData page, string Sort, bool descending, string name, string email, string mobile, int location, int zone, int jobseekerId, int status, int profiledatastatus, int Gender, int sourceofLead, DateTime fromdate, DateTime todate)
        {

            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;

            var filterbuilder = Builders<MAspirant>.Filter.And(
             //  Builders<MAspirant>.Filter.Eq("Location", "Hyderabad")
             Builders<MAspirant>.Filter.Empty

                );

            var eiffelTower = new GeoJson2DGeographicCoordinates(17.430200, 78.449990);

            //find all places within 1km of eiffel tower.

            var result = aspirantDAOservice.ServiceRepository<MAspirant>().Getmongiquerable();

            result = result.Where(x => x.Ispremium == true);
            if (status > 0)
                result = result.Where(x => x.Status == status);
            if (zone > 0)
                result = result.Where(x => x.Zone == zone);
            if (location > 0)
                result = result.Where(x => x.LocationId == location);
            if (profiledatastatus > 0)
                result = result.Where(x => x.ProfileDataStatusId == profiledatastatus);
            if (!string.IsNullOrEmpty(name))
                result = result.Where(x => x.Name.ToUpperInvariant().Contains(name.ToUpperInvariant()));
            if (!string.IsNullOrEmpty(email))
                result = result.Where(x => x.Email.ToUpperInvariant().Contains(email.ToUpperInvariant()));
            if (!string.IsNullOrEmpty(mobile))
                result = result.Where(x => x.Mobile.Contains(mobile));
            if (Gender > 0)
                result = result.Where(x => x.GenderId == Gender);
            if (sourceofLead > 0)
                result = result.Where(x => x.SourceofLeadId == sourceofLead);
            if (fromdate.Isdatenull())
                result = result.Where(x => x.CreatedDate >= fromdate);
            if (todate.Isdatenull())
                result = result.Where(x => x.CreatedDate <= todate);

            gridResponse.pageData = await page.Processasync(result.Count(), skip, limit);
            skip = gridResponse.pageData.selectedPageSize * (gridResponse.pageData.currentPage - 1);
            if (skip < 0) skip = 0;
            var finalresult = result.OrderBy(Sort, descending).Skip(skip).Take(gridResponse.pageData.selectedPageSize);

            gridResponse.rows.AddRange(finalresult
             .Select(x => new
             {
                 name = x.Name,
                 mobile = x.Mobile,
                 email = x.Email,
                 createdat = x.CreatedDate,
                 JobseekerId = x.JobseekerId,
                 statusname = x.Statusname,
                 id = x._id.ToString(),
                 location = x.Location,
                 jobseeker_id = x.AspirantId
             }
            )
             );
            //.OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }


        public Response LastUpdatedDate(string Id)
        {
            var MAspirant = Builders<MAspirant>.Filter.Eq(x =>x._id, ObjectId.Parse(Id));
            var updateAspirantbuilder = Builders<MAspirant>.Update.Set(x => x.UpdatedDate, DateTime.Now);
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(MAspirant, updateAspirantbuilder);
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.Updated
            };
        }
        #endregion

        #region Private methods
        /// <summary>
        /// CHeck the Personal Parameters
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        private List<Exceptions> CheckProfiledata(string JobseekrId)
        {
            List<Exceptions> validations = new List<Exceptions>();

            //Education
            int Educationcount = aspirantDAOservice.ServiceRepository<MAspirantEducation>().Getmongiquerable().Where(x => x.MJobseekerId == JobseekrId &&x.IsActive==true).Count();
            if (Educationcount == 0)
                validations.Add(new Exceptions() {validation="Educational Details is Required" });
            //Skill assessment
            var skillassement = aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoGetFirstOrDefault(x => x.MJobseekerId == JobseekrId).Result;
            if (skillassement == null)
                validations.Add(new Exceptions() { validation = "Skill Assessment Details is Required" });
            //Objective
            var objective = aspirantDAOservice.ServiceRepository<MAspirantObjective>().mongoGetFirstOrDefault(x => x.MJobseekerId == JobseekrId).Result;
            if (objective == null)
                validations.Add(new Exceptions() { validation = "Objective is Required" });
            //Language
            int Languagecount = aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().Getmongiquerable().Where(x => x.MJobseekerId == JobseekrId && x.IsActive == true).Count();
            if (Educationcount == 0)
                validations.Add(new Exceptions() { validation = "Language Details is Required" });

            //Personal
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoGetFirstOrDefault(x => x.MJobseekerId == JobseekrId).Result;
            if (aspirantpersonal == null)
                validations.Add(new Exceptions() { validation = "Personal Details is Required" });

            return validations;

        }

        /// <summary>
        /// Check Documents
        /// </summary>
        /// <param name="JobseekrId"></param>
        /// <returns></returns>
        private List<Exceptions> CheckDocuments(string JobseekrId)
        {
            List<Exceptions> validations = new List<Exceptions>();

            //Resume
            int Resume = aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().Getmongiquerable().Where(x => x.JobseekerId == JobseekrId && x.Documentype == Docuemnts.resume).Count();
            if (Resume == 0)
                validations.Add(new Exceptions() { validation = "Resume is Required,Please Upload Resume" });

            int Addressdocument = aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().Getmongiquerable().Where(x => x.JobseekerId == JobseekrId && x.Documentype == Docuemnts.addressProof).Count();
            if (Addressdocument == 0)
                validations.Add(new Exceptions() { validation = "Address proof is Required,Please Upload Address Proof" });

            int ProfilePicture = aspirantDAOservice.ServiceRepository<MJobseekerDocuments>().Getmongiquerable().Where(x => x.JobseekerId == JobseekrId && x.Documentype == Docuemnts.profilepicture).Count();
            if (ProfilePicture == 0)
                validations.Add(new Exceptions() { validation = "Profile Picture is Required,Please Upload Profile Picture" });

            return validations;

        }

       private void UpdateJobseekrProfilePicture(string filepath,string profilepic_altname,string JobseekrId)
        {          
            var Jobseekerdocuments = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(JobseekrId));
            var updateJobseekrdouumentbuilder = Builders<MAspirant>.Update.Set(x => x.Profilepicture, filepath).Set(x=>x.Profilepicture_altname,profilepic_altname);
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerdocuments, updateJobseekrdouumentbuilder);
        }

        public string GetLoginUserJobseekerId(int UserId)
        {
            var Jobseeker= aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x.AspirantUserId == UserId).Result;
            if (Jobseeker!=null)
            {
                return Jobseeker._id.ToString();
            }
            return "";
        }


        private string AddCampaginAspirant(MAspirant aspirant,string campaginId )
        {
            var Mcampagin = aspirantDAOservice.ServiceRepository<MCampaignmaster>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(campaginId)).Result;
            aspirantDAOservice.ServiceRepository<MCampginAspirants>().mongoAdd(new MCampginAspirants() { 
            AspirantId=aspirant.AspirantId,
            AspirantUserId=aspirant.AspirantUserId,
            campgainId= campaginId,
            CreatedDate=DateTime.Now,
            DOB=aspirant.DOB,
            Email=aspirant.Email,
            EmployeerId= Mcampagin.EmployeerId,
            Organazation= Mcampagin.Organizationname,
            Name=aspirant.Name,
            ModifiedDate=DateTime.Now,
            Employeername=Mcampagin.Name,
            EmployeeruserId=Mcampagin.EmployeerUserId,
            GenderId=aspirant.GenderId,
            fname=aspirant.fname,
            GenderText=aspirant.GenderText,
            JobseekerId=aspirant._id.ToString(),
            lname=aspirant.lname,
            Location=aspirant.Location,
            Mobile=aspirant.Mobile,
            LocationId=aspirant.LocationId
            });

            return Mcampagin.EmployeerId;
        }




        #endregion

    }
}
