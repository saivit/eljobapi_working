﻿using DocumentFormat.OpenXml.Bibliography;
using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Auth;
using ELJob.Business.Viewmodel.Employeer;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Employer;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Grid;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ELJob.Helper.Common.Constants;
using ELJob.Framework.SourceFramework;
using ELJob.Helper.Common;
using ELJob.Domain.Model.Lookup;
using ELJob.Business.Viewmodel.Shared;
using MongoDB.Bson;
using MongoDB.Driver.GeoJsonObjectModel;
using System.IO;
using ELJob.Helper.Communication;
using ELJob.Helper.Formater;
using RestSharp;
using Newtonsoft.Json;
using ELJob.Domain.Model.Employee;
using System.IO.Compression;
using static ELJob.Helper.Common.Foramats;



namespace ELJob.Business.BL.ServiceImpl
{
    /// <summary>
    /// Implemented the All employeer Business Logics. 
    /// </summary>
    public class EmployerBLImpl : IEmployerBL
    {

        #region Private declarations
        IEmployerDAO employerDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;
        #endregion

        #region Constructor
        public EmployerBLImpl(ApplicationDbContext context, IUnitOfWork uintofwork,
            IConfiguration _configuration, IEmployerDAO employerDAOservice)
        {
            this.employerDAOservice = employerDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;

        }

        public Response AddEmployer(EmployerRegisterModel model, int UserId)
        {
            if(model.CreatedBy>0)
            {
                var employer = employerDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.UserId == model.CreatedBy);
                model.CreatedByName = employer.Name;
            }
           
            var Employeer = new Employer()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Mobile = model.Mobile,
                Organizationtype = model.Organizationtype,
                OrganizationtypeId = model.OrganizationtypeId,
                Organization = model.Organization,
                Designation = model.Designation,
                CurrentPlan = model.CurrentPlan,
                Validity = model.Validity,
                Price = model.Price,
                GSTSNumber = model.GSTSNumber,
                planstart = model.PlanStart,
                planend = model.PlanEnd,
                Profileviews = model.Profileviews,
                totalCVdownloads = model.totalCVdownloads,
                tillnowCVdownload = model.tillnowCVdownload,
                tillnowprofileviews = model.tillnowprofileviews,
                profilepicname = model.profilepicname,
                Profilepictype = model.Profilepictype,
                Profilepicpath = model.Profilepicpath,
                CreatedByName = model.CreatedByName,
                BoradcastEmailSMS = model.BoradcastEmailSMS,
                SMSTemplate = model.SMSTemplate,
                UserId = model.Id,
                Enc_id = model.Id.TostringEncrypt()
            };
            var Employerrsult = employerDAOservice.ServiceRepository<Employer>().Add(Employeer);
            this.AddUserrole(model.Id, Constants.Userroles.Employer.ToInt32(), "Employer");
            employerDAOservice.Commit();
            var EmployeerMongo = new MEmployer()
            {

                FirstName = model.FirstName,
                LastName = model.LastName,
                EmployerId = Employerrsult.Result.Id,
                Email = model.Email,
                Mobile = model.Mobile,
                Organizationtype = model.Organizationtype,
                OrganizationtypeId = model.OrganizationtypeId,
                Organization = model.Organization,
                Designation = model.Designation,
                CurrentPlan = model.CurrentPlan,
                Validity = model.Validity,
                Price = model.Price,
                GSTSNumber = model.GSTSNumber,
                planstart = model.PlanStart,
                planend = model.PlanEnd,
                Profileviews = model.Profileviews,
                totalCVdownloads = model.totalCVdownloads,
                tillnowCVdownload = model.tillnowCVdownload,
                tillnowprofileviews = model.tillnowprofileviews,
                profilepicname = model.profilepicname,
                Profilepictype = model.Profilepictype,
                Profilepicpath = model.Profilepicpath,
                CreatedByName = model.CreatedByName,
                BoradcastEmailSMS = model.BoradcastEmailSMS,
                SMSTemplate = model.SMSTemplate,
                UserId = model.Id,
                Createddate = DateTime.Now,
                Enc_id = model.Id.TostringEncrypt(),
                isSelfregistration = false,
                CreatedBy = UserId,
                status = Employeerstatus.EmployeerRegistered.ToInt32(),
                statename = Employeerstatustext.EmployeerRegistered

            };
            employerDAOservice.ServiceRepository<MEmployer>().mongoAdd(EmployeerMongo);
            employerDAOservice.Commitmongo();
            AddFreeplan(EmployeerMongo._id.ToString(), model);
            employerDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };


        }

        public Response GetEmployeerOrganizationaldetails(int userId)
        {
            var employer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == userId).Result;
            //var employeraddress = employerDAOservice.ServiceRepository<MEmployerAddress>().mongoGetFirstOrDefault(x => x.EmployerId == userId).Result;
            var employeraddress = employerDAOservice.ServiceRepository<MEmployerAddress>().mongoGetFirstOrDefault(x => x.MEmployerId == employer._id.ToString()).Result;
            var EmployeerOrganization = new EmployerOrganizationalModel()
            {
                Sectors = employer.Sectors != null ? employer.Sectors.Select(x => x.Id).ToArray() : null,
                designation = employer.Designation,
                GST = employer.GSTStatus,
                GSTnumber = employer.GSTSNumber,
                orgnizaionname = employer.Organization

            };
            EmployerAddress EmployeerAddress = new EmployerAddress();
            if (employeraddress != null)
            {
                EmployeerAddress = new EmployerAddress()
                {
                    Address = employeraddress.Address,
                    Addressstwo = employeraddress.Addressstwo,
                    city = employeraddress.city,
                    pincode = employeraddress.pincode,
                    state = employeraddress.state
                };
            }



            return new Response()
            {
                response = new
                {
                    employer = employer,
                    EmployeerOrganization = EmployeerOrganization,
                    EmployeerAddress = EmployeerAddress,
                    profilepath= configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value+ employer.Profilepicpath
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }

        public Response GetEmployeerProfile(int UserId)
        {
            var Employeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
            var MEmployeer = employerDAOservice.ServiceRepository<MAspirant>().Getmongiquerable();
            var Employeeraddress = employerDAOservice.ServiceRepository<MEmployerAddress>().mongoGetFirstOrDefault(x => x.MEmployerId == Employeer._id.ToString()).Result;
            var filterbuilder = Builders<MAspirant>.Filter.Empty;

            if (Employeeraddress.city != null && Employeeraddress.city!=0)
                filterbuilder &= (Builders<MAspirant>.Filter.AnyIn(x => x.Joblookingcity,new int[] { Employeeraddress.city }));
            
            if (Employeer.Sectors != null && Employeer.Sectors.Count() > 0)
                filterbuilder &= (Builders<MAspirant>.Filter.AnyIn(x => x.Sector, Employeer.Sectors.Select(x => x.Id).ToArray()));

            var Profilesquery = employerDAOservice.ServiceRepository<MAspirant>().mongoGetfilter(filterbuilder).Result;
            var Profiles = Profilesquery.Skip(0).Take(100).ToList().Select(x => new
            {
                name = x.Name,
                Id = x._id.ToString(),
                gender = x.GenderText,
                experince = x.Workingstatus,
                education = x.lst_qualification,
                sectors = x.lst_sectors,
                aditonalqualification = x.lst_additionalqulification,
                checkedvalue = false,
                profilepicture = (x.Profilepicture != string.Empty && x.Profilepicture != null) ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + x.Profilepicture : configuration.GetSection("ApplicationSettings:Deflutuserimage").Value,
                isEmployeerview = x.SearchedEmployeer != null ? x.SearchedEmployeer.Where(y => y.EmployeerId.Contains(Employeer._id.ToString())).Count() > 0 ? true : false : false,
                jobseekrinfo = x.SearchedEmployeer != null ? (x.SearchedEmployeer.Where(y => y.EmployeerId.Contains(Employeer._id.ToString())).Count() > 0) ? new List<object> { x.Mobile, x.Email } : new List<object> { } : new List<object> { },
                lastactivedate = x.UpdatedDate

            });
            return new Response()
            {
                response = new
                {
                    model = Employeer,
                    profile = Profiles,
                    profilepic = configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value+ Employeer.Profilepicpath
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }

        public async Task<GridResponse> GetEmpyolyeer(PageData page, string Sort, bool descending, string name, string email, string mobile, string location, int employeerId, int status)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var filter = Builders<MAspirant>.Filter.GeoWithinCenter("LatLocation", 17.436318440345676, 78.44834121886626, 200);

            var filterbuilder = Builders<MAspirant>.Filter.And(
             //  Builders<MAspirant>.Filter.Eq("Location", "Hyderabad")
             Builders<MAspirant>.Filter.Empty

                );

            var places = employerDAOservice.ServiceRepository<MEmployer>().Getmongiquerable();

            places = places.Where(x => x.Mobile != "");
            //if (status > 0)
            //{
            //    places = places.Where(x => x.Status == status);
            //}

            gridResponse.pageData = await page.Processasync(places.Count(), skip, limit);
            skip = gridResponse.pageData.selectedPageSize * (gridResponse.pageData.currentPage - 1);
            if (skip < 0) skip = 0;
            var finalresult = places.OrderBy(Sort, descending).Skip(skip).Take(gridResponse.pageData.selectedPageSize).ToList();
            gridResponse.rows.AddRange(finalresult
             .Select(x => new
             {
                 name = x.FirstName,
                 lastname = x.LastName,
                 mobile = x.Mobile,
                 email = x.Email,
                 createdat = x.Createddate,
                 organization = x.Organization,
                 createdby = x.CreatedByName,
                 id = x.EmployerId,
                 organizationtype = x.Organizationtype
             }));
            return gridResponse;
        }

        public async Task<GridResponse> GetFileterprofiles(string filterId, PageData page)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            try
            {
               
                var filters = employerDAOservice.ServiceRepository<MSearchfilterParamters>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(filterId)).Result;
                //var filter = Builders<MAspirant>.Filter.GeoWithinCenter("LatLocation", 17.436318440345676, 78.44834121886626, 200);
                var Lookupstatus = employerDAOservice.ServiceRepository<LookupStatus>().Getfilter();

                var filterbuilder = Builders<MAspirant>.Filter.Empty;
                if (filters.Latitude > 0 && filters.Longitude > 0)
                    filterbuilder &= Builders<MAspirant>.Filter.GeoWithinCenter("LatLocation", filters.Longitude, filters.Latitude, 50);
                if (filters.City != null && filters.City.Count() > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.AnyIn(x => x.Joblookingcity, filters.City.Select(x => x.Id).ToArray()));
              
                if (filters.Sectors != null && filters.Sectors.Count() > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.AnyIn(x => x.Sector, filters.Sectors.Select(x => x.Id).ToArray()));
                if (filters.Qualification != null && filters.Qualification.Count() > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.AnyIn(x => x.Qualification, filters.Qualification.Select(x => x.Id).ToArray()));
                if (filters.AditionalQualification != null && filters.AditionalQualification.Count() > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.AnyIn(x => x.AditionalQualification, filters.AditionalQualification.Select(x => x.Id).ToArray()));
                if (filters.Language != null && filters.Language.Count() > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.AnyIn(x => x.Language, filters.Language.Select(x => x.Id).ToArray()));
                if (filters.Gender > 0 && filters.Gender != Constants.Gender.Both.ToInt32())
                    filterbuilder &= (Builders<MAspirant>.Filter.Eq(x => x.GenderId, filters.Gender));

                //Both Id is same for Gender & Experince.
                if (filters.Experince > 0 && filters.Experince != Constants.Gender.Both.ToInt32())
                    filterbuilder &= (Builders<MAspirant>.Filter.Eq(x => x.WorkingstatusId, filters.Experince));
                if (filters.SalaryRange > 0)
                {
                    string[] salaryranage = Lookupstatus.Where(x => x.Id == filters.SalaryRange).FirstOrDefault().Name.Split("-");
                    filterbuilder &= (Builders<MAspirant>.Filter.Gte(x => x.SalaryRange, salaryranage[0].ToInt32()));
                    filterbuilder &= (Builders<MAspirant>.Filter.Lte(x => x.SalaryRange, salaryranage[1].ToInt32()));
                }


                if (filters.Agemin > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.Gte(x => x.Age, filters.Agemin));

                if (filters.Agemax > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.Lte(x => x.Age, filters.Agemax));

                if (filters.communicationskills != null && filters.communicationskills.Count() > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.In(x => x.communicationskills, filters.communicationskills.Select(x => x.Id).ToArray()));
                if (filters.apptitude != null && filters.apptitude.Count() > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.In(x => x.apptitude, filters.apptitude.Select(x => x.Id).ToArray()));
                if (filters.computroperations != null && filters.computroperations.Count() > 0)
                    filterbuilder &= (Builders<MAspirant>.Filter.In(x => x.computroperations, filters.computroperations.Select(x => x.Id).ToArray()));


                var Profiles = employerDAOservice.ServiceRepository<MAspirant>().mongoGetfilter(filterbuilder).Result;

                var Locationfilter = new GeoJson2DGeographicCoordinates(17.430200, 78.449990);

                //find all places within 1km of range.


                var count = Profiles.Count();
                gridResponse.pageData = page.Process(count, out skip, out limit);
                var result = Profiles.AsQueryable().OrderBy("ProfileOrder", true).OrderBy("UpdatedDate", true).Skip(skip).Take(limit);
                gridResponse.rows.AddRange(result.Select(x => new
                {
                    name = x.Name,
                    Id = x._id.ToString(),
                    gender = x.GenderText,
                    experince = x.Workingstatus,
                    education = x.lst_qualification,
                    sectors = x.lst_sectors,
                    aditonalqualification = x.lst_additionalqulification,
                    checkedvalue = false,
                    profilepicture = (x.Profilepicture!=string.Empty&& x.Profilepicture != null) ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + x.Profilepicture : configuration.GetSection("ApplicationSettings:Deflutuserimage").Value,
                    isEmployeerview = x.SearchedEmployeer!=null?x.SearchedEmployeer.Where(y => y.EmployeerId.Contains(filters.EmployerId)).Count()>0?true:false:false,
                    jobseekrinfo = x.SearchedEmployeer != null ? (x.SearchedEmployeer.Where(y => y.EmployeerId.Contains(filters.EmployerId)).Count() > 0) ? new List<object> { x.Mobile, x.Email } : new List<object> { }: new List<object> { },
                    lastactivedate=x.UpdatedDate,
                    
                }));
                gridResponse.Customdata = filters;
                return gridResponse;
            }
            catch(Exception Ex)
            {
                return gridResponse;
            }
            
        }

        public async Task<GridResponse> GetPreviousSearchs(PageData page, string Sort, bool descending, string name, DateTime Fromdate, DateTime Todate,int UserId)
        {

            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var filterbuilder = Builders<MAspirant>.Filter.And(
             //  Builders<MAspirant>.Filter.Eq("Location", "Hyderabad")
             Builders<MAspirant>.Filter.Empty

                );

            var PreviousSearchs = employerDAOservice.ServiceRepository<MSearchfilterParamters>().Getmongiquerable();

            PreviousSearchs = PreviousSearchs.Where(x => x.UserId ==UserId);
            //if (status > 0)
            //{
            //    places = places.Where(x => x.Status == status);
            //}

            gridResponse.pageData = await page.Processasync(PreviousSearchs.Count(), skip, limit);
            skip = gridResponse.pageData.selectedPageSize * (gridResponse.pageData.currentPage - 1);
            if (skip < 0) skip = 0;
            var finalresult = PreviousSearchs.OrderBy(Sort, descending).Skip(skip).Take(gridResponse.pageData.selectedPageSize);

            gridResponse.rows.AddRange(finalresult.ToList()
             .Select(x => new
             {
                 jobtitle = x.Jobtitle,
                 joblocations = x.Jobloaction,
                 sectors = string.Join(",", x.Sectors.Select(x=>x.Name)),
                 createdat = x.Seachedat,
                 qulaification = string.Join(",", x.Qualification.Select(x=>x.Name)),
                 addintionalqualification = string.Join(",",x.AditionalQualification.Select(x=>x.Name)),
                 id = x._id.ToString(),
                 gender = x.Gendertext,
                 experince=x.ExperinceText,
                 no_openings=x.Openings,
                 salary=x.salaryranagetext,
                 age=x.Agemin +"-"+x.Agemax
                 

             }
            )
             );
            //.OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }

        public aspnetusers Getuser(string UserId)
        {
            return employerDAOservice.ServiceRepository<aspnetusers>().GetFirstOrDefault(x => x.Id == UserId.ToInt32decrypt());
        }

        public Response RegisterEmployeer(EmployerRegisterModel model, int UserId, string Password)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Path.Combine("Content", "Html", "Register/Employer Registration.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{email}", model.Email)
               .Replace("{password}", Password).Replace("{url}", configuration.GetSection("ApplicationSettings:url").Value)
                 .Replace("{contact}", configuration.GetSection("ApplicationSettings:contactnumber").Value)
                ;
            model.Id = UserId;
            EmailHelper.To = model.Email;
            EmailHelper.Subject = "Login Credintails-" + model.FirstName;
            EmailHelper.Body = body;
            EmailHelper.IsHtml = true;
            EmailHelper.SendEmail(configuration.GetSection("EmailConfiguration:SmtpServer").Value, configuration.GetSection("EmailConfiguration:SmtpUsername").Value,
                configuration.GetSection("EmailConfiguration:SmtpPassword").Value, Convert.ToBoolean(configuration.GetSection("EmailConfiguration:SSLenabled").Value),
                Convert.ToInt32(configuration.GetSection("EmailConfiguration:SmtpPort").Value));

            var Lookupstatus = employerDAOservice.ServiceRepository<LookupStatus>().Getfilter();
            var Employeer = new Employer()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Mobile = model.Mobile,
                Organizationtype = Lookupstatus.Where(x=>x.Id== model.OrganizationtypeId).FirstOrDefault().Name,
                OrganizationtypeId = model.OrganizationtypeId,
                Organization = model.Organization,
                Designation = model.Designation,
                CurrentPlan = model.CurrentPlan,
                Validity = model.Validity,
                Price = model.Price,
                GSTSNumber = model.GSTSNumber,
                planstart = model.PlanStart,
                planend = model.PlanEnd,
                Profileviews = model.Profileviews,
                totalCVdownloads = model.totalCVdownloads,
                tillnowCVdownload = model.tillnowCVdownload,
                tillnowprofileviews = model.tillnowprofileviews,
                profilepicname = model.profilepicname,
                Profilepictype = model.Profilepictype,
                Profilepicpath = model.Profilepicpath,
                CreatedByName = model.CreatedByName,
                BoradcastEmailSMS = model.BoradcastEmailSMS,
                SMSTemplate = model.SMSTemplate,
                UserId = UserId,
                Enc_id = UserId.TostringEncrypt()

            };
            var Employerrsult = employerDAOservice.ServiceRepository<Employer>().Add(Employeer);
            this.AddUserrole(UserId, Constants.Userroles.Employer.ToInt32(), "Employer");
            employerDAOservice.Commit();

            var EmployeerMongo = new MEmployer()
            {

                FirstName = model.FirstName,
                LastName = model.LastName,
                EmployerId = Employerrsult.Result.Id,
                Email = model.Email,
                Mobile = model.Mobile,
                Organizationtype = model.Organizationtype,
                OrganizationtypeId = model.OrganizationtypeId,
                Organization = model.Organization,
                Designation = model.Designation,
                CurrentPlan = model.CurrentPlan,
                Validity = model.Validity,
                Price = model.Price,
                GSTSNumber = model.GSTSNumber,
                planstart = model.PlanStart,
                planend = model.PlanEnd,
                Profileviews = model.Profileviews,
                totalCVdownloads = model.totalCVdownloads,
                tillnowCVdownload = model.tillnowCVdownload,
                tillnowprofileviews = model.tillnowprofileviews,
                profilepicname = model.profilepicname,
                Profilepictype = model.Profilepictype,
                Profilepicpath = model.Profilepicpath,
                CreatedByName = model.CreatedByName,
                BoradcastEmailSMS = model.BoradcastEmailSMS,
                SMSTemplate = model.SMSTemplate,
                UserId = UserId,
                Createddate = DateTime.Now,
                Enc_id = UserId.TostringEncrypt(),
                CreatedBy = model.CreatedBy,
                status = Employeerstatus.EmployeerRegistered.ToInt32(),
                statename = Employeerstatustext.EmployeerRegistered
            };
            employerDAOservice.ServiceRepository<MEmployer>().mongoAdd(EmployeerMongo);
            employerDAOservice.Commitmongo();
            AddFreeplan(EmployeerMongo._id.ToString(), model);

            employerDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Inserted

            };

        }

        public Response Registerfilter(Filterparameters model, int UserId)
        {
            MSearchfilterParamters filters = new MSearchfilterParamters();
            var Memployeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
            var Lookupstatus = employerDAOservice.ServiceRepository<LookupStatus>().Getfilter();
            filters.Seachedat = DateTime.Now;
            filters.Employeername = Memployeer.FirstName+" "+ Memployeer.LastName;
            filters.AditionalQualification = model.AditionalQualification;
            filters.EmployerId = Memployeer._id.ToString();
            filters.Agemax = model.Agemax;
            filters.Agemin = model.Agemin;
            filters.apptitude = model.apptitude;
            filters.City = model.City;
            filters.communicationskills = model.communicationskills;
            filters.computroperations = model.computroperations;
            filters.CTC = model.CTC;
            filters.Experince = model.Experince;
            filters.Gender = model.Gender;
            filters.Jobloaction = model.Jobloaction;
            filters.Jobtitle = model.Jobtitle;
            filters.Language = model.Language;
            filters.Openings = model.Openings;
            filters.Qualification = model.Qualification;
            filters.SalaryRange = model.SalaryRange;
            filters.Sectors = model.Sectors;
            filters.salaryranagetext = Lookupstatus.FirstOrDefault(x => x.Id == model.SalaryRange).Name;
            filters.Gendertext = Lookupstatus.FirstOrDefault(x => x.Id == model.Gender).Name;
            filters.ExperinceText = Lookupstatus.FirstOrDefault(x => x.Id == model.Experince).Name;
            filters.UserId = UserId;
            filters.Latitude = model.Latitude.HasValue?Convert.ToDouble(model.Latitude):0.00;
            filters.Longitude = model.Longitude.HasValue?Convert.ToDouble(model.Longitude):0.00;
            filters.LatLocation = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
                                     new GeoJson2DGeographicCoordinates(model.Longitude.HasValue ? Convert.ToDouble(model.Longitude) : 0.00, model.Latitude.HasValue ? Convert.ToDouble(model.Latitude) : 0.00));
           employerDAOservice.ServiceRepository<MSearchfilterParamters>().mongoAdd(filters);
            employerDAOservice.Commitmongo();
            return new Response()
            {
                response = new
                {
                    Id = Convert.ToString(filters._id)
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Fileter Registered Successfully"
            };
        }

        public Response UpdateEmployeerprofile(EmployerProfileUpdatemodel model, int UserId)
        {

            var employer = employerDAOservice.ServiceRepository<Employer>().GetFirstOrDefault(x => x.Id == model.EmployeerId);

            var Memployer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == employer.UserId).Result;

            employer.Organization = model.OrganizationName;
            employer.FirstName = model.FirstName;
            employer.LastName = model.LastName;
            employer.Description = model.Description;
            employer.GSTStatus = model.GSTStatus;
            employer.GSTSNumber = model.GST;
            employerDAOservice.ServiceRepository<Employer>().Update(employer, employer);
            _Unitofwork.Commit();

            employerDAOservice.ServiceRepository<MEmployerActivitylog>().mongoAdd(new MEmployerActivitylog()
            {
                EmployerName = model.FirstName + "" + model.LastName,
                Log= EmployeerActivities.UpdateProfile,
                Logtype= EmployeerActivities.UpdateProfile,
                Logdate = DateTime.Now,
                EmployerId = Memployer._id.ToString(),

            });

            var filter = Builders<MEmployer>.Filter.Eq(x => x.UserId, employer.UserId);
            var update = Builders<MEmployer>.Update.Set(x => x.Organization, model.OrganizationName).Set(x=>x.Description,model.Description)
                .Set(x => x.FirstName, model.FirstName).Set(x => x.LastName, model.LastName).Set(x => x.Designation, model.Designation).Set(x=>x.Sectors,model.Sectors).Set(x=>x.GSTStatus,model.GSTStatus).Set(x=>x.GSTSNumber,model.GST);
            employerDAOservice.ServiceRepository<MEmployer>().mongoUpdateasync(filter, update);
            employerDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Address Updated Successfully"
            };
        }

        public Response UpdateEmployerAddress(EmployerAddress model, int UserId)
        {
            try 
            { 
            //Employer employer = employerDAOservice.ServiceRepository<Employer>().GetFirstOrDefault(x => x.Id == model.EmployerId);
            Employer employer = employerDAOservice.ServiceRepository<Employer>().GetFirstOrDefault(x => x.UserId == UserId);
                //var employerAdd = employerDAOservice.ServiceRepository<EmployerAddress>().GetFirstOrDefault(x => x.UserId == employer.UserId);
                var employerAdd = employerDAOservice.ServiceRepository<EmployerAddress>().GetFirstOrDefault(x => x.UserId == employer.UserId);
                var Lookupstate = employerDAOservice.ServiceRepository<Lookupstate>().GetFirstOrDefault(x => x.Id == model.state);
            var LookupCity = employerDAOservice.ServiceRepository<LookupCity>().GetFirstOrDefault(x => x.Id == model.city);
            var Memployer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == employer.UserId).Result;
            model.EmployerId = model.EmployerId;
            model.UserId = employer.UserId.ToInt32();
            if (employerAdd != null)
            {
                    var employerAddidentity = employerDAOservice.ServiceRepository<EmployerAddress>().GetFirstOrDefault(x => x.Id == employerAdd.Id);
                    model.EmployerId = employerAddidentity.EmployerId;
                    model.Id = employerAddidentity.Id;

                    employerDAOservice.ServiceRepository<EmployerAddress>().Update(employerAddidentity, model);
                var filter = Builders<MEmployerAddress>.Filter.Eq(x => x.MEmployerId, Memployer._id.ToString());
                var update = Builders<MEmployerAddress>.Update.Set(x => x.pincode, model.pincode)
                    .Set(x => x.Address, model.Address).Set(x => x.Addressstwo, model.Addressstwo).Set(x => x.city, model.city).Set(x=>x.cityname, LookupCity.Name)
                    .Set(x => x.state, model.state).Set(x=>x.statename,Lookupstate.Name);

                employerDAOservice.ServiceRepository<MEmployerAddress>().mongoUpdateasync(filter, update);
                employerDAOservice.Commitmongo();

            }
            else
            {
                model.Id = 0;
                employerDAOservice.ServiceRepository<EmployerAddress>().Add(model);
                employerDAOservice.ServiceRepository<MEmployerAddress>().mongoAdd(
                    new MEmployerAddress()
                    {
                        Address = model.Address,
                        Addressstwo = model.Addressstwo,
                        city = model.city,
                        EmployerId = model.EmployerId,
                        pincode = model.pincode,
                        state = model.state,
                        cityname = LookupCity.Name,
                        statename= Lookupstate.Name,
                        MEmployerId= Memployer._id.ToString(),
                        UserId=employer.UserId.ToInt32()  
                    });
            }
            UpdateEmployeertate(UserId, model.state, Lookupstate.Name);
            LastActivedate(UserId);
            employerDAOservice.Commit();
            
            employerDAOservice.ServiceRepository<MEmployerActivitylog>().mongoAdd(new MEmployerActivitylog()
            {
               
                Log = EmployeerActivities.UpdateAddress,
                Logdate = DateTime.Now,
                EmployerId = Memployer._id.ToString(),
                Logtype= EmployeerActivities.UpdateAddress,
                EmployerName= Memployer.FirstName+" "+ Memployer.LastName

            });
                employerDAOservice.Commitmongo();
                return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Address Updated Successfully"
            };
            }
            catch (Exception Ex)
            {
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = (StatusCodes.Status206PartialContent),
                    CustomerId = "",
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Title = Constants.messageTitles.Warning,
                    Message = Ex.Message

                };
            }

        }

        public Response UpdateOrganization(EmployerOrganizationalModel model, int UserId)
        {

            var employer = employerDAOservice.ServiceRepository<Employer>().GetFirstOrDefault(x => x.UserId == UserId);
            var memployer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId);
            var lookupsector = employerDAOservice.ServiceRepository<LookupSector>().Getfilter(x => x.IsActive == true);
            List<Selectionvalues> sectors = new List<Selectionvalues>();

            var employersectors = memployer.Result != null && memployer.Result.Sectors != null ? memployer.Result.Sectors.Select(x => x.Id).ToArray() : null;
            foreach (var item in model.Sectors)
            {
                sectors.Add(new Selectionvalues()
                {
                    Id = item,
                    Name = lookupsector.FirstOrDefault(x => x.Id == item).Name,
                    IsActive = true
                });
            }


            if (employer != null)
            {
                employer.Organization = model.orgnizaionname;
                employerDAOservice.ServiceRepository<Employer>().Update(employer, employer);
                var filter = Builders<MEmployer>.Filter.Eq(x => x.UserId, UserId);
                var update = Builders<MEmployer>.Update.Set(x => x.Organization, model.orgnizaionname)
                    .Set(x => x.GSTSNumber, model.GSTnumber).Set(x => x.GSTStatus, model.GST).Set(x => x.Designation, model.designation);

                if (sectors != null)
                {
                    update = update.Set(x => x.Sectors, sectors);
                }
                employerDAOservice.ServiceRepository<MEmployer>().mongoUpdateasync(filter, update);
                employerDAOservice.Commitmongo();
            }
            employerDAOservice.Commit();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Address Updated Successfully"
            };
        }

        public GridResponse GetEmployerlist(PageData page, string Sort, bool descending, string organizationtype, string organization, string fname, string lname, string email, string mobile, string currentplan, int validity, string createdbyname)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = employerDAOservice.ServiceRepository<Employer>().Getfilter().AsQueryable();


            if (fname != "" && fname != null)
                result = result.Where(x => x.FirstName.Contains(fname));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new
            {
                fname = x.FirstName,
                lname = x.LastName,
                organizationtype = x.Organizationtype,
                organization = x.Organization,
                email = x.Email,
                createdbyname = x.CreatedByName,
                mobile = x.Mobile,
                currentplan = x.CurrentPlan,
                validity = x.Validity,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }

        public Task<string> GetEmployeerIdbyUserId(int UserId)
        {
            var Employeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
            if (Employeer != null)
            {
                return Task.FromResult(string.Format(configuration.GetSection("PaymentGateway:PaymentAppPath").Value, Employeer._id.ToString()));
            }
            else
            {
                return Task.FromResult(string.Empty);
            }
        }

        public Task<GridResponse> GetEmployeerTransactions(PageData page, string Sort, bool descending, string name, DateTime Fromdate, DateTime Todate, string EmployeerId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = employerDAOservice.ServiceRepository<MTransactionsmodel>().Getfilter().AsQueryable();


            if (EmployeerId != "" && EmployeerId != null)
                result = result.Where(x => x.EmployerId == Convert.ToUInt32(EmployeerId));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new
            {
                fname = x.FirstName,
                lname = x.LastName,
                planmodel = x.Planmodel,
                amount = x.PriceIncludeGST,
                email = x.GST,
                createdbyname = x.IsUnlimited,
                mobile = x.Validity,
                currentplan = x.ValidityinDays,
                validity = x.Validity,
                Id = x._id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return Task.FromResult(gridResponse);
        }

        public Response GetPaymentInvoive(string TransactionId)
        {
            var CAMhost = new RestClient(configuration.GetSection("DLCPDF:Host").Value);
            var CAMreuest = new RestRequest(configuration.GetSection("DLCPDF:PaymentReceipt").Value, Method.POST);
            var transactionresult = employerDAOservice.ServiceRepository<MTransactionsmodel>().Getmongiquerable().Where(x => x._id == ObjectId.Parse(TransactionId)).FirstOrDefault();
            var employerresult = employerDAOservice.ServiceRepository<MEmployer>().Getmongiquerable().Where(x => x._id == ObjectId.Parse(transactionresult.MEmployerId)).FirstOrDefault();
            var employeraddressresult = employerDAOservice.ServiceRepository<MEmployerAddress>().Getmongiquerable().Where(x => x.EmployerId ==employerresult.EmployerId).FirstOrDefault(); 
            CAMreuest.RequestFormat = DataFormat.Json;
            CAMreuest.AddHeader("Content-Type", "application/json");


            var Request = new
            {
                lbl_Addressto = "Addressed To",
                Address_to = "",
                lbl_partyname = "Party Name",
                partyname = transactionresult.FirstName+" "+ transactionresult.LastName,
                lbl_partyAddress = "Party Address",
                party_address = transactionresult.address,
                lbl_state = "State",
                statename = employeraddressresult!=null ? employeraddressresult.statename:"",
                lbl_statecode = "State Code",
                statecode = transactionresult.StateGSTcode,
                lbl_gststatus = "GST Status",
                gst_status = transactionresult.GSTsatatus,
                lbl_gstn_party = "GSTN of party",
                gstn_party = transactionresult.GSTOfparty,
                lbl_placeof_supplay = "Place of supply",
                place_of_supply = transactionresult.placeofsupply,
                lbl_whether_payable_under_RCM = "Whether payable under RCM",
                unerpayables_rcm = transactionresult.UnderpaybleRCM,
                lbl_Invoice_No = "Invoice No",
                invoice_no = transactionresult.Invoicenumber,
                lbl_Invoice_Date = "Invoice Date",
                invoice_date = transactionresult.purchasedate.ToString("dd/MM/yyyy"),
                lbl_GST_No = "GST No",
                gst_no = employerresult.GSTSNumber,
                lbl_pan = "PAN",
                pan = transactionresult.pannumber,
                sr_no = "000001",
                sac_code = transactionresult.SACcode,
                Category_of_service = transactionresult.SACCategorey,
                Description = "",
                Amount = transactionresult.Price,
                lbl_Total_Gross_Amount = "Total Gross Amount",
                total_gross_amount = transactionresult.Price,
                lbl_Discount_Abatement = "Discount / Abatement",
                Discount_Abatement = transactionresult.disscount,
                lbl_Net_taxable_amount = "Net taxable amount",
                Net_taxable_amount = transactionresult.Price,
                lbl_CGST = "CGST @ 9 %",
                cgst = transactionresult.CGST,
                lbl_SGST = "SGST @ 9%",
                SGST = transactionresult.SGST,
                lbl_IGST = "IGST@18%",
                IGST = transactionresult.IGST,
                lbl_Rounding_Off = "Rounding Off",
                lbl_Total_invoice_Value = "Total invoice Value",
                Total_invoice_Value = transactionresult.InvoiceValue,
                lbl_amount_inwords = "Amount in words : (INR)",
                amount_in_words = transactionresult.Invoicein_words,
                lbl_account_name = "Account Name: ",
                account_name = transactionresult.companyAccounname,
                lbl_account_no = "Account No: ",
                account_no = transactionresult.companyaccountsno,
                lbl_Bank = "Bank:",
                bank = transactionresult.companybank,
                lbl_brach = "Branch:",
                branch = transactionresult.companybranch,
                lbl_IFSC_code = "IFSC code: ",
                ifsc_code = transactionresult.companyifsc,
                plandescription = transactionresult.plandescription,
                Roundingoff = transactionresult.Roundingoff,
                Validity = transactionresult.Validity,
                validity_expies=transactionresult.planenddate.ToString("dd/MM/yyyy"),
                lbl_employerAddress= "Party Address",
                lbl_employername= "Party Name"

            };


            CAMreuest.AddJsonBody(Request);
            System.Net.ServicePointManager.Expect100Continue = false;
            var camresponseexecution = CAMhost.Execute(CAMreuest);
            var ResponseFund = JsonConvert.DeserializeObject<ReturnUitlityPDFResponse>(camresponseexecution.Content);

            MemoryStream stream = new MemoryStream(ResponseFund.Response);

            return new Response()
            {
                response = ResponseFund.Response,
                StatusCode = (StatusCodes.Status200OK),
                CustomerId = "",
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.OK,
                stream = stream

            };


        }
        public Response GetemployerIDbyMobile(string Mobile)
        {
            var employerid = employerDAOservice.ServiceRepository<Employer>().GetFirstOrDefault(x => x.Mobile == Mobile);

            return new Response()
            {
                response = new
                {
                    model = employerid.Id

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        #endregion

        #region Privatemethods
        void AddFreeplan(string EMployeerId, EmployerRegisterModel model)
        {
            var Plandata = employerDAOservice.ServiceRepository<LookupPlans>().GetFirstOrDefault(x => x.PriceIncludeGST == 0);
            var Transactionnew = new MTransactionsmodel()
            {
                LastName = model.LastName,
                FirstName = model.FirstName,
                PriceIncludeGST = Plandata.PriceIncludeGST,
                MEmployerId = EMployeerId,
                EmployerId = model.Id,
                ValidityinDays = Plandata.ValidityinDays,
                GST = Plandata.GST,
                Validity = Plandata.Validity,
                TotalCVdownload = Plandata.TotalCVdownload,
                SMSandEmail = Plandata.SMSandEmail,
                ProfileViewsnumber = Plandata.ProfileViewsnumber,
                ProfileViews = Plandata.ProfileViews,
                IsUnlimited = Plandata.IsUnlimited,
                Planmodel = Plandata.Planmodel,
                Price = Plandata.Price,
                Status = Appdata.Paymentwelcomeplan,
                purchasedate=DateTime.Now,
                planstartdate=DateTime.Now,
                planenddate=DateTime.Now.AddDays(Plandata.ValidityinDays),
                
            };
            employerDAOservice.ServiceRepository<MTransactionsmodel>().mongoAdd(Transactionnew);
            var MEmployeer = Builders<MEmployer>.Filter.Eq(x => x._id, ObjectId.Parse(EMployeerId));
            var updateEmployeerbuilder = Builders<MEmployer>.Update.Set(x => x.Validity, Plandata.Validity).Set(x => x.Profileviews, Plandata.ProfileViewsnumber).Set(x => x.planstart, DateTime.Now).Set(x => x.planend, DateTime.Now.AddDays(Plandata.Validity)).Set(x=>x.CurrentPlan,Plandata.Planmodel);
            employerDAOservice.ServiceRepository<MEmployer>().mongoUpdateasync(MEmployeer, updateEmployeerbuilder);

        }

        public GridResponse GetActivitylog(PageData page, string Sort, bool descending, string name, DateTime fromdate, DateTime todate, int employeerId, int UserId)
        {

            // var MEmployeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x=>x.EmployerId==employeerId).Result;
            var MEmployeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = employerDAOservice.ServiceRepository<MEmployerActivitylog>().Getmongiquerable();

            if (fromdate.Isdatenull())
            {
               // fromdate = fromdate.AddDays(1);
                result = result.Where(x => x.Logdate >= fromdate);
            }            
            if (todate.Isdatenull())
            {
                todate = todate.AddDays(1);
                result = result.Where(x => x.Logdate <= todate);
            }
              
            //if (classId > 0)
            //    result = result.Where(x => x.classId == classId);
           
            if (UserId != null && UserId > 0)
                result = result.Where(x => x.EmployerId == MEmployeer._id.ToString());

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            var finalresult = result.OrderBy(Sort, descending).Skip(skip).Take(limit).ToList();
            gridresponse.rows.AddRange(finalresult.Select(x => new
            {
                Logdate_yyyy_dd_mm = x.Logdate.ToString("yyyy-MM-dd"),
                month = x.Logdate.ToString("MM"),
                day = x.Logdate.ToString("dd"),
                name=x.EmployerName,
                log=x.Log,
                id=x.EmployerId
            })); ;


            return gridresponse;
        }

        public async Task<byte[]> ExportUsersExcel(List<string> Jobseekrs,int UserId)
        {
            var Profileviewcount = await CheckLimitBeforeExport(Jobseekrs, UserId);
            if (Profileviewcount.ISAuthoruizetoviewProfiles)
            {
                List<ObjectId> BsonObjectid = new List<ObjectId>();
                foreach (var item in Jobseekrs)
                {
                    BsonObjectid.Add(ObjectId.Parse(item));
                }
                if (Jobseekrs.Count() > 0)
                {
                    GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
                    var result = employerDAOservice.ServiceRepository<MAspirant>().Getmongiquerable();


                    //if (classId > 0)
                    //    result = result.Where(x => x.classId == classId);

                    if (Jobseekrs != null && Jobseekrs.Count() > 0)
                        result = result.Where(x => BsonObjectid.Contains(x._id));
                    var finalresult = result.ToList().Select(x => new
                    {
                        name = x.Name,
                        mobile = x.Mobile,
                        email = x.Email,
                        gender = x.GenderText,
                        workexperince = x.Workingstatus,
                        qulification = string.Join(",", x.lst_qualification.Select(x => x.Name.ToString())),
                        location = x.Location
                    }).ToList();

                    ///update count of viewd profiles
                    this.Updateprofileviewcount(Jobseekrs, UserId);
                    return DatatableConverter.generateexcel(DatatableConverter.ConvertToDataTable(finalresult));
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

               
            
        }

        public Task<Response> CheckLimitBeforeExport(List<string> Jobseekes,int UserId)
        {
            MEmployer employeer = new MEmployer();
            //check the employeer profiles limit.
            if (employerDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == UserId).RoleId == Constants.Userroles.Employer.ToInt32())
            {
                employeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
            }
            if(employeer==null)
            {
                return Task.FromResult(new Response()
                {
                    StatusCode = StatusCodes.Status202Accepted,
                    Message = "Something went to wrong! Please try again",
                    Iserror = true,
                    Status = SucessMessages.Exception
                });
            }
            else
            {
                var checkisjobseekrisviewerlier = employerDAOservice.ServiceRepository<MEmployeerViewdProfiles>().Getmongiquerable()
           .Where(x => x.MEmployeerId == employeer._id.ToString() && Jobseekes.Contains(x.JobseekerId)).Select(x => x.JobseekerId);

                var NonViewedProfiles = Jobseekes.Except(checkisjobseekrisviewerlier.ToList());

                if(NonViewedProfiles.Count()>0)
                {
                    if (employeer.tillnowprofileviews <= employeer.Profileviews && employeer.planend >= DateTime.Now)
                    {
                        int limitprofiles = employeer.Profileviews - employeer.tillnowprofileviews;
                        if (limitprofiles >= Jobseekes.Count)
                        {
                            return Task.FromResult(new Response()
                            {
                                response = new
                                {
                                    model = true
                                },
                                StatusCode = StatusCodes.Status200OK,
                                Message = "Eligible to viwe the Profiles",
                                Iserror = false,
                                Status = SucessMessages.OK,
                                ISAuthoruizetoviewProfiles = true
                            });
                        }
                        else
                        {
                            if(NonViewedProfiles.Count()>1)
                            {
                                return Task.FromResult(new Response()
                                {
                                    response = new
                                    {
                                        model = true
                                    },
                                    StatusCode = StatusCodes.Status206PartialContent,
                                    Message = $"You have limit of  {limitprofiles} Profiles ,But you have choose { Jobseekes.Count} Profiles. Please Unselect {limitprofiles - Jobseekes.Count} Profiles",
                                    Iserror = false,
                                    Status = SucessMessages.OK,
                                    ISAuthoruizetoviewProfiles = false
                                });
                            }
                            else
                            {
                                return Task.FromResult(new Response()
                                {
                                    response = new
                                    {
                                        model = true
                                    },
                                    StatusCode = StatusCodes.Status206PartialContent,
                                    Message = $"Your Plan is Expired.Please Renew and Proceed Furthur",
                                    Iserror = false,
                                    Status = SucessMessages.OK,
                                    ISAuthoruizetoviewProfiles = false
                                });
                            }
                         
                        }

                    }
                    else
                    {
                        string Exceptionmessage = "";
                        if (employeer.planend <= DateTime.Now)
                        {
                            Exceptionmessage = "Your Plan is Expired.Please Renew and Proceed Furthur";
                            return Task.FromResult(new Response()
                            {
                                response = new
                                {
                                    model = false
                                },
                                StatusCode = StatusCodes.Status206PartialContent,
                                Message = Exceptionmessage,
                                Iserror = true,
                                Status = SucessMessages.Exception,
                                ISAuthoruizetoviewProfiles = false
                            });
                        }
                        if (employeer.tillnowprofileviews >= employeer.Profileviews)
                        {
                            Exceptionmessage = "Your Limit is Exhausted.Please Renew and Proceed Furthur.";
                            return Task.FromResult(new Response()
                            {
                                response = new
                                {
                                    model = false
                                },
                                StatusCode = StatusCodes.Status206PartialContent,
                                Message = Exceptionmessage,
                                Iserror = false,
                                Status = SucessMessages.Exception,
                                ISAuthoruizetoviewProfiles = false
                            });
                        }

                        Exceptionmessage = "Something went to Wrong";
                        return Task.FromResult(new Response()
                        {
                            response = new
                            {
                                model = false
                            },
                            StatusCode = StatusCodes.Status206PartialContent,
                            Message = Exceptionmessage,
                            Iserror = true,
                            Status = SucessMessages.Exception,
                            ISAuthoruizetoviewProfiles = false
                        });


                    }
                }
                else
                {
                    
                    return Task.FromResult(new Response()
                    {
                        response = new
                        {
                            model = false
                        },
                        StatusCode = StatusCodes.Status200OK,
                        Message = "",
                        Iserror = true,
                        Status = SucessMessages.Exception,
                        ISAuthoruizetoviewProfiles = true
                    });
                }
              
            }
        }

        public async Task<Filedonwload> ExportUsersResume(List<Selectionvalues> Jobseekrs,int UserId)
        {
            var Profileviewcount = await CheckLimitBeforeExport(Jobseekrs.Select(x=>x.Name).ToList(), UserId);
            if (Profileviewcount.ISAuthoruizetoviewProfiles)
            {
                if (Jobseekrs.Count() > 0)
                {
                    string Sesionresourcepath = "EmployeerExportDocuments";
                    var storagepath = Path.Combine(configuration.GetSection("ApplicationSettings:fileStoragePath").Value, Sesionresourcepath, UserId.ToString(), Docuemnts.resume);
                    foreach (var item in Jobseekrs)
                    {
                        var CAMhost = new RestClient(configuration.GetSection("DLCPDF:Host").Value);
                        var CAMreuest = new RestRequest(configuration.GetSection("DLCPDF:AspirantResume").Value, Method.POST);

                        CAMreuest.RequestFormat = DataFormat.Json;
                        CAMreuest.AddHeader("Content-Type", "application/json");

                        var maspirantresult = employerDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(item.Name)).Result;
                        var personalresult = employerDAOservice.ServiceRepository<MAspirantPersonal>().mongoGetFirstOrDefault(x => x.MJobseekerId == item.Name).Result;
                        var objectiveresult = employerDAOservice.ServiceRepository<MAspirantObjective>().mongoGetFirstOrDefault(x => x.MJobseekerId == item.Name).Result;
                        var educationresult = employerDAOservice.ServiceRepository<MAspirantEducation>().Getmongiquerable().Where(x => x.MJobseekerId == item.Name)
                            .Select(x => new
                            {
                                Particulars = x.QulificationName,
                                Year = x.Completedyear,
                                Institute = x.Institue,
                                Mode = x.ModeofEducation
                            });

                        var othereducation = employerDAOservice.ServiceRepository<MAspirantCertification>().Getmongiquerable().Where(x => x.MJobseekerId == item.Name)
                             .Select(x => new
                             {
                                 Particulars = x.Certification,
                                 Year = x.Completedyear,
                                 Institute = x.Institute,
                                 Duration = x.Duration

                             });
                        var languagesresult = employerDAOservice.ServiceRepository<MJobseekerLanguge>().Getmongiquerable().Where(x => x.MJobseekerId == item.Name)
                            .Select(x => new
                            {
                                Language = x.LanguageName,
                                Speak = x.Readwrite.Where(y => y.Id == LanguageGrip.Speak.ToInt32()).Select(y => y.Name).FirstOrDefault(),
                                Read = x.Readwrite.Where(y => y.Id == LanguageGrip.Read.ToInt32()).Select(y => y.Name).FirstOrDefault(),
                                Write = x.Readwrite.Where(y => y.Id == LanguageGrip.Write.ToInt32()).Select(y => y.Name).FirstOrDefault(),

                            });
                        var documnetsresult = employerDAOservice.ServiceRepository<MJobseekerDocuments>().Getmongiquerable().Where(x => x.JobseekerId == item.Name);


                        var documents = new
                        {
                            addressProof = documnetsresult.Where(x => x.Documentype == Docuemnts.addressProof).FirstOrDefault() != null ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.addressProof).FirstOrDefault().Filpath : string.Empty,
                            Euducational = documnetsresult.Where(x => x.Documentype == Docuemnts.Euducational).FirstOrDefault() != null ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.Euducational).FirstOrDefault().Filpath : string.Empty,
                            additinalEducation = documnetsresult.Where(x => x.Documentype == Docuemnts.additinalEducation).FirstOrDefault() != null ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.additinalEducation).FirstOrDefault().Filpath : string.Empty,
                            workexperinceCertificate = documnetsresult.Where(x => x.Documentype == Docuemnts.workexperinceCertificate).FirstOrDefault() != null ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.workexperinceCertificate).FirstOrDefault().Filpath : string.Empty,
                            profilepicture = documnetsresult.Where(x => x.Documentype == Docuemnts.profilepicture).FirstOrDefault() != null ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.profilepicture).FirstOrDefault().Filpath : string.Empty,
                            identityDocument = documnetsresult.Where(x => x.Documentype == Docuemnts.identityDocument).FirstOrDefault() != null ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.identityDocument).FirstOrDefault().Filpath : string.Empty,
                            dirivingLicence = documnetsresult.Where(x => x.Documentype == Docuemnts.dirivingLicence).FirstOrDefault() != null ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + documnetsresult.Where(x => x.Documentype == Docuemnts.dirivingLicence).FirstOrDefault().Filpath : string.Empty,
                        };

                        var pesrsonal = new
                        {

                            Name = maspirantresult.Name,
                            lbl_Contact = "Contact",
                            contact = maspirantresult.Mobile,
                            lbl_Email = "Email Id",
                            Email = maspirantresult.Email,
                            Address = personalresult.Housenumber,
                            Address2 = personalresult.streetName,
                            City = personalresult.City_Name,
                            lbl_State = "State",
                            State = personalresult.State_Name,
                            lbl_Pincode = "Pincode",
                            Pincode = personalresult.Pincode,
                            DOB = personalresult.DOB.ToString("dd/MM/yyyy"),
                            Objective = objectiveresult.ObjectiveName

                        };

                        var Request = new
                        {
                            pesrsonal = pesrsonal,
                            education = educationresult.ToList(),
                            otherqualification = othereducation.ToList(),
                            languages = languagesresult.ToList(),
                            documents = documents,

                        };

                        CAMreuest.AddJsonBody(Request);
                        System.Net.ServicePointManager.Expect100Continue = false;
                        var camresponseexecution = CAMhost.Execute(CAMreuest);
                        var ResponseFund = JsonConvert.DeserializeObject<ReturnUitlityPDFResponse>(camresponseexecution.Content);
                        string PDFFileName = string.Format("{0}-Resume.pdf", pesrsonal.Name);
                        var filepath = Path.Combine(storagepath, PDFFileName);

                        if (!Directory.Exists(storagepath))
                        {
                            Directory.CreateDirectory(storagepath);
                        }

                        if (File.Exists(filepath))
                        {
                            File.Delete(filepath);
                        }

                        if (!File.Exists(filepath))
                        {
                            using (FileStream fs = new FileStream(filepath, FileMode.Create))
                            {
                                fs.Write(ResponseFund.Response, 0, ResponseFund.Response.Length);
                            }
                        }
                        //test

                        MemoryStream stream = new MemoryStream(ResponseFund.Response);


                    }
                    var InputDirectory = storagepath;
                    Stream zipStream;
                    var OutputFilename = Path.Combine(storagepath + ".zip");
                    zipStream = new FileStream(Path.GetFullPath(OutputFilename), FileMode.Create, FileAccess.Write);
                    using (ZipArchive archive = new ZipArchive(zipStream, ZipArchiveMode.Create))
                    {
                        foreach (var filePath in System.IO.Directory.GetFiles(InputDirectory, "*.*", System.IO.SearchOption.AllDirectories))
                        {
                            if (!filePath.Contains(".zip"))
                            {
                                var relativePath = filePath.Replace(InputDirectory, string.Empty);
                                using (Stream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                                using (Stream fileStreamInZip = archive.CreateEntry(relativePath).Open())
                                    fileStream.CopyTo(fileStreamInZip);
                            }

                        }
                    }
                    this.Updateprofileviewcount(Jobseekrs.Select(x=>x.Name).ToList(), UserId);
                    return await Task.FromResult(new Filedonwload() { stream = zipStream, contenttype = "", filename = storagepath, filepath1 = storagepath, path = OutputFilename });

                }
                else
                {
                    return await Task.FromResult(new Filedonwload()
                    {
                    });
                }
            }
            else
            {
                return await Task.FromResult(new Filedonwload()
                {
                }); 
            }
               
        }

        public async Task<Response> UpdateEmployeerProfilepic(int UserId, IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return await Task.FromResult(new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = StatusCodes.Status206PartialContent,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = "Please select the file"
                });
            }
            var Jobseekr = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId ==UserId).Result;
            string Sesionresourcepath = "EmployeerDocuments";
            var DBstoragelocation = Path.Combine(Sesionresourcepath, UserId.ToString(), "EmployeeProfile", file.FileName);
            var storagepath = Path.Combine(configuration.GetSection("ApplicationSettings:fileStoragePath").Value, Sesionresourcepath, UserId.ToString(), "EmployeeProfile");
            var filepath = Path.Combine(storagepath, file.FileName);

            if (!Directory.Exists(storagepath))
            {
                Directory.CreateDirectory(storagepath);
            }

            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }

            if (!File.Exists(filepath))
            {
                using (var stream = new FileStream(filepath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
               
           this.UploadEmployeerProfilepicture(UserId,DBstoragelocation, file.FileName, file.ContentType);

                await employerDAOservice.Commitmongo();
                return await Task.FromResult(new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = StatusCodes.Status200OK,
                    Message = SucessMessages.Updated,
                    Iserror = false,
                    Status = SucessMessages.Inserted
                });
            }
            else
            {
                return await Task.FromResult(new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = StatusCodes.Status206PartialContent,
                    Message = SucessMessages.OK,
                    Iserror = false,
                    Status = "Same file already avilable  for this session"
                });
            }
        }

        public async Task<Response> GetJobseekerContactDetails(string JobseekrId,int UserId)
        {
            var Profileviewcount = await CheckLimitBeforeExport(new List<string>() { JobseekrId }, UserId);
            if(Profileviewcount.ISAuthoruizetoviewProfiles)
            {
                var Jobseekr = employerDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(JobseekrId)).Result;
                var Jobseerkobj = new
                {
                    name = Jobseekr.Name,
                    mobile = Jobseekr.Mobile,
                    email = Jobseekr.Email,
                    id = Jobseekr._id.ToString()
                };

                var Memployeerobj = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
                var MEmployeer = Builders<MEmployer>.Filter.Eq(x => x.UserId, UserId);
                var updateEmployeerbuilder = Builders<MEmployer>.Update.Set(x => x.tillnowprofileviews, Memployeerobj.tillnowprofileviews + 1).Set(x => x.tillnowCVdownload, Memployeerobj.tillnowprofileviews + 1);

                employerDAOservice.ServiceRepository<MEmployer>().mongoUpdateasync(MEmployeer, updateEmployeerbuilder);
                var MJobseekr = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(JobseekrId));

                List<EmployeerSearchsmodel> employeersearchs = new List<EmployeerSearchsmodel>();
                if (Jobseekr.SearchedEmployeer != null)
                    employeersearchs = Jobseekr.SearchedEmployeer;

                employeersearchs.Add(new EmployeerSearchsmodel() { EmployeerId = Memployeerobj._id.ToString(), EmployeerName = Memployeerobj.FirstName + " " + Memployeerobj.LastName, Viewdate = DateTime.Now });
                var updateJobseekrbuilder = Builders<MAspirant>.Update.Set(x => x.SearchedEmployeer, employeersearchs).Set(x=>x.ProfileOrder,ProfileOrder.employeer_selected.ToInt32()).Set(x=>x.EmployeerViewddate,DateTime.Now);
                employerDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(MJobseekr, updateJobseekrbuilder);

                AddJobseekrProfile(new MEmployeerViewdProfiles() { AspirantId = Jobseekr.AspirantId, Email = Jobseekr.Email, Employeername = Memployeerobj.FirstName + " " + Memployeerobj.LastName, JobseekerId = Jobseekr._id.ToString(), Viewedat = DateTime.Now, Name = Jobseekr.Name, MEmployeerId = Memployeerobj._id.ToString(), Mobile = Jobseekr.Mobile, Jobseekrprofilepath = Jobseekr.Profilepicture });

                employerDAOservice.Commitmongo();

                return await Task.FromResult(new Response()
                {
                    response = new
                    {
                        model = Jobseerkobj
                    },
                    StatusCode = StatusCodes.Status200OK,
                    Message = SucessMessages.Updated,
                    Iserror = false,
                    Status = SucessMessages.Inserted,
                    ISAuthoruizetoviewProfiles=true
                });
            }
            else
            {
                return Profileviewcount;
            }
           
        }

        public Response AddEmployerTransactions(MTransactionsmodel model)
        {
            //var transaction = employerDAOservice.ServiceRepository<MEmployeerTransactions>().GetFirstOrDefault(x => x.EmployeerId == model.EmployerId);
            MEmployer Memployeerobj = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.EmployerId == model.EmployerId).Result;

            decimal CGST = 0, SGST = 0, IGST = 0, Totalinvoice = 0, final_invoiceamount = 0, rounding = 0;
            string TotalAmountwords = "";

                CGST =model.CGST;
                SGST =model.SGST;
                Totalinvoice = model.finalamount + CGST + SGST;

            rounding = Math.Round(Totalinvoice) - Totalinvoice;
            final_invoiceamount = Math.Round(Totalinvoice);
            TotalAmountwords = NumberToWords.ConvertToWords(final_invoiceamount.ToString());
            employerDAOservice.ServiceRepository<MTransactionsmodel>().mongoAdd(new MTransactionsmodel()
            {
                MEmployerId = Memployeerobj._id.ToString(),
                Invoicenumber = model.Invoicenumber,
                EmployerId = model.EmployerId,
                finalamount = model.finalamount,
                InvoiceValue = final_invoiceamount,
                Invoicein_words = TotalAmountwords,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Planmodel = model.Planmodel,
                Validity = model.Validity,
                ValidityinDays = model.ValidityinDays,
                ProfileViews = model.ProfileViews,
                Price = model.Price,
                purchasedate = model.purchasedate,
                planstartdate = model.planstartdate,
                planenddate = model.planenddate,
                PriceIncludeGST = final_invoiceamount,
                RazorpayInvoiceNumber = model.RazorpayInvoiceNumber,
                CGST = CGST,
                SGST = SGST,
                IGST = IGST,
                disscount = 0,
                companyAccounname = configuration.GetSection("ApplicationSettings:companyname").Value,
                companyaccountsno = configuration.GetSection("ApplicationSettings:companyaccountno").Value,
                companybank = configuration.GetSection("ApplicationSettings:compnaybank").Value,
                companybranch = configuration.GetSection("ApplicationSettings:compnanybrach").Value,
                companyifsc = configuration.GetSection("ApplicationSettings:compnayifsc").Value,
                pannumber = configuration.GetSection("ApplicationSettings:PAN").Value,
                GSTOfparty = configuration.GetSection("ApplicationSettings:GSTno").Value,
                placeofsupply = configuration.GetSection("ApplicationSettings:placeofsupplay").Value,
                SACCategorey = configuration.GetSection("ApplicationSettings:ServiceCategorey").Value,
                SACcode = configuration.GetSection("ApplicationSettings:SACcode").Value,
                SMSandEmail = "",
                StateGSTcode = "",
                Roundingoff = rounding,
                Status = "",
                GST = CGST + SGST,
                GSTsatatus = "",
                IsUnlimited = true,
                plandescription = "",
                ProfileViewsnumber = 0,
                TotalCVdownload = 0,
                UnderpaybleRCM = "NA",
                _id = model._id,
            });
            employerDAOservice.Commitmongo();
                return new Response()
                {

                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.Inserted,

                };
            

            
        }

        public Response GetFifthplandetails(EmployerFifthPlan model)
        {
            var fiftplan = employerDAOservice.ServiceRepository<EmployerFifthPlan>().Getfilter();
            return new Response()
            {
                response = new
                {
                    model = fiftplan
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }

        #region Privatemethods
        private void UploadEmployeerProfilepicture(int UserId,string filepath,string filename,string filetype)
        {
            var MEmployeer = Builders<MEmployer>.Filter.Eq(x => x.UserId, UserId);
            var updateEmployeerbuilder = Builders<MEmployer>.Update.Set(x => x.Profilepicpath, filepath).Set(x => x.profilepicname, filename).Set(x=>x.Profilepictype, filetype);
            employerDAOservice.ServiceRepository<MEmployer>().mongoUpdateasync(MEmployeer, updateEmployeerbuilder);
        }

        private void AddUserrole(int UserId, int RoleId,string Role)
        {
            employerDAOservice.ServiceRepository<aspnetuserroles>().Add(new aspnetuserroles() { UserId = UserId, RoleId = RoleId, RoleId1 = RoleId, UserId1 = UserId, Discriminator = Role });
        }

        public Task<Response> UpdateJobseekershuffling()
        {
            throw new NotImplementedException();
        }

        private void UpdateEmployeertate(int UserId,int state,string statename)
        {
            var MEmployeer = Builders<MEmployer>.Filter.Eq(x => x.UserId, UserId);
            var updateEmployeerbuilder = Builders<MEmployer>.Update.Set(x => x.state, state).Set(x => x.statename, statename).Set(x => x.LastActivedate, DateTime.Now);
            employerDAOservice.ServiceRepository<MEmployer>().mongoUpdateasync(MEmployeer, updateEmployeerbuilder);
        }
        private void LastActivedate(int UserId)
        {
            var MEmployeer = Builders<MEmployer>.Filter.Eq(x => x.UserId, UserId);
            var updateEmployeerbuilder = Builders<MEmployer>.Update.Set(x => x.LastActivedate, DateTime.Now);
            employerDAOservice.ServiceRepository<MEmployer>().mongoUpdateasync(MEmployeer, updateEmployeerbuilder);
        }
        private void AddJobseekrProfile(MEmployeerViewdProfiles model)
        {
            employerDAOservice.ServiceRepository<MEmployeerViewdProfiles>().mongoAdd(model);
        }

        public GridResponse GetEmployeerviewdProfiles(PageData page, string Sort, bool descending, string name, string email, string mobile, DateTime fromdate, DateTime todate,int UserId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = employerDAOservice.ServiceRepository<MEmployeerViewdProfiles>().Getmongiquerable().AsQueryable();


            if (employerDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == UserId).RoleId == Constants.Userroles.Employer.ToInt32())
            {
                var MEmployeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
                result = result.Where(x => x.MEmployeerId == MEmployeer._id.ToString());
            }

            if (fromdate.Isdatenull())
            {
               // fromdate = fromdate.AddDays(1);
                result = result.Where(x => x.Viewedat >= fromdate);
            }


            if (todate.Isdatenull())
            {

                todate = todate.AddDays(1);
                result = result.Where(x => x.Viewedat <= todate);
            }

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(x.Name));
            if (mobile != "" && mobile != null)
                result = result.Where(x => x.Mobile.Contains(x.Mobile));          
            if (email != "" && email != null)
                result = result.Where(x => x.Email.Contains(x.Email));


            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.OrderBy(Sort, descending).Skip(skip).Take(limit).ToList().Select(x => new
            {
                name = x.Name,
                mobile = x.Mobile,
                email = x.Email,
                employeername = x.Employeername,
                viewedat = x.Viewedat,
                jobseekrprofilepath = (x.Jobseekrprofilepath != string.Empty && x.Jobseekrprofilepath != null) ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + x.Jobseekrprofilepath : configuration.GetSection("ApplicationSettings:Deflutuserimage").Value,
                jobseekerId = x.JobseekerId,
                id = x._id.ToString(),
               
            }));

            return gridResponse;
        }

        public Response GetEmployeerviewdProfiles(int UserId)
        {
            var MEmployeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x=>x.UserId==UserId).Result;
            var profileviews = employerDAOservice.ServiceRepository<MEmployeerViewdProfiles>().Getmongiquerable().Where(x=>x.MEmployeerId== MEmployeer._id.ToString())
                .OrderByDescending(x=>x.Viewedat).Take(10).Select(x=>new { 
                    name=x.Name,
                    mobile=x.Mobile,
                    email=x.Email,
                    viewedat = x.Viewedat,
                    jobseekrprofilepath = (x.Jobseekrprofilepath != string.Empty && x.Jobseekrprofilepath != null) ? configuration.GetSection("ApplicationSettings:fileStoragePublishpath").Value + x.Jobseekrprofilepath : configuration.GetSection("ApplicationSettings:Deflutuserimage").Value,
                });
            var Employeerobj = new
            {
                totalview= MEmployeer.Profileviews,
                tillnowprofile= MEmployeer.tillnowprofileviews
            };
            return new Response()
            {
                response =new 
                {
                    Employeerobj=Employeerobj,
                    profileviews= profileviews

                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Inserted,

            };
        }

        public void Updateprofileviewcount(List<string>Jobseekers,int userId)
        {
            var Employeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == userId).Result;

            var checkisjobseekrisviewerlier = employerDAOservice.ServiceRepository<MEmployeerViewdProfiles>().Getmongiquerable()
              .Where(x => x.MEmployeerId == Employeer._id.ToString() && Jobseekers.Contains(x.JobseekerId)).Select(x=>x.JobseekerId);

            var NonViewedProfiles = Jobseekers.Except(checkisjobseekrisviewerlier.ToList());

            if (NonViewedProfiles.Count() > 0)
            {
                foreach (var item in NonViewedProfiles)
                {
                    var MJobseekr = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(item));
                    var Jobseekr = employerDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(item)).Result;
                    var Jobseerkobj = new
                    {
                        name = Jobseekr.Name,
                        mobile = Jobseekr.Mobile,
                        email = Jobseekr.Email,
                        id = Jobseekr._id.ToString()
                    };
                    List<EmployeerSearchsmodel> employeersearchs = new List<EmployeerSearchsmodel>();
                    if (Jobseekr.SearchedEmployeer != null)
                        employeersearchs = Jobseekr.SearchedEmployeer;

                    employeersearchs.Add(new EmployeerSearchsmodel() { EmployeerId = Employeer._id.ToString(), EmployeerName = Employeer.FirstName + " " + Employeer.LastName, Viewdate = DateTime.Now });
                    var updateJobseekrbuilder = Builders<MAspirant>.Update.Set(x => x.SearchedEmployeer, employeersearchs).Set(x => x.ProfileOrder, ProfileOrder.employeer_selected.ToInt32()).Set(x=>x.EmployeerViewddate,DateTime.Now);
                    employerDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(MJobseekr, updateJobseekrbuilder);

                    AddJobseekrProfile(new MEmployeerViewdProfiles() { AspirantId = Jobseekr.AspirantId, Email = Jobseekr.Email, Employeername = Employeer.FirstName + " " + Employeer.LastName, JobseekerId = Jobseekr._id.ToString(), Viewedat = DateTime.Now, Name = Jobseekr.Name, MEmployeerId = Employeer._id.ToString(), Mobile = Jobseekr.Mobile, Jobseekrprofilepath = Jobseekr.Profilepicture });

                    employerDAOservice.Commitmongo();
                }


                var MEmployeer = Builders<MEmployer>.Filter.Eq(x => x._id, Employeer._id);
                var updateEmployeerbuilder = Builders<MEmployer>.Update.Set(x => x.tillnowCVdownload, (Employeer.tillnowCVdownload + NonViewedProfiles.Count())).Set(x => x.tillnowprofileviews, (Employeer.tillnowCVdownload + NonViewedProfiles.Count())).Set(x => x.LastActivedate, DateTime.Now);
                employerDAOservice.ServiceRepository<MEmployer>().mongoUpdateasync(MEmployeer, updateEmployeerbuilder);
                employerDAOservice.Commitmongo();
            }
            
        }

        public Response FilterEmployeer(string name, string mobile, string email, string organization)
        {

            var result = employerDAOservice.ServiceRepository<MEmployer>().Getmongiquerable().AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.FirstName.Contains(name)|| x.LastName.Contains(name));
            if (mobile != "" && mobile != null)
                result = result.Where(x => x.Mobile.Contains(mobile));
            if (email != "" && email != null)
                result = result.Where(x => x.Email.Contains(email));
            if (organization != "" && organization != null)
                result = result.Where(x => x.Organization.Contains(organization));

            return new Response()
            {
                response = new
                {
                   
                    employeers= result.OrderBy(x => x.FirstName).Skip(0).Take(10).Select(x => new 
                    { 
                    name=x.FirstName+" "+x.LastName,
                    orgnization=x.Organization,
                    mobile=x.Mobile,
                    email=x.Email,
                    id=x._id.ToString(),
                    })
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Inserted,

            };
        }

        public Response AddCmapignforEmployeer(MCampaignmaster model, int UserId,string EmployeerId)
        {
            try
            {
                //Get the Login User name based on userId
                model.CreatedBy = GetUsernamebyUserId(UserId);
                model.createdbyUserId = UserId;

                var Employeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(EmployeerId)).Result;
                //Ad the Model of Mcampign 
                AddMasterCmapginplan(model, Employeer);

                //Add the Transaction model
                Addcampgintransaction(model, UserId, Employeer);

                employerDAOservice.Commitmongo();

                return new Response()
                {
                    response = new
                    {
                    },
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Added Transaction Sucessfully",

                };
            }
            catch(Exception Ex)
            {
                employerDAOservice.ServiceRepository<MExceptionStoarge>().mongoAdd(new MExceptionStoarge() { CreatedDate=DateTime.Now,Exception=Ex.Message +"  Inner Exception "+Ex.InnerException,Screen="Campagin Plan Request",Methods="Campign Plan Request Add",UserId=UserId});
                employerDAOservice.Commitmongo();
                throw Ex;
            }
           
        }

        private string GetUsernamebyUserId(int UserId)
        {
            var Employee = employerDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.UserId == UserId);
            return Employee.Name;
        }

        private void AddMasterCmapginplan(MCampaignmaster model, MEmployer employeer)
        {
            model.CreatedDate = DateTime.Now;
            model.Organizationname = employeer.Organization;
            model.Mobile = employeer.Mobile;
            model.Email = employeer.Email;
            model.EmployeerUserId = employeer.UserId;
            model.Location = employeer.Cityname;

            employerDAOservice.ServiceRepository<MCampaignmaster>().mongoAdd(model);

        }

        private void Addcampgintransaction(MCampaignmaster campignmodel,int UserId,MEmployer employeer)
        {
            MTransactionsmodel model = new MTransactionsmodel();
            var EmployeerAddress = employerDAOservice.ServiceRepository<MEmployerAddress>().mongoGetFirstOrDefault(x => x.MEmployerId == employeer._id.ToString()).Result;
            var Lookupstate = employerDAOservice.ServiceRepository<Lookupstate>().GetFirstOrDefault(x=>x.Id== EmployeerAddress.state);

            model.address = EmployeerAddress.Address;
            model.CGST = campignmodel.CGST;
            model.SGST = campignmodel.SGST;
            model.IGST = campignmodel.IGST;
            model.disscount=campignmodel.disscount;
         
            model.EmployerId= employeer.EmployerId;
            model.finalamount=campignmodel.disscount;
            model.FirstName= employeer.FirstName;
            model.GST=campignmodel.GST;
            model.Gstno= employeer.GSTSNumber;
           
            model.GSTsatatus= employeer.GSTStatus==9?"Yes":"NO";
            model.Invoicein_words = NumberToWords.ConvertToWords(campignmodel.Totalinvoicevalue.ToString());
            model.Invoicenumber = GenerateInvoicenumber();
            model.InvoiceValue = campignmodel.totalgrossamount;
            model.LastName = employeer.LastName;
            model.MEmployerId = employeer._id.ToString();
          
            model.plandescription ="";
           
            model.Planmodel =campignmodel.Plan_name;
            
            model.Price = campignmodel.amount;
            model.PriceIncludeGST = campignmodel.totalgrossamount;
            model.ProfileViews = 0;
            model.ProfileViewsnumber = 0;
            model.purchasedate = DateTime.Now;
           
            model.StateGSTcode = Lookupstate.code;
            model.StateId = campignmodel.stateId;
            model.Statename = Lookupstate.Name;
            model.Status = "Payment Success";
            model.TotalCVdownload =0;
            model.UnderpaybleRCM = "NA";
            model.Validity = 0;
            model.ValidityinDays = 0;

            model.companyAccounname = configuration.GetSection("ApplicationSettings:companyname").Value;
            model.companyaccountsno = configuration.GetSection("ApplicationSettings:companyaccountno").Value;
            model.companybank = configuration.GetSection("ApplicationSettings:compnaybank").Value;
            model.companybranch = configuration.GetSection("ApplicationSettings:compnanybrach").Value;
            model.companyifsc = configuration.GetSection("ApplicationSettings:compnayifsc").Value;
            model.pannumber = configuration.GetSection("ApplicationSettings:PAN").Value;
            model.GSTOfparty = configuration.GetSection("ApplicationSettings:GSTno").Value;
            model.placeofsupply = configuration.GetSection("ApplicationSettings:placeofsupplay").Value;
            model.SACCategorey = configuration.GetSection("ApplicationSettings:ServiceCategorey").Value;
            model.SACcode = configuration.GetSection("ApplicationSettings:SACcode").Value;

            employerDAOservice.ServiceRepository<MTransactionsmodel>().mongoAdd(model);
        }


        private string GenerateInvoicenumber()
        {
            var Mtransacationfilter = Builders<MTransactionsmodel>.Filter.Gt(x => x.Price, 0);
            var Mtrasnsactionmodel = employerDAOservice.ServiceRepository<MTransactionsmodel>().mongoGetfilter(Mtransacationfilter).Result;
            string invoicenumber = string.Format("VISELJ{0}{1}", DateTime.Now.Year.ToString().Substring(2, 2) + "" + DateTime.Now.AddYears(1).Year.ToString().Substring(2, 2), (Mtrasnsactionmodel.ToList().Count + 1).ToString("00000"));
            return invoicenumber;
        }

        public GridResponse GetCampigtransactions(PageData page, string Sort, bool descending, string name, string email, string mobile, DateTime fromdate, DateTime todate, int UserId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = employerDAOservice.ServiceRepository<MCampaignmaster>().Getmongiquerable().AsQueryable();


            if (employerDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == UserId).RoleId == Constants.Userroles.Employer.ToInt32())
            {
                var MEmployeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
                result = result.Where(x => x.EmployeerId == MEmployeer._id.ToString());
            }
            if (fromdate.Isdatenull())
            {
                result = result.Where(x => x.CreatedDate >= fromdate);
            }
            if (todate.Isdatenull())
            {
                todate = todate.AddDays(1);
                result = result.Where(x => x.CreatedDate <= todate);
            }
            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(x.Name));
            if (mobile != "" && mobile != null)
                result = result.Where(x => x.Mobile.Contains(x.Mobile));
            if (email != "" && email != null)
                result = result.Where(x => x.Email.Contains(x.Email));


            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.OrderBy(Sort, descending).Skip(skip).Take(limit).Select(x => new
            {
                name = x.Name,
                mobile = x.Mobile,
                email = x.Email,
                plan_name = x.Plan_name,
                start_at = x.Startedat,
                campginurl = configuration.GetSection("Campaginplan:EmployeerURL").Value + "" + x._id.ToString(),
                endat = x.Endedat,
                createdby=x.CreatedBy,
                id = x._id.ToString(),
                amount=x.Totalinvoicevalue,
                organization=x.Organizationname

            }));

            return gridResponse;
        }

        public GridResponse GetCampignRegisteredAspirants(PageData page, string Sort, bool descending, string name, string email, string mobile, DateTime fromdate, DateTime todate, int UserId, string campaginid)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = employerDAOservice.ServiceRepository<MCampginAspirants>().Getmongiquerable().AsQueryable();


            if (employerDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == UserId).RoleId == Constants.Userroles.Employer.ToInt32())
            {
                var MEmployeer = employerDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
                result = result.Where(x => x.EmployeerId == MEmployeer._id.ToString());
            }
            if (fromdate.Isdatenull())
            {
                result = result.Where(x => x.CreatedDate >= fromdate);
            }
            if (todate.Isdatenull())
            {
                todate = todate.AddDays(1);
                result = result.Where(x => x.CreatedDate <= todate);
            }
            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));
            if (mobile != "" && mobile != null)
                result = result.Where(x => x.Mobile.Contains(mobile));
            if (email != "" && email != null)
                result = result.Where(x => x.Email.Contains(email));
            if (campaginid != "" && campaginid != null)
                result = result.Where(x => x.campgainId.Contains(campaginid));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.OrderBy(Sort, descending).Skip(skip).Take(limit).ToList().Select(x => new
            {
                name = x.Name,
                mobile = x.Mobile,
                email = x.Email,
                employeer = x.Employeername,
                gender = x.GenderText,
                dob = x.DOB,
                createdat = x.CreatedDate,
                id = x._id.ToString(),
                jobseekerid = x.JobseekerId,
                location = x.Location

            }));

            return gridResponse;
        }
        #endregion
    }




    #endregion

}

