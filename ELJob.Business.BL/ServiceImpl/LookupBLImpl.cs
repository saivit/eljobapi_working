﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Lookup;
using ELJob.Domain.Model.Views;
using ELJob.Helper.Common;
using ELJob.Helper.Formater;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static ELJob.Helper.Common.Constants;

namespace ELJob.Business.BL.ServiceImpl
{
   public class LookupBLImpl: ILookupBL
    {
        #region Private declarations
        ILookUpDAO lookupDAOservice;
        ILocationsDAO locationsDAOservice;
        IAspirantDAO aspirantDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;
        #endregion

        #region Constructor
        public LookupBLImpl(ApplicationDbContext context, IUnitOfWork uintofwork, 
            IConfiguration _configuration, ILookUpDAO lookupDAOservice, ILocationsDAO locationsDAOservice, IAspirantDAO aspirantDAOservice)
        {
            this.lookupDAOservice = lookupDAOservice;
            this.locationsDAOservice = locationsDAOservice;
            this.aspirantDAOservice = aspirantDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;

        }

        public Response GetStatusbyType(int StatusType)
        {
            var Result = lookupDAOservice.ServiceRepository<vw_Lookupstatus>().Getfilter(x => x.staustype == StatusType);
            return new Response()
            {
                response = new
                {
                    model = Result
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }

        public Response GetZones()
        {
            var Result = lookupDAOservice.ServiceRepository<LookupZone>().Getfilter();
            return new Response()
            {
                response = new
                {
                    model = Result
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }



        public Response GetTerritoriesbyId(int Zone)
        {
            var Territory = lookupDAOservice.ServiceRepository<LookupLocation>().Getfilter(x => x.Zone == Zone);

            return new Response()
            {
                response =new
                {
                    model= Territory
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message =SucessMessages.ScuessLoad

            };
        }

        public Response GetRoles()
        {
            var Result = lookupDAOservice.ServiceRepository<aspnetroles>().Getfilter();
            return new Response()
            {
                response = new
                {
                    model = Result
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }

        public Response GetOtherEducationById(int Id)
        {
            var otheredubyid = lookupDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model=otheredubyid
                },
                StatusCode=(StatusCodes.Status200OK),
                Status=SucessMessages.OK,
                Iserror=false,
                Message=SucessMessages.ScuessLoad
            };
        }

        public Response GetCouncelingstatus()
        {
            var Sourceof_lead = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.SourceOfLead.ToInt32());
            var Conditionalyes = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.ConditionalYes.ToInt32());
            var Rejectionreason = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.RejectionReason.ToInt32());
            var followupreason = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.Followup.ToInt32());
            var jobeerkcounceling_reason = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.Jobseeker_counceling_status.ToInt32());

            return new Response()
            {
                response = new
                {
                    
                    Sourceof_lead = Sourceof_lead.ToList(),
                    Conditionalyes = Conditionalyes.ToList(),
                    Rejectionreason = Rejectionreason.ToList(),
                    followupreason = followupreason.ToList(),
                    jobeerkcounceling_reason = jobeerkcounceling_reason.ToList(),
                 
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
          
        }

        public Response GetEmployeerUpdateProfileParameters()
        {
            var Gststatus = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.GSTstatus.ToInt32());
            var Lookupstates = lookupDAOservice.ServiceRepository<Lookupstate>().Getfilter(x => x.IsActive ==true);
            var Lookupcity = lookupDAOservice.ServiceRepository<LookupCity>().Getfilter(x => x.IsActive == true);
            var Lookupsector = lookupDAOservice.ServiceRepository<LookupSector>().Getfilter(x => x.IsActive == true);

            return new Response()
            {
                response = new
                {

                    Gststatus = Gststatus.ToList(),
                    Lookupstates = Lookupstates.ToList(),
                    Lookupcity = Lookupcity.ToList(),
                    Lookupsector= Lookupsector.ToList()


                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }

        public Response GetJobseekerDDLValues()
        {
            var ReadytoMigrate = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.ConditionalYes.ToInt32());
            var MigrateReason = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.MigrateReason.ToInt32());
            var SourceofLead = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.SourceOfLead.ToInt32());
            var Experience = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.Experience.ToInt32());
            var ModeofEducation = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.ModeOfEducation.ToInt32());
            var LanguageProficiency = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.LanguageProficiency.ToInt32())
                .Select(x => new {
                    id = x.Id,
                    name = x.status
                });

            var NumericAptitude = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.AptitudeandComputerSkills.ToInt32())

                 .Select(x => new {
                     id = x.Id,
                     name = x.status
                 });
            var CommunicativeEnglish = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.SkillScale.ToInt32()).Select(x => new {
                id = x.Id,
                name = x.status
            });
            var ComputerOperations = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.AptitudeandComputerSkills.ToInt32()).Select(x => new {
                id = x.Id,
                name = x.status
            });
            var Location = locationsDAOservice.ServiceRepository<LookupLocation>().Getfilter().Select(x => new {
                Id = x.Id,
                Location = x.Name
            });
            var State = locationsDAOservice.ServiceRepository<Lookupstate>().Getfilter().Select(x => new {
                Id = x.Id,
                State = x.Name
            });
            var City = locationsDAOservice.ServiceRepository<LookupCity>().Getfilter().Select(x => new {
                Id = x.Id,
                City = x.Name
            });
            var Sector = locationsDAOservice.ServiceRepository<LookupSector>().Getfilter().Select(x => new {
                id = x.Id,
                name = x.Name
            });
            var Qualification = locationsDAOservice.ServiceRepository<LookupEducation>().Getfilter().Select(x => new {
                Id = x.Id,
                Qualification = x.Name
            });

            var OtherQualification = locationsDAOservice.ServiceRepository<LookupOtherEducation>().Getfilter().Select(x => new
            {
                id=x.Id,
                OtherQualification=x.Name
            });
            var Language = locationsDAOservice.ServiceRepository<LookupLanguage>().Getfilter().Select(x => new {
                Id = x.Id,
                Language = x.Name

            });



            return new Response()
            {
                response = new
                {
                    ReadytoMigrate = ReadytoMigrate.ToList(),
                    MigrateReason = MigrateReason.ToList(),
                    SourceofLead = SourceofLead.ToList(),
                    Experience = Experience.ToList(),
                    ModeofEducation = ModeofEducation.ToList(),
                    LanguageProficiency = LanguageProficiency.ToList(),
                    NumericAptitude = NumericAptitude.ToList(),
                    CommunicativeEnglish = CommunicativeEnglish.ToList(),
                    ComputerOperations = ComputerOperations.ToList(),
                    Location = Location.ToList(),
                    Sector = Sector.ToList(),
                    Qualification = Qualification.ToList(),
                    Language = Language.ToList(),
                    State = State.ToList(),
                    City= City.ToList()




                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };

            
        }

        public Response GetJosbsearchParameters()
        {
             var Location = locationsDAOservice.ServiceRepository<LookupLocation>().Getfilter(x=>x.IsActive==true).Select(x => new {
                Id = x.Id,
                name = x.Name
            });
            var Sector = locationsDAOservice.ServiceRepository<LookupSector>().Getfilter(x => x.IsActive == true).Select(x => new {
                Id = x.Id,
                name = x.Name
            });
            var Qualification = locationsDAOservice.ServiceRepository<LookupEducation>().Getfilter(x => x.IsActive == true).Select(x => new {
                Id = x.Id,
                name = x.Name
            });

            var AdditinalQualification = locationsDAOservice.ServiceRepository<LookupOtherEducation>().Getfilter(x => x.IsActive == true).Select(x => new {
                Id = x.Id,
                name = x.Name
            });

            
            var Language = locationsDAOservice.ServiceRepository<LookupLanguage>().Getfilter(x => x.IsActive == true).Select(x => new {
                Id = x.Id,
                name = x.Name

            });

            var NumericAptitude = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.AptitudeandComputerSkills.ToInt32());
            var CommunicativeEnglish = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.SkillScale.ToInt32());
            var ComputerOperations = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.AptitudeandComputerSkills.ToInt32());

            var SalarayRange = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.Salary_Ranage.ToInt32());

            var YesorNo = lookupDAOservice.ServiceRepository<vw_StatusMapping>().Getfilter(x => x.StatusType == Constants.Lookupstatustype.ConditionalYes.ToInt32());

            return new Response()
            {
                response = new
                {
                  
                    NumericAptitude = NumericAptitude.ToList(),
                    CommunicativeEnglish = CommunicativeEnglish.ToList(),
                    ComputerOperations = ComputerOperations.ToList(),
                    Location = Location.ToList(),
                    Sector = Sector.ToList(),
                    Qualification = Qualification.ToList(),
                    Language = Language.ToList(),
                    AdditinalQualification= AdditinalQualification.ToList(),
                    SalarayRange= SalarayRange.ToList(),
                    YesorNo= YesorNo.ToList()


                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }

        public Response GetLanguages()
        {
            var Result = lookupDAOservice.ServiceRepository<LookupLanguage>().Getfilter();
            return new Response()
            {
                response = new
                {
                    model = Result
                },
                StatusCode = (StatusCodes.Status200OK),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }


        #endregion

    }
}
