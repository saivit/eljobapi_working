﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Shared;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Lookup;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Formater;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static ELJob.Helper.Common.Constants;

namespace ELJob.Business.BL.ServiceImpl
{
    public class AspirantSelfEnrollmentBLImpl: IAspirantSelfEnrollmentBL
    {
        #region Private declarations
        IAspirantDAO aspirantDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;

        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 


        #endregion

        #region Constructor
        public AspirantSelfEnrollmentBLImpl(ApplicationDbContext context, IUnitOfWork uintofwork,
            IConfiguration _configuration, IAspirantDAO aspirantDAOservice, UserManager<ApplicationUser> userManager)
        {
            this.aspirantDAOservice = aspirantDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;
            this.userManager = userManager;
        }
        #endregion
        #region AspirantSelfEnrollment BL Implimentation

        public Response AddAspirantPersonal(MAspirantPersonal model)
        {
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));
            var asppersonal = aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoGetFirstOrDefault(x => x.MJobseekerId == model.MJobseekerId).Result;
            int Age = (int)((DateTime.Now - model.DOB).TotalDays) / 365;
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.Age, Age).Set(x => x.DOB, model.DOB);
            var Lookuplocarion = aspirantDAOservice.ServiceRepository<LookupLocation>().Getfilter();
            var LookupReason = aspirantDAOservice.ServiceRepository<LookupStatus>().Getfilter();
            var Lookupcity = aspirantDAOservice.ServiceRepository<LookupCity>().Getfilter();
            var Lookupstate = aspirantDAOservice.ServiceRepository<Lookupstate>().Getfilter();

            model.Reason_Name1 = ""; model.Reason_Name2 = ""; model.Reason_Name3 = "";
            model.Location1_Name = ""; model.Location2_Name = ""; model.Location3_Name = "";


            List<Selectionvalues> lstlocation = new List<Selectionvalues>();
            List<int> Locations = new List<int>();
            if (model.readToMigrateLocation)
            {
                if (model.Location1 > 0)
                {
                    string Location1_name = Lookuplocarion.Where(x => x.Id == model.Location1).FirstOrDefault().Name;
                    lstlocation.Add(new Selectionvalues()
                    {
                        Id = model.Location1,
                        Name = Location1_name
                    });
                    Locations.Add(model.Location1);
                    model.Location1_Name = Location1_name;
                }

                // Location 2

                if (model.Location2 > 0)
                {
                    string Location2_name = Lookuplocarion.Where(x => x.Id == model.Location2).FirstOrDefault().Name;
                    lstlocation.Add(new Selectionvalues()
                    {
                        Id = model.Location2,
                        Name = Location2_name
                    });
                    Locations.Add(model.Location2);
                    model.Location2_Name = Location2_name;
                }

                //Location 3

                if (model.Location3 > 0)
                {
                    string Location3_name = Lookuplocarion.Where(x => x.Id == model.Location3).FirstOrDefault().Name;
                    lstlocation.Add(new Selectionvalues()
                    {
                        Id = model.Location3,
                        Name = Location3_name
                    });
                    Locations.Add(model.Location3);
                    model.Location3_Name = Location3_name;
                }


                //Reason 1

                if (model.Reason1 > 0)
                {
                    string Reason_Name1 = LookupReason.Where(x => x.Id == model.Reason1).FirstOrDefault().Name;
                    //lstlocation.Add(new Selectionvalues()
                    //{
                    //    Id = model.Reason1,
                    //    Name = Reason_Name1
                    //});
                    //Locations.Add(model.Reason1);
                    model.Reason_Name1 = Reason_Name1;
                }

                //Reason 2

                if (model.Reason2 > 0)
                {
                    string Reason_Name2 = LookupReason.Where(x => x.Id == model.Reason2).FirstOrDefault().Name;
                    //lstlocation.Add(new Selectionvalues()
                    //{
                    //    Id = model.Reason2,
                    //    Name = Reason_Name2
                    //});
                    //Locations.Add(model.Reason2);
                    model.Reason_Name2 = Reason_Name2;
                }
                //Reason 3
                if (model.Reason3 > 0)
                {
                    string Reason_Name3 = LookupReason.Where(x => x.Id == model.Reason3).FirstOrDefault().Name;
                    //lstlocation.Add(new Selectionvalues()
                    //{
                    //    Id = model.Reason3,
                    //    Name = Reason_Name3
                    //});
                    //Locations.Add(model.Reason3);
                    model.Reason_Name3 = Reason_Name3;
                }

            }
            else
            {
                model.Location1 = 0; model.Location2 = 0; model.Location3 = 0;
                model.Reason1 = 0; model.Reason2 = 0; model.Reason3 = 0;

            }


            List<Selectionvalues> lstcity = new List<Selectionvalues>();
            List<int> cities = new List<int>();
            if (model.City > 0)
            {
                string City_Name = Lookupcity.Where(x => x.Id == model.City).FirstOrDefault().Name;
                lstcity.Add(new Selectionvalues()
                {
                    Id = model.City,
                    Name = City_Name
                });
                cities.Add(model.City);

                model.City_Name = City_Name;

                string Reason_Name3 = LookupReason.Where(x => x.Id == model.Reason3).FirstOrDefault().Name;
                lstlocation.Add(new Selectionvalues()
                {
                    Id = model.City,
                    Name = City_Name
                });
                Locations.Add(model.City);

            }

            List<Selectionvalues> lststate = new List<Selectionvalues>();
            List<int> states = new List<int>();
            if (model.State > 0)
            {
                string State_Name = Lookupstate.Where(x => x.Id == model.State).FirstOrDefault().Name;
                lstcity.Add(new Selectionvalues()
                {
                    Id = model.State,
                    Name = State_Name
                });
                states.Add(model.State);
                model.State_Name = State_Name;
            }

            if (asppersonal == null)
            {

                aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoAdd(model);
                aspirantDAOservice.Commitmongo();
                updateJobseekrBuilder.Set(x => x.score, aspirantpersonal.Result.score + Profilescores.personaldetails.ToInt32());
            }
            else
            {

                var filter = Builders<MAspirantPersonal>.Filter.Eq(x => x.MJobseekerId, model.MJobseekerId);
                var update = Builders<MAspirantPersonal>.Update.Set(x => x.Housenumber, model.Housenumber)
                    .Set(x => x.streetName, model.streetName).Set(x => x.Latitude, model.Latitude)
                    .Set(x => x.Longitude, model.Longitude).Set(x => x.City, model.City).Set(x => x.State, model.State)
                    .Set(x => x.Pincode, model.Pincode).Set(x => x.readToMigrateLocation, model.readToMigrateLocation).Set(x => x.State_Name, model.State_Name)
                    .Set(x => x.City_Name, model.City_Name).Set(x => x.DOB, model.DOB).Set(x => x.Age, Age).Set(x => x.Location1, model.Location1)
                    .Set(x => x.Location1_Name, model.Location1_Name)
                    .Set(x => x.Location2, model.Location2).Set(x => x.Location2_Name, model.Location2_Name)
                     .Set(x => x.Location3, model.Location3).Set(x => x.Location3_Name, model.Location3_Name)
                    .Set(x => x.Reason1, model.Reason1).Set(x => x.Reason_Name1, model.Reason_Name1)
                    .Set(x => x.Reason2, model.Reason1).Set(x => x.Reason_Name2, model.Reason_Name2)
                    .Set(x => x.Reason_Name3, model.Reason_Name3);

                aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoUpdateasync(filter, update);


            }

            if (Locations.Count() > 0)
            {
                updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.lst_Jobblookingcity, lstlocation).Set(x => x.Joblookingcity, Locations.ToArray());
                lstlocation.Add(new Selectionvalues() { Id = aspirantpersonal.Result.LocationId, Name = aspirantpersonal.Result.Location });
                Locations.Add(aspirantpersonal.Result.LocationId);
            }
            //updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.LatLocation, new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
            //                          new GeoJson2DGeographicCoordinates(Convert.ToDouble(model.Longitude), Convert.ToDouble(model.Latitude))));

            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };



        }
        public Response AddAspirantEducation(MAspirantEducation model)
        {
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            var LookupEducation = aspirantDAOservice.ServiceRepository<LookupEducation>().GetFirstOrDefault(x => x.Id == model.Qulification).Name;
            var LookupmodeofEducation = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.ModeofEducation).Name;
            model.QulificationName = LookupEducation;
            model.ModeofEducationName = LookupmodeofEducation;
            model.IsActive = true;
            model.AspirantId = aspirantpersonal.Result.AspirantId;
            LookupOtherEducation Othereducation;
            if (model.Qulification == Qulification.PG_and_Other.ToInt32())
            {

                Othereducation = aspirantDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Id == model.OtherQulification);
                model.OtherQulificationName = Othereducation.Name;
            }

            aspirantDAOservice.ServiceRepository<MAspirantEducation>().mongoAdd(model);
            aspirantDAOservice.Commitmongo();

            var MJobseekerEducation = aspirantDAOservice.ServiceRepository<MAspirantEducation>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId && x.IsActive == true);
            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Educationlist = MJobseekerEducation.Select(x => new Selectionvalues()
            {
                Id=x.Qulification,
                Name=x.QulificationName,

            }).ToList();
            //check is PG & Other Education.


            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_qualification, Educationlist).Set(x => x.Qualification, Educationlist.Select(x => x.Id).ToArray());
            if (model.Qulification == Qulification.PG_and_Other.ToInt32())
            {

                updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.AditionalQualification, MJobseekerEducation.Where(x => x.OtherQulification > 0 && x.IsActive == true).Select(x => x.OtherQulification).ToArray()).
                    Set(x => x.lst_additionalqulification, MJobseekerEducation.Where(x => x.OtherQulification > 0 && x.IsActive == true).Select(x => new Selectionvalues() { Id = x.OtherQulification, Name = x.QulificationName, IsActive = true }).ToList())
                    ;
            }
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);

            if (MJobseekerEducation.Count() == 1 || MJobseekerEducation != null)
            {
                updateJobseekrBuilder.Set(x => x.score, aspirantpersonal.Result.score + Profilescores.euducational.ToInt32());
            }
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode=StatusCodes.Status201Created,
                Status=SucessMessages.OK,
                Iserror=false,
                Message=SucessMessages.ScuessLoad

            };
        }
        public Response AddAspirantLanguages(MJobseekerLanguge model)
        {
            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            var LookupLanguage = aspirantDAOservice.ServiceRepository<LookupLanguage>().GetFirstOrDefault(x => x.Id == model.Language).Name;
            model.LanguageName = LookupLanguage;
            model.IsActive = true;
            model.AspirantId = aspirantpersonal.Result.AspirantId;
            aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().mongoAdd(model);
            aspirantDAOservice.Commitmongo();

            var MJobseekerLanguages = aspirantDAOservice.ServiceRepository<MJobseekerLanguge>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId && x.IsActive == true);


            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Languageslist = MJobseekerLanguages.Select(x => new Selectionvalues()
            {
                Id = x.Language,
                Name = x.LanguageName
            }).ToList();
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_Languages, Languageslist).Set(x => x.Language, Languageslist.Select(x => x.Id).ToArray());
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }
        public Response AddSkillAssessment(MAspirantSkillAssessment model)
        {
            var aspirantskill = aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoGetFirstOrDefault(x => x.MJobseekerId == model.MJobseekerId).Result;

            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));

            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));
            string Objective = "";
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.lst_sectors, model.lst_sectors)
                .Set(x => x.Sector, model.lst_sectors.Select(x => x.Id).ToArray()).Set(x => x.SalaryRange, model.Salary).Set(x => x.Salary, model.Salary);

            var Lookupstatus = aspirantDAOservice.ServiceRepository<LookupStatus>().Getfilter();

            model.Apptitude_name = Lookupstatus.Where(x => x.Id == model.Apptitude).FirstOrDefault().Name;
            model.CommunicativeEnglish_name = Lookupstatus.Where(x => x.Id == model.CommunicativeEnglish).FirstOrDefault().Name;
            model.Computeroperations_name = Lookupstatus.Where(x => x.Id == model.Computeroperations).FirstOrDefault().Name;

            List<Selectionvalues> lstsectors = new List<Selectionvalues>();
            List<int> sectors = new List<int>();

            if (aspirantskill == null)
            {
                updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.score, aspirantpersonal.Result.score + Profilescores.skill_assessment.ToInt32());
                aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoAdd(model);
            }
            else
            {
                var JobseekerSkillfilter = Builders<MAspirantSkillAssessment>.Filter.Eq(x => x.MJobseekerId, model.MJobseekerId);
                var updateJobseekrskillBuilder = Builders<MAspirantSkillAssessment>.Update.Set(x => x.lst_sectors, model.lst_sectors)
                    .Set(x => x.NotIntrestRoles, model.NotIntrestRoles)
                    .Set(x => x.Salary, model.Salary).Set(x => x.Computeroperations_name, model.Computeroperations_name)
                    .Set(x => x.Apptitude_name, model.Apptitude_name).Set(x => x.CommunicativeEnglish_name, model.CommunicativeEnglish_name)
                    .Set(x => x.CommunicativeEnglish, model.CommunicativeEnglish).Set(x => x.Apptitude, model.Apptitude)
                    .Set(x => x.Computeroperations, model.Computeroperations);

                aspirantDAOservice.ServiceRepository<MAspirantSkillAssessment>().mongoUpdateasync(JobseekerSkillfilter, updateJobseekrskillBuilder);
            }

            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);

            aspirantDAOservice.Commitmongo();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };
        }
        public Response AddAspirantCertification(MAspirantCertification model)
        {

            var aspirantpersonal = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x._id == ObjectId.Parse(model.MJobseekerId));
            model.AspirantId = aspirantpersonal.Result.AspirantId;
            model.IsActive = true;
            aspirantDAOservice.ServiceRepository<MAspirantCertification>().mongoAdd(model);
            aspirantDAOservice.Commitmongo();
            var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x._id, ObjectId.Parse(model.MJobseekerId));

            var Certifications = aspirantDAOservice.ServiceRepository<MAspirantCertification>().Getmongiquerable().Where(x => x.MJobseekerId == model.MJobseekerId).ToList();
            var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.Certification, "Certified");

            var certificates = Certifications.Select(x => new Selectionvalues() { Id = 0, Name = x.Institute }).ToList();
            if (Certifications.Count() > 0)
            {
                updateJobseekrBuilder.Set(x => x.lst_certificates, certificates);
            }
            aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
            aspirantDAOservice.Commitmongo();

            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };


        }




        #endregion

    }
}
