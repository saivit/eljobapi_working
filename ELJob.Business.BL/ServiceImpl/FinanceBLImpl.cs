﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Formater;
using ELJob.Helper.Grid;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELJob.Framework.SourceFramework;
using ELJob.Helper.Common;

namespace ELJob.Business.BL.ServiceImpl
{
   public class FinanceBLImpl: IFinanceBL
    {
        #region Private declarations
        IAspirantDAO aspirantDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;

        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 


        #endregion

        #region Constructor
        public FinanceBLImpl(ApplicationDbContext context, IUnitOfWork uintofwork,
            IConfiguration _configuration, IAspirantDAO aspirantDAOservice, UserManager<ApplicationUser> userManager)
        {
            this.aspirantDAOservice = aspirantDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;
            this.userManager = userManager;

        }
        #endregion
        public async Task<GridResponse> GetTransactions(PageData page, string Sort, bool descending, string name, string email, string mobile, int location, int zone, int jobseekerId, int status, int profiledatastatus, int Gender, int sourceofLead, DateTime fromdate, DateTime todate,int UserId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            try
            {
              
                var filterbuilder = Builders<MTransactionsmodel>.Filter.And(
                 //  Builders<MAspirant>.Filter.Eq("Location", "Hyderabad")
                 Builders<MTransactionsmodel>.Filter.Empty);
                var result = aspirantDAOservice.ServiceRepository<MTransactionsmodel>().Getmongiquerable();

                if( aspirantDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == UserId).RoleId==Constants.Userroles.Employer.ToInt32())
                {
                    var MEmployeer = aspirantDAOservice.ServiceRepository<MEmployer>().mongoGetFirstOrDefault(x => x.UserId == UserId).Result;
                    result = result.Where(x => x.MEmployerId== MEmployeer._id.ToString());
                }
                   
                if (fromdate.Isdatenull())
                    result = result.Where(x => x.purchasedate >= fromdate);
                if (todate.Isdatenull())
                    result = result.Where(x => x.purchasedate <= todate);
                if (todate.Isdatenull())
                    result = result.Where(x => x.purchasedate <= todate);

                double longi = 17.430200;
                gridResponse.pageData = await page.Processasync(result.Count(), skip, limit);
                skip = gridResponse.pageData.selectedPageSize * (gridResponse.pageData.currentPage - 1);
                if (skip < 0) skip = 0;
                var finalresult = result.OrderBy(Sort, descending).Skip(skip).Take(gridResponse.pageData.selectedPageSize).ToList();

                gridResponse.rows.AddRange(finalresult
                 .Select(x => new
                 {
                     purchasedate = x.purchasedate.Isdatenull()? x.purchasedate.ToString():"",
                     Price = x.PriceIncludeGST,
                     Planmodel = x.Planmodel,
                     planstartdate = x.planstartdate.Isdatenull() ? x.planstartdate.ToString() : "",
                     planenddate = x.planenddate.Isdatenull() ? x.planenddate.ToString() : "",
                     name = x.FirstName + " " + x.LastName,
                     id = x._id.ToString(),
                     status = x.Status,
                     EmployerId = x.EmployerId,
                     cgst=x.CGST,
                     sgst=x.SGST,
                     igst=x.IGST,
                     totalviews=x.ProfileViews,
                     invoice=x.Invoicenumber
                 }
                )
                 );
                //.OrderBy(Sort, descending).Skip(skip).Take(limit));

                return gridResponse;
            }
            catch(Exception Ex)
            {
                return gridResponse;
            }
           
        }

        public Response GetTransactionById(string Id)
        {
            throw new NotImplementedException();
        }


       


    }
}
