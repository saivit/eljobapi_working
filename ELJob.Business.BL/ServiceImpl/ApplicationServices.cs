﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Shared;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Aspirant;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Lookup;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Formater;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static ELJob.Helper.Common.Constants;
using ELJob.Domain.Model.Employee;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace ELJob.Business.BL.ServiceImpl
{
   public class ApplicationServices : IApplicationServices
    {
        #region Private declarations
        IAspirantDAO aspirantDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;
        IEmployerBL EmployeerBl;

        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 

        /// <summary>
        /// Customer service Business Layer
        /// </summary>
        /// 
        private readonly IAuthBL Authservice;

        #endregion

        #region Constructor
        public ApplicationServices(ApplicationDbContext context, IUnitOfWork uintofwork,
            IConfiguration _configuration, IAspirantDAO aspirantDAOservice, UserManager<ApplicationUser> userManager, IEmployerBL _EmployeerBl, IAuthBL _AuthBlservice)
        {
            this.aspirantDAOservice = aspirantDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;
            this.userManager = userManager;
            EmployeerBl = _EmployeerBl;
            Authservice = _AuthBlservice;

        }

        public Task<Response> JobProfileViewCraweler()
        {
            throw new NotImplementedException();
        }

        public Task<Response> StatusConevrtion(Migrationmodel model)
        {
            try
            {
                var aspirant = aspirantDAOservice.ServiceRepository<Aspirant>().GetFirstOrDefault(x => x.Id == model.Aspirantid);
                var Jobseekerfilter = Builders<MAspirant>.Filter.Eq(x => x.AspirantId, model.Aspirantid);
                var Maspirant = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetFirstOrDefault(x => x.AspirantId == model.Aspirantid).Result;

                var Jobseekerpersonalfilter = Builders<MAspirantPersonal>.Filter.Eq(x => x.MJobseekerId, Maspirant._id.ToString());
                var statusname = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.status).Name;
                var sourceofLead = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.sourceOfLead).Name;
                aspirant.statusId = model.status;
                aspirant.Status = statusname;
                aspirant.Createdat = model.Createddate;
                aspirant.Modifiedat = model.Createddate;
                aspirant.Createdby = model.UserId;
                aspirant.modifiedby = model.UserId;
                aspirant.SourceofLead = sourceofLead;
                aspirant.SourceofLeadId = model.sourceOfLead;

                var Lookpstate = aspirantDAOservice.ServiceRepository<Lookupstate>().GetFirstOrDefault(x => x.Id == model.state);
                var updateJobseekrperosnalBuilder = Builders<MAspirantPersonal>.Update.Set(x => x.State_Name, Lookpstate.Name).Set(x => x.streetName, model.street_name);
                string workingstatusname = model.Workingstatus == WorkingExperience.Experience.ToInt32() ? "Experince" : "Fresher";
                var updateJobseekrBuilder = Builders<MAspirant>.Update.Set(x => x.CreatedDate, model.Createddate).Set(x => x.Workingstatus, workingstatusname).Set(x => x.WorkingstatusId, model.Workingstatus)
                    .Set(x => x.Status, model.status).Set(x => x.Statusname, statusname).Set(x => x.UpdatedDate, model.LastUpdateddate)
                    .Set(x => x.ProfileDataStatusId, model.Profiledatastatus).Set(x => x.ProfileDataStatus, model.Profiledatastatusname)
                    .Set(x => x.SourceofLead, sourceofLead).Set(x => x.SourceofLeadId, model.sourceOfLead).Set(x => x.score, model.score)
                    
                    ;


                if (model.Reasdulereason > 0)
                {
                    var reshduleresaonstatusname = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.Reasdulereason).Name;
                    updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.ReschduleReasonId, model.Reasdulereason).Set(x => x.ReschduleReason, reshduleresaonstatusname).Set(x => x.ReschduleDate, model.Reschduledate).Set(x => x.Reschduletime, model.ReschduleTime);

                    var Employee = aspirantDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.UserId == model.UserId);

                    aspirantDAOservice.ServiceRepository<MAspirantResshduleHistory>().Add(new MAspirantResshduleHistory()
                    {
                        AspirantId = Maspirant.AspirantId,
                        Createdat = DateTime.Now,
                        Createdby = Employee.Name,
                        Name = Maspirant.Name,
                        JobseekerId = Maspirant._id.ToString(),
                        Reschduledate = model.Reschduledate,
                        Reschduletime = model.ReschduleTime,
                        RescduleId = model.Reasdulereason,
                        ReschduleReason = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.Reasdulereason).Name
                    });

                }

                if (model.RejectReason > 0)
                {

                    var Rejectionstatusname = aspirantDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.Reasdulereason).Name;
                    updateJobseekrBuilder = updateJobseekrBuilder.Set(x => x.rejectReasonId, model.RejectReason).Set(x => x.rejectReason, Rejectionstatusname).Set(x => x.otherRejectionReason, model.OtherreshduleReason);

                }

                aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerfilter, updateJobseekrBuilder);
                aspirantDAOservice.ServiceRepository<MAspirantPersonal>().mongoUpdateasync(Jobseekerpersonalfilter, updateJobseekrperosnalBuilder);
                aspirantDAOservice.Commitmongo();
                aspirantDAOservice.Commit();

                return Task.FromResult(new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = (StatusCodes.Status201Created),
                    CustomerId = "",
                    Status = SucessMessages.Exception,
                    Iserror = false,
                    Message = "Data Inserted"

                });

            }
            catch (Exception Ex)
            {
                aspirantDAOservice.ServiceRepository<MExceptionStoarge>().mongoAdd(new MExceptionStoarge() { 
                CreatedDate=DateTime.Now,
                Exception=$"Aspirant Id- {model.Aspirantid}  ::" + Ex.Message+" "+Ex.Message,
                Screen="Migration",Methods="Old data Migration"
                });
                aspirantDAOservice.Commitmongo();
                return Task.FromResult(new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = (StatusCodes.Status500InternalServerError),
                    CustomerId = "",
                    Status = SucessMessages.Exception,
                    Iserror = false,
                    Message = Ex.Message + " " + Ex.InnerException

                });
            }
        }

        public Task<Response> UpdateWorkexperincestatus()
        {
            var Maspirant = aspirantDAOservice.ServiceRepository<MAspirant>().Getmongiquerable().Where(x => x.Workingstatus == "Fresher");

            foreach (var item in Maspirant)
            {
                try
                {
                    var Jobseekerpersonalfilter = Builders<MAspirant>.Filter.Eq(x => x._id, item._id);
                    var updateJobseekrperosnalBuilder = Builders<MAspirant>.Update.Set(x => x.WorkingstatusId, WorkingExperience.Fresher.ToInt32());
                    aspirantDAOservice.ServiceRepository<MAspirant>().mongoUpdateasync(Jobseekerpersonalfilter, updateJobseekrperosnalBuilder);
                    aspirantDAOservice.Commitmongo();
                }
                catch(Exception Ex)
                {
                    Console.WriteLine($"Exception:{Ex.Message}");
                }
            
            }
            return Task.FromResult(new Response()
            {
                response = new
                {

                },
                StatusCode = (StatusCodes.Status200OK),
                CustomerId = "",
                Status = SucessMessages.Exception,
                Iserror = false,
                Message = ""

            });
        }


        #endregion
    }
}
