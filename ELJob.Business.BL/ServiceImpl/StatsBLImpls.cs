﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Business.Viewmodel.Stats;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Auth;
using ELJob.Domain.Model.Mongo;
using ELJob.Helper.Formater;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ELJob.Helper.Common.Constants;

namespace ELJob.Business.BL.ServiceImpl
{
   public class StatsBLImpls: IStatsBL
    {
        #region Private declarations
        IAspirantDAO aspirantDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;

        private readonly UserManager<ApplicationUser> userManager;
        /// <summary>
        /// User Signin Manager
        /// </summary>
        /// 


        #endregion

        #region Constructor
        public StatsBLImpls(ApplicationDbContext context, IUnitOfWork uintofwork,
            IConfiguration _configuration, IAspirantDAO aspirantDAOservice, UserManager<ApplicationUser> userManager)
        {
            this.aspirantDAOservice = aspirantDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;
            this.userManager = userManager;

        }

        public Task<Response> Jobseekerstats(StatasModel model)
        {
           
            var filterbuilder = Builders<MAspirant>.Filter.Empty;
            if (model.Location != null && model.Location.Count() > 0)
                filterbuilder &= (Builders<MAspirant>.Filter.AnyIn(x => x.LocationIdarray, model.Location.Select(x=>x.Id).ToArray()));
            if (model.fromdate.Isdatenull())
                filterbuilder &= (Builders<MAspirant>.Filter.Gt(x => x.CreatedDate, model.fromdate));
            if (model.fromdate.Isdatenull())
                filterbuilder &= (Builders<MAspirant>.Filter.Lt(x => x.CreatedDate, model.Todate));
            return Task.FromResult(new Response()
            {
                response = new
                {
                    jobseekerbystaus = aspirantDAOservice.ServiceRepository<MAspirant>().mongoGetfilter(filterbuilder).Result.Select(x => new
                    {
                        id=x.JobseekerId,
                        statusname = x.Statusname,
                        ProfileDataStatus = x.ProfileDataStatus,
                        Location = x.Location,
                        Gender = x.GenderText,
                        SourceofLead = x.SourceofLead,
                        Workingstatus = x.Workingstatus,
                        IsEmailvalidated = x.IsEmailvalidated,
                        SalaryRange = x.SalaryRange,
                        Age = x.Age,
                        score = x.score,
                        rejectReason = x.rejectReason,
                        ReschduleReason = x.ReschduleReason,
                        CreatedDate = x.CreatedDate,
                        name=x.Name,
                        mobile=x.Mobile,
                        email=x.Email
                        

                    })
                },
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            });
        }

        public Task<Response> EmployeerStats(StatasModel model)
        {
            var filterbuilder = Builders<MEmployer>.Filter.Empty;
            if (model.Location != null && model.Location.Count() > 0)
                filterbuilder &= (Builders<MEmployer>.Filter.AnyIn(x => x.city_array, model.Location.Select(x => x.Id).ToArray()));
            if (model.fromdate.Isdatenull())
                filterbuilder &= (Builders<MEmployer>.Filter.Gt(x => x.Createddate, model.fromdate));
            if (model.fromdate.Isdatenull())
                filterbuilder &= (Builders<MEmployer>.Filter.Lt(x => x.Createddate, model.Todate));
            if (model.Activefromdate.Isdatenull())
                filterbuilder &= (Builders<MEmployer>.Filter.Gt(x => x.LastActivedate, model.fromdate));
            if (model.ActiveTodate.Isdatenull())
                filterbuilder &= (Builders<MEmployer>.Filter.Lt(x => x.LastActivedate, model.Todate));
            if (model.state != null && model.state.Count() > 0)
                filterbuilder &= (Builders<MEmployer>.Filter.AnyIn(x => x.state_array, model.state.Select(x => x.Id).ToArray()));

            return Task.FromResult(new Response()
            {
                response = new
                {
                    jobseekerbystaus = aspirantDAOservice.ServiceRepository<MEmployer>().mongoGetfilter(filterbuilder).Result.Select(x => new
                    {
                        id = x._id.ToString(),
                        statusname = x.statename,
                        ProfileDataStatus = x.GSTStatus,
                        Location = x.CreatedByName,
                        Organizationtype = x.Organizationtype,
                        Sectors = x.Sectors,
                        statename = x.statename,
                        tillnowCVdownload = x.tillnowCVdownload,
                        tillnowprofileviews = x.tillnowprofileviews,
                        Validity = x.Validity,
                        Profileviews = x.Profileviews,
                        Price = x.Price,
                        planstart = x.planstart,
                        planend = x.planend,
                        CurrentPlan = x.CurrentPlan,
                        name=x.FirstName+" "+x.LastName,
                        mobile = x.Mobile,
                        email = x.Email,
                        createat=x.Createddate,
                        lastactive=x.LastActivedate,
                        employeerid=x.EmployerId
                    })
                },
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            });
        }


        #endregion

    }
}
