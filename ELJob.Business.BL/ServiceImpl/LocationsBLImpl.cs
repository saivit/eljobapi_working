﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Lookup;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using static ELJob.Helper.Common.Constants;

namespace ELJob.Business.BL.ServiceImpl
{
    public class LocationsBLImpl : ILocationsBL
    {
        #region Private declarations
        ILocationsDAO locationsDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;
        #endregion

        #region Constructor
        public LocationsBLImpl(ApplicationDbContext context, IUnitOfWork uintofwork,
            IConfiguration _configuration, ILocationsDAO locationsDAOservice)
        {
            this.locationsDAOservice = locationsDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;

        }

       

        public Response AddZone(string Zone, int UserId)
        {
            var zoneadd = locationsDAOservice.ServiceRepository<LookupZone>().GetFirstOrDefault(x => x.Name==Zone);
            if(zoneadd==null)
            {
                locationsDAOservice.ServiceRepository<LookupZone>().Add(new LookupZone() { Name = Zone });
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad

                };
            }
            else
            {
                return new Response()
                {
                    StatusCode=(StatusCodes.Status206PartialContent),
                    Status=SucessMessages.OK,
                    Iserror=false,
                    Message="Zone Name already exit please enter new Zone"
                };
            }

        }

        
        public Response UpdateZone(int Id, string Zone, int UserId)
        {
            var zoneupdate = locationsDAOservice.ServiceRepository<LookupZone>().GetFirstOrDefault(x => x.Id == Id);

            if (zoneupdate!=null)
            {
                var LookupZone = locationsDAOservice.ServiceRepository<LookupZone>().GetFirstOrDefault(x => x.Id == Id);
                LookupZone.Name = Zone;
                locationsDAOservice.ServiceRepository<LookupZone>().Update(LookupZone, LookupZone);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
        }

       
        public Response Addlocation(LookupLocation model)
        {
            var val = locationsDAOservice.ServiceRepository<LookupLocation>().GetFirstOrDefault(x=>x.Name==model.Name);
            if(val==null)
            {
                model.IsActive = true;
                locationsDAOservice.ServiceRepository<LookupLocation>().Add(model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = StatusCodes.Status201Created,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message=SucessMessages.ScuessLoad,
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode=StatusCodes.Status206PartialContent,
                    Status=SucessMessages.OK,
                    Iserror=false,
                    Message= "Location name already exits please enter new Location"
                };
            }
        }

        public Response UpdateLocation(LookupLocation model)
        {
            var locupdate = locationsDAOservice.ServiceRepository<LookupLocation>().GetFirstOrDefault(x => x.Id == model.Id);
            if(locupdate!=null)
            {
                var LookupZone = locationsDAOservice.ServiceRepository<LookupLocation>().GetFirstOrDefault(x => x.Id ==model.Id);
                model.IsActive = true;
                locationsDAOservice.ServiceRepository<LookupLocation>().Update(locupdate, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = StatusCodes.Status201Created,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };

            }
            else
            {
                return new Response()
                {
                    StatusCode = StatusCodes.Status206PartialContent,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
        }

        public Response RemoveLocation(int Id)
        {
            var removeloc = locationsDAOservice.ServiceRepository<LookupLocation>().GetFirstOrDefault(x => x.Id == Id);
            removeloc.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Location Removed Successfully"
            };
        }

        public Response GetLocationId(int Id)
        {
            var locationbyid = locationsDAOservice.ServiceRepository<LookupLocation>().GetFirstOrDefault(x => x.Id == Id);

            return new Response()
            {
                response = new
                {
                    model = locationbyid
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetLocationbyName(string name)
        {
            var locationbyname = locationsDAOservice.ServiceRepository<LookupLocation>().GetFirstOrDefault(x => x.Name == name);

            return new Response()
            {
                response = new
                {
                    model = locationbyname
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetLocationList()
        {
            var locationlist = locationsDAOservice.ServiceRepository<LookupLocation>().Getfilter(x=>x.IsActive==true);

            return new Response()
            {
                response = new
                {
                    model = locationlist
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        #endregion

    }
}
