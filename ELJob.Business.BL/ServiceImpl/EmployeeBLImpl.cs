﻿using System;
using System.Collections.Generic;
using System.Text;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Infra.Repository;
using ELJob.Core.Context;
using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Domain.Model.Auth;
using Microsoft.Extensions.Configuration;
using ELJob.Business.Viewmodel.Auth;
using ELJob.Helper.Grid;
using Microsoft.AspNetCore.Http;
using static ELJob.Helper.Common.Constants;
using ELJob.Domain.Model.Employee;
using System.Linq;
using ELJob.Domain.Model.Lookup;
using ELJob.Framework.SourceFramework;
using ELJob.Domain.Model.Message;
using MongoDB.Driver.GeoJsonObjectModel;
using MongoDB.Driver;
using ELJob.Domain.Model.Mongo;
using ELJob.Domain.Model.Views;
using ELJob.Helper.Formater;

namespace ELJob.Business.BL.ServiceImpl
{
  public class EmployeeBLImpl: IEmployeeBL 
    {
        #region Private declarations
       IEmployeeDAO EmployeeDAOservice;
        IUnitOfWork _Unitofwork;
        private readonly IConfiguration configuration;
        #endregion

        #region Constructor
        public EmployeeBLImpl(ApplicationDbContext context, IUnitOfWork uintofwork, IConfiguration _configuration, IEmployeeDAO EmployeeDAOservice)
        {
            this.EmployeeDAOservice = EmployeeDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;
           
        }
        #endregion

        public async System.Threading.Tasks.Task<Response> AddRole(aspnetroles model, int UserId)
        {

            var roleadd = EmployeeDAOservice.ServiceRepository<aspnetroles>().GetFirstOrDefault(x => x.Name == model.Name);
            await EmployeeDAOservice.ServiceRepository<aspnetroles>().Add(model);
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode=StatusCodes.Status201Created,
                Message=SucessMessages.OK,
                Iserror=false,
                Status=SucessMessages.ScuessLoad
            };

         //   //var point = GeoJson.Point(GeoJson.Geographic(17.436318440345676, 78.44834121886626));
          
         //   // var filter = Builders<live>.Filter.Near("Location", point, 20000,500);
         //   var filter = Builders<live>.Filter.GeoWithinCenter("Location", 17.436318440345676, 78.44834121886626,20);
         //   //GeoJsonGeometry<GeoJson2DGeographicCoordinates> geometry = null;
         //   //geometry = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
         //   //    new GeoJson2DGeographicCoordinates(17.436318440345676, 78.44834121886626));
         //   var filterbuilder = Builders<live>.Filter.And(
         //       Builders<live>.Filter.Eq("Jobcity", "Hyd"),
         //        //Builders<live>.Filter.Eq("Name", "user1"),

         // filter 
         //       );
         //   var result =await EmployeeDAOservice.ServiceRepository<live>().mongoGetfilter(filterbuilder);
            
         //double longi = 17.430200;
         //   double lati = 78.449990;

         //   List<live> modelist = new List<live>();
            //for (int i = 0; i < 3000; i++)
            //{

            //    modelist.Add(new Domain.Model.Mongo.live()
            //    {
            //        TotalEnrolledStudents = 1,
            //        Location = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
            //           new GeoJson2DGeographicCoordinates(17.430200, 78.449990)),
            //        Name = "user" + i,
            //        Jobcity="Hyd"


            //    });
            //    //await this.EmployeeDAOservice.ServiceRepository<Domain.Model.Mongo.live>().mongoAdd(
            //    //              new Domain.Model.Mongo.live()
            //    //              {
            //    //                  TotalEnrolledStudents = 1,
            //    //                  Location = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
            //    //       new GeoJson2DGeographicCoordinates(17.430200, 78.449990)),
            //    //                  Name ="user"+i


            //    //       });

            //}
            //await this.EmployeeDAOservice.ServiceRepository<Domain.Model.Mongo.live>().mongoAddMany(modelist);
            //await EmployeeDAOservice.Commitmongo();






            //  var point = GeoJson.Point(GeoJson.Geographic(17.436318440345676, 78.44834121886626));
            //var filter = Builders<live>.Filter.Near("Location",point, 2 * 1000);
            // return collection.Find(filter).ToList().AsQueryable();
            //var finalresult = result.Skip(00).Take(50);
            //return new Response() { response=new { result=finalresult,count= result.Count() } , };
        }

        public Response Createemployee(RegisterModel model,int UserId)
        {
            var Employee = EmployeeDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.Id == model.Reporting);

            var reporting =EmployeeDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.UserId == Employee.UserId);
            var role=EmployeeDAOservice.ServiceRepository<aspnetroles>().GetFirstOrDefault(x => x.Id == model.Role).Name;
            EmployeeDAOservice.ServiceRepository<Employee>().Add(new Employee()
            {
                Email = model.Email,
                EmployeeCode=model.employeecode,
                Role=model.Role,
                Location=model.Location,
                Mobile=model.mobile,
                Reporting = reporting.FirstName + " " + reporting.MiddleName + " " + reporting.LastName,
                ReportingId =model.Reporting,
                Zone=model.ZoneId,
                IsActive=true,
                Createdat=DateTime.Now,
                Createdby= EmployeeDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.UserId == UserId).FirstName,
                Modifiedat=DateTime.Now,
                Modifiedby=UserId,
                Name=model.Name,
                UserId=model.UserId,
                Locationname= model.Location>0?EmployeeDAOservice.ServiceRepository<LookupLocation>().GetFirstOrDefault(x => x.Id == model.Location).Name:" ",
                Zonename = model.ZoneId>0?EmployeeDAOservice.ServiceRepository<LookupZone>().GetFirstOrDefault(x => x.Id == model.ZoneId).Name:" ",
                Rolename = role,
            });

            EmployeeDAOservice.ServiceRepository<Person>().Add(new Person()
            {
                FirstName = model.Name,
                Email = model.Email,         
                Mobile = model.mobile,
                UserId = model.UserId,
                RoleId=model.Role,
                DOB=model.DOB,
                Role= EmployeeDAOservice.ServiceRepository<aspnetroles>().GetFirstOrDefault(x=>x.Id==model.Role).Name

            });
            var aspnetusers = EmployeeDAOservice.ServiceRepository<aspnetusers>().GetFirstOrDefault(x => x.Id == model.UserId);
            aspnetusers.Email = model.Email;
            aspnetusers.PhoneNumber = model.mobile;

            var userroles = EmployeeDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == model.UserId);
            if (userroles != null)
            {
                userroles.RoleId = model.Role;
            }
            else
            {
                EmployeeDAOservice.ServiceRepository<aspnetuserroles>().Add(new aspnetuserroles()
                {
                    RoleId = model.Role,
                    UserId = model.UserId,
                    Discriminator = "IdentityUserRole<int>",
                    UserId1 = model.UserId,
                    RoleId1 = model.Role
                });
            }
            EmployeeDAOservice.Commit();

            return new Response()
            {
                response = new
                {

                },
                StatusCode = (StatusCodes.Status201Created),
                CustomerId = "",
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Inserted

            };
        }

        public Response Updateemployee(RegisterModel model,int UserId)
        {
            var Employee = EmployeeDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x=>x.UserId==model.UserId);
            var Reporting = EmployeeDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.UserId == model.Reporting);
           // Employee.Email = model.Email;
            Employee.EmployeeCode = model.employeecode;
            Employee.Role = model.Role;
            Employee.Location = model.Location;
            Employee.Mobile = model.mobile;
            Employee.Reporting = Reporting.FirstName+" "+ Reporting.MiddleName+" "+ Reporting.LastName;
            Employee.ReportingId = model.Reporting;
            Employee.Zone = model.ZoneId;
            Employee.IsActive = true;
            Employee.Modifiedat = DateTime.Now;
            Employee.Modifiedby = UserId;
            Employee.Name = model.Name;
            Employee.Rolename = EmployeeDAOservice.ServiceRepository<aspnetroles>().GetFirstOrDefault(x => x.Id == model.Role).Name;

            if (model.Location>0)
            {
                Employee.Locationname = EmployeeDAOservice.ServiceRepository<LookupLocation>().GetFirstOrDefault(x => x.Id == model.Location).Name;
                Employee.Zonename = EmployeeDAOservice.ServiceRepository<LookupZone>().GetFirstOrDefault(x => x.Id == model.ZoneId).Name;
            }
   

            var Person = EmployeeDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.UserId == model.UserId);
            Person.FirstName = model.Name;
            //Person.Email = model.Email;
            Person.Mobile = model.mobile;
            Person.UserId = model.UserId;
            Person.DOB = model.DOB;
        
            var userroles = EmployeeDAOservice.ServiceRepository<aspnetuserroles>().GetFirstOrDefault(x => x.UserId == model.UserId);
            if (userroles != null)
            {
                userroles.RoleId = model.Role;
            }
            else
            {
                EmployeeDAOservice.ServiceRepository<aspnetuserroles>().Add(new aspnetuserroles()
                {
                    RoleId = model.Role,
                    UserId = model.UserId
                });
            }

            var aspnetusers = EmployeeDAOservice.ServiceRepository<aspnetusers>().GetFirstOrDefault(x => x.Id == model.UserId);
            aspnetusers.PhoneNumber = model.mobile;

            EmployeeDAOservice.Commit();

            return new Response()
            {
                response = new
                {

                },
                StatusCode = (StatusCodes.Status201Created),
                CustomerId = "",
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Updated

            };
        }

        public Response GetEmployeebyId(int UserId)
        {
            var employeemodel = EmployeeDAOservice.ServiceRepository<Employee>().GetFirstOrDefault(x => x.UserId == UserId);
            var Person = EmployeeDAOservice.ServiceRepository<Person>().GetFirstOrDefault(x => x.UserId == UserId);
            var aspnetusers = EmployeeDAOservice.ServiceRepository<aspnetusers>().GetFirstOrDefault(x => x.Id == UserId);

            RegisterModel model = new RegisterModel();
            model.UserId = employeemodel.UserId.ToInt32();
            model.DOB = Person.DOB;
            model.Email = aspnetusers.Email;
            model.employeecode = employeemodel.EmployeeCode;
            model.Location = employeemodel.Location;
            model.mobile = aspnetusers.PhoneNumber;
            model.Name = Person.FirstName;
            model.Reporting = employeemodel.ReportingId;
            model.Role = employeemodel.Role.ToInt32();
            model.Username = employeemodel.Email;
            model.ZoneId = employeemodel.Zone;

            return new Response()
            {
                response = new
                {
                    model = model
                },
                StatusCode = (StatusCodes.Status200OK),
                CustomerId = "",
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.Inserted

            };
        }

        public GridResponse GetEmployees(PageData page, string Sort, bool descending, int zone, int Location, string name, DateTime fromdate, DateTime todate, int UserId, int RoleId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = EmployeeDAOservice.ServiceRepository<Employee>().Query(x => x.IsActive == true).AsQueryable();

            if (Location > 0)
                result = result.Where(x => x.Location == Location);
            if (zone > 0)
                result = result.Where(x => x.Zone == zone);

            if (name != "" && name != null)
                result = result.Where(x => x.Email.Contains(name));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new {
                name = x.Name,
                employeecode = x.EmployeeCode,
                email = x.Email,
                role = x.Rolename,
                location=x.Locationname,
                zone = x.Zonename,
                createdat=x.Createdat,
                UserId=x.UserId,

                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }

        public GridResponse GetRoles(PageData page, string Sort, bool descending, int UserId, int RoleId, string name)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = EmployeeDAOservice.ServiceRepository<aspnetroles>().Getfilter().AsQueryable();


            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new {
                name = x.Name,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }

        public GridResponse GetLoginAudit(PageData page, string Sort, bool descending, int Zone, int Location, DateTime fromdate, DateTime todate, int UserId, int RoleId, string name)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = EmployeeDAOservice.ServiceRepository<tbl_userloginaudit>().Getfilter().AsQueryable();

            if (Location > 0)
                result = result.Where(x => x.LocationId == Location);


            if (name != "" && name != null)
                result = result.Where(x => x.Username.Contains(name));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new {
                name = x.Username,
                location = x.Location,
                role = x.Role,
                Id = x.Id,
                logindate = x.Logindate
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }

        public Response UpdateRole(aspnetroles model, int UserId)
        {
            var userroles = EmployeeDAOservice.ServiceRepository<aspnetroles>().GetFirstOrDefault(x => x.Name == model.Name);
            if (userroles != null)
            {
                return new Response()
                {
                    response = new
                    {

                    },
                    StatusCode = (StatusCodes.Status206PartialContent),
                    CustomerId = "",
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Same Role Existis"

                };
            }
            else
            {
                var userrolesbyId = EmployeeDAOservice.ServiceRepository<aspnetroles>().GetFirstOrDefault(x => x.Id == model.Id);
                userrolesbyId.Name = model.Name;
                EmployeeDAOservice.ServiceRepository<aspnetroles>().Update(userrolesbyId, model);
            }

            _Unitofwork.Commit();

            return new Response()
            {
                response = new
                {

                },
                StatusCode = (StatusCodes.Status201Created),
                CustomerId = "",
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad

            };
        }

        public object GetLastlogin(int UserId)
        {
            try
            {

                return new Response()
                {
                    response = new
                    {
                        loginaduit = EmployeeDAOservice.ServiceRepository<tbl_userloginaudit>().Query(x => x.UserId == UserId).OrderByDescending(x=>x.Logindate).FirstOrDefault()
                    },
                    StatusCode = (StatusCodes.Status200OK),
                    CustomerId = "",
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad

                };
            }
            catch (Exception Ex)
            {
                return new Response() { StatusCode = StatusCodes.Status500InternalServerError, Status = "Error", Message = "An Error Has Occured " + Ex.Message };
            }
        }

        public GridResponse GetStateList(PageData page, string Sort, bool descending, string name, int UserId)
        {
            throw new NotImplementedException();
        }

        public GridResponse GetEmployeeHistoryList(PageData page, string Sort, bool descending, string role, string reportring, string zone)
        {

            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = EmployeeDAOservice.ServiceRepository<EmployeeHistory>().Query().AsQueryable();

           
            if (role != "" && role != null)
                result = result.Where(x => x.Role.Contains(role));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new {
                role = x.Role,
                reportring = x.Reporting,
                zone=x.Zone,
                createdat = x.Createdat,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }

        public GridResponse GetMessageList(PageData page, string Sort, bool descending, string name, int AspirantId, int JobseekerId, string Mobile, string Email, string Event, string Message, string Location, string Mode, int Type, DateTime Createddate)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = EmployeeDAOservice.ServiceRepository<messagelist>().Query(x => x.IsActive == true).AsQueryable();


            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new {
                name = x.Name,
                aspirantId = x.AspirantId,
                jobSeekrID = x.JobSeekrID,
                mobile = x.Mobile,
                email = x.Email,
                eventt = x.Event,
                message = x.Message,
                location = x.Location,
                mode = x.Mode,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));
            return gridResponse;
        }

        public Response EmployeeReportingList()
        {
            var empreporting = EmployeeDAOservice.ServiceRepository<vw_employeereporting>().Getfilter();
            return new Response()
            {
                response = new
                {
                    model = empreporting
                },
                StatusCode = (StatusCodes.Status200OK),
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetRolebyId(int Id)
        {
            var rolebybyid = EmployeeDAOservice.ServiceRepository<aspnetroles>().GetFirstOrDefault(x => x.Id == Id);

            return new Response()
            {
                response = new
                {
                    model = rolebybyid
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }
    }
}
