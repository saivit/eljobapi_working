﻿using ELJob.Business.BL.Service;
using ELJob.Business.Viewmodel;
using ELJob.Core.Context;
using ELJob.DAO.DAOLayer.Service;
using ELJob.Domain.Model.Lookup;
using ELJob.Domain.Model.Screens;
using ELJob.Helper.Grid;
using ELJob.Infra.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static ELJob.Helper.Common.Constants;
using ELJob.Framework.SourceFramework;
using ELJob.Domain.Model.Views;

namespace ELJob.Business.BL.ServiceImpl
{
    public class FOundationBLImpl: IFoundationBL
    {
        #region Private declarations
        IFoundationDAO foundationDAOservice;
        IUnitOfWork _Unitofwork;
        IConfiguration configuration;
        #endregion

        #region Constructor
        public FOundationBLImpl(ApplicationDbContext context, IUnitOfWork uintofwork,
            IConfiguration _configuration, IFoundationDAO foundationDAOservice)
        {
            this.foundationDAOservice = foundationDAOservice;
            _Unitofwork = uintofwork;
            this.configuration = _configuration;

        }

        public Response AddScreen(Screen model,int UserId)
        {
            var screenadd = foundationDAOservice.ServiceRepository<Screen>().GetFirstOrDefault(x => x.Name == model.Name);
            if(screenadd==null)
            {
                model.IsActive = true;
                foundationDAOservice.ServiceRepository<Screen>().Add(model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = StatusCodes.Status201Created,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = StatusCodes.Status206PartialContent,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Screen Name Already Exixts Please enter New Screen Name"
                };
            }
        }


        public Response UpdateScreen(Screen model, int UserId)
        {
            var screenupdate = foundationDAOservice.ServiceRepository<Screen>().GetFirstOrDefault(x => x.Id == model.Id);
            if(screenupdate!=null)
            {
                var screenupd = foundationDAOservice.ServiceRepository<Screen>().GetFirstOrDefault(x => x.Id == model.Id);
                model.IsActive = true;
                foundationDAOservice.ServiceRepository<Screen>().Update(screenupd, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = StatusCodes.Status201Created,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad,
                };

            }
            else
            {
                return new Response()
                {
                    StatusCode = StatusCodes.Status206PartialContent,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad,
                };
            }
                                                                                                                                                                                                                                            
        
        }

        public Response RemoveScreen(int Id, int UserId)
        {
            var screenremove = foundationDAOservice.ServiceRepository<Screen>().GetFirstOrDefault(x => x.Id == Id);
            screenremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Screen Removed Successfully"
            };

        }

        public Response AddSubScreen(SubScreen model, int UserId)
        {
            var subadd = foundationDAOservice.ServiceRepository<SubScreen>().GetFirstOrDefault(x => x.Name == model.Name);
            if(subadd==null)
            {
                model.IsActive = true;
                foundationDAOservice.ServiceRepository<SubScreen>().Add(model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode=StatusCodes.Status201Created,
                    Status=SucessMessages.OK,
                    Iserror=false,
                    Message=SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = StatusCodes.Status206PartialContent,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "SubScreen Name Already Exixts Please Enter New SubScreen Name"
                };
            }
        }

        public Response UpdateSubScreen(SubScreen model, int UserId)
        {
            var updstess = foundationDAOservice.ServiceRepository<SubScreen>().GetFirstOrDefault(x => x.Id == model.Id);
            if(updstess!=null)
            {
                
               var subscreen= foundationDAOservice.ServiceRepository<SubScreen>().GetFirstOrDefault(x => x.Id == model.Id);
                model.IsActive = true;
                foundationDAOservice.ServiceRepository<SubScreen>().Update(subscreen, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = StatusCodes.Status201Created,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };

            }
            else
            {
                return new Response()
                {
                    StatusCode = StatusCodes.Status206PartialContent,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message =SucessMessages.ScuessLoad
                };
            }
        }

        public Response RemoveSubScreen(int Id, int UserId)
        {
            var subscreenremove = foundationDAOservice.ServiceRepository<SubScreen>().GetFirstOrDefault(x => x.Id == Id);
            subscreenremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "SubScreen Removed Successfully"
            };
        }


        public Response AddState(Lookupstate model, int UserId)
        {
            var stateadd = foundationDAOservice.ServiceRepository<Lookupstate>().GetFirstOrDefault(x => x.Name == model.Name);
            if (stateadd == null)
            {
                
                foundationDAOservice.ServiceRepository<Lookupstate>().Add(model);
                model.IsActive = true;
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad

                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "State Name already exit please enter new State"
                };
            }
        }



        public Response UpdateState(Lookupstate model, int UserId)
        {
            var stateupdate = foundationDAOservice.ServiceRepository<Lookupstate>().GetFirstOrDefault(x => x.Id == model.Id);
            if (stateupdate != null)
            {
                var state = foundationDAOservice.ServiceRepository<Lookupstate>().GetFirstOrDefault(x => x.Id == model.Id);
                state.Name = model.Name;
                state.IsActive = true;
                foundationDAOservice.ServiceRepository<Lookupstate>().Update(state, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
        }


        public Response RemoveState(int Id, int UserId)
        {
            var stateremove = foundationDAOservice.ServiceRepository<Lookupstate>().GetFirstOrDefault(x => x.Id == Id);
            stateremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "State Removed Successfully"
            };
        }



        public Response AddCity(LookupCity model, int UserId)
        {
            var cityadd = foundationDAOservice.ServiceRepository<LookupCity>().GetFirstOrDefault(x => x.Name == model.Name);
            if (cityadd == null)
            {
                foundationDAOservice.ServiceRepository<LookupCity>().Add(model);
                model.IsActive = true;
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad

                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "City Name already exits please enter new City"
                };
            }
        }

        public Response UpdateCity(LookupCity model, int UserId)
        {
            var cityupdate = foundationDAOservice.ServiceRepository<LookupCity>().GetFirstOrDefault(x => x.Id == model.Id);
            if (cityupdate != null)
            {
                var city = foundationDAOservice.ServiceRepository<LookupCity>().GetFirstOrDefault(x => x.Id == model.Id);
                city.Name = model.Name;
                city.IsActive = true;
                foundationDAOservice.ServiceRepository<LookupCity>().Update(city, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };

            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "City Name already exits please enter new City"
                };
            }
        }

        public Response RemoveCity(int Id, int UserId)
        {
            var cityremove = foundationDAOservice.ServiceRepository<LookupCity>().GetFirstOrDefault(x => x.Id == Id);
            cityremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "City Removed Successfully"
            };
        }

        public Response AddSector(LookupSector model, int UserId)
        {
            var sectroadd = foundationDAOservice.ServiceRepository<LookupSector>().GetFirstOrDefault(x => x.Name == model.Name);
            if(sectroadd==null)
            {
                foundationDAOservice.ServiceRepository<LookupSector>().Add(model);
                model.IsActive = true;
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };

            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Sector Name already exits please enter new Sector"
                };
            }
        }

        public Response UpdateSector(LookupSector model, int UserId)
        {
            var sectorupdate = foundationDAOservice.ServiceRepository<LookupSector>().GetFirstOrDefault(x => x.Id == model.Id);
            if (sectorupdate != null)
            {
                var sector = foundationDAOservice.ServiceRepository<LookupSector>().GetFirstOrDefault(x => x.Id == model.Id);
                sector.IsActive = true;
                sector.Name = model.Name;
                foundationDAOservice.ServiceRepository<LookupSector>().Update(sector, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }



        }

        public Response RemoveSector(int Id, int UserId)
        {
            var sectorremove = foundationDAOservice.ServiceRepository<LookupSector>().GetFirstOrDefault(x => x.Id == Id);
            sectorremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Sector Removed Successfully"

            };
        }

        public Response AddObjective(LookupObjective model, int UserId)
        {
            var objectiveadd = foundationDAOservice.ServiceRepository<LookupObjective>().GetFirstOrDefault(x => x.Name == model.Name);
            if(objectiveadd==null)
            {
                foundationDAOservice.ServiceRepository<LookupObjective>().Add(model);
                model.IsActive = true;
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Objective already exits please enter new Objective"
                };
            }
        }

        public Response UpdateObjective(LookupObjective model, int UserId)
        {
            var objectiveupdate = foundationDAOservice.ServiceRepository<LookupObjective>().GetFirstOrDefault(x => x.Id == model.Id);
            if(objectiveupdate!=null)
            {
               var objective= foundationDAOservice.ServiceRepository<LookupObjective>().GetFirstOrDefault(x => x.Id == model.Id);
                objective.IsActive =true;
                objective.Name = model.Name;
                foundationDAOservice.ServiceRepository<LookupObjective>().Update(objective, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };

            }
            else
            {

                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
        }

        public Response RemoveObjective(int Id, int UserId)
        {
            var objectiveremove = foundationDAOservice.ServiceRepository<LookupObjective>().GetFirstOrDefault(x => x.Id ==Id);
            objectiveremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Objective Removed Successfully"
            };
        }

        public Response AddPlan(LookupPlans model, int UserId)
        {
            var planadd = foundationDAOservice.ServiceRepository<LookupPlans>().GetFirstOrDefault(x => x.Planmodel == model.Planmodel);
            if(planadd==null)
            {
                foundationDAOservice.ServiceRepository<LookupPlans>().Add(model);
                model.IsActive = true;
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Plan Model Nmae exits please enter new Plan Model"
                };
            }
        }

        public Response UpdatePlan(LookupPlans model, int UserId)
        {
            var planupdate = foundationDAOservice.ServiceRepository<LookupPlans>().GetFirstOrDefault(x => x.Id == model.Id);
            if(planupdate!=null)
            {
                var plan = foundationDAOservice.ServiceRepository<LookupPlans>().GetFirstOrDefault(x => x.Id == model.Id);
                plan.IsActive = true;
                plan.Planmodel = model.Planmodel;
                foundationDAOservice.ServiceRepository<LookupPlans>().Update(plan, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
               
                };
            }
        }

        public Response RemovePlan(LookupPlans model, int UserId)
        {
            var removeplan = foundationDAOservice.ServiceRepository<LookupPlans>().GetFirstOrDefault(x => x.Id == model.Id);
            
            removeplan.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Plan Removed Successfully"
            };

        }

        public Response AddScreenAssign(ScreenAssign model, int UserId)
        {
            var screenassignadd = foundationDAOservice.ServiceRepository<ScreenAssign>().Getfilter();
            foundationDAOservice.ServiceRepository<ScreenAssign>().Add(model);
            model.IsActive = true;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = (StatusCodes.Status201Created),
                Status = SucessMessages.OK,
                Iserror = false,
                Message = SucessMessages.ScuessLoad
            };

        }
        public Response UpdateScreenAssign(ScreenAssign model, int UserId)
        {
            var screenassignupdate = foundationDAOservice.ServiceRepository<ScreenAssign>().GetFirstOrDefault(x => x.Id == model.Id);
            if (screenassignupdate != null)
            {
                var assignscreen = foundationDAOservice.ServiceRepository<ScreenAssign>().GetFirstOrDefault(x => x.Id == model.Id);
                assignscreen.IsActive = true;
                assignscreen.Screen = model.Screen;
                foundationDAOservice.ServiceRepository<ScreenAssign>().Update(assignscreen, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "AssignScreen already exits please enter new AssignScreen"
                };
            }
        }

        public Response RemoveScreenAssign(int Id, int UsedrId)
        {
            var screenassignremove = foundationDAOservice.ServiceRepository<ScreenAssign>().GetFirstOrDefault(x => x.Id == Id);
            screenassignremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "AssignScreen Removed Successfully"
            };
        }

        public Response AddEducation(LookupEducation model, int UserId)
        {
            var eduadd = foundationDAOservice.ServiceRepository<LookupEducation>().GetFirstOrDefault(x => x.Name == model.Name);
            if(eduadd==null)
            {
                foundationDAOservice.ServiceRepository<LookupEducation>().Add(model);
                model.IsActive = true;
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };

            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Education already exits please enter new Education"
                };
            }
        }

        public Response UpdateEducation(LookupEducation model, int UserId)
        {
            var eduupdate = foundationDAOservice.ServiceRepository<LookupEducation>().GetFirstOrDefault(x => x.Id == model.Id);
            if(eduupdate!=null)
            {
               var education= foundationDAOservice.ServiceRepository<LookupEducation>().GetFirstOrDefault(x => x.Id == model.Id);
                education.IsActive = true;
                education.Name = model.Name;
                foundationDAOservice.ServiceRepository<LookupEducation>().Update(education, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };

            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }

        }

        public Response RemoveEducation(int Id, int UserId)
        {
            var eduremove = foundationDAOservice.ServiceRepository<LookupEducation>().GetFirstOrDefault(x => x.Id == Id);
            eduremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Education Removed Successfully"
            };

        }

        public Response AddOtherEducation(LookupOtherEducation model, int UserId)
        {
            var addOE = foundationDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Name == model.Name);
            if(addOE==null)
            {
                foundationDAOservice.ServiceRepository<LookupOtherEducation>().Add(model);
                model.IsActive = true;

                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "This is already exits please enter new one"
                };
            }
               
        }

        public Response UpdateOtherEducation(LookupOtherEducation model, int UserId)
        {
            var updateOE = foundationDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Id == model.Id);
            if(updateOE!=null)
            {
                var otheredu=foundationDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Id == model.Id);
                otheredu.IsActive = true;
                otheredu.Name = model.Name;
                foundationDAOservice.ServiceRepository<LookupOtherEducation>().Update(otheredu, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };


            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
        }

        public Response RemoveOtherEducation(int Id, int UserId)
        {
            var removeOE = foundationDAOservice.ServiceRepository<LookupOtherEducation>().GetFirstOrDefault(x => x.Id == Id);
            removeOE.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "OtherEducation Removed Successfully"
            };

        }

        public Response AddStatus(LookupStatus model, int UserId)
        {
            var statusadd = foundationDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Name == model.Name);
            if(statusadd==null)
            {
                foundationDAOservice.ServiceRepository<LookupStatus>().Add(model);
                model.IsActive = true;
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "This is already exits please enter new one"
                };
            }
        }

        public Response UpdateStatus(LookupStatus model, int UserId)
        {
            var statusupdate = foundationDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.Id);
            if(statusupdate!=null)
            {
                var status = foundationDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == model.Id);
                status.IsActive = true;
                status.Name = model.Name;
                foundationDAOservice.ServiceRepository<LookupStatus>().Update(status, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
        }

        public Response RemoveStatus(int Id, int UserId)
        {
            var statusremove = foundationDAOservice.ServiceRepository<LookupStatus>().GetFirstOrDefault(x => x.Id == Id);
            statusremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status200OK,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Status Removed Successfully"
            };

        }

        public Response AddSatatusMapping(vw_StatusMapping model, int UserId)
        {
            var addSM = foundationDAOservice.ServiceRepository<vw_StatusMapping>().GetFirstOrDefault(x => x.status == model.status);
            if(addSM==null)
            {
                foundationDAOservice.ServiceRepository<vw_StatusMapping>().Add(model);
                model.IsActive = true;
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "This is already exits please enter new one"
                };
            }
        }

        public Response UpdateStatusMapping(vw_StatusMapping model, int UserId)
        {
            var updateSE = foundationDAOservice.ServiceRepository<vw_StatusMapping>().GetFirstOrDefault(x => x.Id == model.Id);
            if(updateSE!=null)
            {
                var statusmap = foundationDAOservice.ServiceRepository<vw_StatusMapping>().GetFirstOrDefault(x => x.Id == model.Id);
                statusmap.IsActive = true;
                statusmap.status = model.status;
                foundationDAOservice.ServiceRepository<vw_StatusMapping>().Update(statusmap, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
        }

        public Response RemoveStatusMapping(int Id, int UserId)
        {
            var removeSM = foundationDAOservice.ServiceRepository<vw_StatusMapping>().GetFirstOrDefault(x => x.Id == Id);
            removeSM.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "StatusMapping Removed Successfully"
            };

        }

        public GridResponse GetStateList(PageData page, string Sort, bool descending, string name, string code, decimal cgst, decimal igst, decimal sgst, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result =foundationDAOservice.ServiceRepository<Lookupstate>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new {
                name = x.Name,
                code=x.code,
                cgst=x.CGST,
                sgst=x.SGST,
                igst=x.IGST,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;
        }

        public GridResponse GetCityList(PageData page, string Sort, bool descending, string name, int Stateid, int UserId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<vw_citylist>().Query(x => x.IsActive == true).AsQueryable();
            if (name != "" && name != null)
                result = result.Where(x => x.City.Contains(name));
            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new
            {
                name = x.City,
                stateid = x.Stateid,
                StateName = x.StateName,
                
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));
            return gridResponse;
        }

        public GridResponse GetSctorList(PageData page, string Sort, bool descending, string name, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupSector>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Name,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;


        }

        public GridResponse GetStatusList(PageData page, string Sort, bool descending, string name, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupStatus>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Name,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;

        }

       

        public GridResponse GetPlanList(PageData page, string Sort, bool descending, string name, string ValidityinDays, string Price, int ProfileViewsnumber, int TotalCVdownload, int UserId)
        {

            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupPlans>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Planmodel.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Planmodel,
                ValidityinDays=x.ValidityinDays,
                Price=x.Price,
                ProfileViewsnumber=x.ProfileViewsnumber,
                TotalCVdownload =x.TotalCVdownload,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;

        }

        public GridResponse GetObjectiveList(PageData page, string Sort, bool descending, string name, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupObjective>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Name,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;
        }

        public GridResponse GetScreenList(PageData page, string Sort, bool descending, string name, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<Screen>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Name,
                route=x.Route,
                icon=x.Icon,
                label=x.Lable,
                treeview=x.Treeview,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;
        }

        public GridResponse GetSubScreenList(PageData page, string Sort, bool descending, string name, string SubScreen, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<vw_SubScreen>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Screen.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Screen,
                SubScreen=x.SubScreen,
                route=x.Route,
                icon=x.Icon,
                label=x.Lable,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;
        }

        public GridResponse GetEducationList(PageData page, string Sort, bool descending, string name, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupEducation>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Name,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;
        }

       
        public GridResponse GetStatusMappingList(PageData page, string Sort, bool descending, string name, string StatusType, int UserId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<vw_statusmapping>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Status.Contains(name));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new
            {
                name = x.Status,
                StatusType=x.StatusType,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));
            return gridResponse;
        }

        public GridResponse GetOtherEducationList(PageData page, string Sort, bool descending, string name, int UserId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupOtherEducation>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new
            {
                name = x.Name,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));
            return gridResponse;
        }

        public GridResponse GetAssignScreenToRole(PageData page, string Sort, bool descending, string role, string screen, int UserId)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<vw_AssignScreenToRole>().Getfilter(x => x.IsActive == true).AsQueryable();
            if (role != "" && role != null)
                result = result.Where(x => x.Role.Contains(role));
            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new
            {
                role = x.Role,
                screen = x.Screen,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));
            return gridResponse;
        }

        public GridResponse GetPlanHistoryList(PageData page, string Sort, bool descending, string name, string ValidityinDays, string Price, decimal PriceIncludeGST, int ProfileViewsnumber, int TotalCVdownload, bool IsUnlimited, string SMSandEmail, DateTime Createdat, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupPlanhistory>().Getfilter(x => x.IsActive == true).AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Planmodel.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Planmodel,
                ValidityinDays = x.ValidityinDays,
                Price = x.Price,
                PriceIncludeGST=x.PriceIncludeGST,
                ProfileViewsnumber = x.ProfileViewsnumber,
                TotalCVdownload = x.TotalCVdownload,
                IsUnlimited=x.IsUnlimited,
                SMSandEmail=x.SMSandEmail,
                Id = x.Id,
                Createdat=x.Createdat
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));


            return gridresponse;
        }

        public Response AssignScreentoRole(int Id)
        {
            var assignscreentorole = foundationDAOservice.ServiceRepository<vw_AssignScreenToRole>().GetFirstOrDefault(x => x.Id == Id);

            return new Response()
            {
                response =new
                {
                    model=assignscreentorole
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };


        }

        public Response ScreenList()
        {
            var screen = foundationDAOservice.ServiceRepository<Screen>().Getfilter().Select(x=>new { 
            Id=x.Id,
            Name=x.Name
            });
            return new Response()
            {
                response = new
                {
                    model = screen
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetScreenbyId(int Id)
        {
            var screenbyid = foundationDAOservice.ServiceRepository<Screen>().GetFirstOrDefault(x => x.Id ==Id);

            return new Response()
            {
                response = new
                {
                    model = screenbyid
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public GridResponse GetZonelist(PageData page, string Sort, bool descending, string name, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupZone>().Getfilter().AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Name,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridresponse;
        }

        public Response GetPlanbyId(int Id)
        {
            var rolebyid = foundationDAOservice.ServiceRepository<LookupPlans>().GetFirstOrDefault(x => x.Id == Id);

            return new Response()
            {
                response = new
                {
                    model = rolebyid
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public GridResponse GetLocationlist(PageData page, string Sort, bool descending, string name, string Zone, int UserId)
        {
            GridResponse gridresponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<vw_locationlist>().Getfilter().AsQueryable();

            if (name != "" && name != null)
                result = result.Where(x => x.Location.Contains(name));

            var count = result.Count();
            gridresponse.pageData = page.Process(count, out skip, out limit);
            gridresponse.rows.AddRange(result.Select(x => new
            {
                name = x.Location,
                zone = x.Zone,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit)) ;

            return gridresponse;
        }

        public Response GeteducationbyId(int Id)
        {
            var educationbybyid = foundationDAOservice.ServiceRepository<LookupEducation>().GetFirstOrDefault(x => x.Id == Id);

            return new Response()
            {
                response = new
                {
                    model = educationbybyid
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetStatebyId(int Id)
        {
            var getstatebyid = foundationDAOservice.ServiceRepository<Lookupstate>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = getstatebyid
                },
                StatusCode=StatusCodes.Status200OK,
                Message=SucessMessages.OK,
                Iserror=false,
                Status=SucessMessages.ScuessLoad


            };
        }

        public Response GetSectorById(int Id)
        {
            var getsectorbyid = foundationDAOservice.ServiceRepository<LookupSector>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = getsectorbyid
                },
                StatusCode=StatusCodes.Status200OK,
                Message=SucessMessages.OK,
                Iserror=false,
                Status=SucessMessages.ScuessLoad
            };
        }

        public Response GetObjectiveById(int Id)
        {
            var objectivebyid = foundationDAOservice.ServiceRepository<LookupObjective>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = objectivebyid

                },
                StatusCode=StatusCodes.Status200OK,
                Message=SucessMessages.OK,
                Iserror=false,
                Status=SucessMessages.ScuessLoad
            };

        }

        public Response GetSubScreenById(int Id)
        {
            var subscreenbyid = foundationDAOservice.ServiceRepository<SubScreen>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = subscreenbyid

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetEducation()
        {
            var education = foundationDAOservice.ServiceRepository<LookupEducation>().Getfilter(x=>x.IsActive==true).Select(x => new {
                Id = x.Id,
                Name = x.Name
            });
            var othereducation = foundationDAOservice.ServiceRepository<LookupOtherEducation>().Getfilter(x => x.IsActive == true).Select(x => new {
                Id = x.Id,
                Name = x.Name
            });

            return new Response()
            {
                response = new
                {
                    model = education,
                    othereducation= othereducation

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response AddLanguage(LookupLanguage model)
        {

            var language = foundationDAOservice.ServiceRepository<LookupLanguage>().GetFirstOrDefault(x => x.Name == model.Name);
            if (language == null)
            {
                model.IsActive = true;
                foundationDAOservice.ServiceRepository<LookupLanguage>().Add(model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = StatusCodes.Status201Created,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = StatusCodes.Status206PartialContent,
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = "Language Already Exixts Please Enter New One"
                };
            }
        }

        public Response UpdateLanguage(LookupLanguage model)
        {
            var Languageupdate = foundationDAOservice.ServiceRepository<LookupLanguage>().GetFirstOrDefault(x => x.Id == model.Id);
            if (Languageupdate != null)
            {
                var state = foundationDAOservice.ServiceRepository<LookupLanguage>().GetFirstOrDefault(x => x.Id == model.Id);
                state.Name = model.Name;
                state.IsActive = true;
                foundationDAOservice.ServiceRepository<LookupLanguage>().Update(state, model);
                _Unitofwork.Commit();
                return new Response()
                {
                    StatusCode = (StatusCodes.Status201Created),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
            else
            {
                return new Response()
                {
                    StatusCode = (StatusCodes.Status206PartialContent),
                    Status = SucessMessages.OK,
                    Iserror = false,
                    Message = SucessMessages.ScuessLoad
                };
            }
        }

        public Response RemoveLanguage(int Id)
        {
            var languageremove = foundationDAOservice.ServiceRepository<LookupLanguage>().GetFirstOrDefault(x => x.Id == Id);
            languageremove.IsActive = false;
            _Unitofwork.Commit();
            return new Response()
            {
                StatusCode = StatusCodes.Status201Created,
                Status = SucessMessages.OK,
                Iserror = false,
                Message = "Language Removed Successfully"
            };
        }

        public Response GetLanguagebyId(int Id)
        {
            var Languagebyid = foundationDAOservice.ServiceRepository<LookupLanguage>().GetFirstOrDefault(x => x.Id == Id);
            return new Response()
            {
                response = new
                {
                    model = Languagebyid

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetLanguages()
        {
            var language = foundationDAOservice.ServiceRepository<LookupLanguage>().Getfilter().Select(x => new {
                Id = x.Id,
                Language=x.Name

            });

            return new Response()
            {
                response = new
                {
                    model = language
                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public GridResponse LanguageList(PageData page, string Sort, bool descending, string name)
        {
            GridResponse gridResponse = new GridResponse(); var skip = 0; var limit = 0;
            var result = foundationDAOservice.ServiceRepository<LookupLanguage>().Query().AsQueryable();


            if (name != "" && name != null)
                result = result.Where(x => x.Name.Contains(name));
            var count = result.Count();
            gridResponse.pageData = page.Process(count, out skip, out limit);
            gridResponse.rows.AddRange(result.Select(x => new {
                name = x.Name,
                Id = x.Id
            }).OrderBy(Sort, descending).Skip(skip).Take(limit));

            return gridResponse;
        }

        public Response GetObjective()
        {
            var objective = foundationDAOservice.ServiceRepository<LookupObjective>().Getfilter().Select(x => new {
                Id = x.Id,
                Name = x.Name
            });

            return new Response()
            {
                response = new
                {
                    model = objective

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetCities()
        {
            var cities = foundationDAOservice.ServiceRepository<LookupCity>().Getfilter().Select(x => new {
                Id = x.Id,
                Name = x.Name
            });

            return new Response()
            {
                response = new
                {
                    model = cities

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetStates()
        {
            var states = foundationDAOservice.ServiceRepository<Lookupstate>().Getfilter().Select(x => new {
                Id = x.Id,
                Name = x.Name
            });

            return new Response()
            {
                response = new
                {
                    model = states

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }

        public Response GetSectors()
        {
            var sectors = foundationDAOservice.ServiceRepository<LookupSector>().Getfilter().Select(x => new {
                Id = x.Id,
                Name = x.Name
            });

            return new Response()
            {
                response = new
                {
                    model = sectors

                },
                StatusCode = StatusCodes.Status200OK,
                Message = SucessMessages.OK,
                Iserror = false,
                Status = SucessMessages.ScuessLoad
            };
        }













        #endregion
    }
}
