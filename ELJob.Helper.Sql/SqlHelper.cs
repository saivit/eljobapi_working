﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace ELJob.Helper.Sql
{
    /// <summary>
    /// Sql Helper Class
    /// </summary>
    public class SqlHelper
    {
        /// <summary>
        /// The connection string
        /// </summary>
        private string ConnectionString = string.Empty;

        /// <summary>
        /// The con
        /// </summary>
        private static SqlConnection con;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlHelper"/> class.
        /// </summary>
        public SqlHelper()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            con = new SqlConnection(ConnectionString);
        }

        /// <summary>
        /// Sets the connection.
        /// </summary>
        public void SetConnection()
        {
            if (ConnectionString == string.Empty)
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
            con = new SqlConnection(ConnectionString);
        }

        /// <summary>
        /// Executes the procudere.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parms">The parms.</param>
        /// <returns></returns>
        public DataSet ExecuteProcudere(string procName, Hashtable parms)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            cmd.CommandText = procName;
            cmd.CommandType = CommandType.StoredProcedure;
            if (con == null)
            {
                SetConnection();
            }
            cmd.Connection = con;
            if (parms.Count > 0)
            {
                foreach (DictionaryEntry de in parms)
                {
                    cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                }
            }
            da.SelectCommand = cmd;
            da.Fill(ds);
            return ds;
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parms">The parms.</param>
        /// <returns></returns>
        public int ExecuteQuery(string procName, Hashtable parms)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procName;
            if (parms.Count > 0)
            {
                foreach (DictionaryEntry de in parms)
                {
                    cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                }
            }
            if (con == null)
            {
                SetConnection();
            }
            cmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            int result = cmd.ExecuteNonQuery();
            return result;
        }

        /// <summary>
        /// Executes the querywith outputparams.
        /// </summary>
        /// <param name="cmd">The command.</param>
        /// <returns></returns>
        public int ExecuteQuerywithOutputparams(SqlCommand cmd)
        {
            if (con == null)
            {
                SetConnection();
            }
            cmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            int result = cmd.ExecuteNonQuery();
            return result;
        }

        /// <summary>
        /// Executes the query with out parameter.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parms">The parms.</param>
        /// <returns></returns>
        public int ExecuteQueryWithOutParam(string procName, Hashtable parms)
        {
            SqlCommand cmd = new SqlCommand();
            SqlParameter sqlparam = new SqlParameter();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procName;
            if (parms.Count > 0)
            {
                foreach (DictionaryEntry de in parms)
                {
                    if (de.Key.ToString().Contains("_out"))
                    {
                        sqlparam = new SqlParameter(de.Key.ToString(), de.Value);
                        sqlparam.DbType = DbType.Int32;
                        sqlparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(sqlparam);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue(de.Key.ToString(), de.Value);
                    }
                }
            }
            if (con == null)
            {
                SetConnection();
            }
            cmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            int result = cmd.ExecuteNonQuery();
            if (sqlparam != null)
                result = Convert.ToInt32(sqlparam.SqlValue.ToString());
            return result;
        }
    }
}
