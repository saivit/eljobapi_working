﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ELJob.Helper.Grid
{
    public class PageData
    {
        public int selectedPageSize { get; set; }
        public int totalPages { get; set; }
        [Required]
        public int currentPage { get; set; }
        public int totalRecords { get; set; }
        public int currentFromRecord { get; set; }
        public int currentToRecord { get; set; }
    }
}
