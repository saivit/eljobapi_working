﻿
using ELJob.Helper.Formater;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELJob.Helper.Grid
{
    public static class GridHelper
    {
        public static PageData Process(this PageData pageData, int count, out int skip, out int limit)
        {
            if (pageData.currentPage <= 0) pageData.currentPage = 1;
            if (pageData.selectedPageSize <= 0) pageData.selectedPageSize = 10;
            pageData.totalRecords = count;
            pageData.totalPages = Math.Ceiling((pageData.totalRecords / (decimal)pageData.selectedPageSize)).ToInt32();
            pageData.currentPage = (pageData.currentPage <= 0) ? 1 : (pageData.currentPage > pageData.totalPages) ? pageData.totalPages : pageData.currentPage;
            pageData.currentFromRecord = pageData.selectedPageSize * (pageData.currentPage - 1) + 1;
            pageData.currentToRecord = pageData.currentFromRecord + pageData.selectedPageSize - 1;
            if (pageData.currentToRecord > pageData.totalRecords) pageData.currentToRecord = pageData.totalRecords;

            skip = pageData.selectedPageSize * (pageData.currentPage - 1);
            if (skip < 0) skip = 0;
            limit = pageData.selectedPageSize;

            return pageData;
        }

        public static async Task<PageData> Processasync(this PageData pageData, int count,  int skip,  int limit)
        {
            if (pageData.currentPage <= 0) pageData.currentPage = 1;
            if (pageData.selectedPageSize <= 0) pageData.selectedPageSize = 10;
            pageData.totalRecords = count;
            pageData.totalPages = Math.Ceiling((pageData.totalRecords / (decimal)pageData.selectedPageSize)).ToInt32();
            pageData.currentPage = (pageData.currentPage <= 0) ? 1 : (pageData.currentPage > pageData.totalPages) ? pageData.totalPages : pageData.currentPage;
            pageData.currentFromRecord = pageData.selectedPageSize * (pageData.currentPage - 1) + 1;
            pageData.currentToRecord = pageData.currentFromRecord + pageData.selectedPageSize - 1;
            if (pageData.currentToRecord > pageData.totalRecords) pageData.currentToRecord = pageData.totalRecords;

            skip = pageData.selectedPageSize * (pageData.currentPage - 1);
            if (skip < 0) skip = 0;
            limit = pageData.selectedPageSize;

            return pageData;
        }
    }
}
