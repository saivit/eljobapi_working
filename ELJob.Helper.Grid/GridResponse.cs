﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELJob.Framework.SourceFramework;


namespace ELJob.Helper.Grid
{
    public class GridResponse
    {
        public GridResponse()
        {
            rows = new List<object>();
        }

        public List<object> rows { get; set; }
        public Object Customdata { get; set; }
        public PageData pageData { get; set; }
    }

    public class GridResponsequerable<T> : List<T>
    {

       
        public  List<object> rows { get; set; }
        public PageData pageData { get; set; }


        public GridResponsequerable(List<T> items, PageData page, int count, int pageIndex, int pageSize)
        {
            pageData = page;
            this.AddRange(items);
        }

        public static async Task<GridResponsequerable<T>> CreateAsync(IQueryable<T> source, string sort,PageData page, int pageIndex, int pageSize)
        {
            var skip = 0;var limit = 0;
            var count = await source.CountAsync();
            page.Process(count,out skip,out limit);
          
            var items = await source.OrderBy(sort).Skip(skip).Take(limit).ToListAsync();
            return new GridResponsequerable<T>(items, page, count, pageIndex, pageSize);
       
        }


    }

    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }
        public PageData pageData { get; set; }
        public PaginatedList(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            this.AddRange(items);
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> source, int pageIndex, int pageSize)
        {
            var count = await source.CountAsync();
            var items = await source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            return new PaginatedList<T>(items, count, pageIndex, pageSize);
        }
    }
}
