﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELJob.DLCP.Generator.Models
{
    public class AspirantResume
    {
      public AspirantPersonal pesrsonal { get; set; }
      public List<AspirantEducation> education { get; set; }
      public List<AspirantOtherQualifications> otherqualification { get; set; }
      public List<AspirantLanguages> languages { get; set; }
      public ApirantDocuments documents { get; set; }

    }
    public class AspirantPersonal
    {
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Pincode { get; set; }
        public string Objective { get; set; }
        public string DOB { get; set; }
    }
    public class AspirantEducation
    {
        public string Particulars { get; set; }
        public int Year { get; set; }
        public string Institute { get; set; }
        public string Mode { get; set; }
    }
    public class AspirantOtherQualifications
    {
        public string Particulars { get; set; }
        public int Year { get; set; }
        public string Institute { get; set; }
        public int Duration { get; set; }
    }
    public class AspirantLanguages
    {
        public string Language { get; set; }
        public string Speak { get; set; }
        public string Read { get; set; }
        public string Write { get; set; }
    }
    public class ApirantDocuments
    {
        public string addressProof { get; set; }
        public string Euducational { get; set; }
        public string additinalEducation { get; set; }
        public string workexperinceCertificate { get; set; }
        public string profilepicture { get; set; }
        public string identityDocument { get; set; }
        public string dirivingLicence { get; set; }
    }
}