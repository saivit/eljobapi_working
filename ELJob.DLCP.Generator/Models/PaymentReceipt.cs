﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELJob.DLCP.Generator.Models
{
    public class PaymentReceipt
    {

		public string Address_to { get; set; }
		public string Amount { get; set; }
		public string Category_of_service { get; set; }
		public string Description { get; set; }
		public string Discount_Abatement { get; set; }
		public string IGST { get; set; }
		public string Net_taxable_amount { get; set; }
		public string SGST { get; set; }
		public string Total_invoice_Value { get; set; }
		public string account_name { get; set; }
		public string account_no { get; set; }
		public string amount_in_words { get; set; }
		public string bank { get; set; }
		public string branch { get; set; }
		public string cgst { get; set; }
		public string gst_no { get; set; }
		public string gst_status { get; set; }
		public string gstn_party { get; set; }
		public string unerpayables_rcm { get; set; }
		public string ifsc_code { get; set; }
		public string invoice_date { get; set; }
		public string invoice_no { get; set; }
		public string lbl_Addressto { get; set; }
		public string lbl_Bank { get; set; }
		public string lbl_CGST { get; set; }
		public string lbl_Discount_Abatement { get; set; }
		public string lbl_GST_No { get; set; }
		public string lbl_IFSC_code { get; set; }
		public string lbl_IGST { get; set; }
		public string lbl_Invoice_Date { get; set; }
		public string lbl_Invoice_No { get; set; }
		public string lbl_Net_taxable_amount { get; set; }
		public string lbl_Rounding_Off { get; set; }
		public string lbl_SGST { get; set; }
		public string lbl_Total_Gross_Amount { get; set; }
		public string lbl_Total_invoice_Value { get; set; }
		public string lbl_account_name { get; set; }
		public string lbl_account_no { get; set; }
		public string lbl_amount_inwords { get; set; }
		public string lbl_brach { get; set; }
		public string lbl_gstn_party { get; set; }
		public string lbl_gststatus { get; set; }
		public string lbl_pan { get; set; }
		public string lbl_employerAddress { get; set; }
		public string lbl_employername { get; set; }
		public string lbl_placeof_supplay { get; set; }
		public string lbl_state { get; set; }
		public string lbl_statecode { get; set; }
		public string lbl_whether_payable_under_RCM { get; set; }
		public string pan { get; set; }
		public string party_address { get; set; }
		public string partyname { get; set; }
		public string place_of_supply { get; set; }
		public string sac_code { get; set; }
		public string sr_no { get; set; }
		public string statecode { get; set; }
		public string statename { get; set; }
		public string total_gross_amount { get; set; }
		public string Roundingoff { get; set; }
		public string Validity { get; set; }
		public string validity_expies { get; set; }
		
	}
}