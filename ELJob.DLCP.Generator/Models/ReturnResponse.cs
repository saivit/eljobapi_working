﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELJob.DLCP.Generator.Models
{
    public class ReturnResponse
    {
        public byte[] Response { get; set; }
    }
}