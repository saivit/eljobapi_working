﻿using ELJob.DLCP.Generator.Models;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ELJob.DLCP.Generator.Controllers
{
    [RoutePrefix("api/pdfgenerator")]
    public class PDFGeneratorController : ApiController
    {
        [Route("paymentreceipt")]
        [HttpPost]
        public IHttpActionResult PaymentReceipt(PaymentReceipt model)
        {
            string FileName = string.Format("PaymentReceipt.txt");
            string TempFilePath = HttpContext.Current.Server.MapPath(@System.Configuration.ConfigurationManager.AppSettings["TempFilePath"]) + "/" + FileName;
            //test
            var output = JsonConvert.SerializeObject(model);
            using (StreamWriter bw = new StreamWriter(File.Create(TempFilePath)))
            {
                bw.Write(output);
                bw.Close();
            }

            string myTempFile = Path.Combine(TempFilePath, FileName);
            if (File.Exists(myTempFile))
            {
                File.Delete(myTempFile);
            }
            //File.Create(TempFilePath);
            byte[] rdeder = new byte[] { 23, 34 };


            string contentType = string.Empty;
            Hashtable parms = new Hashtable();

            var reportViewer = new ReportViewer();
            reportViewer.LocalReport.DataSources.Clear();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~" + ConfigurationManager.AppSettings["RootFilePath"] + "/PaymnetReceipt/PaymentReceipt.rdlc");

            ReportDataSource ReceiptDs = new ReportDataSource();
            ReceiptDs.Name = "ReceiptDS";
            ReceiptDs.Value = new List<object>() { model };
            reportViewer.LocalReport.DataSources.Add(ReceiptDs);


            reportViewer.LocalReport.EnableExternalImages = true;
            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>PDF</OutputFormat>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            //Render the report           
            renderedBytes = reportViewer.LocalReport.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            string PDFFileName = string.Format("EmployeerPaymentReceipt.pdf");
            string PDFTempFilePath = HttpContext.Current.Server.MapPath(@System.Configuration.ConfigurationManager.AppSettings["TempFilePath"]);
            //test
            string PDFmyTempFile = Path.Combine(PDFTempFilePath, PDFFileName);
            if (File.Exists(PDFmyTempFile))
            {
                File.Delete(PDFmyTempFile);
            }
            using (FileStream fs = new FileStream(PDFmyTempFile, FileMode.Create))
            {
                fs.Write(renderedBytes, 0, renderedBytes.Length);
            }
            var response = new ReturnResponse() { Response = renderedBytes };
            return Json(response);
        }

        [Route("aspirantresume")]
        [HttpPost]
        public IHttpActionResult AspirantResume(AspirantResume model)
        {
            string FileName = string.Format("AspirantResume.txt");
            string TempFilePath = HttpContext.Current.Server.MapPath(@System.Configuration.ConfigurationManager.AppSettings["TempFilePath"]) + "/" + FileName;
            //test
            var output = JsonConvert.SerializeObject(model);
            using (StreamWriter bw = new StreamWriter(File.Create(TempFilePath)))
            {
                bw.Write(output);
                bw.Close();
            }

            string myTempFile = Path.Combine(TempFilePath, FileName);
            if (File.Exists(myTempFile))
            {
                File.Delete(myTempFile);
            }
            //File.Create(TempFilePath);
            byte[] rdeder = new byte[] { 23, 34 };


            string contentType = string.Empty;
            Hashtable parms = new Hashtable();

            var reportViewer = new ReportViewer();
            reportViewer.LocalReport.DataSources.Clear();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~" + ConfigurationManager.AppSettings["RootFilePath"] + "/AspirantResume/AspirantResume.rdlc");

            ReportDataSource PersonalDs = new ReportDataSource();
            PersonalDs.Name = "PesonalDS";
            PersonalDs.Value = new List<object>() { model.pesrsonal };
            reportViewer.LocalReport.DataSources.Add(PersonalDs);

            ReportDataSource EducationalDs = new ReportDataSource();
            EducationalDs.Name = "EducationDS";
            EducationalDs.Value = model.education;
            reportViewer.LocalReport.DataSources.Add(EducationalDs);

            ReportDataSource OtherEducationDs = new ReportDataSource();
            OtherEducationDs.Name = "OtherEducationDS";
            OtherEducationDs.Value = model.otherqualification;
            reportViewer.LocalReport.DataSources.Add(OtherEducationDs);

            ReportDataSource LanguagesDS = new ReportDataSource();
            LanguagesDS.Name = "LanguagesDS";
            LanguagesDS.Value = model.languages;
            reportViewer.LocalReport.DataSources.Add(LanguagesDS);

            ReportDataSource documentsDS = new ReportDataSource();
            documentsDS.Name = "DocumentsDS";
            documentsDS.Value = new List<object>() { model.documents };
            reportViewer.LocalReport.DataSources.Add(documentsDS);


            reportViewer.LocalReport.EnableExternalImages = true;
            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>PDF</OutputFormat>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            //Render the report           
            renderedBytes = reportViewer.LocalReport.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            string PDFFileName = string.Format("EmployeerPaymentReceipt.pdf");
            string PDFTempFilePath = HttpContext.Current.Server.MapPath(@System.Configuration.ConfigurationManager.AppSettings["TempFilePath"]);
            //test
            string PDFmyTempFile = Path.Combine(PDFTempFilePath, PDFFileName);
            if (File.Exists(PDFmyTempFile))
            {
                File.Delete(PDFmyTempFile);
            }
            using (FileStream fs = new FileStream(PDFmyTempFile, FileMode.Create))
            {
                fs.Write(renderedBytes, 0, renderedBytes.Length);
            }
            var response = new ReturnResponse() { Response = renderedBytes };
            return Json(response);
        }


    }
}
