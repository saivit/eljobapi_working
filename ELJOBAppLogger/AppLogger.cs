﻿using System;

namespace ELJOBAppLogger
{
    public class AppLogger
    {
        public void Log(string text)
        {
            Console.WriteLine(text);
        }
    }
}
