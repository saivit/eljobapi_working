﻿using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Security;
using System.Text;
using System.Threading;

namespace ELJob.Helper.Communication
{
    /// <summary>
    /// Email Helper Class
    /// </summary>
    public static class EmailHelper
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Gets or sets from.
        /// </summary>
        /// <value>
        /// From.
        /// </value>
        public static string From { get; set; }

        /// <summary>
        /// Gets or sets to.
        /// </summary>
        /// <value>
        /// To.
        /// </value>
        public static string To { get; set; }

        /// <summary>
        /// Gets or sets cc.
        /// </summary>
        /// <value>
        /// The cc.
        /// </value>
        public static string CC { get; set; }

        /// <summary>
        /// Gets or sets bcc.
        /// </summary>
        /// <value>
        /// The BCC.
        /// </value>
        public static string BCC { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        /// <value>
        /// The subject.
        /// </value>
        public static string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        /// <value>
        /// The body.
        /// </value>
        public static string Body { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is HTML.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is HTML; otherwise, <c>false</c>.
        /// </value>
        public static bool IsHtml { get; set; }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public static string FileName { get; set; }


        /// <summary>
        /// Gets or sets the sender password.
        /// </summary>
        /// <value>
        /// sender password.
        /// </value>
        public static string Sendrpassword { get; set; }


        /// <summary>
        /// Sends the email asynchronous.
        /// </summary>
        public static void SendEmailAsync(string Host,string UserName,string password,bool SSLenabled,int Port)
        {
            using (new SynchronizationContextSwitcher())
            {
                MailMessage mailMsg = new MailMessage();
                mailMsg.From = new MailAddress(UserName);
                mailMsg.To.Add(new MailAddress(To));

                if (!string.IsNullOrEmpty(CC))
                {
                    mailMsg.CC.Add(new MailAddress(CC));
                }

                if (!string.IsNullOrEmpty(BCC))
                {
                    mailMsg.Bcc.Add(new MailAddress(BCC));
                }

                mailMsg.Subject = Subject;
                mailMsg.Body = Body;
                mailMsg.IsBodyHtml = IsHtml;
                if (!string.IsNullOrEmpty(FileName))
                {
                    // Create  the file attachment for this e-mail message.
                    Attachment data = new Attachment(FileName, MediaTypeNames.Application.Octet);
                    // Add time stamp information for the file.
                    ContentDisposition disposition = data.ContentDisposition;
                    disposition.CreationDate = System.IO.File.GetCreationTime(FileName);
                    disposition.ModificationDate = System.IO.File.GetLastWriteTime(FileName);
                    disposition.ReadDate = System.IO.File.GetLastAccessTime(FileName);
                    // Add the file attachment to this e-mail message.
                    mailMsg.Attachments.Add(data);
                }
                mailMsg.Priority = MailPriority.High;

                // Get the SMTP client.
                string securepassword = password;
                SecureString sec_pass = new SecureString();
                Array.ForEach(password.ToArray(), sec_pass.AppendChar);
                sec_pass.MakeReadOnly();
                SmtpClient client = new SmtpClient()
                {
                    Host = Host,
                    Port = Port,
                    EnableSsl = SSLenabled,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(
                        UserName, sec_pass)
                };
               
                Object state = mailMsg;
                client.ServicePoint.MaxIdleTime = 0;
                client.ServicePoint.ConnectionLimit = 3;
                client.SendCompleted += new SendCompletedEventHandler(OnSendCompletedCallback);
                try
                {
                    client.SendAsync(mailMsg, state);
                }
                catch (Exception ex)
                {
                    logger.Error("Error occurred. " + ex);
                }
            }
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        public static void SendEmail(string Host, string UserName, string password, bool SSLenabled, int Port)
        {
            using (var mailMsg = new MailMessage())
            {
                try
                {

                    mailMsg.From = new MailAddress(UserName);
                    mailMsg.To.Add(new MailAddress(To));

                    if (!string.IsNullOrEmpty(CC))
                    {
                        mailMsg.CC.Add(new MailAddress(CC));
                    }

                    if (!string.IsNullOrEmpty(BCC))
                    {
                        var bccemail = BCC.Split(';');
                        foreach (var item in bccemail)
                        {
                            mailMsg.Bcc.Add(new MailAddress(item));
                        }

                    }

                    mailMsg.Subject = Subject;
                    mailMsg.Body = Body;
                    mailMsg.IsBodyHtml = IsHtml;
                    if (!string.IsNullOrEmpty(FileName))
                    {
                        // Create  the file attachment for this e-mail message.
                        Attachment data = new Attachment(FileName, MediaTypeNames.Application.Octet);
                        // Add time stamp information for the file.
                        ContentDisposition disposition = data.ContentDisposition;
                        disposition.CreationDate = System.IO.File.GetCreationTime(FileName);
                        disposition.ModificationDate = System.IO.File.GetLastWriteTime(FileName);
                        disposition.ReadDate = System.IO.File.GetLastAccessTime(FileName);
                        // Add the file attachment to this e-mail message.
                        mailMsg.Attachments.Add(data);
                    }
                    mailMsg.Priority = MailPriority.High;

                    // Get the SMTP client.
                    string securepassword = password;
                    SecureString sec_pass = new SecureString();
                    Array.ForEach(password.ToArray(), sec_pass.AppendChar);
                    sec_pass.MakeReadOnly();
                    SmtpClient client = new SmtpClient()
                    {
                        Host = Host,
                        Port = Port,
                        EnableSsl = SSLenabled,
                        UseDefaultCredentials = false,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(
                          UserName,
                          sec_pass)
                    };
                    client.ServicePoint.MaxIdleTime = 0;
                    client.ServicePoint.ConnectionLimit = 3;

                    bool isSMSSendingEnabled = SSLenabled;
                    if (isSMSSendingEnabled)
                    {
                        client.Send(mailMsg);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error occurred. " + ex);
                }
            }
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        public static void SendCertificateEmail()
        {
            using (var mailMsg = new MailMessage())
            {
                try
                {
                    //MailMessage mailMsg = new MailMessage();
                    mailMsg.From = new MailAddress(string.IsNullOrEmpty(From) ? ConfigurationManager.AppSettings["TriningcertificateQAEMail"] : From);
                    mailMsg.To.Add(new MailAddress(To));

                    if (!string.IsNullOrEmpty(CC))
                    {
                        mailMsg.CC.Add(new MailAddress(CC));
                    }

                    if (!string.IsNullOrEmpty(BCC))
                    {
                        var bccemail = BCC.Split(';');
                        foreach (var item in bccemail)
                        {
                            mailMsg.Bcc.Add(new MailAddress(item));
                        }

                    }

                    mailMsg.Subject = Subject;
                    mailMsg.Body = Body;
                    mailMsg.IsBodyHtml = IsHtml;
                    if (!string.IsNullOrEmpty(FileName))
                    {
                        // Create  the file attachment for this e-mail message.
                        Attachment data = new Attachment(FileName, MediaTypeNames.Application.Octet);
                        // Add time stamp information for the file.
                        ContentDisposition disposition = data.ContentDisposition;
                        disposition.CreationDate = System.IO.File.GetCreationTime(FileName);
                        disposition.ModificationDate = System.IO.File.GetLastWriteTime(FileName);
                        disposition.ReadDate = System.IO.File.GetLastAccessTime(FileName);
                        // Add the file attachment to this e-mail message.
                        mailMsg.Attachments.Add(data);
                    }
                    mailMsg.Priority = MailPriority.High;

                    // Get the SMTP client.
                    SmtpClient client = new SmtpClient()
                    {
                        Host = ConfigurationManager.AppSettings["SMTPHost"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SMTPEnableSSL"]),
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(
                          ConfigurationManager.AppSettings["Mail"], ConfigurationManager.AppSettings["credintilas"]),
                    };
                    client.ServicePoint.MaxIdleTime = 0;
                    client.ServicePoint.ConnectionLimit = 3;

                    bool isSMSSendingEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["isSMSSendingEnabled"]);
                    if (isSMSSendingEnabled)
                    {
                        client.Send(mailMsg);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error occurred. " + ex);
                }
            }
        }



        /// <summary>
        /// Called when [send completed callback].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.AsyncCompletedEventArgs"/> instance containing the event data.</param>
        private static void OnSendCompletedCallback(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            MailMessage mail = e.UserState as MailMessage;
            if (!e.Cancelled && e.Error != null)
            {
                logger.Info(string.Format("Mail sent successfully"));
            }
        }
    }

    /// <summary>
    /// Synchronization Context Switcher Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public class SynchronizationContextSwitcher
        : IDisposable
    {
        /// <summary>
        /// The _execution context
        /// </summary>
        private ExecutionContext _executionContext;

        /// <summary>
        /// The _old context
        /// </summary>
        private readonly SynchronizationContext _oldContext;

        /// <summary>
        /// The _new context
        /// </summary>
        private readonly SynchronizationContext _newContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationContextSwitcher"/> class.
        /// </summary>
        public SynchronizationContextSwitcher()
            : this(new SynchronizationContext())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationContextSwitcher"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public SynchronizationContextSwitcher(SynchronizationContext context)
        {
            _newContext = context;
            _executionContext = Thread.CurrentThread.ExecutionContext;
            _oldContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(context);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">
        /// Dispose called on wrong thread.
        /// or
        /// The SynchronizationContext has changed.
        /// </exception>
        public void Dispose()
        {
            if (null != _executionContext)
            {
                if (_executionContext != Thread.CurrentThread.ExecutionContext)
                    throw new InvalidOperationException("Dispose called on wrong thread.");

                if (_newContext != SynchronizationContext.Current)
                    throw new InvalidOperationException("The SynchronizationContext has changed.");

                SynchronizationContext.SetSynchronizationContext(_oldContext);
                _executionContext = null;
            }
        }
    }
}
