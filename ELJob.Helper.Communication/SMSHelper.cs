﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace ELJob.Helper.Communication
{
    public static class SMSHelper
    {
        /// <summary>
        /// Gets or sets the mobile nos.
        /// </summary>
        /// <value>
        /// The mobile nos.
        /// </value>
        public static string MobileNos { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public static string Message { get; set; }

        /// <summary>
        /// Sends the SMS.
        /// </summary>
        public static void SendSMS(string url, string username, string password, string from,bool isSMSSendingEnabled)
        {
           // string url = ConfigurationManager.GetSection("SMSGateway").ToString();
           // string username = ConfigurationManager.AppSettings["SMSGateway:UserName"];
          //  string password = ConfigurationManager.AppSettings["SMSGateway:Password"];
           // string from = ConfigurationManager.AppSettings["SMSGateway:SMSfrom"];
            //example url
            //string url= http://182.18.160.225/index.php/api/bulk-sms?username=karthikmetamorph&password=123456&from=mtmrph&to=9030866877,8886789690&message=hi this is sample sms&sms_type=2
            string smsAPIUrl = string.Format("{0}?username={1}&password={2}&from={3}&to={4}&message={5}&sms_type=2",
                url, username, password, from, MobileNos, Message);
          //  bool isSMSSendingEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["isSMSSendingEnabled"]);
            if (isSMSSendingEnabled)
            {
                string result = Apicall(smsAPIUrl);
                //result:   { JobId: 19414495, Ack: 1 message(s) has been sent}
            }
        }

        /// <summary>
        /// Apicalls the specified URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        private static string Apicall(string url)
        {
            HttpWebRequest httpreq = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                HttpWebResponse httpres = (HttpWebResponse)httpreq.GetResponse();
                StreamReader sr = new StreamReader(httpres.GetResponseStream());
                string results = sr.ReadToEnd();
                sr.Close();
                return results;
            }
            catch
            {
                return "0";
            }
        }

        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <param name="sURL">The s URL.</param>
        /// <returns></returns>
        private static string GetResponse(string sURL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
            //request.MaximumAutomaticRedirections = 4;
            //request.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string sResponse = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
                return sResponse;
            }
            catch
            {
                return "";
            }
        }
    }
}
