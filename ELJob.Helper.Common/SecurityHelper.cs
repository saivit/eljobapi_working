﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ELJob.Helper.Common
{
    public static class SecurityHelper
    {
        /// <summary>
        /// Encrypts the specified raw.
        /// </summary>
        /// <param name="raw">The raw.</param>
        /// <returns></returns>
        public static string Encrypt(string raw)
        {
            using (var csp = new AesCryptoServiceProvider())
            {
                ICryptoTransform e = GetCryptoTransform(csp, true);
                byte[] inputBuffer = Encoding.UTF8.GetBytes(raw);
                byte[] output = e.TransformFinalBlock(inputBuffer, 0, inputBuffer.Length);
                string encrypted = Convert.ToBase64String(output);
                encrypted= encrypted.Replace("/", "buo0972").Replace(" ", "SpcnbKhx").Replace("+","qpoKHx").Replace("=","ekluionmk");
                return encrypted;
            }
        }

        /// <summary>
        /// Decrypts the specified encrypted.
        /// </summary>
        /// <param name="encrypted">The encrypted.</param>
        /// <returns></returns>
        public static string Decrypt(string encrypted)
        {
            using (var csp = new AesCryptoServiceProvider())
            {
                var d = GetCryptoTransform(csp, false);
                byte[] output = Convert.FromBase64String(encrypted.Replace("buo0972","/").Replace("SpcnbKhx", " ").Replace("qpoKHx", "+").Replace("ekluionmk", "="));
                byte[] decryptedOutput = d.TransformFinalBlock(output, 0, output.Length);

                string decypted = Encoding.UTF8.GetString(decryptedOutput);
                return decypted;
            }
        }


        /// <summary>
        /// Gets the crypto transform.
        /// </summary>
        /// <param name="csp">The CSP.</param>
        /// <param name="encrypting">if set to <c>true</c> [encrypting].</param>
        /// <returns></returns>
        private static ICryptoTransform GetCryptoTransform(AesCryptoServiceProvider csp, bool encrypting)
        {
            csp.Mode = CipherMode.CBC;
            csp.Padding = PaddingMode.PKCS7;
            var passWord = "Password1";
            var salt = "Sa1tSAlt";

            //a random Init. Vector. just for testing
            String iv = "e675f725e675f725";

            var spec = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(passWord), Encoding.UTF8.GetBytes(salt), 65536);
            byte[] key = spec.GetBytes(16);

            csp.IV = Encoding.UTF8.GetBytes(iv);
            csp.Key = key;
            if (encrypting)
            {
                return csp.CreateEncryptor();
            }
            return csp.CreateDecryptor();
        }

    }
}
