﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ELJob.Helper.Common
{
    public static class ExceptionHelper
    {
        public static string GetFormattedError(Exception ex)
        {
            if (ex != null)
            {
                StringBuilder formattedError = new StringBuilder();

                MethodBase site = ex.TargetSite;
                string methodName = site == null ? "" : site.Name;
                formattedError.AppendLine("");
                formattedError.AppendFormat("Method : {0}\r\n", methodName);
                formattedError.AppendLine("");
                formattedError.AppendFormat("Exception: {0}\r\n", ex.Message);
                formattedError.AppendLine("");
                if (ex.InnerException != null)
                {
                    formattedError.AppendFormat("InnerException: {0}\r\n", ex.InnerException.Message);
                    if (ex.InnerException.InnerException != null)
                    {
                        formattedError.AppendFormat("InnerException -> InnerException: {0}\r\n", ex.InnerException.InnerException.Message);
                    }
                    formattedError.AppendFormat("StackTrace: {0}\r\n", ex.InnerException.StackTrace);
                    formattedError.AppendLine("");
                    foreach (DictionaryEntry de in ex.InnerException.Data)
                        formattedError.AppendFormat("{0}: {1}\r\n", de.Key, de.Value);
                }

                formattedError.AppendFormat("StackTrace: {0}\r\n", ex.StackTrace);
                formattedError.AppendLine("");
                foreach (DictionaryEntry de in ex.Data)
                    formattedError.AppendFormat("{0}: {1}\r\n", de.Key, de.Value);
                return formattedError.ToString();
            }
            else { return ""; };
        }
    }
}
