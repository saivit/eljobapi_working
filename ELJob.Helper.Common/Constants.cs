﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Helper.Common
{
    public static class Constants
    {
        public enum CummunicationType
        {
            Mobile = 1,
            Email = 2
        }
        public enum Userroles
        {
            SuperAdmin = 1,
            Admin = 2,
            Job_Seeker_Admin = 3,
            Employer_Admin = 4,
            L1_User = 5,
            Job_Seeker = 6,
            Employer = 7,
            Finance = 8
        }


        public enum Lookupstatus
        {
            Social_Media = 1,
            Friends = 2,
            Phamplets = 3,
            Website = 4,
            Yes = 9,
            No = 10,
            New = 51,
            Rescheduled = 52, Rejected = 53,
            Inprocess = 54,
            Yet_to_Submit_document = 55,
            Completed = 56,
            
        }

        public enum Profilescores
        {
            Quickregistration=20,
            personaldetails=10,
            workexperince=20,
            euducational=30,
            skill_assessment=10,
            fill_documents=10
        }

        public enum Lookupstatustype
        {
            SourceOfLead = 1,
            Gender = 2,
            ConditionalYes = 3,
            Followup = 4,
            RejectionReason = 5,
            GenderInFilter = 6,
            MigrateReason = 7,
            Experience = 8,
            ModeOfEducation = 9,
            LanguageProficiency = 10,
            AptitudeandComputerSkills = 11,
            SkillScale = 12,
            OrganizationType = 13,
            GSTstatus = 14,
            PricingPlans = 15,
            Reschdulereason=16,
            Jobseeker_counceling_status=18,
            Salary_Ranage=19
        }

        public enum WorkingExperience
        {
            Fresher=36,
            Experience=9
        }
        /// <summary>
        /// This is Static class . store the exceptional general messages.
        /// </summary>
        public static class ExceptionMessages
        {
            /// <summary>
            /// after accessing this string need to format and appedn the actual error.
            /// </summary>
            public static string Generalmessage = "An Error Has Occured {0}";
        }

        public static class ImageFormat
        {
            public static string Bmp = "Bmp";
            public static string Gif = "Gif";
            public static string Jpeg = "image/jpeg";
            public static string Png = "image/png";
            public static string Tiff = "Tiff";
            public static string Wmp = "wmp";
            public static string Pdf = "application/pdf";
            public static string Zip = "application/x-zip-compressed";
            public static string rar = "rar";
            public static string excel = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }

        /// <summary>
        /// This is Static class . store the Success general messages.
        /// </summary>
        public static class SucessMessages
        {
            /// <summary>
            /// Returns Static text
            /// </summary>
            public static string OK = "Ok";

            /// <summary>
            /// Returns Static text
            /// </summary>
            public static string ScuessLoad = "Data Loaded Successfully";

            /// <summary>
            /// Returns Static text
            /// 
            /// </summary>
            public static string Inserted = "Data Saved Successfully";

            /// <summary>
            /// Returns Static text
            /// 
            /// </summary>
            public static string Updated = "Data Updated Successfully";

            /// <summary>
            /// Returns Static text
            /// </summary>
            public static string Filedeleted = "File Deleted Successfully";

            /// <summary>
            /// Returns Static text approved
            /// used to status message
            /// </summary>
            public static string Approved = "Approved Successfully";

            /// <summary>
            /// Returns Static text approved
            /// </summary>
            public static string Removed = "Removed Successfully";


            /// <summary>
            /// Returns Static text Hold
            /// </summary>
            public static string Hold = "Hold file Successfully";

            /// <summary>
            /// Returns Static text approved
            /// </summary>
            public static string Rejected = "Rejected  Successfully";

            /// <summary>
            /// Returns Static text approved
            /// </summary>
            public static string signdocument = "Application Form shared to {0}  mail , Please complete the e-sign process.";

            /// <summary>
            /// Returns Static text OTP Validated
            /// </summary>
            public static string OTPvalidate = "OTP Verifid & Signed Sucessfully.";

            /// <summary>
            /// Returns Static text OTP Validated
            /// </summary>
            public static string OTPinvalid = "OTP Is Invalid ,Please try again once";

            /// <summary>
            /// Returns Static text OTP Validated
            /// </summary>
            public static string PartBsend = "Concent Form will sent Sucessfully.";

            /// <summary>
            /// Returns Static text OTP Validated
            /// </summary>
            public static string DocumentVerified = "Document Verified Sucessfully.";

            /// <summary>
            /// Returns Static text OTP Validated
            /// </summary>
            public static string Empty = "Seems no actionable items found.";

            /// <summary>
            /// Returns Static text OTP Validated
            /// </summary>
            public static string Exception = "Internal Server Error.";


        }



        public enum YESorNO
        {

        }

        public enum documents
        {

        }





        public enum ApplicationType
        {
            DigitialLoan = 1, SupplierLoan = 2, SecureLoan = 3
        }

        public enum OTP_Status
        {

        }
        public enum Gender
        {
            Male = 7, Female = 8,Both=28    
        }


        public static class messageTitles
        {
            public static string success = "Success";
            public static string Error = "Error";
            public static string Warning = "Warning";
            public static string info = "Info";

        }

        public static class Priority
        {
            public static string Low = "Low";
            public static string Medium = "Medium";
            public static string High = "High";
            public static string Critical = "Critical";
        }


        public static class Docuemnts
        {
            public static string resume = "resume";
            public static string Euducational = "Euducational";
            public static string profilepicture = "profilepicture";
            public static string addressProof = "addressProof";
            public static string identityDocument = "identityDocument";
            public static string workexperinceCertificate = "workexperinceCertificate";
            public static string additinalEducation = "additinalEducation";
            public static string dirivingLicence = "dirivingLicence";

        }
        public static class Appdata
        {
            public static string PaymentInprocess = "Inprocess";
            public static string PaymentSuccess = "Payment Success";
            public static string PaymentFailure = "Payment Failure";
            public static string Paymentwelcomeplan = "Welcome Plan";

        }

        public enum ProfiledataStatus
        {
                Regisitered=65,
                CompletedPersonalProfile = 66,
	            CompletedEductionProfile= 67,
	            CompletedCeritificationProfile= 68,
	            CompletedExperinceProfile= 69,
	            CompletedLanguageProfile= 70,
                CompletedSkillAssessmentProfile=71,
	            CompletedObjectiveProfile= 72
        }

        public static class ProfiledataStatustext
        {
            public static string Regisitered = "Regisitered";
            public static string CompletedPersonalProfile = "Completed Personal Profile";
            public static string CompletedEductionProfile = "Completed Eduction Profile";
            public static string CompletedCeritificationProfile = "Completed Ceritification Profile";
            public static string CompletedExperinceProfile = "Completed Experince Profile";
            public static string CompletedLanguageProfile = "Completed Language Profile";
            public static string CompletedSkillAssessmentProfile = "Completed Skill Assessment Profile";
            public static string CompletedObjectiveProfile = "Completed Objective Profile";
        }

        public enum ProfileOrder
        {
            preimumUser=1,
            Yettosearch=15,
            employeer_selected=10,
            show_in_search = 5,
        }

        public enum Qulification
        {
            	PG_and_Other=5
        }

        public enum Employeerstatus
        {
            EmployeerRegistered =73,
            EmployeerInactive=74,
            EmployeerActive=75,
            EmployeerPlanpurchased=76,
            EmployeerPlanExpired=77
        }

        public static class Employeerstatustext
        {
            public static string EmployeerRegistered = "Employeer Registered";
            public static string EmployeerInactive = "Employeer Inactive";
            public static string EmployeerActive = "Employeer Active";
            public static string EmployeerPlanpurchased = "Employeer Plan purchased";
            public static string EmployeerPlanExpired = "Employeer Plan Expired";
           
        }

        public static class EmployeerActivities
        {
            public static string UpdateAddress = "Updated Employeer Address";
            public static string UpdateProfile = "Updated Employeer Profile";
            public static string SearchProfiles = "Search Job seeker Profiles";
            public static string EmployeerPlanpurchased = "Employeer Plan purchased";
            public static string EmployeerLogin = "Employeer Login";

        }

        public enum DefalutState
        {
            Defalutstate = 2
        }

        public enum LanguageGrip
        {
            Read=40,
            Write=41,
            Speak=39
        }
    }
}
