﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;

namespace ELJob.Helper.Common
{
    public static class DatatableConverter
    {
        // T is a generic class  
        public static DataTable ConvertToDataTable<T>(List<T> models)
        {
            // creating a data table instance and typed it as our incoming model   
            // as I make it generic, if you want, you can make it the model typed you want.  
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties of that model  
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            // Loop through all the properties              
            // Adding Column name to our datatable  
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names    
                dataTable.Columns.Add(prop.Name);
            }
            // Adding Row and its value to our dataTable  
            foreach (T item in models)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows    
                    values[i] = Props[i].GetValue(item, null);
                }
                // Finally add value to datatable    
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public static byte[] generateexcel(DataTable DT)
        {
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(DT);

            using (var workbook = new XLWorkbook())
            {
                // var worksheet = workbook.Worksheets.Add("Users");
                //var currentRow = 1;
                //  worksheet.Cell(currentRow, 1).Value = "Id";
                //worksheet.Cell(currentRow, 2).Value = "Username";
                foreach (DataTable table in dataSet.Tables)
                {
                    //Add a new worksheet to workbook with the Datatable name  
                    var excelWorkSheet = workbook.Worksheets.Add();
                    excelWorkSheet.Name = table.TableName;

                    // add all the columns  
                    for (int i = 1; i < table.Columns.Count + 1; i++)
                    {
                        excelWorkSheet.Cell(1, i).Value = table.Columns[i - 1].ColumnName;
                    }

                    // add all the rows  
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        for (int k = 0; k < table.Columns.Count; k++)
                        {
                            excelWorkSheet.Cell(j + 2, k + 1).Value = table.Rows[j].ItemArray[k].ToString();
                        }
                    }
                }


                var stream = new MemoryStream();

                workbook.SaveAs(stream);
                var content = stream.ToArray();
                return content;

            }
        }

    }
}
