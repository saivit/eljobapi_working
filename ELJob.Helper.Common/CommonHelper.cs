﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ELJob.Helper.Common
{
  public  class CommonHelper
    {

        /// <summary>
        /// The indian zone
        /// </summary>
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");


        /// <summary>
        /// Converts the string to date.
        /// </summary>
        /// <param name="strDate">The string date.</param>
        /// <returns></returns>
        public static DateTime? ConvertStringToDate(string strDate)
        {
            DateTime? date = null;

            if (strDate != "")
            {
                date = DateTime.Parse(strDate);
            }

            return date;
        }

        /// <summary>
        /// Converts the UTC to ist.
        /// </summary>
        /// <returns></returns>
        public static DateTime? ConvertUTCToIST()
        {
            DateTime? convertedDate = null;
            convertedDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            return convertedDate;
        }


        public static StringBuilder Randompassword()
        {
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                // Buffer storage.
                byte[] data = new byte[4];
                StringBuilder Sb = new StringBuilder();
                // Ten iterations.              
                // Fill buffer.
                rng.GetBytes(data);
                // Convert to int 32.
                Sb.Append("E");
                int value = BitConverter.ToInt32(data, 0);
                Sb.Append("l");
                Sb.Append(value);
                Sb.Append("@");
                return Sb;
            }
            // return sb;
        }


    


    }
}
