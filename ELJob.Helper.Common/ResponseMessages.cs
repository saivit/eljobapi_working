﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Helper.Common
{
   public static class ResponseMessages
    {
        public static string OTPset_to_mobile = "Dear Please Verify  OTP - {0}  \n Team Vreedhi";
        public static string OTPResendMessage = "OTP Resend Sucessfully";
        public static string OTPsendMessage = "OTP send Sucessfully";
        public static string OTPvalidationMessage = "Error: OTP Missmatch, a OTP sent to regsitered mobile  \n ";
        public static string Update = "Updated Successfully";
        public static string Insert = "Created Successfully";
        public static string Delete = "Removed Successfully";
        public static string GPSLocationAdded = "GPS Location Added Successfully";
        public static string VoterIdAdd = "Voter Id  Added Successfully";
        public static string VoterReason = "Voter Id Reason  Added Successfully";
        public static string Aadhar = "Aadhar  Added Successfully";
        // Error Message.
        public static string Error = "An Error Has occured {0}";


        //Email
        public static string Email_sent = "Email Successfully Sent to Registered Email";


    }
}
