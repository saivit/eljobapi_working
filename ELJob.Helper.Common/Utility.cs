﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ELJob.Helper.Common
{
   public static class Utility
    {
        public static string HmacSha256Digest(this string message, string secret)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] keyBytes = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            System.Security.Cryptography.HMACSHA256 cryptographer = new System.Security.Cryptography.HMACSHA256(keyBytes);

            byte[] bytes = cryptographer.ComputeHash(messageBytes);

            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }
        public static string OTPGenerator()
        {
            Random random = new Random();
            return Convert.ToString(random.Next(1000, 9999));
        }
        public static StringBuilder Randompassword()
        {
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                // Buffer storage.
                byte[] data = new byte[4];
                StringBuilder Sb = new StringBuilder();
                // Ten iterations.              
                // Fill buffer.
                rng.GetBytes(data);
                // Convert to int 32.
                Sb.Append("V");
                int value = BitConverter.ToInt32(data, 0);
                Sb.Append("d");

                Sb.Append(value);
                Sb.Append("@");
                //Console.WriteLine(Sb);

                return Sb;
            }
            // return sb;
        }

        public static int DaytillDiffrence(this object obj)
        {
            int day = 0;

            day = (Convert.ToDateTime(obj) - DateTime.Now).Days;

            return day;
        }
        public static string shortname(this object obj)
        {
            string name = Convert.ToString(obj);
            string shortname = name.Substring(0, 1) ;
            string secondshortname = name.Contains(" ") ? name.Split(" ")[1].Substring(0,1) : name.Substring(1, 1);
            shortname = shortname + secondshortname;
            return shortname;
        }

        public static bool HasProperty(this object obj, string propertyName)
        {
            return obj != null;
        }
    }
}
