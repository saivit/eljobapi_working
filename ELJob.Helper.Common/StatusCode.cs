﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELJob.Helper.Common
{
   public static class StatusCode
    {
        public static int OK = 200;
        public static int Created = 201;
        public static int NoContent = 204;
        public static int notexecuted = 202;
        public static int BadRequest = 400;
        public static int Unauthorized = 401;
        public static int Forbidden = 403;
        public static int NotFound = 404;
        public static int MethodNotAllowed =405;
        public static int RequestTimeout = 408;
        public static int InternalServerError = 500;

    }
}
